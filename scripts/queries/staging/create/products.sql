create table if not exists
    Products (
        ProductID int4 primary key,
        ProductName text,
        SupplierID int4,
        CategoryID int4,
        QuantityPerUnit text,
        UnitPrice numeric(10,2),
        UnitsInStock int4,
        UnitsOnOrder int4,
        ReorderLevel int4, 
        Discontinued boolean
    )
;