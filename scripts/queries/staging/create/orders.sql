create table if not exists
    Orders (
        OrderID int4 primary key,
        CustomerID varchar(5),
        EmployeeID int4,
        OrderDate timestamp,
        RequiredDate timestamp,
        ShippedDate timestamp,
        ShipVia int4,
        Freight numeric(10,2),
        ShipName text,
        ShipAddress text,
        ShipCity varchar(50),
        ShipRegion varchar(50),
        ShipPostalCode varchar(50),
        ShipCountry varchar(50)
    )
;