--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: foreign_staging; Type: SCHEMA; Schema: -; Owner: admin
--

CREATE SCHEMA foreign_staging;


ALTER SCHEMA foreign_staging OWNER TO admin;

--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- Name: nwind_staging; Type: SERVER; Schema: -; Owner: admin
--

CREATE SERVER nwind_staging FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'northwind_stag',
    host 'localhost'
);


ALTER SERVER nwind_staging OWNER TO admin;

--
-- Name: USER MAPPING admin SERVER nwind_staging; Type: USER MAPPING; Schema: -; Owner: admin
--

CREATE USER MAPPING FOR admin SERVER nwind_staging OPTIONS (
    password 'admin',
    "user" 'admin'
);


SET default_tablespace = '';

--
-- Name: categories; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.categories (
    categoryid integer NOT NULL,
    categoryname character varying(50),
    "Description" text,
    picture bytea
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'categories'
);
ALTER FOREIGN TABLE foreign_staging.categories ALTER COLUMN categoryid OPTIONS (
    column_name 'categoryid'
);
ALTER FOREIGN TABLE foreign_staging.categories ALTER COLUMN categoryname OPTIONS (
    column_name 'categoryname'
);
ALTER FOREIGN TABLE foreign_staging.categories ALTER COLUMN "Description" OPTIONS (
    column_name 'Description'
);
ALTER FOREIGN TABLE foreign_staging.categories ALTER COLUMN picture OPTIONS (
    column_name 'picture'
);


ALTER FOREIGN TABLE foreign_staging.categories OWNER TO admin;

--
-- Name: customers; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.customers (
    customerid character varying(5) NOT NULL,
    companyname character varying(100),
    contactname character varying(100),
    contacttitle character varying(100),
    "Address" text,
    city character varying(50),
    region character varying(50),
    postalcode character varying(50),
    country character varying(50),
    phone character varying(50),
    fax character varying(50)
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'customers'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN customerid OPTIONS (
    column_name 'customerid'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN companyname OPTIONS (
    column_name 'companyname'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN contactname OPTIONS (
    column_name 'contactname'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN contacttitle OPTIONS (
    column_name 'contacttitle'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN "Address" OPTIONS (
    column_name 'Address'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN city OPTIONS (
    column_name 'city'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN region OPTIONS (
    column_name 'region'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN postalcode OPTIONS (
    column_name 'postalcode'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN country OPTIONS (
    column_name 'country'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN phone OPTIONS (
    column_name 'phone'
);
ALTER FOREIGN TABLE foreign_staging.customers ALTER COLUMN fax OPTIONS (
    column_name 'fax'
);


ALTER FOREIGN TABLE foreign_staging.customers OWNER TO admin;

--
-- Name: employees; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.employees (
    employeeid integer NOT NULL,
    lastname character varying(100),
    firstname character varying(100),
    title character varying(100),
    titleofcourtesy character varying(5),
    birthdate timestamp without time zone,
    hiredate timestamp without time zone,
    "Address" text,
    city character varying(50),
    region character varying(50),
    postalcode character varying(50),
    country character varying(50),
    homephone character varying(50),
    extension integer,
    photo bytea,
    notes text,
    reportsto integer
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'employees'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN employeeid OPTIONS (
    column_name 'employeeid'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN lastname OPTIONS (
    column_name 'lastname'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN firstname OPTIONS (
    column_name 'firstname'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN title OPTIONS (
    column_name 'title'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN titleofcourtesy OPTIONS (
    column_name 'titleofcourtesy'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN birthdate OPTIONS (
    column_name 'birthdate'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN hiredate OPTIONS (
    column_name 'hiredate'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN "Address" OPTIONS (
    column_name 'Address'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN city OPTIONS (
    column_name 'city'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN region OPTIONS (
    column_name 'region'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN postalcode OPTIONS (
    column_name 'postalcode'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN country OPTIONS (
    column_name 'country'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN homephone OPTIONS (
    column_name 'homephone'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN extension OPTIONS (
    column_name 'extension'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN photo OPTIONS (
    column_name 'photo'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN notes OPTIONS (
    column_name 'notes'
);
ALTER FOREIGN TABLE foreign_staging.employees ALTER COLUMN reportsto OPTIONS (
    column_name 'reportsto'
);


ALTER FOREIGN TABLE foreign_staging.employees OWNER TO admin;

--
-- Name: orderdetails; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.orderdetails (
    orderid integer NOT NULL,
    productid integer NOT NULL,
    unitprice numeric(10,2),
    quantity integer,
    discount numeric(3,2)
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'orderdetails'
);
ALTER FOREIGN TABLE foreign_staging.orderdetails ALTER COLUMN orderid OPTIONS (
    column_name 'orderid'
);
ALTER FOREIGN TABLE foreign_staging.orderdetails ALTER COLUMN productid OPTIONS (
    column_name 'productid'
);
ALTER FOREIGN TABLE foreign_staging.orderdetails ALTER COLUMN unitprice OPTIONS (
    column_name 'unitprice'
);
ALTER FOREIGN TABLE foreign_staging.orderdetails ALTER COLUMN quantity OPTIONS (
    column_name 'quantity'
);
ALTER FOREIGN TABLE foreign_staging.orderdetails ALTER COLUMN discount OPTIONS (
    column_name 'discount'
);


ALTER FOREIGN TABLE foreign_staging.orderdetails OWNER TO admin;

--
-- Name: orders; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.orders (
    orderid integer NOT NULL,
    customerid character varying(5),
    employeeid integer,
    orderdate timestamp without time zone,
    requireddate timestamp without time zone,
    shippeddate timestamp without time zone,
    shipvia integer,
    freight numeric(10,2),
    shipname text,
    shipaddress text,
    shipcity character varying(50),
    shipregion character varying(50),
    shippostalcode character varying(50),
    shipcountry character varying(50)
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'orders'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN orderid OPTIONS (
    column_name 'orderid'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN customerid OPTIONS (
    column_name 'customerid'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN employeeid OPTIONS (
    column_name 'employeeid'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN orderdate OPTIONS (
    column_name 'orderdate'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN requireddate OPTIONS (
    column_name 'requireddate'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shippeddate OPTIONS (
    column_name 'shippeddate'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shipvia OPTIONS (
    column_name 'shipvia'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN freight OPTIONS (
    column_name 'freight'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shipname OPTIONS (
    column_name 'shipname'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shipaddress OPTIONS (
    column_name 'shipaddress'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shipcity OPTIONS (
    column_name 'shipcity'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shipregion OPTIONS (
    column_name 'shipregion'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shippostalcode OPTIONS (
    column_name 'shippostalcode'
);
ALTER FOREIGN TABLE foreign_staging.orders ALTER COLUMN shipcountry OPTIONS (
    column_name 'shipcountry'
);


ALTER FOREIGN TABLE foreign_staging.orders OWNER TO admin;

--
-- Name: products; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.products (
    productid integer NOT NULL,
    productname text,
    supplierid integer,
    categoryid integer,
    quantityperunit text,
    unitprice numeric(10,2),
    unitsinstock integer,
    unitsonorder integer,
    reorderlevel integer,
    discontinued boolean
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'products'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN productid OPTIONS (
    column_name 'productid'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN productname OPTIONS (
    column_name 'productname'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN supplierid OPTIONS (
    column_name 'supplierid'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN categoryid OPTIONS (
    column_name 'categoryid'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN quantityperunit OPTIONS (
    column_name 'quantityperunit'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN unitprice OPTIONS (
    column_name 'unitprice'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN unitsinstock OPTIONS (
    column_name 'unitsinstock'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN unitsonorder OPTIONS (
    column_name 'unitsonorder'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN reorderlevel OPTIONS (
    column_name 'reorderlevel'
);
ALTER FOREIGN TABLE foreign_staging.products ALTER COLUMN discontinued OPTIONS (
    column_name 'discontinued'
);


ALTER FOREIGN TABLE foreign_staging.products OWNER TO admin;

--
-- Name: shippers; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.shippers (
    shipperid integer NOT NULL,
    companyname text,
    phone character varying(50)
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'shippers'
);
ALTER FOREIGN TABLE foreign_staging.shippers ALTER COLUMN shipperid OPTIONS (
    column_name 'shipperid'
);
ALTER FOREIGN TABLE foreign_staging.shippers ALTER COLUMN companyname OPTIONS (
    column_name 'companyname'
);
ALTER FOREIGN TABLE foreign_staging.shippers ALTER COLUMN phone OPTIONS (
    column_name 'phone'
);


ALTER FOREIGN TABLE foreign_staging.shippers OWNER TO admin;

--
-- Name: suppliers; Type: FOREIGN TABLE; Schema: foreign_staging; Owner: admin
--

CREATE FOREIGN TABLE foreign_staging.suppliers (
    supplierid integer NOT NULL,
    companyname text,
    contactname text,
    contacttitle text,
    "Address" text,
    city character varying(50),
    region character varying(50),
    postalcode character varying(50),
    country character varying(50),
    phone character varying(50),
    fax character varying(50),
    homepage text
)
SERVER nwind_staging
OPTIONS (
    schema_name 'public',
    table_name 'suppliers'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN supplierid OPTIONS (
    column_name 'supplierid'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN companyname OPTIONS (
    column_name 'companyname'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN contactname OPTIONS (
    column_name 'contactname'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN contacttitle OPTIONS (
    column_name 'contacttitle'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN "Address" OPTIONS (
    column_name 'Address'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN city OPTIONS (
    column_name 'city'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN region OPTIONS (
    column_name 'region'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN postalcode OPTIONS (
    column_name 'postalcode'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN country OPTIONS (
    column_name 'country'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN phone OPTIONS (
    column_name 'phone'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN fax OPTIONS (
    column_name 'fax'
);
ALTER FOREIGN TABLE foreign_staging.suppliers ALTER COLUMN homepage OPTIONS (
    column_name 'homepage'
);


ALTER FOREIGN TABLE foreign_staging.suppliers OWNER TO admin;

SET default_table_access_method = heap;

--
-- Name: d_customer; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_customer (
    customer_key integer NOT NULL,
    customer_id character varying(5),
    customer_name character varying(100),
    customer_contact_name character varying(100),
    customer_contact_title character varying(100),
    customer_phone character varying(50),
    customer_fax character varying(50),
    customer_street_address text,
    customer_postal_code_id character varying(50),
    customer_city_name character varying(50),
    customer_region_name character varying(50),
    customer_country_code character varying(5),
    customer_country_name character varying(50),
    audit_key integer
);


ALTER TABLE public.d_customer OWNER TO admin;

--
-- Name: d_customer_customer_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_customer_customer_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_customer_customer_key_seq OWNER TO admin;

--
-- Name: d_customer_customer_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_customer_customer_key_seq OWNED BY public.d_customer.customer_key;


--
-- Name: d_employee; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_employee (
    employee_key integer NOT NULL,
    employee_id integer,
    employee_last_name character varying(100),
    employee_first_name character varying(100),
    employee_title character varying(100),
    employee_title_of_courtesy character varying(5),
    employee_birth_date timestamp without time zone,
    employee_hire_date timestamp without time zone,
    employee_home_phone character varying(50),
    employee_extension integer,
    employee_street_address text,
    employee_postal_code_id character varying(50),
    employee_city_name character varying(50),
    employee_region_name character varying(50),
    employee_country_name character varying(50)
);


ALTER TABLE public.d_employee OWNER TO admin;

--
-- Name: d_employee_employee_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_employee_employee_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_employee_employee_key_seq OWNER TO admin;

--
-- Name: d_employee_employee_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_employee_employee_key_seq OWNED BY public.d_employee.employee_key;


--
-- Name: d_order_date_dim; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_order_date_dim (
    order_date_key integer NOT NULL,
    order_date_id_yyyymmdd character varying(8),
    "Order_Day_Date_yyyy-mm-dd" character varying(10),
    order_full_date timestamp without time zone,
    order_day_of_week_name character varying(10),
    order_day_of_week_abbrev character varying(3)
);


ALTER TABLE public.d_order_date_dim OWNER TO admin;

--
-- Name: d_order_date_dim_order_date_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_order_date_dim_order_date_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_order_date_dim_order_date_key_seq OWNER TO admin;

--
-- Name: d_order_date_dim_order_date_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_order_date_dim_order_date_key_seq OWNED BY public.d_order_date_dim.order_date_key;


--
-- Name: d_order_transaction; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_order_transaction (
    order_transaction_key integer NOT NULL,
    order_id integer,
    audit_key integer
);


ALTER TABLE public.d_order_transaction OWNER TO admin;

--
-- Name: d_order_transaction_order_transaction_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_order_transaction_order_transaction_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_order_transaction_order_transaction_key_seq OWNER TO admin;

--
-- Name: d_order_transaction_order_transaction_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_order_transaction_order_transaction_key_seq OWNED BY public.d_order_transaction.order_transaction_key;


--
-- Name: d_product; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_product (
    product_key integer NOT NULL,
    product_id integer,
    product_name text,
    product_category_id integer,
    product_category_name text,
    product_quantity_per_unit text,
    product_standard_unit_price numeric(10,2),
    product_reorder_level integer,
    product_discontinued_ind boolean,
    supplier_id integer,
    supplier_company_name text,
    supplier_contact_title text,
    supplier_street_address text,
    supplier_phone character varying(25),
    supplier_fax character varying(25),
    supplier_home_page text,
    supplier_postal_code_id character varying(50),
    supplier_region_name character varying(50),
    supplier_city_name character varying(50),
    supplier_country_code character varying(50),
    supplier_country_name text,
    current_row_ind integer,
    effective_date timestamp without time zone,
    audit_key integer
);


ALTER TABLE public.d_product OWNER TO admin;

--
-- Name: d_product_product_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_product_product_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_product_product_key_seq OWNER TO admin;

--
-- Name: d_product_product_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_product_product_key_seq OWNED BY public.d_product.product_key;


--
-- Name: d_required_date; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_required_date (
    required_date_key integer NOT NULL,
    required_date_id_yyyymmdd character varying(8),
    "Required_Day_Date_yyyy-mm-dd" character varying(10),
    required_full_date timestamp without time zone,
    required_day_of_week_name character varying(10),
    requried_day_of_week_abbrev character varying(3)
);


ALTER TABLE public.d_required_date OWNER TO admin;

--
-- Name: d_required_date_required_date_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_required_date_required_date_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_required_date_required_date_key_seq OWNER TO admin;

--
-- Name: d_required_date_required_date_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_required_date_required_date_key_seq OWNED BY public.d_required_date.required_date_key;


--
-- Name: d_shipped_date; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_shipped_date (
    shipped_date_key integer NOT NULL,
    shipped_date_id_yyyymmdd character varying(8),
    "Shipped_Day_Date_yyyy-mm-dd" character varying(10),
    shipped_full_date timestamp without time zone,
    shipped_day_of_week_name character varying(10),
    shipped_day_of_week_abbrev character varying(3)
);


ALTER TABLE public.d_shipped_date OWNER TO admin;

--
-- Name: d_shipped_date_shipped_date_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_shipped_date_shipped_date_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_shipped_date_shipped_date_key_seq OWNER TO admin;

--
-- Name: d_shipped_date_shipped_date_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_shipped_date_shipped_date_key_seq OWNED BY public.d_shipped_date.shipped_date_key;


--
-- Name: d_shipper; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.d_shipper (
    shipper_key integer NOT NULL,
    shipper_id integer,
    shipper_company_name text,
    current_shipper_phone character varying(50),
    previous_shipper_phone character varying(50),
    effective_date timestamp without time zone,
    current_row_ind integer,
    audit_key integer
);


ALTER TABLE public.d_shipper OWNER TO admin;

--
-- Name: d_shipper_shipper_key_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.d_shipper_shipper_key_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.d_shipper_shipper_key_seq OWNER TO admin;

--
-- Name: d_shipper_shipper_key_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.d_shipper_shipper_key_seq OWNED BY public.d_shipper.shipper_key;


--
-- Name: f_order_line_item; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.f_order_line_item (
    order_transaction_key integer,
    order_date_key integer,
    required_date_key integer,
    customer_key integer,
    product_key integer,
    employee_key integer,
    item_unit_price numeric(10,2),
    item_quantity integer,
    item_extended_amt integer,
    item_discount_amt numeric(3,2),
    item_total_amt numeric(10,2),
    audit_key integer
);


ALTER TABLE public.f_order_line_item OWNER TO admin;

--
-- Name: f_order_transaction; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.f_order_transaction (
    order_transaction_key integer,
    order_date_key integer,
    required_date_key integer,
    customer_key integer,
    employee_key integer,
    order_nbr_of_products integer,
    order_extended_amt integer,
    order_discount_amt numeric(3,2),
    order_total_amt numeric(10,2),
    audit_key integer
);


ALTER TABLE public.f_order_transaction OWNER TO admin;

--
-- Name: f_shipment_transaction; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.f_shipment_transaction (
    order_transaction_key integer,
    order_date_key integer,
    required_date_key integer,
    shipped_date_key integer,
    customer_key integer,
    employee_key integer,
    shipper_key integer,
    shipper_nbr_of_products integer,
    shipment_extended_amt numeric(10,2),
    shipment_discount_amt numeric(3,2),
    shipment_freight_amt numeric(10,2),
    shipment_total_amt numeric(10,2),
    audit_key integer
);


ALTER TABLE public.f_shipment_transaction OWNER TO admin;

--
-- Name: r_categories; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.r_categories (
    categoryid integer NOT NULL,
    categoryname character varying(50),
    "Description" text,
    picture bytea
);


ALTER TABLE public.r_categories OWNER TO admin;

--
-- Name: r_suppliers; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.r_suppliers (
    supplierid integer NOT NULL,
    companyname text,
    contactname text,
    contacttitle text,
    "Address" text,
    city character varying(50),
    region character varying(50),
    postalcode character varying(50),
    country character varying(50),
    phone character varying(25),
    fax character varying(25),
    homepage text
);


ALTER TABLE public.r_suppliers OWNER TO admin;

--
-- Name: d_customer customer_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_customer ALTER COLUMN customer_key SET DEFAULT nextval('public.d_customer_customer_key_seq'::regclass);


--
-- Name: d_employee employee_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_employee ALTER COLUMN employee_key SET DEFAULT nextval('public.d_employee_employee_key_seq'::regclass);


--
-- Name: d_order_date_dim order_date_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_order_date_dim ALTER COLUMN order_date_key SET DEFAULT nextval('public.d_order_date_dim_order_date_key_seq'::regclass);


--
-- Name: d_order_transaction order_transaction_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_order_transaction ALTER COLUMN order_transaction_key SET DEFAULT nextval('public.d_order_transaction_order_transaction_key_seq'::regclass);


--
-- Name: d_product product_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_product ALTER COLUMN product_key SET DEFAULT nextval('public.d_product_product_key_seq'::regclass);


--
-- Name: d_required_date required_date_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_required_date ALTER COLUMN required_date_key SET DEFAULT nextval('public.d_required_date_required_date_key_seq'::regclass);


--
-- Name: d_shipped_date shipped_date_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_shipped_date ALTER COLUMN shipped_date_key SET DEFAULT nextval('public.d_shipped_date_shipped_date_key_seq'::regclass);


--
-- Name: d_shipper shipper_key; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_shipper ALTER COLUMN shipper_key SET DEFAULT nextval('public.d_shipper_shipper_key_seq'::regclass);


--
-- Data for Name: d_customer; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_customer (customer_key, customer_id, customer_name, customer_contact_name, customer_contact_title, customer_phone, customer_fax, customer_street_address, customer_postal_code_id, customer_city_name, customer_region_name, customer_country_code, customer_country_name, audit_key) FROM stdin;
365	ALFKI	Alfreds Futterkiste	Maria Anders	Sales Representative	030-0074321	030-0076545	Obere Str. 57	12209	Berlin	\N	\N	Germany	\N
366	ANATR	Ana Trujillo Emparedados y helados	Ana Trujillo	Owner	(5) 555-4729	(5) 555-3745	Avda. de la Constitución 2222	05021	México D.F.	\N	\N	Mexico	\N
367	ANTON	Antonio Moreno Taquería	Antonio Moreno	Owner	(5) 555-3932	\N	Mataderos  2312	05023	México D.F.	\N	\N	Mexico	\N
368	AROUT	Around the Horn	Thomas Hardy	Sales Representative	(171) 555-7788	(171) 555-6750	120 Hanover Sq.	WA1 1DP	London	\N	\N	UK	\N
369	BERGS	Berglunds snabbköp	Christina Berglund	Order Administrator	0921-12 34 65	0921-12 34 67	Berguvsvägen  8	S-958 22	Luleå	\N	\N	Sweden	\N
370	BLAUS	Blauer See Delikatessen	Hanna Moos	Sales Representative	0621-08460	0621-08924	Forsterstr. 57	68306	Mannheim	\N	\N	Germany	\N
371	BLONP	Blondel père et fils	Frédérique Citeaux	Marketing Manager	88.60.15.31	88.60.15.32	24, place Kléber	67000	Strasbourg	\N	\N	France	\N
372	BOLID	Bólido Comidas preparadas	Martín Sommer	Owner	(91) 555 22 82	(91) 555 91 99	C/ Araquil, 67	28023	Madrid	\N	\N	Spain	\N
373	BONAP	Bon app'	Laurence Lebihan	Owner	91.24.45.40	91.24.45.41	12, rue des Bouchers	13008	Marseille	\N	\N	France	\N
374	BOTTM	Bottom-Dollar Markets	Elizabeth Lincoln	Accounting Manager	(604) 555-4729	(604) 555-3745	23 Tsawassen Blvd.	T2F 8M4	Tsawassen	BC	\N	Canada	\N
375	BSBEV	B's Beverages	Victoria Ashworth	Sales Representative	(171) 555-1212	\N	Fauntleroy Circus	EC2 5NT	London	\N	\N	UK	\N
376	CACTU	Cactus Comidas para llevar	Patricio Simpson	Sales Agent	(1) 135-5555	(1) 135-4892	Cerrito 333	1010	Buenos Aires	\N	\N	Argentina	\N
377	CENTC	Centro comercial Moctezuma	Francisco Chang	Marketing Manager	(5) 555-3392	(5) 555-7293	Sierras de Granada 9993	05022	México D.F.	\N	\N	Mexico	\N
378	CHOPS	Chop-suey Chinese	Yang Wang	Owner	0452-076545	\N	Hauptstr. 29	3012	Bern	\N	\N	Switzerland	\N
379	COMMI	Comércio Mineiro	Pedro Afonso	Sales Associate	(11) 555-7647	\N	Av. dos Lusíadas, 23	05432-043	São Paulo	SP	\N	Brazil	\N
380	CONSH	Consolidated Holdings	Elizabeth Brown	Sales Representative	(171) 555-2282	(171) 555-9199	Berkeley Gardens\n12  Brewery 	WX1 6LT	London	\N	\N	UK	\N
381	DRACD	Drachenblut Delikatessen	Sven Ottlieb	Order Administrator	0241-039123	0241-059428	Walserweg 21	52066	Aachen	\N	\N	Germany	\N
382	DUMON	Du monde entier	Janine Labrune	Owner	40.67.88.88	40.67.89.89	67, rue des Cinquante Otages	44000	Nantes	\N	\N	France	\N
383	EASTC	Eastern Connection	Ann Devon	Sales Agent	(171) 555-0297	(171) 555-3373	35 King George	WX3 6FW	London	\N	\N	UK	\N
384	ERNSH	Ernst Handel	Roland Mendel	Sales Manager	7675-3425	7675-3426	Kirchgasse 6	8010	Graz	\N	\N	Austria	\N
385	FAMIA	Familia Arquibaldo	Aria Cruz	Marketing Assistant	(11) 555-9857	\N	Rua Orós, 92	05442-030	São Paulo	SP	\N	Brazil	\N
386	FISSA	FISSA Fabrica Inter. Salchichas S.A.	Diego Roel	Accounting Manager	(91) 555 94 44	(91) 555 55 93	C/ Moralzarzal, 86	28034	Madrid	\N	\N	Spain	\N
387	FOLIG	Folies gourmandes	Martine Rancé	Assistant Sales Agent	20.16.10.16	20.16.10.17	184, chaussée de Tournai	59000	Lille	\N	\N	France	\N
388	FOLKO	Folk och fä HB	Maria Larsson	Owner	0695-34 67 21	\N	Åkergatan 24	S-844 67	Bräcke	\N	\N	Sweden	\N
389	FRANK	Frankenversand	Peter Franken	Marketing Manager	089-0877310	089-0877451	Berliner Platz 43	80805	München	\N	\N	Germany	\N
390	FRANR	France restauration	Carine Schmitt	Marketing Manager	40.32.21.21	40.32.21.20	54, rue Royale	44000	Nantes	\N	\N	France	\N
391	FRANS	Franchi S.p.A.	Paolo Accorti	Sales Representative	011-4988260	011-4988261	Via Monte Bianco 34	10100	Torino	\N	\N	Italy	\N
392	FURIB	Furia Bacalhau e Frutos do Mar	Lino Rodriguez 	Sales Manager	(1) 354-2534	(1) 354-2535	Jardim das rosas n. 32	1675	Lisboa	\N	\N	Portugal	\N
393	GALED	Galería del gastrónomo	Eduardo Saavedra	Marketing Manager	(93) 203 4560	(93) 203 4561	Rambla de Cataluña, 23	08022	Barcelona	\N	\N	Spain	\N
394	GODOS	Godos Cocina Típica	José Pedro Freyre	Sales Manager	(95) 555 82 82	\N	C/ Romero, 33	41101	Sevilla	\N	\N	Spain	\N
395	GOURL	Gourmet Lanchonetes	André Fonseca	Sales Associate	(11) 555-9482	\N	Av. Brasil, 442	04876-786	Campinas	SP	\N	Brazil	\N
396	GREAL	Great Lakes Food Market	Howard Snyder	Marketing Manager	(503) 555-7555	\N	2732 Baker Blvd.	97403	Eugene	OR	\N	USA	\N
397	GROSR	GROSELLA-Restaurante	Manuel Pereira	Owner	(2) 283-2951	(2) 283-3397	5ª Ave. Los Palos Grandes	1081	Caracas	DF	\N	Venezuela	\N
398	HANAR	Hanari Carnes	Mario Pontes	Accounting Manager	(21) 555-0091	(21) 555-8765	Rua do Paço, 67	05454-876	Rio de Janeiro	RJ	\N	Brazil	\N
399	HILAA	HILARIÓN-Abastos	Carlos Hernández	Sales Representative	(5) 555-1340	(5) 555-1948	Carrera 22 con Ave. Carlos Soublette #8-35	5022	San Cristóbal	Táchira	\N	Venezuela	\N
400	HUNGC	Hungry Coyote Import Store	Yoshi Latimer	Sales Representative	(503) 555-6874	(503) 555-2376	City Center Plaza\n516 Main St.	97827	Elgin	OR	\N	USA	\N
401	HUNGO	Hungry Owl All-Night Grocers	Patricia McKenna	Sales Associate	2967 542	2967 3333	8 Johnstown Road	\N	Cork	Co. Cork	\N	Ireland	\N
402	ISLAT	Island Trading	Helen Bennett	Marketing Manager	(198) 555-8888	\N	Garden House\nCrowther Way	PO31 7PJ	Cowes	Isle of Wight	\N	UK	\N
403	KOENE	Königlich Essen	Philip Cramer	Sales Associate	0555-09876	\N	Maubelstr. 90	14776	Brandenburg	\N	\N	Germany	\N
404	LACOR	La corne d'abondance	Daniel Tonini	Sales Representative	30.59.84.10	30.59.85.11	67, avenue de l'Europe	78000	Versailles	\N	\N	France	\N
405	LAMAI	La maison d'Asie	Annette Roulet	Sales Manager	61.77.61.10	61.77.61.11	1 rue Alsace-Lorraine	31000	Toulouse	\N	\N	France	\N
406	LAUGB	Laughing Bacchus Wine Cellars	Yoshi Tannamuri	Marketing Assistant	(604) 555-3392	(604) 555-7293	1900 Oak St.	V3F 2K1	Vancouver	BC	\N	Canada	\N
407	LAZYK	Lazy K Kountry Store	John Steel	Marketing Manager	(509) 555-7969	(509) 555-6221	12 Orchestra Terrace	99362	Walla Walla	WA	\N	USA	\N
408	LEHMS	Lehmanns Marktstand	Renate Messner	Sales Representative	069-0245984	069-0245874	Magazinweg 7	60528	Frankfurt a.M. 	\N	\N	Germany	\N
409	LETSS	Let's Stop N Shop	Jaime Yorres	Owner	(415) 555-5938	\N	87 Polk St.\nSuite 5	94117	San Francisco	CA	\N	USA	\N
410	LILAS	LILA-Supermercado	Carlos González	Accounting Manager	(9) 331-6954	(9) 331-7256	Carrera 52 con Ave. Bolívar #65-98 Llano Largo	3508	Barquisimeto	Lara	\N	Venezuela	\N
411	LINOD	LINO-Delicateses	Felipe Izquierdo	Owner	(8) 34-56-12	(8) 34-93-93	Ave. 5 de Mayo Porlamar	4980	I. de Margarita	Nueva Esparta	\N	Venezuela	\N
412	LONEP	Lonesome Pine Restaurant	Fran Wilson	Sales Manager	(503) 555-9573	(503) 555-9646	89 Chiaroscuro Rd.	97219	Portland	OR	\N	USA	\N
413	MAGAA	Magazzini Alimentari Riuniti	Giovanni Rovelli	Marketing Manager	035-640230	035-640231	Via Ludovico il Moro 22	24100	Bergamo	\N	\N	Italy	\N
414	MAISD	Maison Dewey	Catherine Dewey	Sales Agent	(02) 201 24 67	(02) 201 24 68	Rue Joseph-Bens 532	B-1180	Bruxelles	\N	\N	Belgium	\N
415	MEREP	Mère Paillarde	Jean Fresnière	Marketing Assistant	(514) 555-8054	(514) 555-8055	43 rue St. Laurent	H1J 1C3	Montréal	Québec	\N	Canada	\N
416	MORGK	Morgenstern Gesundkost	Alexander Feuer	Marketing Assistant	0342-023176	\N	Heerstr. 22	04179	Leipzig	\N	\N	Germany	\N
417	NORTS	North/South	Simon Crowther	Sales Associate	(171) 555-7733	(171) 555-2530	South House\n300 Queensbridge	SW7 1RZ	London	\N	\N	UK	\N
418	OCEAN	Océano Atlántico Ltda.	Yvonne Moncada	Sales Agent	(1) 135-5333	(1) 135-5535	Ing. Gustavo Moncada 8585\nPiso 20-A	1010	Buenos Aires	\N	\N	Argentina	\N
419	OLDWO	Old World Delicatessen	Rene Phillips	Sales Representative	(907) 555-7584	(907) 555-2880	2743 Bering St.	99508	Anchorage	AK	\N	USA	\N
420	OTTIK	Ottilies Käseladen	Henriette Pfalzheim	Owner	0221-0644327	0221-0765721	Mehrheimerstr. 369	50739	Köln	\N	\N	Germany	\N
421	PARIS	Paris spécialités	Marie Bertrand	Owner	(1) 42.34.22.66	(1) 42.34.22.77	265, boulevard Charonne	75012	Paris	\N	\N	France	\N
422	PERIC	Pericles Comidas clásicas	Guillermo Fernández	Sales Representative	(5) 552-3745	(5) 545-3745	Calle Dr. Jorge Cash 321	05033	México D.F.	\N	\N	Mexico	\N
423	PICCO	Piccolo und mehr	Georg Pipps	Sales Manager	6562-9722	6562-9723	Geislweg 14	5020	Salzburg	\N	\N	Austria	\N
424	PRINI	Princesa Isabel Vinhos	Isabel de Castro	Sales Representative	(1) 356-5634	\N	Estrada da saúde n. 58	1756	Lisboa	\N	\N	Portugal	\N
425	QUEDE	Que Delícia	Bernardo Batista	Accounting Manager	(21) 555-4252	(21) 555-4545	Rua da Panificadora, 12	02389-673	Rio de Janeiro	RJ	\N	Brazil	\N
426	QUEEN	Queen Cozinha	Lúcia Carvalho	Marketing Assistant	(11) 555-1189	\N	Alameda dos Canàrios, 891	05487-020	São Paulo	SP	\N	Brazil	\N
427	QUICK	QUICK-Stop	Horst Kloss	Accounting Manager	0372-035188	\N	Taucherstraße 10	01307	Cunewalde	\N	\N	Germany	\N
428	RANCH	Rancho grande	Sergio Gutiérrez	Sales Representative	(1) 123-5555	(1) 123-5556	Av. del Libertador 900	1010	Buenos Aires	\N	\N	Argentina	\N
429	RATTC	Rattlesnake Canyon Grocery	Paula Wilson	Assistant Sales Representative	(505) 555-5939	(505) 555-3620	2817 Milton Dr.	87110	Albuquerque	NM	\N	USA	\N
430	REGGC	Reggiani Caseifici	Maurizio Moroni	Sales Associate	0522-556721	0522-556722	Strada Provinciale 124	42100	Reggio Emilia	\N	\N	Italy	\N
431	RICAR	Ricardo Adocicados	Janete Limeira	Assistant Sales Agent	(21) 555-3412	\N	Av. Copacabana, 267	02389-890	Rio de Janeiro	RJ	\N	Brazil	\N
432	RICSU	Richter Supermarkt	Michael Holz	Sales Manager	0897-034214	\N	Grenzacherweg 237	1203	Genève	\N	\N	Switzerland	\N
433	ROMEY	Romero y tomillo	Alejandra Camino	Accounting Manager	(91) 745 6200	(91) 745 6210	Gran Vía, 1	28001	Madrid	\N	\N	Spain	\N
434	SANTG	Santé Gourmet	Jonas Bergulfsen	Owner	07-98 92 35	07-98 92 47	Erling Skakkes gate 78	4110	Stavern	\N	\N	Norway	\N
435	SAVEA	Save-a-lot Markets	Jose Pavarotti	Sales Representative	(208) 555-8097	\N	187 Suffolk Ln.	83720	Boise	ID	\N	USA	\N
436	SEVES	Seven Seas Imports	Hari Kumar	Sales Manager	(171) 555-1717	(171) 555-5646	90 Wadhurst Rd.	OX15 4NB	London	\N	\N	UK	\N
437	SIMOB	Simons bistro	Jytte Petersen	Owner	31 12 34 56	31 13 35 57	Vinbæltet 34	1734	København	\N	\N	Denmark	\N
438	SPECD	Spécialités du monde	Dominique Perrier	Marketing Manager	(1) 47.55.60.10	(1) 47.55.60.20	25, rue Lauriston	75016	Paris	\N	\N	France	\N
439	SPLIR	Split Rail Beer & Ale	Art Braunschweiger	Sales Manager	(307) 555-4680	(307) 555-6525	P.O. Box 555	82520	Lander	WY	\N	USA	\N
440	SUPRD	Suprêmes délices	Pascale Cartrain	Accounting Manager	(071) 23 67 22 20	(071) 23 67 22 21	Boulevard Tirou, 255	B-6000	Charleroi	\N	\N	Belgium	\N
441	THEBI	The Big Cheese	Liz Nixon	Marketing Manager	(503) 555-3612	\N	89 Jefferson Way\nSuite 2	97201	Portland	OR	\N	USA	\N
442	THECR	The Cracker Box	Liu Wong	Marketing Assistant	(406) 555-5834	(406) 555-8083	55 Grizzly Peak Rd.	59801	Butte	MT	\N	USA	\N
443	TOMSP	Toms Spezialitäten	Karin Josephs	Marketing Manager	0251-031259	0251-035695	Luisenstr. 48	44087	Münster	\N	\N	Germany	\N
444	TORTU	Tortuga Restaurante	Miguel Angel Paolino	Owner	(5) 555-2933	\N	Avda. Azteca 123	05033	México D.F.	\N	\N	Mexico	\N
445	TRADH	Tradição Hipermercados	Anabela Domingues	Sales Representative	(11) 555-2167	(11) 555-2168	Av. Inês de Castro, 414	05634-030	São Paulo	SP	\N	Brazil	\N
446	TRAIH	Trail's Head Gourmet Provisioners	Helvetius Nagy	Sales Associate	(206) 555-8257	(206) 555-2174	722 DaVinci Blvd.	98034	Kirkland	WA	\N	USA	\N
447	VAFFE	Vaffeljernet	Palle Ibsen	Sales Manager	86 21 32 43	86 22 33 44	Smagsløget 45	8200	Århus	\N	\N	Denmark	\N
448	VICTE	Victuailles en stock	Mary Saveley	Sales Agent	78.32.54.86	78.32.54.87	2, rue du Commerce	69004	Lyon	\N	\N	France	\N
449	VINET	Vins et alcools Chevalier	Paul Henriot	Accounting Manager	26.47.15.10	26.47.15.11	59 rue de l'Abbaye	51100	Reims	\N	\N	France	\N
450	WANDK	Die Wandernde Kuh	Rita Müller	Sales Representative	0711-020361	0711-035428	Adenauerallee 900	70563	Stuttgart	\N	\N	Germany	\N
451	WARTH	Wartian Herkku	Pirkko Koskitalo	Accounting Manager	981-443655	981-443655	Torikatu 38	90110	Oulu	\N	\N	Finland	\N
452	WELLI	Wellington Importadora	Paula Parente	Sales Manager	(14) 555-8122	\N	Rua do Mercado, 12	08737-363	Resende	SP	\N	Brazil	\N
453	WHITC	White Clover Markets	Karl Jablonski	Owner	(206) 555-4112	(206) 555-4115	305 - 14th Ave. S.\nSuite 3B	98128	Seattle	WA	\N	USA	\N
454	WILMK	Wilman Kala	Matti Karttunen	Owner/Marketing Assistant	90-224 8858	90-224 8858	Keskuskatu 45	21240	Helsinki	\N	\N	Finland	\N
455	WOLZA	Wolski  Zajazd	Zbyszek Piestrzeniewicz	Owner	(26) 642-7012	(26) 642-7012	ul. Filtrowa 68	01-012	Warszawa	\N	\N	Poland	\N
\.


--
-- Data for Name: d_employee; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_employee (employee_key, employee_id, employee_last_name, employee_first_name, employee_title, employee_title_of_courtesy, employee_birth_date, employee_hire_date, employee_home_phone, employee_extension, employee_street_address, employee_postal_code_id, employee_city_name, employee_region_name, employee_country_name) FROM stdin;
37	1	Davolio	Nancy	Sales Representative	Ms.	1968-12-08 00:00:00	1992-05-01 00:00:00	(206) 555-9857	5467	507 - 20th Ave. E.\nApt. 2A	98122	Seattle	WA	USA
38	2	Fuller	Andrew	Vice President, Sales	Dr.	1952-02-19 00:00:00	1992-08-14 00:00:00	(206) 555-9482	3457	908 W. Capital Way	98401	Tacoma	WA	USA
39	3	Leverling	Janet	Sales Representative	Ms.	1963-08-30 00:00:00	1992-04-01 00:00:00	(206) 555-3412	3355	722 Moss Bay Blvd.	98033	Kirkland	WA	USA
40	4	Peacock	Margaret	Sales Representative	Mrs.	1958-09-19 00:00:00	1993-05-03 00:00:00	(206) 555-8122	5176	4110 Old Redmond Rd.	98052	Redmond	WA	USA
41	5	Buchanan	Steven	Sales Manager	Mr.	1955-03-04 00:00:00	1993-10-17 00:00:00	(71) 555-4848	3453	14 Garrett Hill	SW1 8JR	London	\N	UK
42	6	Suyama	Michael	Sales Representative	Mr.	1963-07-02 00:00:00	1993-10-17 00:00:00	(71) 555-7773	428	Coventry House\nMiner Rd.	EC2 7JR	London	\N	UK
43	7	King	Robert	Sales Representative	Mr.	1960-05-29 00:00:00	1994-01-02 00:00:00	(71) 555-5598	465	Edgeham Hollow\nWinchester Way	RG1 9SP	London	\N	UK
44	8	Callahan	Laura	Inside Sales Coordinator	Ms.	1958-01-09 00:00:00	1994-03-05 00:00:00	(206) 555-1189	2344	4726 - 11th Ave. N.E.	98105	Seattle	WA	USA
45	9	Dodsworth	Anne	Sales Representative	Ms.	1969-07-02 00:00:00	1994-11-15 00:00:00	(71) 555-4444	452	7 Houndstooth Rd.	WG2 7LT	London	\N	UK
\.


--
-- Data for Name: d_order_date_dim; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_order_date_dim (order_date_key, order_date_id_yyyymmdd, "Order_Day_Date_yyyy-mm-dd", order_full_date, order_day_of_week_name, order_day_of_week_abbrev) FROM stdin;
3581	19970306	1997-03-06	1997-03-06 00:00:00	Thursday 	Thu
3582	19970612	1997-06-12	1997-06-12 00:00:00	Thursday 	Thu
3583	19971121	1997-11-21	1997-11-21 00:00:00	Friday   	Fri
3584	19960814	1996-08-14	1996-08-14 00:00:00	Wednesday	Wed
3585	19970509	1997-05-09	1997-05-09 00:00:00	Friday   	Fri
3586	19970805	1997-08-05	1997-08-05 00:00:00	Tuesday  	Tue
3587	19980116	1998-01-16	1998-01-16 00:00:00	Friday   	Fri
3588	19971014	1997-10-14	1997-10-14 00:00:00	Tuesday  	Tue
3589	19970212	1997-02-12	1997-02-12 00:00:00	Wednesday	Wed
3590	19970311	1997-03-11	1997-03-11 00:00:00	Tuesday  	Tue
3591	19970508	1997-05-08	1997-05-08 00:00:00	Thursday 	Thu
3592	19980408	1998-04-08	1998-04-08 00:00:00	Wednesday	Wed
3593	19960904	1996-09-04	1996-09-04 00:00:00	Wednesday	Wed
3594	19970916	1997-09-16	1997-09-16 00:00:00	Tuesday  	Tue
3595	19970717	1997-07-17	1997-07-17 00:00:00	Thursday 	Thu
3596	19971031	1997-10-31	1997-10-31 00:00:00	Friday   	Fri
3597	19970925	1997-09-25	1997-09-25 00:00:00	Thursday 	Thu
3598	19961113	1996-11-13	1996-11-13 00:00:00	Wednesday	Wed
3599	19960905	1996-09-05	1996-09-05 00:00:00	Thursday 	Thu
3600	19971212	1997-12-12	1997-12-12 00:00:00	Friday   	Fri
3601	19970602	1997-06-02	1997-06-02 00:00:00	Monday   	Mon
3602	19970715	1997-07-15	1997-07-15 00:00:00	Tuesday  	Tue
3603	19971017	1997-10-17	1997-10-17 00:00:00	Friday   	Fri
3604	19960924	1996-09-24	1996-09-24 00:00:00	Tuesday  	Tue
3605	19970116	1997-01-16	1997-01-16 00:00:00	Thursday 	Thu
3606	19971204	1997-12-04	1997-12-04 00:00:00	Thursday 	Thu
3607	19971110	1997-11-10	1997-11-10 00:00:00	Monday   	Mon
3608	19970120	1997-01-20	1997-01-20 00:00:00	Monday   	Mon
3609	19980127	1998-01-27	1998-01-27 00:00:00	Tuesday  	Tue
3610	19970929	1997-09-29	1997-09-29 00:00:00	Monday   	Mon
3611	19960926	1996-09-26	1996-09-26 00:00:00	Thursday 	Thu
3612	19960723	1996-07-23	1996-07-23 00:00:00	Tuesday  	Tue
3613	19970811	1997-08-11	1997-08-11 00:00:00	Monday   	Mon
3614	19970604	1997-06-04	1997-06-04 00:00:00	Wednesday	Wed
3615	19980327	1998-03-27	1998-03-27 00:00:00	Friday   	Fri
3616	19980225	1998-02-25	1998-02-25 00:00:00	Wednesday	Wed
3617	19970501	1997-05-01	1997-05-01 00:00:00	Thursday 	Thu
3618	19970319	1997-03-19	1997-03-19 00:00:00	Wednesday	Wed
3619	19980108	1998-01-08	1998-01-08 00:00:00	Thursday 	Thu
3620	19970922	1997-09-22	1997-09-22 00:00:00	Monday   	Mon
3621	19970828	1997-08-28	1997-08-28 00:00:00	Thursday 	Thu
3622	19960731	1996-07-31	1996-07-31 00:00:00	Wednesday	Wed
3623	19970924	1997-09-24	1997-09-24 00:00:00	Wednesday	Wed
3624	19970109	1997-01-09	1997-01-09 00:00:00	Thursday 	Thu
3625	19960708	1996-07-08	1996-07-08 00:00:00	Monday   	Mon
3626	19970807	1997-08-07	1997-08-07 00:00:00	Thursday 	Thu
3627	19970812	1997-08-12	1997-08-12 00:00:00	Tuesday  	Tue
3628	19970401	1997-04-01	1997-04-01 00:00:00	Tuesday  	Tue
3629	19970218	1997-02-18	1997-02-18 00:00:00	Tuesday  	Tue
3630	19970321	1997-03-21	1997-03-21 00:00:00	Friday   	Fri
3631	19970930	1997-09-30	1997-09-30 00:00:00	Tuesday  	Tue
3632	19960821	1996-08-21	1996-08-21 00:00:00	Wednesday	Wed
3633	19970129	1997-01-29	1997-01-29 00:00:00	Wednesday	Wed
3634	19970324	1997-03-24	1997-03-24 00:00:00	Monday   	Mon
3635	19960704	1996-07-04	1996-07-04 00:00:00	Thursday 	Thu
3636	19961030	1996-10-30	1996-10-30 00:00:00	Wednesday	Wed
3637	19971127	1997-11-27	1997-11-27 00:00:00	Thursday 	Thu
3638	19971113	1997-11-13	1997-11-13 00:00:00	Thursday 	Thu
3639	19961017	1996-10-17	1996-10-17 00:00:00	Thursday 	Thu
3640	19961231	1996-12-31	1996-12-31 00:00:00	Tuesday  	Tue
3641	19971202	1997-12-02	1997-12-02 00:00:00	Tuesday  	Tue
3642	19960923	1996-09-23	1996-09-23 00:00:00	Monday   	Mon
3643	19980223	1998-02-23	1998-02-23 00:00:00	Monday   	Mon
3644	19971119	1997-11-19	1997-11-19 00:00:00	Wednesday	Wed
3645	19980101	1998-01-01	1998-01-01 00:00:00	Thursday 	Thu
3646	19980421	1998-04-21	1998-04-21 00:00:00	Tuesday  	Tue
3647	19971218	1997-12-18	1997-12-18 00:00:00	Thursday 	Thu
3648	19971028	1997-10-28	1997-10-28 00:00:00	Tuesday  	Tue
3649	19970709	1997-07-09	1997-07-09 00:00:00	Wednesday	Wed
3650	19980210	1998-02-10	1998-02-10 00:00:00	Tuesday  	Tue
3651	19980324	1998-03-24	1998-03-24 00:00:00	Tuesday  	Tue
3652	19961206	1996-12-06	1996-12-06 00:00:00	Friday   	Fri
3653	19970305	1997-03-05	1997-03-05 00:00:00	Wednesday	Wed
3654	19980226	1998-02-26	1998-02-26 00:00:00	Thursday 	Thu
3655	19980312	1998-03-12	1998-03-12 00:00:00	Thursday 	Thu
3656	19980401	1998-04-01	1998-04-01 00:00:00	Wednesday	Wed
3657	19970411	1997-04-11	1997-04-11 00:00:00	Friday   	Fri
3658	19980317	1998-03-17	1998-03-17 00:00:00	Tuesday  	Tue
3659	19971029	1997-10-29	1997-10-29 00:00:00	Wednesday	Wed
3660	19970320	1997-03-20	1997-03-20 00:00:00	Thursday 	Thu
3661	19960910	1996-09-10	1996-09-10 00:00:00	Tuesday  	Tue
3662	19970521	1997-05-21	1997-05-21 00:00:00	Wednesday	Wed
3663	19970217	1997-02-17	1997-02-17 00:00:00	Monday   	Mon
3664	19971003	1997-10-03	1997-10-03 00:00:00	Friday   	Fri
3665	19970512	1997-05-12	1997-05-12 00:00:00	Monday   	Mon
3666	19961212	1996-12-12	1996-12-12 00:00:00	Thursday 	Thu
3667	19980310	1998-03-10	1998-03-10 00:00:00	Tuesday  	Tue
3668	19971106	1997-11-06	1997-11-06 00:00:00	Thursday 	Thu
3669	19970127	1997-01-27	1997-01-27 00:00:00	Monday   	Mon
3670	19960725	1996-07-25	1996-07-25 00:00:00	Thursday 	Thu
3671	19980331	1998-03-31	1998-03-31 00:00:00	Tuesday  	Tue
3672	19970123	1997-01-23	1997-01-23 00:00:00	Thursday 	Thu
3673	19970613	1997-06-13	1997-06-13 00:00:00	Friday   	Fri
3674	19980309	1998-03-09	1998-03-09 00:00:00	Monday   	Mon
3675	19970918	1997-09-18	1997-09-18 00:00:00	Thursday 	Thu
3676	19971224	1997-12-24	1997-12-24 00:00:00	Wednesday	Wed
3677	19970318	1997-03-18	1997-03-18 00:00:00	Tuesday  	Tue
3678	19970901	1997-09-01	1997-09-01 00:00:00	Monday   	Mon
3679	19970211	1997-02-11	1997-02-11 00:00:00	Tuesday  	Tue
3680	19961218	1996-12-18	1996-12-18 00:00:00	Wednesday	Wed
3681	19961217	1996-12-17	1996-12-17 00:00:00	Tuesday  	Tue
3682	19971112	1997-11-12	1997-11-12 00:00:00	Wednesday	Wed
3683	19961018	1996-10-18	1996-10-18 00:00:00	Friday   	Fri
3684	19980209	1998-02-09	1998-02-09 00:00:00	Monday   	Mon
3685	19960717	1996-07-17	1996-07-17 00:00:00	Wednesday	Wed
3686	19960726	1996-07-26	1996-07-26 00:00:00	Friday   	Fri
3687	19970714	1997-07-14	1997-07-14 00:00:00	Monday   	Mon
3688	19961115	1996-11-15	1996-11-15 00:00:00	Friday   	Fri
3689	19970422	1997-04-22	1997-04-22 00:00:00	Tuesday  	Tue
3690	19980313	1998-03-13	1998-03-13 00:00:00	Friday   	Fri
3691	19970530	1997-05-30	1997-05-30 00:00:00	Friday   	Fri
3692	19961016	1996-10-16	1996-10-16 00:00:00	Wednesday	Wed
3693	19970227	1997-02-27	1997-02-27 00:00:00	Thursday 	Thu
3694	19960716	1996-07-16	1996-07-16 00:00:00	Tuesday  	Tue
3695	19970513	1997-05-13	1997-05-13 00:00:00	Tuesday  	Tue
3696	19970908	1997-09-08	1997-09-08 00:00:00	Monday   	Mon
3697	19980203	1998-02-03	1998-02-03 00:00:00	Tuesday  	Tue
3698	19970623	1997-06-23	1997-06-23 00:00:00	Monday   	Mon
3699	19960826	1996-08-26	1996-08-26 00:00:00	Monday   	Mon
3700	19960809	1996-08-09	1996-08-09 00:00:00	Friday   	Fri
3701	19961121	1996-11-21	1996-11-21 00:00:00	Thursday 	Thu
3702	19971013	1997-10-13	1997-10-13 00:00:00	Monday   	Mon
3703	19961129	1996-11-29	1996-11-29 00:00:00	Friday   	Fri
3704	19980112	1998-01-12	1998-01-12 00:00:00	Monday   	Mon
3705	19960919	1996-09-19	1996-09-19 00:00:00	Thursday 	Thu
3706	19961011	1996-10-11	1996-10-11 00:00:00	Friday   	Fri
3707	19970626	1997-06-26	1997-06-26 00:00:00	Thursday 	Thu
3708	19970528	1997-05-28	1997-05-28 00:00:00	Wednesday	Wed
3709	19970327	1997-03-27	1997-03-27 00:00:00	Thursday 	Thu
3710	19980121	1998-01-21	1998-01-21 00:00:00	Wednesday	Wed
3711	19971126	1997-11-26	1997-11-26 00:00:00	Wednesday	Wed
3712	19961216	1996-12-16	1996-12-16 00:00:00	Monday   	Mon
3713	19970424	1997-04-24	1997-04-24 00:00:00	Thursday 	Thu
3714	19961224	1996-12-24	1996-12-24 00:00:00	Tuesday  	Tue
3715	19970526	1997-05-26	1997-05-26 00:00:00	Monday   	Mon
3716	19960813	1996-08-13	1996-08-13 00:00:00	Tuesday  	Tue
3717	19980417	1998-04-17	1998-04-17 00:00:00	Friday   	Fri
3718	19970101	1997-01-01	1997-01-01 00:00:00	Wednesday	Wed
3719	19980224	1998-02-24	1998-02-24 00:00:00	Tuesday  	Tue
3720	19970520	1997-05-20	1997-05-20 00:00:00	Tuesday  	Tue
3721	19961028	1996-10-28	1996-10-28 00:00:00	Monday   	Mon
3722	19970624	1997-06-24	1997-06-24 00:00:00	Tuesday  	Tue
3723	19970210	1997-02-10	1997-02-10 00:00:00	Monday   	Mon
3724	19971023	1997-10-23	1997-10-23 00:00:00	Thursday 	Thu
3725	19980129	1998-01-29	1998-01-29 00:00:00	Thursday 	Thu
3726	19970131	1997-01-31	1997-01-31 00:00:00	Friday   	Fri
3727	19980325	1998-03-25	1998-03-25 00:00:00	Wednesday	Wed
3728	19960912	1996-09-12	1996-09-12 00:00:00	Thursday 	Thu
3729	19971230	1997-12-30	1997-12-30 00:00:00	Tuesday  	Tue
3730	19980415	1998-04-15	1998-04-15 00:00:00	Wednesday	Wed
3731	19970522	1997-05-22	1997-05-22 00:00:00	Thursday 	Thu
3732	19961112	1996-11-12	1996-11-12 00:00:00	Tuesday  	Tue
3733	19970429	1997-04-29	1997-04-29 00:00:00	Tuesday  	Tue
3734	19980311	1998-03-11	1998-03-11 00:00:00	Wednesday	Wed
3735	19961025	1996-10-25	1996-10-25 00:00:00	Friday   	Fri
3736	19971024	1997-10-24	1997-10-24 00:00:00	Friday   	Fri
3737	19971125	1997-11-25	1997-11-25 00:00:00	Tuesday  	Tue
3738	19970303	1997-03-03	1997-03-03 00:00:00	Monday   	Mon
3739	19961227	1996-12-27	1996-12-27 00:00:00	Friday   	Fri
3740	19970829	1997-08-29	1997-08-29 00:00:00	Friday   	Fri
3741	19960911	1996-09-11	1996-09-11 00:00:00	Wednesday	Wed
3742	19980319	1998-03-19	1998-03-19 00:00:00	Thursday 	Thu
3743	19961107	1996-11-07	1996-11-07 00:00:00	Thursday 	Thu
3744	19960925	1996-09-25	1996-09-25 00:00:00	Wednesday	Wed
3745	19971006	1997-10-06	1997-10-06 00:00:00	Monday   	Mon
3746	19970827	1997-08-27	1997-08-27 00:00:00	Wednesday	Wed
3747	19961002	1996-10-02	1996-10-02 00:00:00	Wednesday	Wed
3748	19970226	1997-02-26	1997-02-26 00:00:00	Wednesday	Wed
3749	19970103	1997-01-03	1997-01-03 00:00:00	Friday   	Fri
3750	19980220	1998-02-20	1998-02-20 00:00:00	Friday   	Fri
3751	19961223	1996-12-23	1996-12-23 00:00:00	Monday   	Mon
3752	19980506	1998-05-06	1998-05-06 00:00:00	Wednesday	Wed
3753	19970403	1997-04-03	1997-04-03 00:00:00	Thursday 	Thu
3754	19970204	1997-02-04	1997-02-04 00:00:00	Tuesday  	Tue
3755	19961104	1996-11-04	1996-11-04 00:00:00	Monday   	Mon
3756	19970926	1997-09-26	1997-09-26 00:00:00	Friday   	Fri
3757	19970627	1997-06-27	1997-06-27 00:00:00	Friday   	Fri
3758	19960828	1996-08-28	1996-08-28 00:00:00	Wednesday	Wed
3759	19970826	1997-08-26	1997-08-26 00:00:00	Tuesday  	Tue
3760	19970711	1997-07-11	1997-07-11 00:00:00	Friday   	Fri
3761	19980211	1998-02-11	1998-02-11 00:00:00	Wednesday	Wed
3762	19970206	1997-02-06	1997-02-06 00:00:00	Thursday 	Thu
3763	19961014	1996-10-14	1996-10-14 00:00:00	Monday   	Mon
3764	19960819	1996-08-19	1996-08-19 00:00:00	Monday   	Mon
3765	19970317	1997-03-17	1997-03-17 00:00:00	Monday   	Mon
3766	19970804	1997-08-04	1997-08-04 00:00:00	Monday   	Mon
3767	19980219	1998-02-19	1998-02-19 00:00:00	Thursday 	Thu
3768	19970606	1997-06-06	1997-06-06 00:00:00	Friday   	Fri
3769	19971117	1997-11-17	1997-11-17 00:00:00	Monday   	Mon
3770	19960705	1996-07-05	1996-07-05 00:00:00	Friday   	Fri
3771	19970414	1997-04-14	1997-04-14 00:00:00	Monday   	Mon
3772	19980122	1998-01-22	1998-01-22 00:00:00	Thursday 	Thu
3773	19971114	1997-11-14	1997-11-14 00:00:00	Friday   	Fri
3774	19980430	1998-04-30	1998-04-30 00:00:00	Thursday 	Thu
3775	19970619	1997-06-19	1997-06-19 00:00:00	Thursday 	Thu
3776	19980123	1998-01-23	1998-01-23 00:00:00	Friday   	Fri
3777	19970310	1997-03-10	1997-03-10 00:00:00	Monday   	Mon
3778	19970113	1997-01-13	1997-01-13 00:00:00	Monday   	Mon
3779	19970903	1997-09-03	1997-09-03 00:00:00	Wednesday	Wed
3780	19980119	1998-01-19	1998-01-19 00:00:00	Monday   	Mon
3781	19960917	1996-09-17	1996-09-17 00:00:00	Tuesday  	Tue
3782	19980130	1998-01-30	1998-01-30 00:00:00	Friday   	Fri
3783	19970730	1997-07-30	1997-07-30 00:00:00	Wednesday	Wed
3784	19971022	1997-10-22	1997-10-22 00:00:00	Wednesday	Wed
3785	19960920	1996-09-20	1996-09-20 00:00:00	Friday   	Fri
3786	19970514	1997-05-14	1997-05-14 00:00:00	Wednesday	Wed
3787	19960802	1996-08-02	1996-08-02 00:00:00	Friday   	Fri
3788	19960815	1996-08-15	1996-08-15 00:00:00	Thursday 	Thu
3789	19970707	1997-07-07	1997-07-07 00:00:00	Monday   	Mon
3790	19970904	1997-09-04	1997-09-04 00:00:00	Thursday 	Thu
3791	19970625	1997-06-25	1997-06-25 00:00:00	Wednesday	Wed
3792	19980320	1998-03-20	1998-03-20 00:00:00	Friday   	Fri
3793	19980403	1998-04-03	1998-04-03 00:00:00	Friday   	Fri
3794	19971222	1997-12-22	1997-12-22 00:00:00	Monday   	Mon
3795	19961219	1996-12-19	1996-12-19 00:00:00	Thursday 	Thu
3796	19970307	1997-03-07	1997-03-07 00:00:00	Friday   	Fri
3797	19980316	1998-03-16	1998-03-16 00:00:00	Monday   	Mon
3798	19970806	1997-08-06	1997-08-06 00:00:00	Wednesday	Wed
3799	19960812	1996-08-12	1996-08-12 00:00:00	Monday   	Mon
3800	19970815	1997-08-15	1997-08-15 00:00:00	Friday   	Fri
3801	19971008	1997-10-08	1997-10-08 00:00:00	Wednesday	Wed
3802	19960820	1996-08-20	1996-08-20 00:00:00	Tuesday  	Tue
3803	19971020	1997-10-20	1997-10-20 00:00:00	Monday   	Mon
3804	19960807	1996-08-07	1996-08-07 00:00:00	Wednesday	Wed
3805	19971027	1997-10-27	1997-10-27 00:00:00	Monday   	Mon
3806	19960909	1996-09-09	1996-09-09 00:00:00	Monday   	Mon
3807	19980204	1998-02-04	1998-02-04 00:00:00	Wednesday	Wed
3808	19961118	1996-11-18	1996-11-18 00:00:00	Monday   	Mon
3809	19980126	1998-01-26	1998-01-26 00:00:00	Monday   	Mon
3810	19980416	1998-04-16	1998-04-16 00:00:00	Thursday 	Thu
3811	19970611	1997-06-11	1997-06-11 00:00:00	Wednesday	Wed
3812	19961031	1996-10-31	1996-10-31 00:00:00	Thursday 	Thu
3813	19970331	1997-03-31	1997-03-31 00:00:00	Monday   	Mon
3814	19980304	1998-03-04	1998-03-04 00:00:00	Wednesday	Wed
3815	19970114	1997-01-14	1997-01-14 00:00:00	Tuesday  	Tue
3816	19970228	1997-02-28	1997-02-28 00:00:00	Friday   	Fri
3817	19980330	1998-03-30	1998-03-30 00:00:00	Monday   	Mon
3818	19971217	1997-12-17	1997-12-17 00:00:00	Wednesday	Wed
3819	19971118	1997-11-18	1997-11-18 00:00:00	Tuesday  	Tue
3820	19970725	1997-07-25	1997-07-25 00:00:00	Friday   	Fri
3821	19970527	1997-05-27	1997-05-27 00:00:00	Tuesday  	Tue
3822	19970610	1997-06-10	1997-06-10 00:00:00	Tuesday  	Tue
3823	19980407	1998-04-07	1998-04-07 00:00:00	Tuesday  	Tue
3824	19971209	1997-12-09	1997-12-09 00:00:00	Tuesday  	Tue
3825	19970911	1997-09-11	1997-09-11 00:00:00	Thursday 	Thu
3826	19971203	1997-12-03	1997-12-03 00:00:00	Wednesday	Wed
3827	19970729	1997-07-29	1997-07-29 00:00:00	Tuesday  	Tue
3828	19961126	1996-11-26	1996-11-26 00:00:00	Tuesday  	Tue
3829	19970121	1997-01-21	1997-01-21 00:00:00	Tuesday  	Tue
3830	19970124	1997-01-24	1997-01-24 00:00:00	Friday   	Fri
3831	19980213	1998-02-13	1998-02-13 00:00:00	Friday   	Fri
3832	19970117	1997-01-17	1997-01-17 00:00:00	Friday   	Fri
3833	19960718	1996-07-18	1996-07-18 00:00:00	Thursday 	Thu
3834	19961009	1996-10-09	1996-10-09 00:00:00	Wednesday	Wed
3835	19970822	1997-08-22	1997-08-22 00:00:00	Friday   	Fri
3836	19961125	1996-11-25	1996-11-25 00:00:00	Monday   	Mon
3837	19960918	1996-09-18	1996-09-18 00:00:00	Wednesday	Wed
3838	19970425	1997-04-25	1997-04-25 00:00:00	Friday   	Fri
3839	19970819	1997-08-19	1997-08-19 00:00:00	Tuesday  	Tue
3840	19961111	1996-11-11	1996-11-11 00:00:00	Monday   	Mon
3841	19971010	1997-10-10	1997-10-10 00:00:00	Friday   	Fri
3842	19980402	1998-04-02	1998-04-02 00:00:00	Thursday 	Thu
3843	19980409	1998-04-09	1998-04-09 00:00:00	Thursday 	Thu
3844	19980305	1998-03-05	1998-03-05 00:00:00	Thursday 	Thu
3845	19980217	1998-02-17	1998-02-17 00:00:00	Tuesday  	Tue
3846	19960827	1996-08-27	1996-08-27 00:00:00	Tuesday  	Tue
3847	19970708	1997-07-08	1997-07-08 00:00:00	Tuesday  	Tue
3848	19970505	1997-05-05	1997-05-05 00:00:00	Monday   	Mon
3849	19960906	1996-09-06	1996-09-06 00:00:00	Friday   	Fri
3850	19970115	1997-01-15	1997-01-15 00:00:00	Wednesday	Wed
3851	19971128	1997-11-28	1997-11-28 00:00:00	Friday   	Fri
3852	19961203	1996-12-03	1996-12-03 00:00:00	Tuesday  	Tue
3853	19970516	1997-05-16	1997-05-16 00:00:00	Friday   	Fri
3854	19961001	1996-10-01	1996-10-01 00:00:00	Tuesday  	Tue
3855	19971210	1997-12-10	1997-12-10 00:00:00	Wednesday	Wed
3856	19961114	1996-11-14	1996-11-14 00:00:00	Thursday 	Thu
3857	19960829	1996-08-29	1996-08-29 00:00:00	Thursday 	Thu
3858	19970702	1997-07-02	1997-07-02 00:00:00	Wednesday	Wed
3859	19970502	1997-05-02	1997-05-02 00:00:00	Friday   	Fri
3860	19970910	1997-09-10	1997-09-10 00:00:00	Wednesday	Wed
3861	19970723	1997-07-23	1997-07-23 00:00:00	Wednesday	Wed
3862	19970224	1997-02-24	1997-02-24 00:00:00	Monday   	Mon
3863	19980428	1998-04-28	1998-04-28 00:00:00	Tuesday  	Tue
3864	19980323	1998-03-23	1998-03-23 00:00:00	Monday   	Mon
3865	19970205	1997-02-05	1997-02-05 00:00:00	Wednesday	Wed
3866	19960722	1996-07-22	1996-07-22 00:00:00	Monday   	Mon
3867	19961210	1996-12-10	1996-12-10 00:00:00	Tuesday  	Tue
3868	19960712	1996-07-12	1996-07-12 00:00:00	Friday   	Fri
3869	19970813	1997-08-13	1997-08-13 00:00:00	Wednesday	Wed
3870	19980216	1998-02-16	1998-02-16 00:00:00	Monday   	Mon
3871	19961008	1996-10-08	1996-10-08 00:00:00	Tuesday  	Tue
3872	19970605	1997-06-05	1997-06-05 00:00:00	Thursday 	Thu
3873	19970905	1997-09-05	1997-09-05 00:00:00	Friday   	Fri
3874	19980106	1998-01-06	1998-01-06 00:00:00	Tuesday  	Tue
3875	19961021	1996-10-21	1996-10-21 00:00:00	Monday   	Mon
3876	19960903	1996-09-03	1996-09-03 00:00:00	Tuesday  	Tue
3877	19970128	1997-01-28	1997-01-28 00:00:00	Tuesday  	Tue
3878	19980206	1998-02-06	1998-02-06 00:00:00	Friday   	Fri
3879	19961230	1996-12-30	1996-12-30 00:00:00	Monday   	Mon
3880	19971124	1997-11-24	1997-11-24 00:00:00	Monday   	Mon
3881	19970609	1997-06-09	1997-06-09 00:00:00	Monday   	Mon
3882	19971225	1997-12-25	1997-12-25 00:00:00	Thursday 	Thu
3883	19970421	1997-04-21	1997-04-21 00:00:00	Monday   	Mon
3884	19970923	1997-09-23	1997-09-23 00:00:00	Tuesday  	Tue
3885	19970821	1997-08-21	1997-08-21 00:00:00	Thursday 	Thu
3886	19960711	1996-07-11	1996-07-11 00:00:00	Thursday 	Thu
3887	19970417	1997-04-17	1997-04-17 00:00:00	Thursday 	Thu
3888	19970430	1997-04-30	1997-04-30 00:00:00	Wednesday	Wed
3889	19970225	1997-02-25	1997-02-25 00:00:00	Tuesday  	Tue
3890	19961226	1996-12-26	1996-12-26 00:00:00	Thursday 	Thu
3891	19961106	1996-11-06	1996-11-06 00:00:00	Wednesday	Wed
3892	19970326	1997-03-26	1997-03-26 00:00:00	Wednesday	Wed
3893	19970214	1997-02-14	1997-02-14 00:00:00	Friday   	Fri
3894	19970710	1997-07-10	1997-07-10 00:00:00	Thursday 	Thu
3895	19961108	1996-11-08	1996-11-08 00:00:00	Friday   	Fri
3896	19961015	1996-10-15	1996-10-15 00:00:00	Tuesday  	Tue
3897	19961202	1996-12-02	1996-12-02 00:00:00	Monday   	Mon
3898	19971103	1997-11-03	1997-11-03 00:00:00	Monday   	Mon
3899	19980504	1998-05-04	1998-05-04 00:00:00	Monday   	Mon
3900	19971021	1997-10-21	1997-10-21 00:00:00	Tuesday  	Tue
3901	19970506	1997-05-06	1997-05-06 00:00:00	Tuesday  	Tue
3902	19970108	1997-01-08	1997-01-08 00:00:00	Wednesday	Wed
3903	19961024	1996-10-24	1996-10-24 00:00:00	Thursday 	Thu
3904	19971215	1997-12-15	1997-12-15 00:00:00	Monday   	Mon
3905	19970416	1997-04-16	1997-04-16 00:00:00	Wednesday	Wed
3906	19970325	1997-03-25	1997-03-25 00:00:00	Tuesday  	Tue
3907	19980318	1998-03-18	1998-03-18 00:00:00	Wednesday	Wed
3908	19980218	1998-02-18	1998-02-18 00:00:00	Wednesday	Wed
3909	19970519	1997-05-19	1997-05-19 00:00:00	Monday   	Mon
3910	19980505	1998-05-05	1998-05-05 00:00:00	Tuesday  	Tue
3911	19971211	1997-12-11	1997-12-11 00:00:00	Thursday 	Thu
3912	19961010	1996-10-10	1996-10-10 00:00:00	Thursday 	Thu
3913	19970912	1997-09-12	1997-09-12 00:00:00	Friday   	Fri
3914	19961213	1996-12-13	1996-12-13 00:00:00	Friday   	Fri
3915	19971030	1997-10-30	1997-10-30 00:00:00	Thursday 	Thu
3916	19970620	1997-06-20	1997-06-20 00:00:00	Friday   	Fri
3917	19960710	1996-07-10	1996-07-10 00:00:00	Wednesday	Wed
3918	19971007	1997-10-07	1997-10-07 00:00:00	Tuesday  	Tue
3919	19970722	1997-07-22	1997-07-22 00:00:00	Tuesday  	Tue
3920	19971009	1997-10-09	1997-10-09 00:00:00	Thursday 	Thu
3921	19961204	1996-12-04	1996-12-04 00:00:00	Wednesday	Wed
3922	19961004	1996-10-04	1996-10-04 00:00:00	Friday   	Fri
3923	19961029	1996-10-29	1996-10-29 00:00:00	Tuesday  	Tue
3924	19970410	1997-04-10	1997-04-10 00:00:00	Thursday 	Thu
3925	19971016	1997-10-16	1997-10-16 00:00:00	Thursday 	Thu
3926	19970724	1997-07-24	1997-07-24 00:00:00	Thursday 	Thu
3927	19970529	1997-05-29	1997-05-29 00:00:00	Thursday 	Thu
3928	19960715	1996-07-15	1996-07-15 00:00:00	Monday   	Mon
3929	19960709	1996-07-09	1996-07-09 00:00:00	Tuesday  	Tue
3930	19970304	1997-03-04	1997-03-04 00:00:00	Tuesday  	Tue
3931	19971229	1997-12-29	1997-12-29 00:00:00	Monday   	Mon
3932	19970106	1997-01-06	1997-01-06 00:00:00	Monday   	Mon
3933	19980420	1998-04-20	1998-04-20 00:00:00	Monday   	Mon
3934	19960719	1996-07-19	1996-07-19 00:00:00	Friday   	Fri
3935	19980410	1998-04-10	1998-04-10 00:00:00	Friday   	Fri
3936	19980424	1998-04-24	1998-04-24 00:00:00	Friday   	Fri
3937	19970102	1997-01-02	1997-01-02 00:00:00	Thursday 	Thu
3938	19980302	1998-03-02	1998-03-02 00:00:00	Monday   	Mon
3939	19970721	1997-07-21	1997-07-21 00:00:00	Monday   	Mon
3940	19980306	1998-03-06	1998-03-06 00:00:00	Friday   	Fri
3941	19970616	1997-06-16	1997-06-16 00:00:00	Monday   	Mon
3942	19961022	1996-10-22	1996-10-22 00:00:00	Tuesday  	Tue
3943	19961120	1996-11-20	1996-11-20 00:00:00	Wednesday	Wed
3944	19961209	1996-12-09	1996-12-09 00:00:00	Monday   	Mon
3945	19980114	1998-01-14	1998-01-14 00:00:00	Wednesday	Wed
3946	19980107	1998-01-07	1998-01-07 00:00:00	Wednesday	Wed
3947	19970617	1997-06-17	1997-06-17 00:00:00	Tuesday  	Tue
3948	19970919	1997-09-19	1997-09-19 00:00:00	Friday   	Fri
3949	19980120	1998-01-20	1998-01-20 00:00:00	Tuesday  	Tue
3950	19960916	1996-09-16	1996-09-16 00:00:00	Monday   	Mon
3951	19971216	1997-12-16	1997-12-16 00:00:00	Tuesday  	Tue
3952	19970731	1997-07-31	1997-07-31 00:00:00	Thursday 	Thu
3953	19960822	1996-08-22	1996-08-22 00:00:00	Thursday 	Thu
3954	19970808	1997-08-08	1997-08-08 00:00:00	Friday   	Fri
3955	19961105	1996-11-05	1996-11-05 00:00:00	Tuesday  	Tue
3956	19970314	1997-03-14	1997-03-14 00:00:00	Friday   	Fri
3957	19960808	1996-08-08	1996-08-08 00:00:00	Thursday 	Thu
3958	19970814	1997-08-14	1997-08-14 00:00:00	Thursday 	Thu
3959	19960830	1996-08-30	1996-08-30 00:00:00	Friday   	Fri
3960	19960730	1996-07-30	1996-07-30 00:00:00	Tuesday  	Tue
3961	19960724	1996-07-24	1996-07-24 00:00:00	Wednesday	Wed
3962	19970130	1997-01-30	1997-01-30 00:00:00	Thursday 	Thu
3963	19980115	1998-01-15	1998-01-15 00:00:00	Thursday 	Thu
3964	19980202	1998-02-02	1998-02-02 00:00:00	Monday   	Mon
3965	19980102	1998-01-02	1998-01-02 00:00:00	Friday   	Fri
3966	19961225	1996-12-25	1996-12-25 00:00:00	Wednesday	Wed
3967	19961101	1996-11-01	1996-11-01 00:00:00	Friday   	Fri
3968	19970428	1997-04-28	1997-04-28 00:00:00	Monday   	Mon
3969	19960801	1996-08-01	1996-08-01 00:00:00	Thursday 	Thu
3970	19980227	1998-02-27	1998-02-27 00:00:00	Friday   	Fri
3971	19980105	1998-01-05	1998-01-05 00:00:00	Monday   	Mon
3972	19971015	1997-10-15	1997-10-15 00:00:00	Wednesday	Wed
3973	19961122	1996-11-22	1996-11-22 00:00:00	Friday   	Fri
3974	19980423	1998-04-23	1998-04-23 00:00:00	Thursday 	Thu
3975	19971208	1997-12-08	1997-12-08 00:00:00	Monday   	Mon
3976	19970402	1997-04-02	1997-04-02 00:00:00	Wednesday	Wed
3977	19970328	1997-03-28	1997-03-28 00:00:00	Friday   	Fri
3978	19970107	1997-01-07	1997-01-07 00:00:00	Tuesday  	Tue
3979	19980406	1998-04-06	1998-04-06 00:00:00	Monday   	Mon
3980	19970203	1997-02-03	1997-02-03 00:00:00	Monday   	Mon
3981	19970728	1997-07-28	1997-07-28 00:00:00	Monday   	Mon
3982	19960805	1996-08-05	1996-08-05 00:00:00	Monday   	Mon
3983	19970909	1997-09-09	1997-09-09 00:00:00	Tuesday  	Tue
3984	19970122	1997-01-22	1997-01-22 00:00:00	Wednesday	Wed
3985	19970219	1997-02-19	1997-02-19 00:00:00	Wednesday	Wed
3986	19970207	1997-02-07	1997-02-07 00:00:00	Friday   	Fri
3987	19970110	1997-01-10	1997-01-10 00:00:00	Friday   	Fri
3988	19970704	1997-07-04	1997-07-04 00:00:00	Friday   	Fri
3989	19970407	1997-04-07	1997-04-07 00:00:00	Monday   	Mon
3990	19970825	1997-08-25	1997-08-25 00:00:00	Monday   	Mon
3991	19980429	1998-04-29	1998-04-29 00:00:00	Wednesday	Wed
3992	19970404	1997-04-04	1997-04-04 00:00:00	Friday   	Fri
3993	19980109	1998-01-09	1998-01-09 00:00:00	Friday   	Fri
3994	19971002	1997-10-02	1997-10-02 00:00:00	Thursday 	Thu
3995	19971107	1997-11-07	1997-11-07 00:00:00	Friday   	Fri
3996	19960806	1996-08-06	1996-08-06 00:00:00	Tuesday  	Tue
3997	19970409	1997-04-09	1997-04-09 00:00:00	Wednesday	Wed
3998	19971105	1997-11-05	1997-11-05 00:00:00	Wednesday	Wed
3999	19971219	1997-12-19	1997-12-19 00:00:00	Friday   	Fri
4000	19970701	1997-07-01	1997-07-01 00:00:00	Tuesday  	Tue
4001	19970801	1997-08-01	1997-08-01 00:00:00	Friday   	Fri
4002	19980414	1998-04-14	1998-04-14 00:00:00	Tuesday  	Tue
4003	19980326	1998-03-26	1998-03-26 00:00:00	Thursday 	Thu
4004	19970718	1997-07-18	1997-07-18 00:00:00	Friday   	Fri
4005	19970618	1997-06-18	1997-06-18 00:00:00	Wednesday	Wed
4006	19971111	1997-11-11	1997-11-11 00:00:00	Tuesday  	Tue
4007	19970818	1997-08-18	1997-08-18 00:00:00	Monday   	Mon
4008	19961127	1996-11-27	1996-11-27 00:00:00	Wednesday	Wed
4009	19961211	1996-12-11	1996-12-11 00:00:00	Wednesday	Wed
4010	19970902	1997-09-02	1997-09-02 00:00:00	Tuesday  	Tue
4011	19970915	1997-09-15	1997-09-15 00:00:00	Monday   	Mon
4012	19960913	1996-09-13	1996-09-13 00:00:00	Friday   	Fri
4013	19971201	1997-12-01	1997-12-01 00:00:00	Monday   	Mon
4014	19961007	1996-10-07	1996-10-07 00:00:00	Monday   	Mon
4015	19980303	1998-03-03	1998-03-03 00:00:00	Tuesday  	Tue
4016	19960729	1996-07-29	1996-07-29 00:00:00	Monday   	Mon
4017	19971104	1997-11-04	1997-11-04 00:00:00	Tuesday  	Tue
4018	19971226	1997-12-26	1997-12-26 00:00:00	Friday   	Fri
4019	19980422	1998-04-22	1998-04-22 00:00:00	Wednesday	Wed
4020	19961220	1996-12-20	1996-12-20 00:00:00	Friday   	Fri
4021	19971001	1997-10-01	1997-10-01 00:00:00	Wednesday	Wed
4022	19970423	1997-04-23	1997-04-23 00:00:00	Wednesday	Wed
4023	19960927	1996-09-27	1996-09-27 00:00:00	Friday   	Fri
4024	19980427	1998-04-27	1998-04-27 00:00:00	Monday   	Mon
4025	19970221	1997-02-21	1997-02-21 00:00:00	Friday   	Fri
4026	19970917	1997-09-17	1997-09-17 00:00:00	Wednesday	Wed
4027	19960902	1996-09-02	1996-09-02 00:00:00	Monday   	Mon
4028	19980113	1998-01-13	1998-01-13 00:00:00	Tuesday  	Tue
4029	19970213	1997-02-13	1997-02-13 00:00:00	Thursday 	Thu
4030	19970603	1997-06-03	1997-06-03 00:00:00	Tuesday  	Tue
4031	19970408	1997-04-08	1997-04-08 00:00:00	Tuesday  	Tue
4032	19961023	1996-10-23	1996-10-23 00:00:00	Wednesday	Wed
4033	19970630	1997-06-30	1997-06-30 00:00:00	Monday   	Mon
4034	19961128	1996-11-28	1996-11-28 00:00:00	Thursday 	Thu
4035	19970523	1997-05-23	1997-05-23 00:00:00	Friday   	Fri
4036	19970418	1997-04-18	1997-04-18 00:00:00	Friday   	Fri
4037	19960816	1996-08-16	1996-08-16 00:00:00	Friday   	Fri
4038	19970220	1997-02-20	1997-02-20 00:00:00	Thursday 	Thu
4039	19960930	1996-09-30	1996-09-30 00:00:00	Monday   	Mon
4040	19970703	1997-07-03	1997-07-03 00:00:00	Thursday 	Thu
4041	19970312	1997-03-12	1997-03-12 00:00:00	Wednesday	Wed
4042	19980212	1998-02-12	1998-02-12 00:00:00	Thursday 	Thu
4043	19970820	1997-08-20	1997-08-20 00:00:00	Wednesday	Wed
4044	19961003	1996-10-03	1996-10-03 00:00:00	Thursday 	Thu
4045	19970507	1997-05-07	1997-05-07 00:00:00	Wednesday	Wed
4046	19971223	1997-12-23	1997-12-23 00:00:00	Tuesday  	Tue
4047	19970515	1997-05-15	1997-05-15 00:00:00	Thursday 	Thu
4048	19961205	1996-12-05	1996-12-05 00:00:00	Thursday 	Thu
4049	19980501	1998-05-01	1998-05-01 00:00:00	Friday   	Fri
4050	19970313	1997-03-13	1997-03-13 00:00:00	Thursday 	Thu
4051	19971120	1997-11-20	1997-11-20 00:00:00	Thursday 	Thu
4052	19960823	1996-08-23	1996-08-23 00:00:00	Friday   	Fri
4053	19980413	1998-04-13	1998-04-13 00:00:00	Monday   	Mon
4054	19970716	1997-07-16	1997-07-16 00:00:00	Wednesday	Wed
4055	19980128	1998-01-28	1998-01-28 00:00:00	Wednesday	Wed
4056	19970415	1997-04-15	1997-04-15 00:00:00	Tuesday  	Tue
4057	19961119	1996-11-19	1996-11-19 00:00:00	Tuesday  	Tue
4058	19971205	1997-12-05	1997-12-05 00:00:00	Friday   	Fri
4059	19971231	1997-12-31	1997-12-31 00:00:00	Wednesday	Wed
4060	19980205	1998-02-05	1998-02-05 00:00:00	Thursday 	Thu
\.


--
-- Data for Name: d_order_transaction; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_order_transaction (order_transaction_key, order_id, audit_key) FROM stdin;
6578	11013	\N
6579	11014	\N
6580	11015	\N
6581	11016	\N
6582	11017	\N
6583	11018	\N
6584	11019	\N
6585	11020	\N
6586	11021	\N
6587	11022	\N
6588	11023	\N
6589	11024	\N
6590	11025	\N
6591	11026	\N
6592	11027	\N
6593	11028	\N
6594	11029	\N
6595	11030	\N
6596	11031	\N
6597	11032	\N
6598	11033	\N
6599	11034	\N
6600	11035	\N
6601	11036	\N
6602	11037	\N
6603	11038	\N
6604	11039	\N
6605	11040	\N
6606	11041	\N
6607	11042	\N
6608	11043	\N
6609	11044	\N
6610	11045	\N
6611	11046	\N
6612	11047	\N
6613	11048	\N
6614	11049	\N
6615	11050	\N
6616	11051	\N
6617	11052	\N
6618	11053	\N
6619	11054	\N
6620	11055	\N
6621	11056	\N
6622	11057	\N
6623	11058	\N
6624	11059	\N
6625	11060	\N
6626	11061	\N
6627	11062	\N
6628	11063	\N
6629	11064	\N
6630	11065	\N
5813	10248	\N
5814	10249	\N
5815	10250	\N
5816	10251	\N
5817	10252	\N
5818	10253	\N
5819	10254	\N
5820	10255	\N
5821	10256	\N
5822	10257	\N
5823	10258	\N
5824	10259	\N
5825	10260	\N
5826	10261	\N
5827	10262	\N
5828	10263	\N
5829	10264	\N
5830	10265	\N
5831	10266	\N
5832	10267	\N
5833	10268	\N
5834	10269	\N
5835	10270	\N
5836	10271	\N
5837	10272	\N
5838	10273	\N
5839	10274	\N
5840	10275	\N
5841	10276	\N
5842	10277	\N
5843	10278	\N
5844	10279	\N
5845	10280	\N
5846	10281	\N
5847	10282	\N
5848	10283	\N
5849	10284	\N
5850	10285	\N
5851	10286	\N
5852	10287	\N
5853	10288	\N
5854	10289	\N
5855	10290	\N
5856	10291	\N
5857	10292	\N
5858	10293	\N
5859	10294	\N
5860	10295	\N
5861	10296	\N
5862	10297	\N
5863	10298	\N
5864	10299	\N
5865	10300	\N
5866	10301	\N
5867	10302	\N
5868	10303	\N
5869	10304	\N
5870	10305	\N
5871	10306	\N
5872	10307	\N
5873	10308	\N
5874	10309	\N
5875	10310	\N
5876	10311	\N
5877	10312	\N
5878	10313	\N
5879	10314	\N
5880	10315	\N
5881	10316	\N
5882	10317	\N
5883	10318	\N
5884	10319	\N
5885	10320	\N
5886	10321	\N
5887	10322	\N
5888	10323	\N
5889	10324	\N
5890	10325	\N
5891	10326	\N
5892	10327	\N
5893	10328	\N
5894	10329	\N
5895	10330	\N
5896	10331	\N
5897	10332	\N
5898	10333	\N
5899	10334	\N
5900	10335	\N
5901	10336	\N
5902	10337	\N
5903	10338	\N
5904	10339	\N
5905	10340	\N
5906	10341	\N
5907	10342	\N
5908	10343	\N
5909	10344	\N
5910	10345	\N
5911	10346	\N
5912	10347	\N
5913	10348	\N
5914	10349	\N
5915	10350	\N
5916	10351	\N
5917	10352	\N
5918	10353	\N
5919	10354	\N
5920	10355	\N
5921	10356	\N
5922	10357	\N
5923	10358	\N
5924	10359	\N
5925	10360	\N
5926	10361	\N
5927	10362	\N
5928	10363	\N
5929	10364	\N
5930	10365	\N
5931	10366	\N
5932	10367	\N
5933	10368	\N
5934	10369	\N
5935	10370	\N
5936	10371	\N
5937	10372	\N
5938	10373	\N
5939	10374	\N
5940	10375	\N
5941	10376	\N
5942	10377	\N
5943	10378	\N
5944	10379	\N
5945	10380	\N
5946	10381	\N
5947	10382	\N
5948	10383	\N
5949	10384	\N
5950	10385	\N
5951	10386	\N
5952	10387	\N
5953	10388	\N
5954	10389	\N
5955	10390	\N
5956	10391	\N
5957	10392	\N
5958	10393	\N
5959	10394	\N
5960	10395	\N
5961	10396	\N
5962	10397	\N
5963	10398	\N
5964	10399	\N
5965	10400	\N
5966	10401	\N
5967	10402	\N
5968	10403	\N
5969	10404	\N
5970	10405	\N
5971	10406	\N
5972	10407	\N
5973	10408	\N
5974	10409	\N
5975	10410	\N
5976	10411	\N
5977	10412	\N
5978	10413	\N
5979	10414	\N
5980	10415	\N
5981	10416	\N
5982	10417	\N
5983	10418	\N
5984	10419	\N
5985	10420	\N
5986	10421	\N
5987	10422	\N
5988	10423	\N
5989	10424	\N
5990	10425	\N
5991	10426	\N
5992	10427	\N
5993	10428	\N
5994	10429	\N
5995	10430	\N
5996	10431	\N
5997	10432	\N
5998	10433	\N
5999	10434	\N
6000	10435	\N
6001	10436	\N
6002	10437	\N
6003	10438	\N
6004	10439	\N
6005	10440	\N
6006	10441	\N
6007	10442	\N
6008	10443	\N
6009	10444	\N
6010	10445	\N
6011	10446	\N
6012	10447	\N
6013	10448	\N
6014	10449	\N
6015	10450	\N
6016	10451	\N
6017	10452	\N
6018	10453	\N
6019	10454	\N
6020	10455	\N
6021	10456	\N
6022	10457	\N
6023	10458	\N
6024	10459	\N
6025	10460	\N
6026	10461	\N
6027	10462	\N
6028	10463	\N
6029	10464	\N
6030	10465	\N
6031	10466	\N
6032	10467	\N
6033	10468	\N
6034	10469	\N
6035	10470	\N
6036	10471	\N
6037	10472	\N
6038	10473	\N
6039	10474	\N
6040	10475	\N
6041	10476	\N
6042	10477	\N
6043	10478	\N
6044	10479	\N
6045	10480	\N
6046	10481	\N
6047	10482	\N
6048	10483	\N
6049	10484	\N
6050	10485	\N
6051	10486	\N
6052	10487	\N
6053	10488	\N
6054	10489	\N
6055	10490	\N
6056	10491	\N
6057	10492	\N
6058	10493	\N
6059	10494	\N
6060	10495	\N
6061	10496	\N
6062	10497	\N
6063	10498	\N
6064	10499	\N
6065	10500	\N
6066	10501	\N
6067	10502	\N
6068	10503	\N
6069	10504	\N
6070	10505	\N
6071	10506	\N
6072	10507	\N
6073	10508	\N
6074	10509	\N
6075	10510	\N
6076	10511	\N
6077	10512	\N
6078	10513	\N
6079	10514	\N
6080	10515	\N
6081	10516	\N
6082	10517	\N
6083	10518	\N
6084	10519	\N
6085	10520	\N
6086	10521	\N
6087	10522	\N
6088	10523	\N
6089	10524	\N
6090	10525	\N
6091	10526	\N
6092	10527	\N
6093	10528	\N
6094	10529	\N
6095	10530	\N
6096	10531	\N
6097	10532	\N
6098	10533	\N
6099	10534	\N
6100	10535	\N
6101	10536	\N
6102	10537	\N
6103	10538	\N
6104	10539	\N
6105	10540	\N
6106	10541	\N
6107	10542	\N
6108	10543	\N
6109	10544	\N
6110	10545	\N
6111	10546	\N
6112	10547	\N
6113	10548	\N
6114	10549	\N
6115	10550	\N
6116	10551	\N
6117	10552	\N
6118	10553	\N
6119	10554	\N
6120	10555	\N
6121	10556	\N
6122	10557	\N
6123	10558	\N
6124	10559	\N
6125	10560	\N
6126	10561	\N
6127	10562	\N
6128	10563	\N
6129	10564	\N
6130	10565	\N
6131	10566	\N
6132	10567	\N
6133	10568	\N
6134	10569	\N
6135	10570	\N
6136	10571	\N
6137	10572	\N
6138	10573	\N
6139	10574	\N
6140	10575	\N
6141	10576	\N
6142	10577	\N
6143	10578	\N
6144	10579	\N
6145	10580	\N
6146	10581	\N
6147	10582	\N
6148	10583	\N
6149	10584	\N
6150	10585	\N
6151	10586	\N
6152	10587	\N
6153	10588	\N
6154	10589	\N
6155	10590	\N
6156	10591	\N
6157	10592	\N
6158	10593	\N
6159	10594	\N
6160	10595	\N
6161	10596	\N
6162	10597	\N
6163	10598	\N
6164	10599	\N
6165	10600	\N
6166	10601	\N
6167	10602	\N
6168	10603	\N
6169	10604	\N
6170	10605	\N
6171	10606	\N
6172	10607	\N
6173	10608	\N
6174	10609	\N
6175	10610	\N
6176	10611	\N
6177	10612	\N
6178	10613	\N
6179	10614	\N
6180	10615	\N
6181	10616	\N
6182	10617	\N
6183	10618	\N
6184	10619	\N
6185	10620	\N
6186	10621	\N
6187	10622	\N
6188	10623	\N
6189	10624	\N
6190	10625	\N
6191	10626	\N
6192	10627	\N
6193	10628	\N
6194	10629	\N
6195	10630	\N
6196	10631	\N
6197	10632	\N
6198	10633	\N
6199	10634	\N
6200	10635	\N
6201	10636	\N
6202	10637	\N
6203	10638	\N
6204	10639	\N
6205	10640	\N
6206	10641	\N
6207	10642	\N
6208	10643	\N
6209	10644	\N
6210	10645	\N
6211	10646	\N
6212	10647	\N
6213	10648	\N
6214	10649	\N
6215	10650	\N
6216	10651	\N
6217	10652	\N
6218	10653	\N
6219	10654	\N
6220	10655	\N
6221	10656	\N
6222	10657	\N
6223	10658	\N
6224	10659	\N
6225	10660	\N
6226	10661	\N
6227	10662	\N
6228	10663	\N
6229	10664	\N
6230	10665	\N
6231	10666	\N
6232	10667	\N
6233	10668	\N
6234	10669	\N
6235	10670	\N
6236	10671	\N
6237	10672	\N
6238	10673	\N
6239	10674	\N
6240	10675	\N
6241	10676	\N
6242	10677	\N
6243	10678	\N
6244	10679	\N
6245	10680	\N
6246	10681	\N
6247	10682	\N
6248	10683	\N
6249	10684	\N
6250	10685	\N
6251	10686	\N
6252	10687	\N
6253	10688	\N
6254	10689	\N
6255	10690	\N
6256	10691	\N
6257	10692	\N
6258	10693	\N
6259	10694	\N
6260	10695	\N
6261	10696	\N
6262	10697	\N
6263	10698	\N
6264	10699	\N
6265	10700	\N
6266	10701	\N
6267	10702	\N
6268	10703	\N
6269	10704	\N
6270	10705	\N
6271	10706	\N
6272	10707	\N
6273	10708	\N
6274	10709	\N
6275	10710	\N
6276	10711	\N
6277	10712	\N
6278	10713	\N
6279	10714	\N
6280	10715	\N
6281	10716	\N
6282	10717	\N
6283	10718	\N
6284	10719	\N
6285	10720	\N
6286	10721	\N
6287	10722	\N
6288	10723	\N
6289	10724	\N
6290	10725	\N
6291	10726	\N
6292	10727	\N
6293	10728	\N
6294	10729	\N
6295	10730	\N
6296	10731	\N
6297	10732	\N
6298	10733	\N
6299	10734	\N
6300	10735	\N
6301	10736	\N
6302	10737	\N
6303	10738	\N
6304	10739	\N
6305	10740	\N
6306	10741	\N
6307	10742	\N
6308	10743	\N
6309	10744	\N
6310	10745	\N
6311	10746	\N
6312	10747	\N
6313	10748	\N
6314	10749	\N
6315	10750	\N
6316	10751	\N
6317	10752	\N
6318	10753	\N
6319	10754	\N
6320	10755	\N
6321	10756	\N
6322	10757	\N
6323	10758	\N
6324	10759	\N
6325	10760	\N
6326	10761	\N
6327	10762	\N
6328	10763	\N
6329	10764	\N
6330	10765	\N
6331	10766	\N
6332	10767	\N
6333	10768	\N
6334	10769	\N
6335	10770	\N
6336	10771	\N
6337	10772	\N
6338	10773	\N
6339	10774	\N
6340	10775	\N
6341	10776	\N
6342	10777	\N
6343	10778	\N
6344	10779	\N
6345	10780	\N
6346	10781	\N
6347	10782	\N
6348	10783	\N
6349	10784	\N
6350	10785	\N
6351	10786	\N
6352	10787	\N
6353	10788	\N
6354	10789	\N
6355	10790	\N
6356	10791	\N
6357	10792	\N
6358	10793	\N
6359	10794	\N
6360	10795	\N
6361	10796	\N
6362	10797	\N
6363	10798	\N
6364	10799	\N
6365	10800	\N
6366	10801	\N
6367	10802	\N
6368	10803	\N
6369	10804	\N
6370	10805	\N
6371	10806	\N
6372	10807	\N
6373	10808	\N
6374	10809	\N
6375	10810	\N
6376	10811	\N
6377	10812	\N
6378	10813	\N
6379	10814	\N
6380	10815	\N
6381	10816	\N
6382	10817	\N
6383	10818	\N
6384	10819	\N
6385	10820	\N
6386	10821	\N
6387	10822	\N
6388	10823	\N
6389	10824	\N
6390	10825	\N
6391	10826	\N
6392	10827	\N
6393	10828	\N
6394	10829	\N
6395	10830	\N
6396	10831	\N
6397	10832	\N
6398	10833	\N
6399	10834	\N
6400	10835	\N
6401	10836	\N
6402	10837	\N
6403	10838	\N
6404	10839	\N
6405	10840	\N
6406	10841	\N
6407	10842	\N
6408	10843	\N
6409	10844	\N
6410	10845	\N
6411	10846	\N
6412	10847	\N
6413	10848	\N
6414	10849	\N
6415	10850	\N
6416	10851	\N
6417	10852	\N
6418	10853	\N
6419	10854	\N
6420	10855	\N
6421	10856	\N
6422	10857	\N
6423	10858	\N
6424	10859	\N
6425	10860	\N
6426	10861	\N
6427	10862	\N
6428	10863	\N
6429	10864	\N
6430	10865	\N
6431	10866	\N
6432	10867	\N
6433	10868	\N
6434	10869	\N
6435	10870	\N
6436	10871	\N
6437	10872	\N
6438	10873	\N
6439	10874	\N
6440	10875	\N
6441	10876	\N
6442	10877	\N
6443	10878	\N
6444	10879	\N
6445	10880	\N
6446	10881	\N
6447	10882	\N
6448	10883	\N
6449	10884	\N
6450	10885	\N
6451	10886	\N
6452	10887	\N
6453	10888	\N
6454	10889	\N
6455	10890	\N
6456	10891	\N
6457	10892	\N
6458	10893	\N
6459	10894	\N
6460	10895	\N
6461	10896	\N
6462	10897	\N
6463	10898	\N
6464	10899	\N
6465	10900	\N
6466	10901	\N
6467	10902	\N
6468	10903	\N
6469	10904	\N
6470	10905	\N
6471	10906	\N
6472	10907	\N
6473	10908	\N
6474	10909	\N
6475	10910	\N
6476	10911	\N
6477	10912	\N
6478	10913	\N
6479	10914	\N
6480	10915	\N
6481	10916	\N
6482	10917	\N
6483	10918	\N
6484	10919	\N
6485	10920	\N
6486	10921	\N
6487	10922	\N
6488	10923	\N
6489	10924	\N
6490	10925	\N
6491	10926	\N
6492	10927	\N
6493	10928	\N
6494	10929	\N
6495	10930	\N
6496	10931	\N
6497	10932	\N
6498	10933	\N
6499	10934	\N
6500	10935	\N
6501	10936	\N
6502	10937	\N
6503	10938	\N
6504	10939	\N
6505	10940	\N
6506	10941	\N
6507	10942	\N
6508	10943	\N
6509	10944	\N
6510	10945	\N
6511	10946	\N
6512	10947	\N
6513	10948	\N
6514	10949	\N
6515	10950	\N
6516	10951	\N
6517	10952	\N
6518	10953	\N
6519	10954	\N
6520	10955	\N
6521	10956	\N
6522	10957	\N
6523	10958	\N
6524	10959	\N
6525	10960	\N
6526	10961	\N
6527	10962	\N
6528	10963	\N
6529	10964	\N
6530	10965	\N
6531	10966	\N
6532	10967	\N
6533	10968	\N
6534	10969	\N
6535	10970	\N
6536	10971	\N
6537	10972	\N
6538	10973	\N
6539	10974	\N
6540	10975	\N
6541	10976	\N
6542	10977	\N
6543	10978	\N
6544	10979	\N
6545	10980	\N
6546	10981	\N
6547	10982	\N
6548	10983	\N
6549	10984	\N
6550	10985	\N
6551	10986	\N
6552	10987	\N
6553	10988	\N
6554	10989	\N
6555	10990	\N
6556	10991	\N
6557	10992	\N
6558	10993	\N
6559	10994	\N
6560	10995	\N
6561	10996	\N
6562	10997	\N
6563	10998	\N
6564	10999	\N
6565	11000	\N
6566	11001	\N
6567	11002	\N
6568	11003	\N
6569	11004	\N
6570	11005	\N
6571	11006	\N
6572	11007	\N
6573	11008	\N
6574	11009	\N
6575	11010	\N
6576	11011	\N
6577	11012	\N
6631	11066	\N
6632	11067	\N
6633	11068	\N
6634	11069	\N
6635	11070	\N
6636	11071	\N
6637	11072	\N
6638	11073	\N
6639	11074	\N
6640	11075	\N
6641	11076	\N
6642	11077	\N
\.


--
-- Data for Name: d_product; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_product (product_key, product_id, product_name, product_category_id, product_category_name, product_quantity_per_unit, product_standard_unit_price, product_reorder_level, product_discontinued_ind, supplier_id, supplier_company_name, supplier_contact_title, supplier_street_address, supplier_phone, supplier_fax, supplier_home_page, supplier_postal_code_id, supplier_region_name, supplier_city_name, supplier_country_code, supplier_country_name, current_row_ind, effective_date, audit_key) FROM stdin;
1002	1	Chai	1	Beverages	10 boxes x 20 bags	18.00	10	f	1	Exotic Liquids	Purchasing Manager	49 Gilbert St.	(171) 555-2222	\N	\N	EC1 4SD	\N	London	\N	UK	\N	\N	\N
1003	2	Chang	1	Beverages	24 - 12 oz bottles	19.00	25	f	1	Exotic Liquids	Purchasing Manager	49 Gilbert St.	(171) 555-2222	\N	\N	EC1 4SD	\N	London	\N	UK	\N	\N	\N
1004	3	Aniseed Syrup	2	Condiments	12 - 550 ml bottles	10.00	25	f	1	Exotic Liquids	Purchasing Manager	49 Gilbert St.	(171) 555-2222	\N	\N	EC1 4SD	\N	London	\N	UK	\N	\N	\N
1005	4	Chef Anton's Cajun Seasoning	2	Condiments	48 - 6 oz jars	22.00	0	f	2	New Orleans Cajun Delights	Order Administrator	P.O. Box 78934	(100) 555-4822	\N	\N	70117	LA	New Orleans	\N	USA	\N	\N	\N
1006	5	Chef Anton's Gumbo Mix	2	Condiments	36 boxes	21.35	0	t	2	New Orleans Cajun Delights	Order Administrator	P.O. Box 78934	(100) 555-4822	\N	\N	70117	LA	New Orleans	\N	USA	\N	\N	\N
1007	6	Grandma's Boysenberry Spread	2	Condiments	12 - 8 oz jars	25.00	25	f	3	Grandma Kelly's Homestead	Sales Representative	707 Oxford Rd.	(313) 555-5735	(313) 555-3349	\N	48104	MI	Ann Arbor	\N	USA	\N	\N	\N
1008	7	Uncle Bob's Organic Dried Pears	7	Produce	12 - 1 lb pkgs.	30.00	10	f	3	Grandma Kelly's Homestead	Sales Representative	707 Oxford Rd.	(313) 555-5735	(313) 555-3349	\N	48104	MI	Ann Arbor	\N	USA	\N	\N	\N
1009	8	Northwoods Cranberry Sauce	2	Condiments	12 - 12 oz jars	40.00	0	f	3	Grandma Kelly's Homestead	Sales Representative	707 Oxford Rd.	(313) 555-5735	(313) 555-3349	\N	48104	MI	Ann Arbor	\N	USA	\N	\N	\N
1010	9	Mishi Kobe Niku	6	Meat/Poultry	18 - 500 g pkgs.	97.00	0	t	4	Tokyo Traders	Marketing Manager	9-8 Sekimai\nMusashino-shi	(03) 3555-5011	\N	\N	100	\N	Tokyo	\N	Japan	\N	\N	\N
1011	10	Ikura	8	Seafood	12 - 200 ml jars	31.00	0	f	4	Tokyo Traders	Marketing Manager	9-8 Sekimai\nMusashino-shi	(03) 3555-5011	\N	\N	100	\N	Tokyo	\N	Japan	\N	\N	\N
1012	11	Queso Cabrales	4	Dairy Products	1 kg pkg.	21.00	30	f	5	Cooperativa de Quesos 'Las Cabras'	Export Administrator	Calle del Rosal 4	(98) 598 76 54	\N	\N	33007	Asturias	Oviedo	\N	Spain	\N	\N	\N
1013	12	Queso Manchego La Pastora	4	Dairy Products	10 - 500 g pkgs.	38.00	0	f	5	Cooperativa de Quesos 'Las Cabras'	Export Administrator	Calle del Rosal 4	(98) 598 76 54	\N	\N	33007	Asturias	Oviedo	\N	Spain	\N	\N	\N
1014	13	Konbu	8	Seafood	2 kg box	6.00	5	f	6	Mayumi's	Marketing Representative	92 Setsuko\nChuo-ku	(06) 431-7877	\N	Mayumi's (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/mayumi.htm#	545	\N	Osaka	\N	Japan	\N	\N	\N
1015	14	Tofu	7	Produce	40 - 100 g pkgs.	23.25	0	f	6	Mayumi's	Marketing Representative	92 Setsuko\nChuo-ku	(06) 431-7877	\N	Mayumi's (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/mayumi.htm#	545	\N	Osaka	\N	Japan	\N	\N	\N
1016	15	Genen Shouyu	2	Condiments	24 - 250 ml bottles	15.50	5	f	6	Mayumi's	Marketing Representative	92 Setsuko\nChuo-ku	(06) 431-7877	\N	Mayumi's (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/mayumi.htm#	545	\N	Osaka	\N	Japan	\N	\N	\N
1017	16	Pavlova	3	Confections	32 - 500 g boxes	17.45	10	f	7	Pavlova, Ltd.	Marketing Manager	74 Rose St.\nMoonie Ponds	(03) 444-2343	(03) 444-6588	\N	3058	Victoria	Melbourne	\N	Australia	\N	\N	\N
1018	17	Alice Mutton	6	Meat/Poultry	20 - 1 kg tins	39.00	0	t	7	Pavlova, Ltd.	Marketing Manager	74 Rose St.\nMoonie Ponds	(03) 444-2343	(03) 444-6588	\N	3058	Victoria	Melbourne	\N	Australia	\N	\N	\N
1019	18	Carnarvon Tigers	8	Seafood	16 kg pkg.	62.50	0	f	7	Pavlova, Ltd.	Marketing Manager	74 Rose St.\nMoonie Ponds	(03) 444-2343	(03) 444-6588	\N	3058	Victoria	Melbourne	\N	Australia	\N	\N	\N
1020	19	Teatime Chocolate Biscuits	3	Confections	10 boxes x 12 pieces	9.20	5	f	8	Specialty Biscuits, Ltd.	Sales Representative	29 King's Way	(161) 555-4448	\N	\N	M14 GSD	\N	Manchester	\N	UK	\N	\N	\N
1021	20	Sir Rodney's Marmalade	3	Confections	30 gift boxes	81.00	0	f	8	Specialty Biscuits, Ltd.	Sales Representative	29 King's Way	(161) 555-4448	\N	\N	M14 GSD	\N	Manchester	\N	UK	\N	\N	\N
1022	21	Sir Rodney's Scones	3	Confections	24 pkgs. x 4 pieces	10.00	5	f	8	Specialty Biscuits, Ltd.	Sales Representative	29 King's Way	(161) 555-4448	\N	\N	M14 GSD	\N	Manchester	\N	UK	\N	\N	\N
1023	22	Gustaf's Knäckebröd	5	Grains/Cereals	24 - 500 g pkgs.	21.00	25	f	9	PB Knäckebröd AB	Sales Agent	Kaloadagatan 13	031-987 65 43	031-987 65 91	\N	S-345 67	\N	Göteborg	\N	Sweden 	\N	\N	\N
1024	23	Tunnbröd	5	Grains/Cereals	12 - 250 g pkgs.	9.00	25	f	9	PB Knäckebröd AB	Sales Agent	Kaloadagatan 13	031-987 65 43	031-987 65 91	\N	S-345 67	\N	Göteborg	\N	Sweden 	\N	\N	\N
1025	24	Guaraná Fantástica	1	Beverages	12 - 355 ml cans	4.50	0	t	10	Refrescos Americanas LTDA	Marketing Manager	Av. das Americanas 12.890	(11) 555 4640	\N	\N	5442	\N	São Paulo	\N	Brazil	\N	\N	\N
1026	25	NuNuCa Nuß-Nougat-Creme	3	Confections	20 - 450 g glasses	14.00	30	f	11	Heli Süßwaren GmbH & Co. KG	Sales Manager	Tiergartenstraße 5	(010) 9984510	\N	\N	10785	\N	Berlin	\N	Germany	\N	\N	\N
1027	26	Gumbär Gummibärchen	3	Confections	100 - 250 g bags	31.23	0	f	11	Heli Süßwaren GmbH & Co. KG	Sales Manager	Tiergartenstraße 5	(010) 9984510	\N	\N	10785	\N	Berlin	\N	Germany	\N	\N	\N
1028	27	Schoggi Schokolade	3	Confections	100 - 100 g pieces	43.90	30	f	11	Heli Süßwaren GmbH & Co. KG	Sales Manager	Tiergartenstraße 5	(010) 9984510	\N	\N	10785	\N	Berlin	\N	Germany	\N	\N	\N
1029	28	Rössle Sauerkraut	7	Produce	25 - 825 g cans	45.60	0	t	12	Plutzer Lebensmittelgroßmärkte AG	International Marketing Mgr.	Bogenallee 51	(069) 992755	\N	Plutzer (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/plutzer.htm#	60439	\N	Frankfurt	\N	Germany	\N	\N	\N
1030	29	Thüringer Rostbratwurst	6	Meat/Poultry	50 bags x 30 sausgs.	123.79	0	t	12	Plutzer Lebensmittelgroßmärkte AG	International Marketing Mgr.	Bogenallee 51	(069) 992755	\N	Plutzer (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/plutzer.htm#	60439	\N	Frankfurt	\N	Germany	\N	\N	\N
1031	30	Nord-Ost Matjeshering	8	Seafood	10 - 200 g glasses	25.89	15	f	13	Nord-Ost-Fisch Handelsgesellschaft mbH	Coordinator Foreign Markets	Frahmredder 112a	(04721) 8713	(04721) 8714	\N	27478	\N	Cuxhaven	\N	Germany	\N	\N	\N
1032	31	Gorgonzola Telino	4	Dairy Products	12 - 100 g pkgs	12.50	20	f	14	Formaggi Fortini s.r.l.	Sales Representative	Viale Dante, 75	(0544) 60323	(0544) 60603	\N	48100	\N	Ravenna	\N	Italy	\N	\N	\N
1033	32	Mascarpone Fabioli	4	Dairy Products	24 - 200 g pkgs.	32.00	25	f	14	Formaggi Fortini s.r.l.	Sales Representative	Viale Dante, 75	(0544) 60323	(0544) 60603	\N	48100	\N	Ravenna	\N	Italy	\N	\N	\N
1034	33	Geitost	4	Dairy Products	500 g	2.50	20	f	15	Norske Meierier	Marketing Manager	Hatlevegen 5	(0)2-953010	\N	\N	1320	\N	Sandvika	\N	Norway	\N	\N	\N
1035	34	Sasquatch Ale	1	Beverages	24 - 12 oz bottles	14.00	15	f	16	Bigfoot Breweries	Regional Account Rep.	3400 - 8th Avenue\nSuite 210	(503) 555-9931	\N	\N	97101	OR	Bend	\N	USA	\N	\N	\N
1036	35	Steeleye Stout	1	Beverages	24 - 12 oz bottles	18.00	15	f	16	Bigfoot Breweries	Regional Account Rep.	3400 - 8th Avenue\nSuite 210	(503) 555-9931	\N	\N	97101	OR	Bend	\N	USA	\N	\N	\N
1037	36	Inlagd Sill	8	Seafood	24 - 250 g  jars	19.00	20	f	17	Svensk Sjöföda AB	Sales Representative	Brovallavägen 231	08-123 45 67	\N	\N	S-123 45	\N	Stockholm	\N	Sweden	\N	\N	\N
1038	37	Gravad lax	8	Seafood	12 - 500 g pkgs.	26.00	25	f	17	Svensk Sjöföda AB	Sales Representative	Brovallavägen 231	08-123 45 67	\N	\N	S-123 45	\N	Stockholm	\N	Sweden	\N	\N	\N
1039	38	Côte de Blaye	1	Beverages	12 - 75 cl bottles	263.50	15	f	18	Aux joyeux ecclésiastiques	Sales Manager	203, Rue des Francs-Bourgeois	(1) 03.83.00.68	(1) 03.83.00.62	\N	75004	\N	Paris	\N	France	\N	\N	\N
1040	39	Chartreuse verte	1	Beverages	750 cc per bottle	18.00	5	f	18	Aux joyeux ecclésiastiques	Sales Manager	203, Rue des Francs-Bourgeois	(1) 03.83.00.68	(1) 03.83.00.62	\N	75004	\N	Paris	\N	France	\N	\N	\N
1041	40	Boston Crab Meat	8	Seafood	24 - 4 oz tins	18.40	30	f	19	New England Seafood Cannery	Wholesale Account Agent	Order Processing Dept.\n2100 Paul Revere Blvd.	(617) 555-3267	(617) 555-3389	\N	02134	MA	Boston	\N	USA	\N	\N	\N
1042	41	Jack's New England Clam Chowder	8	Seafood	12 - 12 oz cans	9.65	10	f	19	New England Seafood Cannery	Wholesale Account Agent	Order Processing Dept.\n2100 Paul Revere Blvd.	(617) 555-3267	(617) 555-3389	\N	02134	MA	Boston	\N	USA	\N	\N	\N
1043	42	Singaporean Hokkien Fried Mee	5	Grains/Cereals	32 - 1 kg pkgs.	14.00	0	t	20	Leka Trading	Owner	471 Serangoon Loop, Suite #402	555-8787	\N	\N	0512	\N	Singapore	\N	Singapore	\N	\N	\N
1044	43	Ipoh Coffee	1	Beverages	16 - 500 g tins	46.00	25	f	20	Leka Trading	Owner	471 Serangoon Loop, Suite #402	555-8787	\N	\N	0512	\N	Singapore	\N	Singapore	\N	\N	\N
1045	44	Gula Malacca	2	Condiments	20 - 2 kg bags	19.45	15	f	20	Leka Trading	Owner	471 Serangoon Loop, Suite #402	555-8787	\N	\N	0512	\N	Singapore	\N	Singapore	\N	\N	\N
1046	45	Røgede sild	8	Seafood	1k pkg.	9.50	15	f	21	Lyngbysild	Sales Manager	Lyngbysild\nFiskebakken 10	43844108	43844115	\N	2800	\N	Lyngby	\N	Denmark	\N	\N	\N
1047	46	Spegesild	8	Seafood	4 - 450 g glasses	12.00	0	f	21	Lyngbysild	Sales Manager	Lyngbysild\nFiskebakken 10	43844108	43844115	\N	2800	\N	Lyngby	\N	Denmark	\N	\N	\N
1048	47	Zaanse koeken	3	Confections	10 - 4 oz boxes	9.50	0	f	22	Zaanse Snoepfabriek	Accounting Manager	Verkoop\nRijnweg 22	(12345) 1212	(12345) 1210	\N	9999 ZZ	\N	Zaandam	\N	Netherlands	\N	\N	\N
1049	48	Chocolade	3	Confections	10 pkgs.	12.75	25	f	22	Zaanse Snoepfabriek	Accounting Manager	Verkoop\nRijnweg 22	(12345) 1212	(12345) 1210	\N	9999 ZZ	\N	Zaandam	\N	Netherlands	\N	\N	\N
1050	49	Maxilaku	3	Confections	24 - 50 g pkgs.	20.00	15	f	23	Karkki Oy	Product Manager	Valtakatu 12	(953) 10956	\N	\N	53120	\N	Lappeenranta	\N	Finland	\N	\N	\N
1051	50	Valkoinen suklaa	3	Confections	12 - 100 g bars	16.25	30	f	23	Karkki Oy	Product Manager	Valtakatu 12	(953) 10956	\N	\N	53120	\N	Lappeenranta	\N	Finland	\N	\N	\N
1052	51	Manjimup Dried Apples	7	Produce	50 - 300 g pkgs.	53.00	10	f	24	G'day, Mate	Sales Representative	170 Prince Edward Parade\nHunter's Hill	(02) 555-5914	(02) 555-4873	G'day Mate (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/gdaymate.htm#	2042	NSW	Sydney	\N	Australia	\N	\N	\N
1053	52	Filo Mix	5	Grains/Cereals	16 - 2 kg boxes	7.00	25	f	24	G'day, Mate	Sales Representative	170 Prince Edward Parade\nHunter's Hill	(02) 555-5914	(02) 555-4873	G'day Mate (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/gdaymate.htm#	2042	NSW	Sydney	\N	Australia	\N	\N	\N
1054	53	Perth Pasties	6	Meat/Poultry	48 pieces	32.80	0	t	24	G'day, Mate	Sales Representative	170 Prince Edward Parade\nHunter's Hill	(02) 555-5914	(02) 555-4873	G'day Mate (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/gdaymate.htm#	2042	NSW	Sydney	\N	Australia	\N	\N	\N
1055	54	Tourtière	6	Meat/Poultry	16 pies	7.45	10	f	25	Ma Maison	Marketing Manager	2960 Rue St. Laurent	(514) 555-9022	\N	\N	H1J 1C3	Québec	Montréal	\N	Canada	\N	\N	\N
1056	55	Pâté chinois	6	Meat/Poultry	24 boxes x 2 pies	24.00	20	f	25	Ma Maison	Marketing Manager	2960 Rue St. Laurent	(514) 555-9022	\N	\N	H1J 1C3	Québec	Montréal	\N	Canada	\N	\N	\N
1057	56	Gnocchi di nonna Alice	5	Grains/Cereals	24 - 250 g pkgs.	38.00	30	f	26	Pasta Buttini s.r.l.	Order Administrator	Via dei Gelsomini, 153	(089) 6547665	(089) 6547667	\N	84100	\N	Salerno	\N	Italy	\N	\N	\N
1058	57	Ravioli Angelo	5	Grains/Cereals	24 - 250 g pkgs.	19.50	20	f	26	Pasta Buttini s.r.l.	Order Administrator	Via dei Gelsomini, 153	(089) 6547665	(089) 6547667	\N	84100	\N	Salerno	\N	Italy	\N	\N	\N
1059	58	Escargots de Bourgogne	8	Seafood	24 pieces	13.25	20	f	27	Escargots Nouveaux	Sales Manager	22, rue H. Voiron	85.57.00.07	\N	\N	71300	\N	Montceau	\N	France	\N	\N	\N
1060	59	Raclette Courdavault	4	Dairy Products	5 kg pkg.	55.00	0	f	28	Gai pâturage	Sales Representative	Bat. B\n3, rue des Alpes	38.76.98.06	38.76.98.58	\N	74000	\N	Annecy	\N	France	\N	\N	\N
1061	60	Camembert Pierrot	4	Dairy Products	15 - 300 g rounds	34.00	0	f	28	Gai pâturage	Sales Representative	Bat. B\n3, rue des Alpes	38.76.98.06	38.76.98.58	\N	74000	\N	Annecy	\N	France	\N	\N	\N
1062	61	Sirop d'érable	2	Condiments	24 - 500 ml bottles	28.50	25	f	29	Forêts d'érables	Accounting Manager	148 rue Chasseur	(514) 555-2955	(514) 555-2921	\N	J2S 7S8	Québec	Ste-Hyacinthe	\N	Canada	\N	\N	\N
1063	62	Tarte au sucre	3	Confections	48 pies	49.30	0	f	29	Forêts d'érables	Accounting Manager	148 rue Chasseur	(514) 555-2955	(514) 555-2921	\N	J2S 7S8	Québec	Ste-Hyacinthe	\N	Canada	\N	\N	\N
1064	63	Vegie-spread	2	Condiments	15 - 625 g jars	43.90	5	f	7	Pavlova, Ltd.	Marketing Manager	74 Rose St.\nMoonie Ponds	(03) 444-2343	(03) 444-6588	\N	3058	Victoria	Melbourne	\N	Australia	\N	\N	\N
1065	64	Wimmers gute Semmelknödel	5	Grains/Cereals	20 bags x 4 pieces	33.25	30	f	12	Plutzer Lebensmittelgroßmärkte AG	International Marketing Mgr.	Bogenallee 51	(069) 992755	\N	Plutzer (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/plutzer.htm#	60439	\N	Frankfurt	\N	Germany	\N	\N	\N
1066	65	Louisiana Fiery Hot Pepper Sauce	2	Condiments	32 - 8 oz bottles	21.05	0	f	2	New Orleans Cajun Delights	Order Administrator	P.O. Box 78934	(100) 555-4822	\N	\N	70117	LA	New Orleans	\N	USA	\N	\N	\N
1067	66	Louisiana Hot Spiced Okra	2	Condiments	24 - 8 oz jars	17.00	20	f	2	New Orleans Cajun Delights	Order Administrator	P.O. Box 78934	(100) 555-4822	\N	\N	70117	LA	New Orleans	\N	USA	\N	\N	\N
1068	67	Laughing Lumberjack Lager	1	Beverages	24 - 12 oz bottles	14.00	10	f	16	Bigfoot Breweries	Regional Account Rep.	3400 - 8th Avenue\nSuite 210	(503) 555-9931	\N	\N	97101	OR	Bend	\N	USA	\N	\N	\N
1069	68	Scottish Longbreads	3	Confections	10 boxes x 8 pieces	12.50	15	f	8	Specialty Biscuits, Ltd.	Sales Representative	29 King's Way	(161) 555-4448	\N	\N	M14 GSD	\N	Manchester	\N	UK	\N	\N	\N
1070	69	Gudbrandsdalsost	4	Dairy Products	10 kg pkg.	36.00	15	f	15	Norske Meierier	Marketing Manager	Hatlevegen 5	(0)2-953010	\N	\N	1320	\N	Sandvika	\N	Norway	\N	\N	\N
1071	70	Outback Lager	1	Beverages	24 - 355 ml bottles	15.00	30	f	7	Pavlova, Ltd.	Marketing Manager	74 Rose St.\nMoonie Ponds	(03) 444-2343	(03) 444-6588	\N	3058	Victoria	Melbourne	\N	Australia	\N	\N	\N
1072	71	Fløtemysost	4	Dairy Products	10 - 500 g pkgs.	21.50	0	f	15	Norske Meierier	Marketing Manager	Hatlevegen 5	(0)2-953010	\N	\N	1320	\N	Sandvika	\N	Norway	\N	\N	\N
1073	72	Mozzarella di Giovanni	4	Dairy Products	24 - 200 g pkgs.	34.80	0	f	14	Formaggi Fortini s.r.l.	Sales Representative	Viale Dante, 75	(0544) 60323	(0544) 60603	\N	48100	\N	Ravenna	\N	Italy	\N	\N	\N
1074	73	Röd Kaviar	8	Seafood	24 - 150 g jars	15.00	5	f	17	Svensk Sjöföda AB	Sales Representative	Brovallavägen 231	08-123 45 67	\N	\N	S-123 45	\N	Stockholm	\N	Sweden	\N	\N	\N
1075	74	Longlife Tofu	7	Produce	5 kg pkg.	10.00	5	f	4	Tokyo Traders	Marketing Manager	9-8 Sekimai\nMusashino-shi	(03) 3555-5011	\N	\N	100	\N	Tokyo	\N	Japan	\N	\N	\N
1076	75	Rhönbräu Klosterbier	1	Beverages	24 - 0.5 l bottles	7.75	25	f	12	Plutzer Lebensmittelgroßmärkte AG	International Marketing Mgr.	Bogenallee 51	(069) 992755	\N	Plutzer (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/plutzer.htm#	60439	\N	Frankfurt	\N	Germany	\N	\N	\N
1077	76	Lakkalikööri	1	Beverages	500 ml 	18.00	20	f	23	Karkki Oy	Product Manager	Valtakatu 12	(953) 10956	\N	\N	53120	\N	Lappeenranta	\N	Finland	\N	\N	\N
1078	77	Original Frankfurter grüne Soße	2	Condiments	12 boxes	13.00	15	f	12	Plutzer Lebensmittelgroßmärkte AG	International Marketing Mgr.	Bogenallee 51	(069) 992755	\N	Plutzer (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/plutzer.htm#	60439	\N	Frankfurt	\N	Germany	\N	\N	\N
\.


--
-- Data for Name: d_required_date; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_required_date (required_date_key, required_date_id_yyyymmdd, "Required_Day_Date_yyyy-mm-dd", required_full_date, required_day_of_week_name, requried_day_of_week_abbrev) FROM stdin;
2647	19960927	1996-09-27	1996-09-27 00:00:00	Friday   	Fri
2648	19980427	1998-04-27	1998-04-27 00:00:00	Monday   	Mon
2649	19971114	1997-11-14	1997-11-14 00:00:00	Friday   	Fri
2650	19970221	1997-02-21	1997-02-21 00:00:00	Friday   	Fri
2651	19970917	1997-09-17	1997-09-17 00:00:00	Wednesday	Wed
2652	19960902	1996-09-02	1996-09-02 00:00:00	Monday   	Mon
2653	19980430	1998-04-30	1998-04-30 00:00:00	Thursday 	Thu
2654	19980113	1998-01-13	1998-01-13 00:00:00	Tuesday  	Tue
2655	19970619	1997-06-19	1997-06-19 00:00:00	Thursday 	Thu
2656	19970213	1997-02-13	1997-02-13 00:00:00	Thursday 	Thu
2657	19980123	1998-01-23	1998-01-23 00:00:00	Friday   	Fri
2658	19970603	1997-06-03	1997-06-03 00:00:00	Tuesday  	Tue
2659	19970408	1997-04-08	1997-04-08 00:00:00	Tuesday  	Tue
2660	19970310	1997-03-10	1997-03-10 00:00:00	Monday   	Mon
2661	19970113	1997-01-13	1997-01-13 00:00:00	Monday   	Mon
2662	19970903	1997-09-03	1997-09-03 00:00:00	Wednesday	Wed
2663	19980119	1998-01-19	1998-01-19 00:00:00	Monday   	Mon
2664	19961023	1996-10-23	1996-10-23 00:00:00	Wednesday	Wed
2665	19960917	1996-09-17	1996-09-17 00:00:00	Tuesday  	Tue
2666	19980130	1998-01-30	1998-01-30 00:00:00	Friday   	Fri
2667	19970630	1997-06-30	1997-06-30 00:00:00	Monday   	Mon
2668	19970730	1997-07-30	1997-07-30 00:00:00	Wednesday	Wed
2669	19961128	1996-11-28	1996-11-28 00:00:00	Thursday 	Thu
2670	19971022	1997-10-22	1997-10-22 00:00:00	Wednesday	Wed
2671	19960920	1996-09-20	1996-09-20 00:00:00	Friday   	Fri
2672	19970514	1997-05-14	1997-05-14 00:00:00	Wednesday	Wed
2673	19970418	1997-04-18	1997-04-18 00:00:00	Friday   	Fri
2674	19960815	1996-08-15	1996-08-15 00:00:00	Thursday 	Thu
2675	19960816	1996-08-16	1996-08-16 00:00:00	Friday   	Fri
2676	19970707	1997-07-07	1997-07-07 00:00:00	Monday   	Mon
2677	19970220	1997-02-20	1997-02-20 00:00:00	Thursday 	Thu
2678	19960930	1996-09-30	1996-09-30 00:00:00	Monday   	Mon
2679	19970703	1997-07-03	1997-07-03 00:00:00	Thursday 	Thu
2680	19970312	1997-03-12	1997-03-12 00:00:00	Wednesday	Wed
2681	19980212	1998-02-12	1998-02-12 00:00:00	Thursday 	Thu
2682	19970904	1997-09-04	1997-09-04 00:00:00	Thursday 	Thu
2683	19970625	1997-06-25	1997-06-25 00:00:00	Wednesday	Wed
2684	19980320	1998-03-20	1998-03-20 00:00:00	Friday   	Fri
2685	19970820	1997-08-20	1997-08-20 00:00:00	Wednesday	Wed
2686	19980403	1998-04-03	1998-04-03 00:00:00	Friday   	Fri
2687	19971222	1997-12-22	1997-12-22 00:00:00	Monday   	Mon
2688	19961003	1996-10-03	1996-10-03 00:00:00	Thursday 	Thu
2689	19961219	1996-12-19	1996-12-19 00:00:00	Thursday 	Thu
2690	19970507	1997-05-07	1997-05-07 00:00:00	Wednesday	Wed
2691	19971223	1997-12-23	1997-12-23 00:00:00	Tuesday  	Tue
2692	19970307	1997-03-07	1997-03-07 00:00:00	Friday   	Fri
2693	19980316	1998-03-16	1998-03-16 00:00:00	Monday   	Mon
2694	19980518	1998-05-18	1998-05-18 00:00:00	Monday   	Mon
2695	19970806	1997-08-06	1997-08-06 00:00:00	Wednesday	Wed
2696	19970515	1997-05-15	1997-05-15 00:00:00	Thursday 	Thu
2697	19961205	1996-12-05	1996-12-05 00:00:00	Thursday 	Thu
2698	19980501	1998-05-01	1998-05-01 00:00:00	Friday   	Fri
2699	19970313	1997-03-13	1997-03-13 00:00:00	Thursday 	Thu
2700	19960812	1996-08-12	1996-08-12 00:00:00	Monday   	Mon
2701	19970815	1997-08-15	1997-08-15 00:00:00	Friday   	Fri
2702	19980413	1998-04-13	1998-04-13 00:00:00	Monday   	Mon
2703	19970716	1997-07-16	1997-07-16 00:00:00	Wednesday	Wed
2704	19980601	1998-06-01	1998-06-01 00:00:00	Monday   	Mon
2705	19971008	1997-10-08	1997-10-08 00:00:00	Wednesday	Wed
2706	19960820	1996-08-20	1996-08-20 00:00:00	Tuesday  	Tue
2707	19980128	1998-01-28	1998-01-28 00:00:00	Wednesday	Wed
2708	19971020	1997-10-20	1997-10-20 00:00:00	Monday   	Mon
2709	19961119	1996-11-19	1996-11-19 00:00:00	Tuesday  	Tue
2710	19971205	1997-12-05	1997-12-05 00:00:00	Friday   	Fri
2711	19971027	1997-10-27	1997-10-27 00:00:00	Monday   	Mon
2712	19960909	1996-09-09	1996-09-09 00:00:00	Monday   	Mon
2713	19980204	1998-02-04	1998-02-04 00:00:00	Wednesday	Wed
2714	19971231	1997-12-31	1997-12-31 00:00:00	Wednesday	Wed
2715	19961118	1996-11-18	1996-11-18 00:00:00	Monday   	Mon
2716	19980126	1998-01-26	1998-01-26 00:00:00	Monday   	Mon
2717	19980416	1998-04-16	1998-04-16 00:00:00	Thursday 	Thu
2718	19980205	1998-02-05	1998-02-05 00:00:00	Thursday 	Thu
2719	19970611	1997-06-11	1997-06-11 00:00:00	Wednesday	Wed
2720	19980602	1998-06-02	1998-06-02 00:00:00	Tuesday  	Tue
2721	19970306	1997-03-06	1997-03-06 00:00:00	Thursday 	Thu
2722	19961031	1996-10-31	1996-10-31 00:00:00	Thursday 	Thu
2723	19970612	1997-06-12	1997-06-12 00:00:00	Thursday 	Thu
2724	19971121	1997-11-21	1997-11-21 00:00:00	Friday   	Fri
2725	19970331	1997-03-31	1997-03-31 00:00:00	Monday   	Mon
2726	19960814	1996-08-14	1996-08-14 00:00:00	Wednesday	Wed
2727	19970509	1997-05-09	1997-05-09 00:00:00	Friday   	Fri
2728	19980304	1998-03-04	1998-03-04 00:00:00	Wednesday	Wed
2729	19970805	1997-08-05	1997-08-05 00:00:00	Tuesday  	Tue
2730	19980519	1998-05-19	1998-05-19 00:00:00	Tuesday  	Tue
2731	19980116	1998-01-16	1998-01-16 00:00:00	Friday   	Fri
2732	19971014	1997-10-14	1997-10-14 00:00:00	Tuesday  	Tue
2733	19970114	1997-01-14	1997-01-14 00:00:00	Tuesday  	Tue
2734	19970212	1997-02-12	1997-02-12 00:00:00	Wednesday	Wed
2735	19970311	1997-03-11	1997-03-11 00:00:00	Tuesday  	Tue
2736	19980330	1998-03-30	1998-03-30 00:00:00	Monday   	Mon
2737	19970508	1997-05-08	1997-05-08 00:00:00	Thursday 	Thu
2738	19971217	1997-12-17	1997-12-17 00:00:00	Wednesday	Wed
2739	19971118	1997-11-18	1997-11-18 00:00:00	Tuesday  	Tue
2740	19970725	1997-07-25	1997-07-25 00:00:00	Friday   	Fri
2741	19970527	1997-05-27	1997-05-27 00:00:00	Tuesday  	Tue
2742	19980408	1998-04-08	1998-04-08 00:00:00	Wednesday	Wed
2743	19960904	1996-09-04	1996-09-04 00:00:00	Wednesday	Wed
2744	19970610	1997-06-10	1997-06-10 00:00:00	Tuesday  	Tue
2745	19980407	1998-04-07	1998-04-07 00:00:00	Tuesday  	Tue
2746	19970916	1997-09-16	1997-09-16 00:00:00	Tuesday  	Tue
2747	19970717	1997-07-17	1997-07-17 00:00:00	Thursday 	Thu
2748	19971209	1997-12-09	1997-12-09 00:00:00	Tuesday  	Tue
2749	19971031	1997-10-31	1997-10-31 00:00:00	Friday   	Fri
2750	19970925	1997-09-25	1997-09-25 00:00:00	Thursday 	Thu
2751	19970911	1997-09-11	1997-09-11 00:00:00	Thursday 	Thu
2752	19961113	1996-11-13	1996-11-13 00:00:00	Wednesday	Wed
2753	19971203	1997-12-03	1997-12-03 00:00:00	Wednesday	Wed
2754	19970729	1997-07-29	1997-07-29 00:00:00	Tuesday  	Tue
2755	19971212	1997-12-12	1997-12-12 00:00:00	Friday   	Fri
2756	19970602	1997-06-02	1997-06-02 00:00:00	Monday   	Mon
2757	19970715	1997-07-15	1997-07-15 00:00:00	Tuesday  	Tue
2758	19961126	1996-11-26	1996-11-26 00:00:00	Tuesday  	Tue
2759	19970121	1997-01-21	1997-01-21 00:00:00	Tuesday  	Tue
2760	19971017	1997-10-17	1997-10-17 00:00:00	Friday   	Fri
2761	19960924	1996-09-24	1996-09-24 00:00:00	Tuesday  	Tue
2762	19970124	1997-01-24	1997-01-24 00:00:00	Friday   	Fri
2763	19970116	1997-01-16	1997-01-16 00:00:00	Thursday 	Thu
2764	19980213	1998-02-13	1998-02-13 00:00:00	Friday   	Fri
2765	19970117	1997-01-17	1997-01-17 00:00:00	Friday   	Fri
2766	19971204	1997-12-04	1997-12-04 00:00:00	Thursday 	Thu
2767	19970120	1997-01-20	1997-01-20 00:00:00	Monday   	Mon
2768	19980127	1998-01-27	1998-01-27 00:00:00	Tuesday  	Tue
2769	19970929	1997-09-29	1997-09-29 00:00:00	Monday   	Mon
2770	19960926	1996-09-26	1996-09-26 00:00:00	Thursday 	Thu
2771	19961009	1996-10-09	1996-10-09 00:00:00	Wednesday	Wed
2772	19970822	1997-08-22	1997-08-22 00:00:00	Friday   	Fri
2773	19970811	1997-08-11	1997-08-11 00:00:00	Monday   	Mon
2774	19961125	1996-11-25	1996-11-25 00:00:00	Monday   	Mon
2775	19960918	1996-09-18	1996-09-18 00:00:00	Wednesday	Wed
2776	19980525	1998-05-25	1998-05-25 00:00:00	Monday   	Mon
2777	19970604	1997-06-04	1997-06-04 00:00:00	Wednesday	Wed
2778	19970425	1997-04-25	1997-04-25 00:00:00	Friday   	Fri
2779	19970819	1997-08-19	1997-08-19 00:00:00	Tuesday  	Tue
2780	19961111	1996-11-11	1996-11-11 00:00:00	Monday   	Mon
2781	19980327	1998-03-27	1998-03-27 00:00:00	Friday   	Fri
2782	19971010	1997-10-10	1997-10-10 00:00:00	Friday   	Fri
2783	19980402	1998-04-02	1998-04-02 00:00:00	Thursday 	Thu
2784	19980225	1998-02-25	1998-02-25 00:00:00	Wednesday	Wed
2785	19970501	1997-05-01	1997-05-01 00:00:00	Thursday 	Thu
2786	19980511	1998-05-11	1998-05-11 00:00:00	Monday   	Mon
2787	19980409	1998-04-09	1998-04-09 00:00:00	Thursday 	Thu
2788	19970319	1997-03-19	1997-03-19 00:00:00	Wednesday	Wed
2789	19980305	1998-03-05	1998-03-05 00:00:00	Thursday 	Thu
2790	19980217	1998-02-17	1998-02-17 00:00:00	Tuesday  	Tue
2791	19960827	1996-08-27	1996-08-27 00:00:00	Tuesday  	Tue
2792	19980108	1998-01-08	1998-01-08 00:00:00	Thursday 	Thu
2793	19970922	1997-09-22	1997-09-22 00:00:00	Monday   	Mon
2794	19970708	1997-07-08	1997-07-08 00:00:00	Tuesday  	Tue
2795	19970828	1997-08-28	1997-08-28 00:00:00	Thursday 	Thu
2796	19970505	1997-05-05	1997-05-05 00:00:00	Monday   	Mon
2797	19960906	1996-09-06	1996-09-06 00:00:00	Friday   	Fri
2798	19970924	1997-09-24	1997-09-24 00:00:00	Wednesday	Wed
2799	19970115	1997-01-15	1997-01-15 00:00:00	Wednesday	Wed
2800	19971128	1997-11-28	1997-11-28 00:00:00	Friday   	Fri
2801	19970109	1997-01-09	1997-01-09 00:00:00	Thursday 	Thu
2802	19970516	1997-05-16	1997-05-16 00:00:00	Friday   	Fri
2803	19970807	1997-08-07	1997-08-07 00:00:00	Thursday 	Thu
2804	19961001	1996-10-01	1996-10-01 00:00:00	Tuesday  	Tue
2805	19970401	1997-04-01	1997-04-01 00:00:00	Tuesday  	Tue
2806	19971210	1997-12-10	1997-12-10 00:00:00	Wednesday	Wed
2807	19970218	1997-02-18	1997-02-18 00:00:00	Tuesday  	Tue
2808	19970321	1997-03-21	1997-03-21 00:00:00	Friday   	Fri
2809	19960829	1996-08-29	1996-08-29 00:00:00	Thursday 	Thu
2810	19970702	1997-07-02	1997-07-02 00:00:00	Wednesday	Wed
2811	19970930	1997-09-30	1997-09-30 00:00:00	Tuesday  	Tue
2812	19960821	1996-08-21	1996-08-21 00:00:00	Wednesday	Wed
2813	19970129	1997-01-29	1997-01-29 00:00:00	Wednesday	Wed
2814	19980514	1998-05-14	1998-05-14 00:00:00	Thursday 	Thu
2815	19970324	1997-03-24	1997-03-24 00:00:00	Monday   	Mon
2816	19970502	1997-05-02	1997-05-02 00:00:00	Friday   	Fri
2817	19961030	1996-10-30	1996-10-30 00:00:00	Wednesday	Wed
2818	19970910	1997-09-10	1997-09-10 00:00:00	Wednesday	Wed
2819	19970723	1997-07-23	1997-07-23 00:00:00	Wednesday	Wed
2820	19970224	1997-02-24	1997-02-24 00:00:00	Monday   	Mon
2821	19980515	1998-05-15	1998-05-15 00:00:00	Friday   	Fri
2822	19980428	1998-04-28	1998-04-28 00:00:00	Tuesday  	Tue
2823	19980323	1998-03-23	1998-03-23 00:00:00	Monday   	Mon
2824	19971127	1997-11-27	1997-11-27 00:00:00	Thursday 	Thu
2825	19971113	1997-11-13	1997-11-13 00:00:00	Thursday 	Thu
2826	19970205	1997-02-05	1997-02-05 00:00:00	Wednesday	Wed
2827	19961017	1996-10-17	1996-10-17 00:00:00	Thursday 	Thu
2828	19961231	1996-12-31	1996-12-31 00:00:00	Tuesday  	Tue
2829	19970813	1997-08-13	1997-08-13 00:00:00	Wednesday	Wed
2830	19980508	1998-05-08	1998-05-08 00:00:00	Friday   	Fri
2831	19980216	1998-02-16	1998-02-16 00:00:00	Monday   	Mon
2832	19961008	1996-10-08	1996-10-08 00:00:00	Tuesday  	Tue
2833	19971202	1997-12-02	1997-12-02 00:00:00	Tuesday  	Tue
2834	19970605	1997-06-05	1997-06-05 00:00:00	Thursday 	Thu
2835	19960923	1996-09-23	1996-09-23 00:00:00	Monday   	Mon
2836	19970905	1997-09-05	1997-09-05 00:00:00	Friday   	Fri
2837	19980106	1998-01-06	1998-01-06 00:00:00	Tuesday  	Tue
2838	19961021	1996-10-21	1996-10-21 00:00:00	Monday   	Mon
2839	19980223	1998-02-23	1998-02-23 00:00:00	Monday   	Mon
2840	19971119	1997-11-19	1997-11-19 00:00:00	Wednesday	Wed
2841	19960903	1996-09-03	1996-09-03 00:00:00	Tuesday  	Tue
2842	19980101	1998-01-01	1998-01-01 00:00:00	Thursday 	Thu
2843	19980421	1998-04-21	1998-04-21 00:00:00	Tuesday  	Tue
2844	19980206	1998-02-06	1998-02-06 00:00:00	Friday   	Fri
2845	19971218	1997-12-18	1997-12-18 00:00:00	Thursday 	Thu
2846	19961230	1996-12-30	1996-12-30 00:00:00	Monday   	Mon
2847	19971124	1997-11-24	1997-11-24 00:00:00	Monday   	Mon
2848	19971028	1997-10-28	1997-10-28 00:00:00	Tuesday  	Tue
2849	19970709	1997-07-09	1997-07-09 00:00:00	Wednesday	Wed
2850	19970609	1997-06-09	1997-06-09 00:00:00	Monday   	Mon
2851	19980210	1998-02-10	1998-02-10 00:00:00	Tuesday  	Tue
2852	19971225	1997-12-25	1997-12-25 00:00:00	Thursday 	Thu
2853	19970421	1997-04-21	1997-04-21 00:00:00	Monday   	Mon
2854	19970923	1997-09-23	1997-09-23 00:00:00	Tuesday  	Tue
2855	19970821	1997-08-21	1997-08-21 00:00:00	Thursday 	Thu
2856	19980324	1998-03-24	1998-03-24 00:00:00	Tuesday  	Tue
2857	19961206	1996-12-06	1996-12-06 00:00:00	Friday   	Fri
2858	19970417	1997-04-17	1997-04-17 00:00:00	Thursday 	Thu
2859	19970430	1997-04-30	1997-04-30 00:00:00	Wednesday	Wed
2860	19970225	1997-02-25	1997-02-25 00:00:00	Tuesday  	Tue
2861	19961226	1996-12-26	1996-12-26 00:00:00	Thursday 	Thu
2862	19980513	1998-05-13	1998-05-13 00:00:00	Wednesday	Wed
2863	19970326	1997-03-26	1997-03-26 00:00:00	Wednesday	Wed
2864	19970214	1997-02-14	1997-02-14 00:00:00	Friday   	Fri
2865	19980526	1998-05-26	1998-05-26 00:00:00	Tuesday  	Tue
2866	19980611	1998-06-11	1998-06-11 00:00:00	Thursday 	Thu
2867	19970710	1997-07-10	1997-07-10 00:00:00	Thursday 	Thu
2868	19970305	1997-03-05	1997-03-05 00:00:00	Wednesday	Wed
2869	19980226	1998-02-26	1998-02-26 00:00:00	Thursday 	Thu
2870	19961108	1996-11-08	1996-11-08 00:00:00	Friday   	Fri
2871	19961015	1996-10-15	1996-10-15 00:00:00	Tuesday  	Tue
2872	19980312	1998-03-12	1998-03-12 00:00:00	Thursday 	Thu
2873	19961202	1996-12-02	1996-12-02 00:00:00	Monday   	Mon
2874	19980401	1998-04-01	1998-04-01 00:00:00	Wednesday	Wed
2875	19970411	1997-04-11	1997-04-11 00:00:00	Friday   	Fri
2876	19971103	1997-11-03	1997-11-03 00:00:00	Monday   	Mon
2877	19980504	1998-05-04	1998-05-04 00:00:00	Monday   	Mon
2878	19971021	1997-10-21	1997-10-21 00:00:00	Tuesday  	Tue
2879	19980317	1998-03-17	1998-03-17 00:00:00	Tuesday  	Tue
2880	19971029	1997-10-29	1997-10-29 00:00:00	Wednesday	Wed
2881	19970506	1997-05-06	1997-05-06 00:00:00	Tuesday  	Tue
2882	19970320	1997-03-20	1997-03-20 00:00:00	Thursday 	Thu
2883	19970108	1997-01-08	1997-01-08 00:00:00	Wednesday	Wed
2884	19960910	1996-09-10	1996-09-10 00:00:00	Tuesday  	Tue
2885	19970217	1997-02-17	1997-02-17 00:00:00	Monday   	Mon
2886	19971003	1997-10-03	1997-10-03 00:00:00	Friday   	Fri
2887	19961024	1996-10-24	1996-10-24 00:00:00	Thursday 	Thu
2888	19971215	1997-12-15	1997-12-15 00:00:00	Monday   	Mon
2889	19970512	1997-05-12	1997-05-12 00:00:00	Monday   	Mon
2890	19970416	1997-04-16	1997-04-16 00:00:00	Wednesday	Wed
2891	19980512	1998-05-12	1998-05-12 00:00:00	Tuesday  	Tue
2892	19970325	1997-03-25	1997-03-25 00:00:00	Tuesday  	Tue
2893	19980318	1998-03-18	1998-03-18 00:00:00	Wednesday	Wed
2894	19961212	1996-12-12	1996-12-12 00:00:00	Thursday 	Thu
2895	19980218	1998-02-18	1998-02-18 00:00:00	Wednesday	Wed
2896	19970519	1997-05-19	1997-05-19 00:00:00	Monday   	Mon
2897	19980505	1998-05-05	1998-05-05 00:00:00	Tuesday  	Tue
2898	19980310	1998-03-10	1998-03-10 00:00:00	Tuesday  	Tue
2899	19971106	1997-11-06	1997-11-06 00:00:00	Thursday 	Thu
2900	19970127	1997-01-27	1997-01-27 00:00:00	Monday   	Mon
2901	19971211	1997-12-11	1997-12-11 00:00:00	Thursday 	Thu
2902	19980507	1998-05-07	1998-05-07 00:00:00	Thursday 	Thu
2903	19961010	1996-10-10	1996-10-10 00:00:00	Thursday 	Thu
2904	19970912	1997-09-12	1997-09-12 00:00:00	Friday   	Fri
2905	19961213	1996-12-13	1996-12-13 00:00:00	Friday   	Fri
2906	19980331	1998-03-31	1998-03-31 00:00:00	Tuesday  	Tue
2907	19971030	1997-10-30	1997-10-30 00:00:00	Thursday 	Thu
2908	19970123	1997-01-23	1997-01-23 00:00:00	Thursday 	Thu
2909	19970613	1997-06-13	1997-06-13 00:00:00	Friday   	Fri
2910	19980309	1998-03-09	1998-03-09 00:00:00	Monday   	Mon
2911	19970918	1997-09-18	1997-09-18 00:00:00	Thursday 	Thu
2912	19970620	1997-06-20	1997-06-20 00:00:00	Friday   	Fri
2913	19971007	1997-10-07	1997-10-07 00:00:00	Tuesday  	Tue
2914	19970722	1997-07-22	1997-07-22 00:00:00	Tuesday  	Tue
2915	19971009	1997-10-09	1997-10-09 00:00:00	Thursday 	Thu
2916	19961204	1996-12-04	1996-12-04 00:00:00	Wednesday	Wed
2917	19961004	1996-10-04	1996-10-04 00:00:00	Friday   	Fri
2918	19971224	1997-12-24	1997-12-24 00:00:00	Wednesday	Wed
2919	19970318	1997-03-18	1997-03-18 00:00:00	Tuesday  	Tue
2920	19961029	1996-10-29	1996-10-29 00:00:00	Tuesday  	Tue
2921	19970410	1997-04-10	1997-04-10 00:00:00	Thursday 	Thu
2922	19971016	1997-10-16	1997-10-16 00:00:00	Thursday 	Thu
2923	19970901	1997-09-01	1997-09-01 00:00:00	Monday   	Mon
2924	19970724	1997-07-24	1997-07-24 00:00:00	Thursday 	Thu
2925	19970211	1997-02-11	1997-02-11 00:00:00	Tuesday  	Tue
2926	19961218	1996-12-18	1996-12-18 00:00:00	Wednesday	Wed
2927	19970529	1997-05-29	1997-05-29 00:00:00	Thursday 	Thu
2928	19961217	1996-12-17	1996-12-17 00:00:00	Tuesday  	Tue
2929	19971112	1997-11-12	1997-11-12 00:00:00	Wednesday	Wed
2930	19970304	1997-03-04	1997-03-04 00:00:00	Tuesday  	Tue
2931	19961018	1996-10-18	1996-10-18 00:00:00	Friday   	Fri
2932	19971229	1997-12-29	1997-12-29 00:00:00	Monday   	Mon
2933	19980209	1998-02-09	1998-02-09 00:00:00	Monday   	Mon
2934	19970106	1997-01-06	1997-01-06 00:00:00	Monday   	Mon
2935	19980420	1998-04-20	1998-04-20 00:00:00	Monday   	Mon
2936	19970714	1997-07-14	1997-07-14 00:00:00	Monday   	Mon
2937	19961115	1996-11-15	1996-11-15 00:00:00	Friday   	Fri
2938	19980313	1998-03-13	1998-03-13 00:00:00	Friday   	Fri
2939	19980410	1998-04-10	1998-04-10 00:00:00	Friday   	Fri
2940	19980424	1998-04-24	1998-04-24 00:00:00	Friday   	Fri
2941	19970530	1997-05-30	1997-05-30 00:00:00	Friday   	Fri
2942	19961016	1996-10-16	1996-10-16 00:00:00	Wednesday	Wed
2943	19970102	1997-01-02	1997-01-02 00:00:00	Thursday 	Thu
2944	19980302	1998-03-02	1998-03-02 00:00:00	Monday   	Mon
2945	19970513	1997-05-13	1997-05-13 00:00:00	Tuesday  	Tue
2946	19980520	1998-05-20	1998-05-20 00:00:00	Wednesday	Wed
2947	19970908	1997-09-08	1997-09-08 00:00:00	Monday   	Mon
2948	19980203	1998-02-03	1998-02-03 00:00:00	Tuesday  	Tue
2949	19970721	1997-07-21	1997-07-21 00:00:00	Monday   	Mon
2950	19970623	1997-06-23	1997-06-23 00:00:00	Monday   	Mon
2951	19980306	1998-03-06	1998-03-06 00:00:00	Friday   	Fri
2952	19970616	1997-06-16	1997-06-16 00:00:00	Monday   	Mon
2953	19960826	1996-08-26	1996-08-26 00:00:00	Monday   	Mon
2954	19961022	1996-10-22	1996-10-22 00:00:00	Tuesday  	Tue
2955	19960809	1996-08-09	1996-08-09 00:00:00	Friday   	Fri
2956	19961120	1996-11-20	1996-11-20 00:00:00	Wednesday	Wed
2957	19980527	1998-05-27	1998-05-27 00:00:00	Wednesday	Wed
2958	19961209	1996-12-09	1996-12-09 00:00:00	Monday   	Mon
2959	19961121	1996-11-21	1996-11-21 00:00:00	Thursday 	Thu
2960	19980114	1998-01-14	1998-01-14 00:00:00	Wednesday	Wed
2961	19980107	1998-01-07	1998-01-07 00:00:00	Wednesday	Wed
2962	19971013	1997-10-13	1997-10-13 00:00:00	Monday   	Mon
2963	19970617	1997-06-17	1997-06-17 00:00:00	Tuesday  	Tue
2964	19970919	1997-09-19	1997-09-19 00:00:00	Friday   	Fri
2965	19980120	1998-01-20	1998-01-20 00:00:00	Tuesday  	Tue
2966	19961129	1996-11-29	1996-11-29 00:00:00	Friday   	Fri
2967	19960916	1996-09-16	1996-09-16 00:00:00	Monday   	Mon
2968	19980112	1998-01-12	1998-01-12 00:00:00	Monday   	Mon
2969	19960919	1996-09-19	1996-09-19 00:00:00	Thursday 	Thu
2970	19961011	1996-10-11	1996-10-11 00:00:00	Friday   	Fri
2971	19970626	1997-06-26	1997-06-26 00:00:00	Thursday 	Thu
2972	19971216	1997-12-16	1997-12-16 00:00:00	Tuesday  	Tue
2973	19970731	1997-07-31	1997-07-31 00:00:00	Thursday 	Thu
2974	19970528	1997-05-28	1997-05-28 00:00:00	Wednesday	Wed
2975	19970327	1997-03-27	1997-03-27 00:00:00	Thursday 	Thu
2976	19960822	1996-08-22	1996-08-22 00:00:00	Thursday 	Thu
2977	19980121	1998-01-21	1998-01-21 00:00:00	Wednesday	Wed
2978	19970808	1997-08-08	1997-08-08 00:00:00	Friday   	Fri
2979	19971126	1997-11-26	1997-11-26 00:00:00	Wednesday	Wed
2980	19961105	1996-11-05	1996-11-05 00:00:00	Tuesday  	Tue
2981	19980529	1998-05-29	1998-05-29 00:00:00	Friday   	Fri
2982	19970314	1997-03-14	1997-03-14 00:00:00	Friday   	Fri
2983	19961216	1996-12-16	1996-12-16 00:00:00	Monday   	Mon
2984	19960808	1996-08-08	1996-08-08 00:00:00	Thursday 	Thu
2985	19970424	1997-04-24	1997-04-24 00:00:00	Thursday 	Thu
2986	19961224	1996-12-24	1996-12-24 00:00:00	Tuesday  	Tue
2987	19970814	1997-08-14	1997-08-14 00:00:00	Thursday 	Thu
2988	19970526	1997-05-26	1997-05-26 00:00:00	Monday   	Mon
2989	19960830	1996-08-30	1996-08-30 00:00:00	Friday   	Fri
2990	19960813	1996-08-13	1996-08-13 00:00:00	Tuesday  	Tue
2991	19960724	1996-07-24	1996-07-24 00:00:00	Wednesday	Wed
2992	19980417	1998-04-17	1998-04-17 00:00:00	Friday   	Fri
2993	19980115	1998-01-15	1998-01-15 00:00:00	Thursday 	Thu
2994	19970101	1997-01-01	1997-01-01 00:00:00	Wednesday	Wed
2995	19980224	1998-02-24	1998-02-24 00:00:00	Tuesday  	Tue
2996	19970520	1997-05-20	1997-05-20 00:00:00	Tuesday  	Tue
2997	19961028	1996-10-28	1996-10-28 00:00:00	Monday   	Mon
2998	19970210	1997-02-10	1997-02-10 00:00:00	Monday   	Mon
2999	19980202	1998-02-02	1998-02-02 00:00:00	Monday   	Mon
3000	19971023	1997-10-23	1997-10-23 00:00:00	Thursday 	Thu
3001	19980102	1998-01-02	1998-01-02 00:00:00	Friday   	Fri
3002	19961225	1996-12-25	1996-12-25 00:00:00	Wednesday	Wed
3003	19980129	1998-01-29	1998-01-29 00:00:00	Thursday 	Thu
3004	19970131	1997-01-31	1997-01-31 00:00:00	Friday   	Fri
3005	19961101	1996-11-01	1996-11-01 00:00:00	Friday   	Fri
3006	19980325	1998-03-25	1998-03-25 00:00:00	Wednesday	Wed
3007	19960912	1996-09-12	1996-09-12 00:00:00	Thursday 	Thu
3008	19971230	1997-12-30	1997-12-30 00:00:00	Tuesday  	Tue
3009	19970428	1997-04-28	1997-04-28 00:00:00	Monday   	Mon
3010	19960801	1996-08-01	1996-08-01 00:00:00	Thursday 	Thu
3011	19980227	1998-02-27	1998-02-27 00:00:00	Friday   	Fri
3012	19980105	1998-01-05	1998-01-05 00:00:00	Monday   	Mon
3013	19971015	1997-10-15	1997-10-15 00:00:00	Wednesday	Wed
3014	19961122	1996-11-22	1996-11-22 00:00:00	Friday   	Fri
3015	19980522	1998-05-22	1998-05-22 00:00:00	Friday   	Fri
3016	19980423	1998-04-23	1998-04-23 00:00:00	Thursday 	Thu
3017	19971208	1997-12-08	1997-12-08 00:00:00	Monday   	Mon
3018	19970402	1997-04-02	1997-04-02 00:00:00	Wednesday	Wed
3019	19980415	1998-04-15	1998-04-15 00:00:00	Wednesday	Wed
3020	19970522	1997-05-22	1997-05-22 00:00:00	Thursday 	Thu
3021	19970328	1997-03-28	1997-03-28 00:00:00	Friday   	Fri
3022	19970107	1997-01-07	1997-01-07 00:00:00	Tuesday  	Tue
3023	19980603	1998-06-03	1998-06-03 00:00:00	Wednesday	Wed
3024	19980406	1998-04-06	1998-04-06 00:00:00	Monday   	Mon
3025	19970203	1997-02-03	1997-02-03 00:00:00	Monday   	Mon
3026	19970728	1997-07-28	1997-07-28 00:00:00	Monday   	Mon
3027	19970429	1997-04-29	1997-04-29 00:00:00	Tuesday  	Tue
3028	19980311	1998-03-11	1998-03-11 00:00:00	Wednesday	Wed
3029	19961025	1996-10-25	1996-10-25 00:00:00	Friday   	Fri
3030	19971024	1997-10-24	1997-10-24 00:00:00	Friday   	Fri
3031	19960805	1996-08-05	1996-08-05 00:00:00	Monday   	Mon
3032	19970909	1997-09-09	1997-09-09 00:00:00	Tuesday  	Tue
3033	19970122	1997-01-22	1997-01-22 00:00:00	Wednesday	Wed
3034	19970219	1997-02-19	1997-02-19 00:00:00	Wednesday	Wed
3035	19970207	1997-02-07	1997-02-07 00:00:00	Friday   	Fri
3036	19970110	1997-01-10	1997-01-10 00:00:00	Friday   	Fri
3037	19970303	1997-03-03	1997-03-03 00:00:00	Monday   	Mon
3038	19961227	1996-12-27	1996-12-27 00:00:00	Friday   	Fri
3039	19970704	1997-07-04	1997-07-04 00:00:00	Friday   	Fri
3040	19970407	1997-04-07	1997-04-07 00:00:00	Monday   	Mon
3041	19960911	1996-09-11	1996-09-11 00:00:00	Wednesday	Wed
3042	19980528	1998-05-28	1998-05-28 00:00:00	Thursday 	Thu
3043	19980319	1998-03-19	1998-03-19 00:00:00	Thursday 	Thu
3044	19970825	1997-08-25	1997-08-25 00:00:00	Monday   	Mon
3045	19961107	1996-11-07	1996-11-07 00:00:00	Thursday 	Thu
3046	19960925	1996-09-25	1996-09-25 00:00:00	Wednesday	Wed
3047	19980429	1998-04-29	1998-04-29 00:00:00	Wednesday	Wed
3048	19971006	1997-10-06	1997-10-06 00:00:00	Monday   	Mon
3049	19970404	1997-04-04	1997-04-04 00:00:00	Friday   	Fri
3050	19970827	1997-08-27	1997-08-27 00:00:00	Wednesday	Wed
3051	19980610	1998-06-10	1998-06-10 00:00:00	Wednesday	Wed
3052	19980109	1998-01-09	1998-01-09 00:00:00	Friday   	Fri
3053	19971002	1997-10-02	1997-10-02 00:00:00	Thursday 	Thu
3054	19970103	1997-01-03	1997-01-03 00:00:00	Friday   	Fri
3055	19980220	1998-02-20	1998-02-20 00:00:00	Friday   	Fri
3056	19971107	1997-11-07	1997-11-07 00:00:00	Friday   	Fri
3057	19960806	1996-08-06	1996-08-06 00:00:00	Tuesday  	Tue
3058	19970409	1997-04-09	1997-04-09 00:00:00	Wednesday	Wed
3059	19961223	1996-12-23	1996-12-23 00:00:00	Monday   	Mon
3060	19971105	1997-11-05	1997-11-05 00:00:00	Wednesday	Wed
3061	19971219	1997-12-19	1997-12-19 00:00:00	Friday   	Fri
3062	19970801	1997-08-01	1997-08-01 00:00:00	Friday   	Fri
3063	19980414	1998-04-14	1998-04-14 00:00:00	Tuesday  	Tue
3064	19980506	1998-05-06	1998-05-06 00:00:00	Wednesday	Wed
3065	19970403	1997-04-03	1997-04-03 00:00:00	Thursday 	Thu
3066	19980326	1998-03-26	1998-03-26 00:00:00	Thursday 	Thu
3067	19970204	1997-02-04	1997-02-04 00:00:00	Tuesday  	Tue
3068	19961104	1996-11-04	1996-11-04 00:00:00	Monday   	Mon
3069	19970618	1997-06-18	1997-06-18 00:00:00	Wednesday	Wed
3070	19971111	1997-11-11	1997-11-11 00:00:00	Tuesday  	Tue
3071	19970818	1997-08-18	1997-08-18 00:00:00	Monday   	Mon
3072	19961127	1996-11-27	1996-11-27 00:00:00	Wednesday	Wed
3073	19970926	1997-09-26	1997-09-26 00:00:00	Friday   	Fri
3074	19970627	1997-06-27	1997-06-27 00:00:00	Friday   	Fri
3075	19961211	1996-12-11	1996-12-11 00:00:00	Wednesday	Wed
3076	19970902	1997-09-02	1997-09-02 00:00:00	Tuesday  	Tue
3077	19970915	1997-09-15	1997-09-15 00:00:00	Monday   	Mon
3078	19960828	1996-08-28	1996-08-28 00:00:00	Wednesday	Wed
3079	19960913	1996-09-13	1996-09-13 00:00:00	Friday   	Fri
3080	19971201	1997-12-01	1997-12-01 00:00:00	Monday   	Mon
3081	19961007	1996-10-07	1996-10-07 00:00:00	Monday   	Mon
3082	19970826	1997-08-26	1997-08-26 00:00:00	Tuesday  	Tue
3083	19980303	1998-03-03	1998-03-03 00:00:00	Tuesday  	Tue
3084	19970711	1997-07-11	1997-07-11 00:00:00	Friday   	Fri
3085	19980211	1998-02-11	1998-02-11 00:00:00	Wednesday	Wed
3086	19970206	1997-02-06	1997-02-06 00:00:00	Thursday 	Thu
3087	19961014	1996-10-14	1996-10-14 00:00:00	Monday   	Mon
3088	19960819	1996-08-19	1996-08-19 00:00:00	Monday   	Mon
3089	19970317	1997-03-17	1997-03-17 00:00:00	Monday   	Mon
3090	19970804	1997-08-04	1997-08-04 00:00:00	Monday   	Mon
3091	19971226	1997-12-26	1997-12-26 00:00:00	Friday   	Fri
3092	19970606	1997-06-06	1997-06-06 00:00:00	Friday   	Fri
3093	19971117	1997-11-17	1997-11-17 00:00:00	Monday   	Mon
3094	19980422	1998-04-22	1998-04-22 00:00:00	Wednesday	Wed
3095	19970414	1997-04-14	1997-04-14 00:00:00	Monday   	Mon
3096	19961220	1996-12-20	1996-12-20 00:00:00	Friday   	Fri
3097	19980122	1998-01-22	1998-01-22 00:00:00	Thursday 	Thu
3098	19980521	1998-05-21	1998-05-21 00:00:00	Thursday 	Thu
3099	19971001	1997-10-01	1997-10-01 00:00:00	Wednesday	Wed
3100	19970423	1997-04-23	1997-04-23 00:00:00	Wednesday	Wed
\.


--
-- Data for Name: d_shipped_date; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_shipped_date (shipped_date_key, shipped_date_id_yyyymmdd, "Shipped_Day_Date_yyyy-mm-dd", shipped_full_date, shipped_day_of_week_name, shipped_day_of_week_abbrev) FROM stdin;
2051	\N	\N	\N	\N	\N
3215	\N	\N	\N	\N	\N
4379	\N	\N	\N	\N	\N
3603	\N	\N	\N	\N	\N
3991	\N	\N	\N	\N	\N
5110	19960917	1996-09-17	1996-09-17 00:00:00	Tuesday  	Tue
5111	19980130	1998-01-30	1998-01-30 00:00:00	Friday   	Fri
5112	19970630	1997-06-30	1997-06-30 00:00:00	Monday   	Mon
5113	19970730	1997-07-30	1997-07-30 00:00:00	Wednesday	Wed
5114	19961128	1996-11-28	1996-11-28 00:00:00	Thursday 	Thu
5115	19971022	1997-10-22	1997-10-22 00:00:00	Wednesday	Wed
5116	19970514	1997-05-14	1997-05-14 00:00:00	Wednesday	Wed
5117	19970523	1997-05-23	1997-05-23 00:00:00	Friday   	Fri
5118	19970418	1997-04-18	1997-04-18 00:00:00	Friday   	Fri
5119	19960802	1996-08-02	1996-08-02 00:00:00	Friday   	Fri
5120	19960816	1996-08-16	1996-08-16 00:00:00	Friday   	Fri
5121	19970220	1997-02-20	1997-02-20 00:00:00	Thursday 	Thu
5122	19970312	1997-03-12	1997-03-12 00:00:00	Wednesday	Wed
5123	19980212	1998-02-12	1998-02-12 00:00:00	Thursday 	Thu
5124	19970625	1997-06-25	1997-06-25 00:00:00	Wednesday	Wed
5125	19980320	1998-03-20	1998-03-20 00:00:00	Friday   	Fri
5126	19970820	1997-08-20	1997-08-20 00:00:00	Wednesday	Wed
5127	19980403	1998-04-03	1998-04-03 00:00:00	Friday   	Fri
5128	19971222	1997-12-22	1997-12-22 00:00:00	Monday   	Mon
5129	19961003	1996-10-03	1996-10-03 00:00:00	Thursday 	Thu
5130	19961219	1996-12-19	1996-12-19 00:00:00	Thursday 	Thu
5131	19970507	1997-05-07	1997-05-07 00:00:00	Wednesday	Wed
5132	19971223	1997-12-23	1997-12-23 00:00:00	Tuesday  	Tue
5133	19970307	1997-03-07	1997-03-07 00:00:00	Friday   	Fri
5134	19980316	1998-03-16	1998-03-16 00:00:00	Monday   	Mon
5135	19970806	1997-08-06	1997-08-06 00:00:00	Wednesday	Wed
5136	19970515	1997-05-15	1997-05-15 00:00:00	Thursday 	Thu
5137	19980501	1998-05-01	1998-05-01 00:00:00	Friday   	Fri
5138	19970313	1997-03-13	1997-03-13 00:00:00	Thursday 	Thu
5139	19960812	1996-08-12	1996-08-12 00:00:00	Monday   	Mon
5140	19970815	1997-08-15	1997-08-15 00:00:00	Friday   	Fri
5141	19971120	1997-11-20	1997-11-20 00:00:00	Thursday 	Thu
5142	19960823	1996-08-23	1996-08-23 00:00:00	Friday   	Fri
5143	19980413	1998-04-13	1998-04-13 00:00:00	Monday   	Mon
5144	19970716	1997-07-16	1997-07-16 00:00:00	Wednesday	Wed
5145	19971008	1997-10-08	1997-10-08 00:00:00	Wednesday	Wed
5146	19971020	1997-10-20	1997-10-20 00:00:00	Monday   	Mon
5147	19971205	1997-12-05	1997-12-05 00:00:00	Friday   	Fri
5148	19971027	1997-10-27	1997-10-27 00:00:00	Monday   	Mon
4767	\N	\N	\N	\N	\N
4768	19970306	1997-03-06	1997-03-06 00:00:00	Thursday 	Thu
4769	19970612	1997-06-12	1997-06-12 00:00:00	Thursday 	Thu
4770	19971121	1997-11-21	1997-11-21 00:00:00	Friday   	Fri
4771	19970331	1997-03-31	1997-03-31 00:00:00	Monday   	Mon
4772	19960814	1996-08-14	1996-08-14 00:00:00	Wednesday	Wed
4773	19970509	1997-05-09	1997-05-09 00:00:00	Friday   	Fri
4774	19980304	1998-03-04	1998-03-04 00:00:00	Wednesday	Wed
4775	19970805	1997-08-05	1997-08-05 00:00:00	Tuesday  	Tue
4776	19980116	1998-01-16	1998-01-16 00:00:00	Friday   	Fri
4777	19971014	1997-10-14	1997-10-14 00:00:00	Tuesday  	Tue
4778	19970114	1997-01-14	1997-01-14 00:00:00	Tuesday  	Tue
4779	19970228	1997-02-28	1997-02-28 00:00:00	Friday   	Fri
4780	19970212	1997-02-12	1997-02-12 00:00:00	Wednesday	Wed
4781	19970311	1997-03-11	1997-03-11 00:00:00	Tuesday  	Tue
4782	19980330	1998-03-30	1998-03-30 00:00:00	Monday   	Mon
4783	19971217	1997-12-17	1997-12-17 00:00:00	Wednesday	Wed
4784	19971118	1997-11-18	1997-11-18 00:00:00	Tuesday  	Tue
4785	19970725	1997-07-25	1997-07-25 00:00:00	Friday   	Fri
4786	19970527	1997-05-27	1997-05-27 00:00:00	Tuesday  	Tue
4787	19980408	1998-04-08	1998-04-08 00:00:00	Wednesday	Wed
4788	19960904	1996-09-04	1996-09-04 00:00:00	Wednesday	Wed
4789	19970610	1997-06-10	1997-06-10 00:00:00	Tuesday  	Tue
4790	19980407	1998-04-07	1998-04-07 00:00:00	Tuesday  	Tue
4791	19971209	1997-12-09	1997-12-09 00:00:00	Tuesday  	Tue
4792	19971031	1997-10-31	1997-10-31 00:00:00	Friday   	Fri
4793	19970911	1997-09-11	1997-09-11 00:00:00	Thursday 	Thu
4794	19960905	1996-09-05	1996-09-05 00:00:00	Thursday 	Thu
4795	19971203	1997-12-03	1997-12-03 00:00:00	Wednesday	Wed
4796	19970729	1997-07-29	1997-07-29 00:00:00	Tuesday  	Tue
4797	19971212	1997-12-12	1997-12-12 00:00:00	Friday   	Fri
4798	19970602	1997-06-02	1997-06-02 00:00:00	Monday   	Mon
4799	19961126	1996-11-26	1996-11-26 00:00:00	Tuesday  	Tue
4800	19970121	1997-01-21	1997-01-21 00:00:00	Tuesday  	Tue
4801	19971017	1997-10-17	1997-10-17 00:00:00	Friday   	Fri
4802	19960924	1996-09-24	1996-09-24 00:00:00	Tuesday  	Tue
4803	19970124	1997-01-24	1997-01-24 00:00:00	Friday   	Fri
4804	19970116	1997-01-16	1997-01-16 00:00:00	Thursday 	Thu
4805	19980213	1998-02-13	1998-02-13 00:00:00	Friday   	Fri
4806	19970117	1997-01-17	1997-01-17 00:00:00	Friday   	Fri
4807	19971204	1997-12-04	1997-12-04 00:00:00	Thursday 	Thu
4808	19971110	1997-11-10	1997-11-10 00:00:00	Monday   	Mon
4809	19970929	1997-09-29	1997-09-29 00:00:00	Monday   	Mon
4810	19960926	1996-09-26	1996-09-26 00:00:00	Thursday 	Thu
4811	19960723	1996-07-23	1996-07-23 00:00:00	Tuesday  	Tue
4812	19961009	1996-10-09	1996-10-09 00:00:00	Wednesday	Wed
4813	19970811	1997-08-11	1997-08-11 00:00:00	Monday   	Mon
4814	19961125	1996-11-25	1996-11-25 00:00:00	Monday   	Mon
4815	19960918	1996-09-18	1996-09-18 00:00:00	Wednesday	Wed
4816	19970604	1997-06-04	1997-06-04 00:00:00	Wednesday	Wed
4817	19970425	1997-04-25	1997-04-25 00:00:00	Friday   	Fri
4818	19970819	1997-08-19	1997-08-19 00:00:00	Tuesday  	Tue
4819	19961111	1996-11-11	1996-11-11 00:00:00	Monday   	Mon
4820	19980327	1998-03-27	1998-03-27 00:00:00	Friday   	Fri
4821	19971010	1997-10-10	1997-10-10 00:00:00	Friday   	Fri
4822	19980402	1998-04-02	1998-04-02 00:00:00	Thursday 	Thu
4823	19980225	1998-02-25	1998-02-25 00:00:00	Wednesday	Wed
4824	19970501	1997-05-01	1997-05-01 00:00:00	Thursday 	Thu
4825	19980409	1998-04-09	1998-04-09 00:00:00	Thursday 	Thu
4826	19970319	1997-03-19	1997-03-19 00:00:00	Wednesday	Wed
4827	19980305	1998-03-05	1998-03-05 00:00:00	Thursday 	Thu
4828	19980217	1998-02-17	1998-02-17 00:00:00	Tuesday  	Tue
4829	19960827	1996-08-27	1996-08-27 00:00:00	Tuesday  	Tue
4830	19980108	1998-01-08	1998-01-08 00:00:00	Thursday 	Thu
4831	19970922	1997-09-22	1997-09-22 00:00:00	Monday   	Mon
4832	19970828	1997-08-28	1997-08-28 00:00:00	Thursday 	Thu
4833	19970505	1997-05-05	1997-05-05 00:00:00	Monday   	Mon
4834	19960731	1996-07-31	1996-07-31 00:00:00	Wednesday	Wed
4835	19970924	1997-09-24	1997-09-24 00:00:00	Wednesday	Wed
4836	19970115	1997-01-15	1997-01-15 00:00:00	Wednesday	Wed
4837	19971128	1997-11-28	1997-11-28 00:00:00	Friday   	Fri
4838	19970109	1997-01-09	1997-01-09 00:00:00	Thursday 	Thu
4839	19961203	1996-12-03	1996-12-03 00:00:00	Tuesday  	Tue
4840	19970516	1997-05-16	1997-05-16 00:00:00	Friday   	Fri
4841	19970807	1997-08-07	1997-08-07 00:00:00	Thursday 	Thu
4842	19970812	1997-08-12	1997-08-12 00:00:00	Tuesday  	Tue
4843	19970401	1997-04-01	1997-04-01 00:00:00	Tuesday  	Tue
4844	19971210	1997-12-10	1997-12-10 00:00:00	Wednesday	Wed
4845	19970218	1997-02-18	1997-02-18 00:00:00	Tuesday  	Tue
4846	19970321	1997-03-21	1997-03-21 00:00:00	Friday   	Fri
4847	19970702	1997-07-02	1997-07-02 00:00:00	Wednesday	Wed
4848	19970930	1997-09-30	1997-09-30 00:00:00	Tuesday  	Tue
4849	19960821	1996-08-21	1996-08-21 00:00:00	Wednesday	Wed
4850	19970324	1997-03-24	1997-03-24 00:00:00	Monday   	Mon
4851	19970502	1997-05-02	1997-05-02 00:00:00	Friday   	Fri
4852	19970910	1997-09-10	1997-09-10 00:00:00	Wednesday	Wed
4853	19970224	1997-02-24	1997-02-24 00:00:00	Monday   	Mon
4854	19980428	1998-04-28	1998-04-28 00:00:00	Tuesday  	Tue
4855	19980323	1998-03-23	1998-03-23 00:00:00	Monday   	Mon
4856	19971127	1997-11-27	1997-11-27 00:00:00	Thursday 	Thu
4857	19961017	1996-10-17	1996-10-17 00:00:00	Thursday 	Thu
4858	19960722	1996-07-22	1996-07-22 00:00:00	Monday   	Mon
4859	19961231	1996-12-31	1996-12-31 00:00:00	Tuesday  	Tue
4860	19960712	1996-07-12	1996-07-12 00:00:00	Friday   	Fri
4861	19970813	1997-08-13	1997-08-13 00:00:00	Wednesday	Wed
4862	19980216	1998-02-16	1998-02-16 00:00:00	Monday   	Mon
4863	19961008	1996-10-08	1996-10-08 00:00:00	Tuesday  	Tue
4864	19971202	1997-12-02	1997-12-02 00:00:00	Tuesday  	Tue
4865	19970605	1997-06-05	1997-06-05 00:00:00	Thursday 	Thu
4866	19960923	1996-09-23	1996-09-23 00:00:00	Monday   	Mon
4867	19970905	1997-09-05	1997-09-05 00:00:00	Friday   	Fri
4868	19980106	1998-01-06	1998-01-06 00:00:00	Tuesday  	Tue
4869	19961021	1996-10-21	1996-10-21 00:00:00	Monday   	Mon
4870	19980223	1998-02-23	1998-02-23 00:00:00	Monday   	Mon
4871	19960903	1996-09-03	1996-09-03 00:00:00	Tuesday  	Tue
4872	19980101	1998-01-01	1998-01-01 00:00:00	Thursday 	Thu
4873	19980421	1998-04-21	1998-04-21 00:00:00	Tuesday  	Tue
4874	19970128	1997-01-28	1997-01-28 00:00:00	Tuesday  	Tue
4875	19980206	1998-02-06	1998-02-06 00:00:00	Friday   	Fri
4876	19971218	1997-12-18	1997-12-18 00:00:00	Thursday 	Thu
4877	19961230	1996-12-30	1996-12-30 00:00:00	Monday   	Mon
4878	19971124	1997-11-24	1997-11-24 00:00:00	Monday   	Mon
4879	19970709	1997-07-09	1997-07-09 00:00:00	Wednesday	Wed
4880	19970609	1997-06-09	1997-06-09 00:00:00	Monday   	Mon
4881	19980210	1998-02-10	1998-02-10 00:00:00	Tuesday  	Tue
4882	19971225	1997-12-25	1997-12-25 00:00:00	Thursday 	Thu
4883	19970421	1997-04-21	1997-04-21 00:00:00	Monday   	Mon
4884	19970923	1997-09-23	1997-09-23 00:00:00	Tuesday  	Tue
4885	19970821	1997-08-21	1997-08-21 00:00:00	Thursday 	Thu
4886	19960711	1996-07-11	1996-07-11 00:00:00	Thursday 	Thu
4887	19980324	1998-03-24	1998-03-24 00:00:00	Tuesday  	Tue
4888	19970417	1997-04-17	1997-04-17 00:00:00	Thursday 	Thu
4889	19970225	1997-02-25	1997-02-25 00:00:00	Tuesday  	Tue
4890	19961226	1996-12-26	1996-12-26 00:00:00	Thursday 	Thu
4891	19961106	1996-11-06	1996-11-06 00:00:00	Wednesday	Wed
4892	19970326	1997-03-26	1997-03-26 00:00:00	Wednesday	Wed
4893	19970214	1997-02-14	1997-02-14 00:00:00	Friday   	Fri
4894	19970710	1997-07-10	1997-07-10 00:00:00	Thursday 	Thu
4895	19970305	1997-03-05	1997-03-05 00:00:00	Wednesday	Wed
4896	19980226	1998-02-26	1998-02-26 00:00:00	Thursday 	Thu
4897	19961108	1996-11-08	1996-11-08 00:00:00	Friday   	Fri
4898	19980312	1998-03-12	1998-03-12 00:00:00	Thursday 	Thu
4899	19961202	1996-12-02	1996-12-02 00:00:00	Monday   	Mon
4900	19980401	1998-04-01	1998-04-01 00:00:00	Wednesday	Wed
4901	19970411	1997-04-11	1997-04-11 00:00:00	Friday   	Fri
4902	19980504	1998-05-04	1998-05-04 00:00:00	Monday   	Mon
4903	19971021	1997-10-21	1997-10-21 00:00:00	Tuesday  	Tue
4904	19980317	1998-03-17	1998-03-17 00:00:00	Tuesday  	Tue
4905	19971029	1997-10-29	1997-10-29 00:00:00	Wednesday	Wed
4906	19970506	1997-05-06	1997-05-06 00:00:00	Tuesday  	Tue
4907	19970108	1997-01-08	1997-01-08 00:00:00	Wednesday	Wed
4908	19960910	1996-09-10	1996-09-10 00:00:00	Tuesday  	Tue
4909	19970521	1997-05-21	1997-05-21 00:00:00	Wednesday	Wed
4910	19971003	1997-10-03	1997-10-03 00:00:00	Friday   	Fri
4911	19961024	1996-10-24	1996-10-24 00:00:00	Thursday 	Thu
4912	19971215	1997-12-15	1997-12-15 00:00:00	Monday   	Mon
4913	19970512	1997-05-12	1997-05-12 00:00:00	Monday   	Mon
4914	19970416	1997-04-16	1997-04-16 00:00:00	Wednesday	Wed
4915	19970325	1997-03-25	1997-03-25 00:00:00	Tuesday  	Tue
4916	19980318	1998-03-18	1998-03-18 00:00:00	Wednesday	Wed
4917	19980218	1998-02-18	1998-02-18 00:00:00	Wednesday	Wed
4918	19970519	1997-05-19	1997-05-19 00:00:00	Monday   	Mon
4919	19980505	1998-05-05	1998-05-05 00:00:00	Tuesday  	Tue
4920	19980310	1998-03-10	1998-03-10 00:00:00	Tuesday  	Tue
4921	19970127	1997-01-27	1997-01-27 00:00:00	Monday   	Mon
4922	19961010	1996-10-10	1996-10-10 00:00:00	Thursday 	Thu
4923	19960725	1996-07-25	1996-07-25 00:00:00	Thursday 	Thu
4924	19961213	1996-12-13	1996-12-13 00:00:00	Friday   	Fri
4925	19980331	1998-03-31	1998-03-31 00:00:00	Tuesday  	Tue
4926	19971030	1997-10-30	1997-10-30 00:00:00	Thursday 	Thu
4927	19970613	1997-06-13	1997-06-13 00:00:00	Friday   	Fri
4928	19980309	1998-03-09	1998-03-09 00:00:00	Monday   	Mon
4929	19970918	1997-09-18	1997-09-18 00:00:00	Thursday 	Thu
4930	19970620	1997-06-20	1997-06-20 00:00:00	Friday   	Fri
4931	19960710	1996-07-10	1996-07-10 00:00:00	Wednesday	Wed
4932	19971007	1997-10-07	1997-10-07 00:00:00	Tuesday  	Tue
4933	19970722	1997-07-22	1997-07-22 00:00:00	Tuesday  	Tue
4934	19971009	1997-10-09	1997-10-09 00:00:00	Thursday 	Thu
4935	19961204	1996-12-04	1996-12-04 00:00:00	Wednesday	Wed
4936	19961004	1996-10-04	1996-10-04 00:00:00	Friday   	Fri
4937	19971224	1997-12-24	1997-12-24 00:00:00	Wednesday	Wed
4938	19970318	1997-03-18	1997-03-18 00:00:00	Tuesday  	Tue
4939	19961029	1996-10-29	1996-10-29 00:00:00	Tuesday  	Tue
4940	19970410	1997-04-10	1997-04-10 00:00:00	Thursday 	Thu
4941	19971016	1997-10-16	1997-10-16 00:00:00	Thursday 	Thu
4942	19970901	1997-09-01	1997-09-01 00:00:00	Monday   	Mon
4943	19970211	1997-02-11	1997-02-11 00:00:00	Tuesday  	Tue
4944	19961218	1996-12-18	1996-12-18 00:00:00	Wednesday	Wed
4945	19970529	1997-05-29	1997-05-29 00:00:00	Thursday 	Thu
4946	19960715	1996-07-15	1996-07-15 00:00:00	Monday   	Mon
4947	19971112	1997-11-12	1997-11-12 00:00:00	Wednesday	Wed
4948	19970304	1997-03-04	1997-03-04 00:00:00	Tuesday  	Tue
4949	19961018	1996-10-18	1996-10-18 00:00:00	Friday   	Fri
4950	19980209	1998-02-09	1998-02-09 00:00:00	Monday   	Mon
4951	19970106	1997-01-06	1997-01-06 00:00:00	Monday   	Mon
4952	19960717	1996-07-17	1996-07-17 00:00:00	Wednesday	Wed
4953	19980420	1998-04-20	1998-04-20 00:00:00	Monday   	Mon
4954	19970714	1997-07-14	1997-07-14 00:00:00	Monday   	Mon
4955	19961115	1996-11-15	1996-11-15 00:00:00	Friday   	Fri
4956	19970422	1997-04-22	1997-04-22 00:00:00	Tuesday  	Tue
4957	19980313	1998-03-13	1998-03-13 00:00:00	Friday   	Fri
4958	19980410	1998-04-10	1998-04-10 00:00:00	Friday   	Fri
4959	19980424	1998-04-24	1998-04-24 00:00:00	Friday   	Fri
4960	19970530	1997-05-30	1997-05-30 00:00:00	Friday   	Fri
4961	19970227	1997-02-27	1997-02-27 00:00:00	Thursday 	Thu
4962	19970102	1997-01-02	1997-01-02 00:00:00	Thursday 	Thu
4963	19960716	1996-07-16	1996-07-16 00:00:00	Tuesday  	Tue
4964	19980302	1998-03-02	1998-03-02 00:00:00	Monday   	Mon
4965	19970513	1997-05-13	1997-05-13 00:00:00	Tuesday  	Tue
4966	19970908	1997-09-08	1997-09-08 00:00:00	Monday   	Mon
4967	19980203	1998-02-03	1998-02-03 00:00:00	Tuesday  	Tue
4968	19970721	1997-07-21	1997-07-21 00:00:00	Monday   	Mon
4969	19980306	1998-03-06	1998-03-06 00:00:00	Friday   	Fri
4970	19970616	1997-06-16	1997-06-16 00:00:00	Monday   	Mon
4971	19960826	1996-08-26	1996-08-26 00:00:00	Monday   	Mon
4972	19960809	1996-08-09	1996-08-09 00:00:00	Friday   	Fri
4973	19961120	1996-11-20	1996-11-20 00:00:00	Wednesday	Wed
4974	19961209	1996-12-09	1996-12-09 00:00:00	Monday   	Mon
4975	19980107	1998-01-07	1998-01-07 00:00:00	Wednesday	Wed
4976	19980114	1998-01-14	1998-01-14 00:00:00	Wednesday	Wed
4977	19971013	1997-10-13	1997-10-13 00:00:00	Monday   	Mon
4978	19970617	1997-06-17	1997-06-17 00:00:00	Tuesday  	Tue
4979	19970919	1997-09-19	1997-09-19 00:00:00	Friday   	Fri
4980	19980120	1998-01-20	1998-01-20 00:00:00	Tuesday  	Tue
4981	19980112	1998-01-12	1998-01-12 00:00:00	Monday   	Mon
4982	19961011	1996-10-11	1996-10-11 00:00:00	Friday   	Fri
4983	19970626	1997-06-26	1997-06-26 00:00:00	Thursday 	Thu
4984	19971216	1997-12-16	1997-12-16 00:00:00	Tuesday  	Tue
4985	19970731	1997-07-31	1997-07-31 00:00:00	Thursday 	Thu
4986	19980121	1998-01-21	1998-01-21 00:00:00	Wednesday	Wed
4987	19970808	1997-08-08	1997-08-08 00:00:00	Friday   	Fri
4988	19971126	1997-11-26	1997-11-26 00:00:00	Wednesday	Wed
4989	19961105	1996-11-05	1996-11-05 00:00:00	Tuesday  	Tue
4990	19970314	1997-03-14	1997-03-14 00:00:00	Friday   	Fri
4991	19961216	1996-12-16	1996-12-16 00:00:00	Monday   	Mon
4992	19970424	1997-04-24	1997-04-24 00:00:00	Thursday 	Thu
4993	19961224	1996-12-24	1996-12-24 00:00:00	Tuesday  	Tue
4994	19970814	1997-08-14	1997-08-14 00:00:00	Thursday 	Thu
4995	19970526	1997-05-26	1997-05-26 00:00:00	Monday   	Mon
4996	19960813	1996-08-13	1996-08-13 00:00:00	Tuesday  	Tue
4997	19960830	1996-08-30	1996-08-30 00:00:00	Friday   	Fri
4998	19960730	1996-07-30	1996-07-30 00:00:00	Tuesday  	Tue
4999	19970130	1997-01-30	1997-01-30 00:00:00	Thursday 	Thu
5000	19980417	1998-04-17	1998-04-17 00:00:00	Friday   	Fri
5001	19980115	1998-01-15	1998-01-15 00:00:00	Thursday 	Thu
5002	19970101	1997-01-01	1997-01-01 00:00:00	Wednesday	Wed
5003	19961028	1996-10-28	1996-10-28 00:00:00	Monday   	Mon
5004	19970624	1997-06-24	1997-06-24 00:00:00	Tuesday  	Tue
5005	19970210	1997-02-10	1997-02-10 00:00:00	Monday   	Mon
5006	19980202	1998-02-02	1998-02-02 00:00:00	Monday   	Mon
2827	\N	\N	\N	\N	\N
5007	19971023	1997-10-23	1997-10-23 00:00:00	Thursday 	Thu
5008	19980102	1998-01-02	1998-01-02 00:00:00	Friday   	Fri
5009	19961225	1996-12-25	1996-12-25 00:00:00	Wednesday	Wed
5010	19980129	1998-01-29	1998-01-29 00:00:00	Thursday 	Thu
5011	19970131	1997-01-31	1997-01-31 00:00:00	Friday   	Fri
5012	19980325	1998-03-25	1998-03-25 00:00:00	Wednesday	Wed
5013	19960912	1996-09-12	1996-09-12 00:00:00	Thursday 	Thu
5014	19970428	1997-04-28	1997-04-28 00:00:00	Monday   	Mon
5015	19980227	1998-02-27	1998-02-27 00:00:00	Friday   	Fri
5016	19980105	1998-01-05	1998-01-05 00:00:00	Monday   	Mon
5017	19971015	1997-10-15	1997-10-15 00:00:00	Wednesday	Wed
5018	19980423	1998-04-23	1998-04-23 00:00:00	Thursday 	Thu
5019	19971208	1997-12-08	1997-12-08 00:00:00	Monday   	Mon
5020	19970402	1997-04-02	1997-04-02 00:00:00	Wednesday	Wed
5021	19980415	1998-04-15	1998-04-15 00:00:00	Wednesday	Wed
5022	19970522	1997-05-22	1997-05-22 00:00:00	Thursday 	Thu
5023	19970328	1997-03-28	1997-03-28 00:00:00	Friday   	Fri
5024	19980406	1998-04-06	1998-04-06 00:00:00	Monday   	Mon
5025	19970203	1997-02-03	1997-02-03 00:00:00	Monday   	Mon
5026	19970429	1997-04-29	1997-04-29 00:00:00	Tuesday  	Tue
5027	19980311	1998-03-11	1998-03-11 00:00:00	Wednesday	Wed
5028	19961025	1996-10-25	1996-10-25 00:00:00	Friday   	Fri
5029	19971024	1997-10-24	1997-10-24 00:00:00	Friday   	Fri
5030	19970909	1997-09-09	1997-09-09 00:00:00	Tuesday  	Tue
5031	19970122	1997-01-22	1997-01-22 00:00:00	Wednesday	Wed
5032	19970219	1997-02-19	1997-02-19 00:00:00	Wednesday	Wed
5033	19971125	1997-11-25	1997-11-25 00:00:00	Tuesday  	Tue
5034	19970207	1997-02-07	1997-02-07 00:00:00	Friday   	Fri
5035	19970110	1997-01-10	1997-01-10 00:00:00	Friday   	Fri
5036	19970303	1997-03-03	1997-03-03 00:00:00	Monday   	Mon
5037	19961227	1996-12-27	1996-12-27 00:00:00	Friday   	Fri
5038	19970829	1997-08-29	1997-08-29 00:00:00	Friday   	Fri
5039	19970704	1997-07-04	1997-07-04 00:00:00	Friday   	Fri
5040	19970407	1997-04-07	1997-04-07 00:00:00	Monday   	Mon
5041	19960911	1996-09-11	1996-09-11 00:00:00	Wednesday	Wed
5042	19980319	1998-03-19	1998-03-19 00:00:00	Thursday 	Thu
5043	19960925	1996-09-25	1996-09-25 00:00:00	Wednesday	Wed
5044	19980429	1998-04-29	1998-04-29 00:00:00	Wednesday	Wed
5045	19970404	1997-04-04	1997-04-04 00:00:00	Friday   	Fri
5046	19970827	1997-08-27	1997-08-27 00:00:00	Wednesday	Wed
5047	19980109	1998-01-09	1998-01-09 00:00:00	Friday   	Fri
5048	19970226	1997-02-26	1997-02-26 00:00:00	Wednesday	Wed
5049	19970103	1997-01-03	1997-01-03 00:00:00	Friday   	Fri
5050	19980220	1998-02-20	1998-02-20 00:00:00	Friday   	Fri
5051	19971107	1997-11-07	1997-11-07 00:00:00	Friday   	Fri
5052	19960806	1996-08-06	1996-08-06 00:00:00	Tuesday  	Tue
5053	19970409	1997-04-09	1997-04-09 00:00:00	Wednesday	Wed
5054	19961223	1996-12-23	1996-12-23 00:00:00	Monday   	Mon
5055	19971105	1997-11-05	1997-11-05 00:00:00	Wednesday	Wed
5056	19971219	1997-12-19	1997-12-19 00:00:00	Friday   	Fri
5057	19970701	1997-07-01	1997-07-01 00:00:00	Tuesday  	Tue
5058	19970801	1997-08-01	1997-08-01 00:00:00	Friday   	Fri
5059	19980414	1998-04-14	1998-04-14 00:00:00	Tuesday  	Tue
5060	19980506	1998-05-06	1998-05-06 00:00:00	Wednesday	Wed
5061	19970403	1997-04-03	1997-04-03 00:00:00	Thursday 	Thu
5062	19980326	1998-03-26	1998-03-26 00:00:00	Thursday 	Thu
5063	19970718	1997-07-18	1997-07-18 00:00:00	Friday   	Fri
5064	19970204	1997-02-04	1997-02-04 00:00:00	Tuesday  	Tue
5065	19961104	1996-11-04	1996-11-04 00:00:00	Monday   	Mon
5066	19970618	1997-06-18	1997-06-18 00:00:00	Wednesday	Wed
5067	19971111	1997-11-11	1997-11-11 00:00:00	Tuesday  	Tue
5068	19970818	1997-08-18	1997-08-18 00:00:00	Monday   	Mon
5069	19961127	1996-11-27	1996-11-27 00:00:00	Wednesday	Wed
5070	19970926	1997-09-26	1997-09-26 00:00:00	Friday   	Fri
5071	19961211	1996-12-11	1996-12-11 00:00:00	Wednesday	Wed
5072	19970902	1997-09-02	1997-09-02 00:00:00	Tuesday  	Tue
5073	19970915	1997-09-15	1997-09-15 00:00:00	Monday   	Mon
5074	19960828	1996-08-28	1996-08-28 00:00:00	Wednesday	Wed
5075	19960913	1996-09-13	1996-09-13 00:00:00	Friday   	Fri
5076	19970826	1997-08-26	1997-08-26 00:00:00	Tuesday  	Tue
5077	19980303	1998-03-03	1998-03-03 00:00:00	Tuesday  	Tue
5078	19970711	1997-07-11	1997-07-11 00:00:00	Friday   	Fri
5079	19960729	1996-07-29	1996-07-29 00:00:00	Monday   	Mon
5080	19980211	1998-02-11	1998-02-11 00:00:00	Wednesday	Wed
5081	19970206	1997-02-06	1997-02-06 00:00:00	Thursday 	Thu
5082	19971104	1997-11-04	1997-11-04 00:00:00	Tuesday  	Tue
5083	19961014	1996-10-14	1996-10-14 00:00:00	Monday   	Mon
5084	19970804	1997-08-04	1997-08-04 00:00:00	Monday   	Mon
5085	19971226	1997-12-26	1997-12-26 00:00:00	Friday   	Fri
5086	19980219	1998-02-19	1998-02-19 00:00:00	Thursday 	Thu
5087	19970606	1997-06-06	1997-06-06 00:00:00	Friday   	Fri
5088	19971117	1997-11-17	1997-11-17 00:00:00	Monday   	Mon
5089	19980422	1998-04-22	1998-04-22 00:00:00	Wednesday	Wed
5090	19961220	1996-12-20	1996-12-20 00:00:00	Friday   	Fri
5091	19980122	1998-01-22	1998-01-22 00:00:00	Thursday 	Thu
5092	19971001	1997-10-01	1997-10-01 00:00:00	Wednesday	Wed
5093	19960927	1996-09-27	1996-09-27 00:00:00	Friday   	Fri
5094	19980427	1998-04-27	1998-04-27 00:00:00	Monday   	Mon
5095	19971114	1997-11-14	1997-11-14 00:00:00	Friday   	Fri
5096	19970221	1997-02-21	1997-02-21 00:00:00	Friday   	Fri
5097	19970917	1997-09-17	1997-09-17 00:00:00	Wednesday	Wed
5098	19960902	1996-09-02	1996-09-02 00:00:00	Monday   	Mon
5099	19980430	1998-04-30	1998-04-30 00:00:00	Thursday 	Thu
5100	19980113	1998-01-13	1998-01-13 00:00:00	Tuesday  	Tue
5101	19970619	1997-06-19	1997-06-19 00:00:00	Thursday 	Thu
5102	19970213	1997-02-13	1997-02-13 00:00:00	Thursday 	Thu
5103	19980123	1998-01-23	1998-01-23 00:00:00	Friday   	Fri
5104	19970603	1997-06-03	1997-06-03 00:00:00	Tuesday  	Tue
5105	19970408	1997-04-08	1997-04-08 00:00:00	Tuesday  	Tue
5106	19970113	1997-01-13	1997-01-13 00:00:00	Monday   	Mon
5107	19970903	1997-09-03	1997-09-03 00:00:00	Wednesday	Wed
5108	19980119	1998-01-19	1998-01-19 00:00:00	Monday   	Mon
5109	19961023	1996-10-23	1996-10-23 00:00:00	Wednesday	Wed
5149	19980204	1998-02-04	1998-02-04 00:00:00	Wednesday	Wed
5150	19971231	1997-12-31	1997-12-31 00:00:00	Wednesday	Wed
5151	19961118	1996-11-18	1996-11-18 00:00:00	Monday   	Mon
5152	19980126	1998-01-26	1998-01-26 00:00:00	Monday   	Mon
5153	19980416	1998-04-16	1998-04-16 00:00:00	Thursday 	Thu
5154	19980205	1998-02-05	1998-02-05 00:00:00	Thursday 	Thu
\.


--
-- Data for Name: d_shipper; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.d_shipper (shipper_key, shipper_id, shipper_company_name, current_shipper_phone, previous_shipper_phone, effective_date, current_row_ind, audit_key) FROM stdin;
19	1	Speedy Express	(503) 555-9831	\N	\N	\N	\N
20	2	United Package	(503) 555-3199	\N	\N	\N	\N
21	3	Federal Shipping	(503) 555-9931	\N	\N	\N	\N
\.


--
-- Data for Name: f_order_line_item; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.f_order_line_item (order_transaction_key, order_date_key, required_date_key, customer_key, product_key, employee_key, item_unit_price, item_quantity, item_extended_amt, item_discount_amt, item_total_amt, audit_key) FROM stdin;
5813	3635	3010	363	996	41	34.80	5	\N	0.00	0.00	35
5814	3770	2675	354	975	42	53.00	40	\N	0.00	0.00	53
5815	3625	3031	307	989	40	21.05	15	\N	0.15	3.16	18
5816	3625	3031	357	989	39	21.05	20	\N	0.00	0.00	21
5817	3929	3057	349	984	40	34.00	40	\N	0.00	0.00	34
5818	3917	2991	307	973	39	20.00	40	\N	0.00	0.00	20
5819	3886	2984	287	998	41	10.00	21	\N	0.00	0.00	10
5820	3868	2955	341	983	45	55.00	30	\N	0.00	0.00	55
5821	3928	2700	361	1001	39	13.00	12	\N	0.00	0.00	13
5822	3694	2990	308	1001	40	13.00	15	\N	0.00	0.00	13
5823	3685	2726	293	956	37	32.00	6	\N	0.20	6.40	26
5824	3833	2674	286	961	40	26.00	1	\N	0.00	0.00	26
5825	3934	2675	328	994	40	15.00	21	\N	0.25	3.75	11
5826	3934	2675	334	959	40	18.00	20	\N	0.00	0.00	18
5827	3866	3088	338	980	44	38.00	2	\N	0.00	0.00	38
5828	3612	2706	293	998	45	10.00	36	\N	0.25	2.50	8
5829	3961	2812	297	965	42	9.65	25	\N	0.15	1.45	8
5830	3670	2976	280	994	38	15.00	20	\N	0.00	0.00	15
5831	3686	2797	360	936	39	38.00	12	\N	0.05	1.90	36
5832	4016	2953	298	1000	40	18.00	15	\N	0.15	2.70	15
5833	3960	2791	306	996	44	34.80	4	\N	0.00	0.00	35
5834	3622	2726	362	996	41	34.80	20	\N	0.05	1.74	33
5835	3969	2809	360	967	37	46.00	25	\N	0.00	0.00	46
5836	3969	2809	348	957	42	2.50	24	\N	0.00	0.00	3
5837	3787	2989	338	996	42	34.80	24	\N	0.00	0.00	35
5838	3982	2652	336	1000	39	18.00	33	\N	0.05	0.90	17
5839	3996	2841	358	996	42	34.80	7	\N	0.00	0.00	35
5840	3804	2743	322	983	37	55.00	6	\N	0.05	2.75	52
5841	3957	2976	353	937	44	6.00	10	\N	0.00	0.00	6
5842	3700	2797	325	986	38	49.30	12	\N	0.00	0.00	49
5843	3799	2712	278	997	44	15.00	25	\N	0.00	0.00	15
5844	3716	2884	317	941	44	39.00	15	\N	0.25	9.75	29
5845	3584	3041	278	999	38	7.75	30	\N	0.00	0.00	8
5846	3584	3078	342	959	40	18.00	4	\N	0.00	0.00	18
5847	3788	3007	342	981	40	19.50	2	\N	0.00	0.00	20
5848	4037	3079	319	996	39	34.80	3	\N	0.00	0.00	35
5849	3764	2967	317	991	40	14.00	5	\N	0.25	3.50	11
5850	3802	2665	336	977	37	32.80	36	\N	0.20	6.56	26
5851	3632	2775	336	986	44	49.30	40	\N	0.00	0.00	49
5852	3953	2969	340	970	44	12.00	15	\N	0.15	1.80	10
5853	4052	2671	339	992	40	12.50	3	\N	0.10	1.25	11
5854	3699	2835	284	988	43	33.25	9	\N	0.00	0.00	33
5855	3846	2761	288	1001	44	13.00	10	\N	0.00	0.00	13
5856	3846	2761	334	975	42	53.00	2	\N	0.10	5.30	48
5857	3758	3046	354	944	37	81.00	20	\N	0.00	0.00	81
5858	3857	2770	353	999	37	7.75	6	\N	0.00	0.00	8
5859	3959	2647	338	999	40	7.75	6	\N	0.00	0.00	8
5860	4027	2678	358	980	38	38.00	4	\N	0.00	0.00	38
5861	3876	2804	319	993	42	36.00	15	\N	0.00	0.00	36
5862	3593	2942	280	996	41	34.80	20	\N	0.00	0.00	35
5863	3599	2688	310	986	42	49.30	15	\N	0.00	0.00	49
5864	3849	2917	340	994	40	15.00	20	\N	0.00	0.00	15
5865	3806	3081	322	992	38	12.50	20	\N	0.00	0.00	13
5866	3806	3081	359	980	44	38.00	20	\N	0.00	0.00	38
5867	3661	2832	349	967	40	46.00	12	\N	0.00	0.00	46
5868	3741	2771	303	992	43	12.50	15	\N	0.10	1.25	11
5869	3728	2903	353	995	37	21.50	2	\N	0.00	0.00	22
5870	4012	2970	328	963	44	18.00	30	\N	0.10	1.80	16
5871	3950	3087	342	978	37	7.45	5	\N	0.00	0.00	7
5872	3781	2871	321	992	38	12.50	3	\N	0.00	0.00	13
5873	3837	2942	275	994	43	15.00	5	\N	0.00	0.00	15
5874	3705	2827	310	995	39	21.50	3	\N	0.00	0.00	22
5875	3785	2931	350	986	44	49.30	5	\N	0.00	0.00	49
5876	3785	2917	291	993	37	36.00	7	\N	0.00	0.00	36
5877	3642	2838	359	999	38	7.75	10	\N	0.00	0.00	8
5878	3604	2954	336	960	38	19.00	12	\N	0.00	0.00	19
5879	3744	2664	338	986	37	49.30	25	\N	0.10	4.93	44
5880	3611	2887	311	994	40	15.00	30	\N	0.00	0.00	15
5881	4023	3029	338	986	37	49.30	70	\N	0.00	0.00	49
5882	4039	2997	321	925	42	18.00	20	\N	0.00	0.00	18
5883	3854	2920	311	1000	44	18.00	6	\N	0.00	0.00	18
5884	3747	2817	353	1000	43	18.00	30	\N	0.00	0.00	18
5885	4044	2827	360	995	41	21.50	30	\N	0.00	0.00	22
5886	4044	2722	311	959	39	18.00	10	\N	0.00	0.00	18
5887	3922	3005	331	976	43	7.00	20	\N	0.00	0.00	7
5888	4014	3068	312	963	40	18.00	4	\N	0.00	0.00	18
5889	3871	2980	344	987	45	43.90	80	\N	0.15	6.59	37
5890	3834	2664	312	996	37	34.80	40	\N	0.00	0.00	35
5891	3912	3045	281	999	40	7.75	50	\N	0.00	0.00	8
5892	3706	2870	297	982	38	13.25	30	\N	0.20	2.65	11
5893	3763	2780	301	992	40	12.50	10	\N	0.00	0.00	13
5894	3896	2758	348	980	40	38.00	12	\N	0.05	1.90	36
5895	3692	2752	319	996	39	34.80	25	\N	0.15	5.22	30
5896	3692	3072	282	978	45	7.45	15	\N	0.00	0.00	7
5897	3639	2669	324	971	39	9.50	16	\N	0.20	1.90	8
5898	3683	2937	360	995	41	21.50	40	\N	0.10	2.15	19
5899	3875	2715	357	992	44	12.50	10	\N	0.00	0.00	13
5900	3942	2709	310	975	43	53.00	48	\N	0.20	10.60	42
5901	4032	2956	333	928	43	22.00	18	\N	0.10	2.20	20
5902	3903	2959	298	996	40	34.80	25	\N	0.00	0.00	35
5903	3735	3014	328	954	40	25.89	15	\N	0.00	0.00	26
5904	3721	2774	324	986	38	49.30	28	\N	0.00	0.00	49
5905	3923	2758	282	967	37	46.00	40	\N	0.05	2.30	44
5906	3923	2758	346	983	43	55.00	9	\N	0.15	8.25	47
5907	3636	2752	298	979	40	24.00	40	\N	0.20	4.80	19
5908	3812	2669	317	1000	40	18.00	15	\N	0.00	0.00	18
5909	3967	2966	362	932	40	40.00	70	\N	0.25	10.00	30
5910	3755	2873	336	966	38	14.00	9	\N	0.00	0.00	14
5911	3955	2928	338	980	39	38.00	20	\N	0.00	0.00	38
5912	3891	2916	294	999	40	7.75	6	\N	0.15	1.16	7
5913	3743	2697	359	947	40	9.00	25	\N	0.00	0.00	9
5914	3895	2857	348	978	43	7.45	24	\N	0.00	0.00	7
5915	3840	2958	314	993	42	36.00	18	\N	0.10	3.60	32
5916	3840	2958	293	989	37	21.05	10	\N	0.05	1.05	20
5917	3732	2758	301	978	39	7.45	20	\N	0.15	1.12	6
5918	3598	3075	332	962	43	263.50	50	\N	0.20	52.70	211
5919	3856	2894	331	953	44	123.79	4	\N	0.00	0.00	124
5920	3688	2905	277	981	42	19.50	25	\N	0.00	0.00	20
5921	3808	2983	359	993	42	36.00	20	\N	0.00	0.00	36
5922	4057	2928	319	984	37	34.00	8	\N	0.20	6.80	27
5923	3943	2926	314	960	41	19.00	20	\N	0.05	0.95	18
5924	3701	2689	345	984	41	34.00	80	\N	0.05	1.70	32
5925	3973	3096	280	978	40	7.45	28	\N	0.00	0.00	7
5926	3973	3096	336	984	37	34.00	55	\N	0.10	3.40	31
5927	3836	3059	282	978	39	7.45	24	\N	0.00	0.00	7
5928	3828	2986	290	1000	40	18.00	12	\N	0.00	0.00	18
5929	3828	3022	292	995	37	21.50	5	\N	0.00	0.00	22
5930	4008	3002	276	935	39	21.00	24	\N	0.00	0.00	21
5931	4034	2801	302	1001	44	13.00	5	\N	0.00	0.00	13
5932	4034	2861	356	1001	43	13.00	7	\N	0.00	0.00	13
5933	3703	3038	293	988	38	33.25	35	\N	0.10	3.33	30
5934	3897	2846	348	980	44	38.00	18	\N	0.25	9.50	29
5935	3852	2828	287	998	42	10.00	20	\N	0.15	1.50	9
5936	3852	2828	314	960	37	19.00	6	\N	0.20	3.80	15
5937	3921	2994	335	996	41	34.80	42	\N	0.25	8.70	26
5938	4048	2943	310	995	40	21.50	50	\N	0.20	4.30	17
5939	4048	2943	364	982	37	13.25	15	\N	0.00	0.00	13
5940	3652	3054	309	978	39	7.45	10	\N	0.00	0.00	7
5941	3944	2934	324	955	37	12.50	42	\N	0.05	0.63	12
5942	3944	2934	345	963	37	18.00	20	\N	0.15	2.70	15
5943	3867	3022	297	995	41	21.50	6	\N	0.00	0.00	22
5944	4009	2883	334	989	38	21.05	20	\N	0.10	2.11	19
5945	3666	2801	310	994	44	15.00	30	\N	0.00	0.00	15
5946	3666	2801	319	998	39	10.00	14	\N	0.00	0.00	10
5947	3914	3036	293	998	40	10.00	50	\N	0.00	0.00	10
5948	3712	2661	277	980	44	38.00	20	\N	0.00	0.00	38
5949	3712	2661	278	984	39	34.00	15	\N	0.00	0.00	34
5950	3681	2733	348	992	37	12.50	8	\N	0.20	2.50	10
5951	3680	2994	294	958	45	14.00	10	\N	0.00	0.00	14
5952	3680	2799	343	995	37	21.50	15	\N	0.00	0.00	22
5953	3795	2763	345	977	38	32.80	40	\N	0.00	0.00	33
5954	4020	2765	283	994	40	15.00	30	\N	0.00	0.00	15
5955	3751	2767	293	996	42	34.80	24	\N	0.10	3.48	31
5956	3751	2767	290	937	39	6.00	18	\N	0.00	0.00	6
5957	3714	2759	332	993	38	36.00	50	\N	0.00	0.00	36
5958	3966	3033	344	955	37	12.50	32	\N	0.00	0.00	13
5959	3966	3033	309	986	37	49.30	10	\N	0.00	0.00	49
5960	3890	2908	308	993	42	36.00	8	\N	0.00	0.00	36
5961	3739	3036	298	996	37	34.80	21	\N	0.00	0.00	35
5962	3739	2762	333	975	41	53.00	18	\N	0.15	7.95	45
5963	3879	2900	344	979	38	24.00	120	\N	0.10	2.40	22
5964	3640	2733	356	1001	44	13.00	14	\N	0.00	0.00	13
5965	3718	2813	292	973	37	20.00	30	\N	0.00	0.00	20
5966	3718	2813	338	995	37	21.50	60	\N	0.00	0.00	22
5967	3937	2656	293	987	44	43.90	65	\N	0.00	0.00	44
5968	3749	3004	293	972	40	12.75	70	\N	0.15	1.91	11
5969	3749	3004	322	973	38	20.00	30	\N	0.05	1.00	19
5970	3932	3025	320	927	37	10.00	50	\N	0.00	0.00	10
5971	3978	2807	335	964	43	18.40	2	\N	0.10	1.84	17
5972	3978	3067	329	995	38	21.50	15	\N	0.00	0.00	22
5973	3902	2826	296	986	44	49.30	35	\N	0.00	0.00	49
5974	3624	3086	327	945	39	10.00	12	\N	0.00	0.00	10
5975	3987	3035	283	983	39	55.00	16	\N	0.00	0.00	55
5976	3987	3035	283	983	45	55.00	9	\N	0.20	11.00	44
5977	3778	2998	360	938	44	23.25	20	\N	0.10	2.33	21
5978	3815	2925	314	1000	39	18.00	14	\N	0.00	0.00	18
5979	3815	2925	294	957	38	2.50	50	\N	0.00	0.00	3
5980	3850	2734	309	957	39	2.50	20	\N	0.00	0.00	3
5981	3605	2656	360	981	44	19.50	20	\N	0.00	0.00	20
5982	3605	2656	346	1001	40	13.00	35	\N	0.00	0.00	13
5983	3832	2864	336	998	40	10.00	15	\N	0.00	0.00	10
5984	3608	2885	341	993	40	36.00	20	\N	0.05	1.80	34
5985	3829	2807	361	997	39	15.00	20	\N	0.10	1.50	14
5986	3829	2930	334	1001	44	13.00	10	\N	0.15	1.95	11
5987	3984	3034	300	950	38	31.23	2	\N	0.00	0.00	31
5988	3672	3086	304	983	42	55.00	20	\N	0.00	0.00	55
5989	3672	2677	324	992	43	12.50	30	\N	0.20	2.50	10
5990	3830	2650	314	1000	42	18.00	20	\N	0.25	4.50	14
5991	3669	2820	302	988	40	33.25	7	\N	0.00	0.00	33
5992	3669	2820	332	938	40	23.25	35	\N	0.00	0.00	23
5993	3877	2860	339	970	43	12.00	20	\N	0.00	0.00	12
5994	3633	2680	310	987	39	43.90	35	\N	0.25	10.98	33
5995	3962	2656	293	983	40	55.00	70	\N	0.20	11.00	44
5996	3962	2656	283	971	40	9.50	30	\N	0.25	2.38	7
5997	3726	2864	348	978	39	7.45	40	\N	0.00	0.00	7
5998	3980	3037	333	980	39	38.00	28	\N	0.00	0.00	38
5999	3980	3037	297	1000	39	18.00	18	\N	0.15	2.70	15
6000	3754	2919	289	996	44	34.80	10	\N	0.00	0.00	35
6001	3865	2868	280	999	39	7.75	24	\N	0.10	0.78	7
6002	3865	2868	360	977	44	32.80	15	\N	0.00	0.00	33
6003	3762	2721	352	981	39	19.50	15	\N	0.20	3.90	16
6004	3986	2692	324	998	42	10.00	30	\N	0.00	0.00	10
6005	3723	2660	344	985	40	28.50	90	\N	0.15	4.28	24
6006	3723	2815	328	951	39	43.90	50	\N	0.00	0.00	44
6007	3679	2735	293	990	39	17.00	60	\N	0.00	0.00	17
6008	3589	2680	339	952	44	45.60	12	\N	0.00	0.00	46
6009	3589	2680	278	965	39	9.65	30	\N	0.00	0.00	10
6010	4029	2699	278	978	39	7.45	15	\N	0.00	0.00	7
6011	3893	2982	352	976	42	7.00	15	\N	0.10	0.70	6
6012	3893	2982	340	995	40	21.50	2	\N	0.00	0.00	22
6013	3663	3089	337	964	40	18.40	20	\N	0.00	0.00	18
6014	3629	2919	280	986	39	49.30	35	\N	0.00	0.00	49
6015	3985	2788	357	978	44	7.45	6	\N	0.20	1.49	6
6016	3985	2868	336	1001	40	13.00	55	\N	0.10	1.30	12
6017	4038	2882	344	968	44	19.45	100	\N	0.05	0.97	18
6018	4025	2808	277	994	37	15.00	25	\N	0.10	1.50	14
6019	4025	2808	314	970	40	12.00	10	\N	0.20	2.40	10
6020	3862	3040	360	995	44	21.50	30	\N	0.00	0.00	22
6021	3889	2659	312	973	44	20.00	21	\N	0.15	3.00	17
6022	3889	2892	312	983	38	55.00	36	\N	0.00	0.00	55
6023	3748	2863	349	995	43	21.50	50	\N	0.00	0.00	22
6024	3693	2975	357	996	40	34.80	40	\N	0.00	0.00	35
6025	3816	3021	297	999	44	7.75	4	\N	0.25	1.94	6
6026	3816	3021	319	979	37	24.00	60	\N	0.25	6.00	18
6027	3738	2725	289	947	38	9.00	21	\N	0.00	0.00	9
6028	3930	2805	349	966	41	14.00	50	\N	0.00	0.00	14
6029	3930	2805	301	984	40	34.00	20	\N	0.00	0.00	34
6030	3653	3018	356	974	37	16.25	25	\N	0.00	0.00	16
6031	3581	3065	288	970	40	12.00	5	\N	0.00	0.00	12
6032	3581	3065	322	949	44	14.00	12	\N	0.00	0.00	14
6033	3796	3049	312	967	39	46.00	15	\N	0.00	0.00	46
6034	3777	3040	362	968	37	19.45	2	\N	0.15	2.92	17
6035	3590	2659	282	988	40	33.25	8	\N	0.00	0.00	33
6036	3590	2659	284	980	38	38.00	20	\N	0.00	0.00	38
6037	4041	3058	345	975	44	53.00	18	\N	0.00	0.00	53
6038	4050	2975	311	995	37	21.50	12	\N	0.00	0.00	22
6039	4050	2921	331	999	41	7.75	10	\N	0.00	0.00	8
6040	3956	2875	349	1000	45	18.00	42	\N	0.15	2.70	15
6041	3765	3095	308	994	44	15.00	12	\N	0.00	0.00	15
6042	3765	3095	333	963	41	18.00	20	\N	0.25	4.50	14
6043	3677	2805	357	934	38	31.00	20	\N	0.05	1.55	29
6044	3618	2890	338	988	39	33.25	30	\N	0.00	0.00	33
6045	3660	2858	296	983	42	55.00	12	\N	0.00	0.00	55
6046	3660	2858	340	984	44	34.00	40	\N	0.00	0.00	34
6047	3630	2673	316	964	37	18.40	10	\N	0.00	0.00	18
6048	3634	2853	362	1001	43	13.00	30	\N	0.05	0.65	12
6049	3634	2853	284	975	39	53.00	3	\N	0.00	0.00	53
6050	3906	2659	320	994	40	15.00	60	\N	0.10	1.50	14
6051	3892	3100	308	998	37	10.00	16	\N	0.00	0.00	10
6052	3892	3100	335	978	38	7.45	24	\N	0.25	1.86	6
6053	3709	2985	298	997	44	15.00	20	\N	0.20	3.00	12
6054	3977	2778	332	940	42	17.45	18	\N	0.00	0.00	17
6055	3813	3009	308	999	43	7.75	36	\N	0.00	0.00	8
6056	3813	3009	301	1001	44	13.00	7	\N	0.15	1.95	11
6057	3628	3027	283	966	39	14.00	20	\N	0.05	0.70	13
6058	3976	2859	314	993	40	36.00	10	\N	0.10	3.60	32
6059	3976	2859	288	980	40	38.00	30	\N	0.00	0.00	38
6060	3753	2785	315	1001	39	13.00	5	\N	0.00	0.00	13
6061	3992	2816	354	955	43	12.50	20	\N	0.05	0.63	12
6062	3992	2816	317	1001	43	13.00	25	\N	0.00	0.00	13
6063	3989	2796	308	966	44	14.00	30	\N	0.00	0.00	14
6064	4031	2881	319	973	40	20.00	25	\N	0.00	0.00	20
6065	3997	2690	314	952	42	45.60	8	\N	0.05	2.28	43
6066	3997	2690	279	978	45	7.45	20	\N	0.00	0.00	7
6067	3924	2737	331	991	38	14.00	30	\N	0.00	0.00	14
6068	3657	2727	310	989	42	21.05	20	\N	0.00	0.00	21
6069	3657	2727	362	985	40	28.50	25	\N	0.00	0.00	29
6070	3771	2889	324	986	39	49.30	3	\N	0.00	0.00	49
6071	4056	2945	312	994	45	15.00	14	\N	0.10	1.50	14
6072	4056	2945	276	972	43	12.75	15	\N	0.15	1.91	11
6073	3905	2672	329	963	37	18.00	10	\N	0.00	0.00	18
6074	3887	2696	279	952	40	45.60	3	\N	0.00	0.00	46
6075	4036	2802	344	999	42	7.75	36	\N	0.10	0.78	7
6076	4036	2802	282	932	40	40.00	10	\N	0.15	6.00	34
6077	3883	2896	294	984	43	34.00	12	\N	0.15	5.10	29
6078	3689	2658	359	985	43	28.50	15	\N	0.20	5.70	23
6079	3689	2996	293	999	39	7.75	50	\N	0.00	0.00	8
6080	4022	2690	336	984	38	34.00	84	\N	0.15	5.10	29
6081	3713	3020	310	966	38	14.00	20	\N	0.00	0.00	14
6082	3713	3020	326	994	39	15.00	6	\N	0.00	0.00	15
6083	3838	2727	353	968	40	19.45	9	\N	0.00	0.00	19
6084	3968	2988	287	984	42	34.00	10	\N	0.05	1.70	32
6085	3733	2741	343	977	43	32.80	5	\N	0.00	0.00	33
6086	3733	2741	285	992	44	12.50	6	\N	0.00	0.00	13
6087	3888	2974	317	964	40	18.40	25	\N	0.20	3.68	15
6088	3617	2927	345	965	43	9.65	6	\N	0.10	0.97	9
6089	3617	2927	278	978	37	7.45	15	\N	0.00	0.00	7
6090	3859	2941	282	964	37	18.40	15	\N	0.10	1.84	17
6091	3848	2756	360	980	40	38.00	30	\N	0.15	5.70	32
6092	3848	2756	336	960	43	19.00	30	\N	0.10	1.90	17
6093	3901	2996	305	996	42	34.80	9	\N	0.00	0.00	35
6094	4045	2777	323	993	41	36.00	10	\N	0.00	0.00	36
6095	3591	2834	332	1000	39	18.00	50	\N	0.00	0.00	18
6096	3591	2834	327	983	43	55.00	2	\N	0.00	0.00	55
6097	3585	3092	292	990	43	17.00	24	\N	0.00	0.00	17
6098	3665	2850	297	997	44	15.00	24	\N	0.05	0.75	14
6099	3665	2850	317	978	44	7.45	10	\N	0.20	1.49	6
6100	3695	2744	276	983	40	55.00	15	\N	0.10	5.50	50
6101	3786	2719	317	984	39	34.00	35	\N	0.25	8.50	26
6102	3786	2974	341	997	37	15.00	9	\N	0.00	0.00	15
6103	4047	2723	284	996	45	34.80	1	\N	0.00	0.00	35
6104	3853	2909	284	973	42	20.00	6	\N	0.00	0.00	20
6105	3909	2952	336	992	39	12.50	35	\N	0.00	0.00	13
6106	3909	2952	307	995	38	21.50	9	\N	0.10	2.15	19
6107	3720	2963	312	978	37	7.45	24	\N	0.05	0.37	7
6108	3662	3069	319	947	44	9.00	70	\N	0.15	1.35	8
6109	3662	3069	321	991	40	14.00	7	\N	0.00	0.00	14
6110	3731	2655	316	935	44	21.00	10	\N	0.00	0.00	21
6111	4035	2912	357	986	37	49.30	40	\N	0.00	0.00	49
6112	4035	2912	345	960	39	19.00	60	\N	0.00	0.00	19
6113	3715	2950	352	965	39	9.65	14	\N	0.00	0.00	10
6114	3821	2744	336	975	41	53.00	48	\N	0.15	7.95	45
6115	3708	2683	303	985	43	28.50	10	\N	0.10	2.85	26
6116	3708	2849	301	968	40	19.45	40	\N	0.00	0.00	19
6117	3927	2971	308	999	38	7.75	30	\N	0.00	0.00	8
6118	3691	3074	360	959	38	18.00	6	\N	0.00	0.00	18
6119	3691	3074	329	1001	40	13.00	10	\N	0.05	0.65	12
6120	3601	2667	344	980	42	38.00	40	\N	0.20	7.60	30
6121	4030	2757	346	996	38	34.80	24	\N	0.00	0.00	35
6122	4030	2963	317	999	45	7.75	20	\N	0.00	0.00	8
6123	3614	2810	277	997	37	15.00	3	\N	0.00	0.00	15
6124	3872	2679	280	979	42	24.00	18	\N	0.05	1.20	23
6125	3768	3039	298	986	44	49.30	15	\N	0.25	12.33	37
6126	3768	3039	297	975	38	53.00	50	\N	0.00	0.00	53
6127	3881	2676	339	986	37	49.30	10	\N	0.10	4.93	44
6128	3822	2914	340	976	38	7.00	70	\N	0.00	0.00	7
6129	3822	2794	338	979	40	24.00	25	\N	0.05	1.20	23
6130	3811	2849	324	988	44	33.25	18	\N	0.10	3.33	30
6131	3582	2867	280	1000	45	18.00	10	\N	0.00	0.00	18
6132	3582	2867	310	983	37	55.00	40	\N	0.20	11.00	44
6133	3673	3084	302	934	39	31.00	5	\N	0.00	0.00	31
6134	3941	2936	338	1000	41	18.00	30	\N	0.00	0.00	18
6135	3947	2757	324	980	39	38.00	60	\N	0.05	1.90	36
6136	3947	2754	293	966	44	14.00	28	\N	0.15	2.10	12
6137	4005	2703	278	999	39	7.75	15	\N	0.10	0.78	7
6138	3775	2747	276	977	43	32.80	25	\N	0.00	0.00	33
6139	3775	2747	355	988	40	33.25	6	\N	0.00	0.00	33
6140	3916	3039	325	1000	41	18.00	10	\N	0.00	0.00	18
6141	3698	2676	353	968	39	19.45	21	\N	0.00	0.00	19
6142	3698	3090	355	1001	45	13.00	18	\N	0.00	0.00	13
6143	3722	2914	284	981	40	19.50	6	\N	0.00	0.00	20
6144	3791	2819	318	999	37	7.75	21	\N	0.00	0.00	8
6145	3707	2924	329	989	40	21.05	30	\N	0.05	1.05	20
6146	3707	2924	294	999	39	7.75	50	\N	0.20	1.55	6
6147	3757	2740	279	1000	39	18.00	14	\N	0.00	0.00	18
6148	4033	3026	360	993	38	36.00	10	\N	0.15	5.40	31
6149	4033	3026	280	955	40	12.50	50	\N	0.05	0.63	12
6150	4000	2754	361	971	43	9.50	15	\N	0.00	0.00	10
6151	3858	2668	339	976	45	7.00	4	\N	0.15	1.05	6
6152	3858	2668	334	1001	37	13.00	20	\N	0.00	0.00	13
6153	4040	2973	336	966	38	14.00	100	\N	0.20	2.80	11
6154	3988	3062	305	959	44	18.00	4	\N	0.00	0.00	18
6155	3789	3090	324	1001	40	13.00	60	\N	0.05	0.65	12
6156	3789	2949	356	978	37	7.45	50	\N	0.00	0.00	7
6157	3847	2729	317	950	39	31.23	5	\N	0.05	1.56	30
6158	3649	2695	317	1000	43	18.00	4	\N	0.20	3.60	14
6159	3649	2695	328	982	39	13.25	30	\N	0.00	0.00	13
6160	3894	2803	293	993	38	36.00	65	\N	0.25	9.00	27
6161	3760	2978	362	999	44	7.75	30	\N	0.20	1.55	6
6162	3760	2978	332	989	43	21.05	12	\N	0.20	4.21	17
6163	3687	2773	338	995	37	21.50	9	\N	0.00	0.00	22
6164	3602	3082	284	986	42	49.30	10	\N	0.00	0.00	49
6165	4054	2829	309	997	40	15.00	30	\N	0.00	0.00	15
6166	4054	3050	308	983	43	55.00	35	\N	0.00	0.00	55
6167	3595	2987	356	1001	44	13.00	5	\N	0.25	3.25	10
6168	4004	2701	344	973	44	20.00	25	\N	0.05	1.00	19
6169	4004	2701	301	1000	37	18.00	10	\N	0.10	1.80	16
6170	3939	3071	324	995	37	21.50	15	\N	0.05	1.08	20
6171	3919	2779	354	986	40	49.30	10	\N	0.20	9.86	39
6172	3919	2779	344	996	41	34.80	12	\N	0.00	0.00	35
6173	3861	2685	352	980	40	38.00	28	\N	0.00	0.00	38
6174	3926	2855	291	945	43	10.00	6	\N	0.00	0.00	10
6175	3820	2772	314	960	44	19.00	21	\N	0.25	4.75	14
6176	3820	2772	364	984	42	34.00	15	\N	0.00	0.00	34
6177	3981	3044	344	1000	37	18.00	80	\N	0.00	0.00	18
6178	3827	3082	308	999	40	7.75	40	\N	0.00	0.00	8
6179	3827	3082	279	963	44	18.00	5	\N	0.00	0.00	18
6180	3783	3050	363	979	38	24.00	5	\N	0.00	0.00	24
6181	3952	2795	305	995	37	21.50	15	\N	0.05	1.08	20
6182	3952	2795	305	983	40	55.00	30	\N	0.15	8.25	47
6183	4001	2904	324	992	37	12.50	15	\N	0.00	0.00	13
6184	3766	2923	324	946	39	21.00	40	\N	0.00	0.00	21
6185	3586	3076	315	976	38	7.00	5	\N	0.00	0.00	7
6186	3586	3076	311	995	40	21.50	15	\N	0.00	0.00	22
6187	3798	2662	340	992	40	12.50	18	\N	0.20	2.50	10
6188	3626	2682	298	959	44	18.00	30	\N	0.10	1.80	16
6189	3626	2682	351	968	40	19.45	10	\N	0.00	0.00	19
6190	3954	2836	275	984	39	34.00	10	\N	0.00	0.00	34
6191	3613	2947	278	995	37	21.50	20	\N	0.00	0.00	22
6192	3613	2793	344	997	44	15.00	35	\N	0.15	2.25	13
6193	3627	3032	280	925	40	18.00	25	\N	0.00	0.00	18
6194	3627	3032	303	988	40	33.25	9	\N	0.00	0.00	33
6195	3869	2818	312	1000	37	18.00	35	\N	0.00	0.00	18
6196	3958	2751	314	999	44	7.75	8	\N	0.10	0.78	7
6197	3958	2751	359	957	44	2.50	20	\N	0.05	0.13	2
6198	3800	2904	293	986	43	49.30	80	\N	0.15	7.40	42
6199	3800	2904	296	999	40	7.75	2	\N	0.00	0.00	8
6200	4007	3077	322	946	44	21.00	40	\N	0.00	0.00	21
6201	3839	2746	360	982	40	13.25	6	\N	0.00	0.00	13
6202	3839	2746	335	980	42	38.00	60	\N	0.05	1.90	36
6203	4043	2651	320	996	39	34.80	60	\N	0.00	0.00	35
6204	4043	2651	343	942	43	62.50	8	\N	0.00	0.00	63
6205	3885	2911	359	994	40	15.00	15	\N	0.25	3.75	11
6206	3835	2964	308	964	40	18.40	60	\N	0.00	0.00	18
6207	3835	2964	346	985	43	28.50	20	\N	0.20	5.70	23
6208	3990	2793	274	970	42	12.00	2	\N	0.25	3.00	9
6209	3990	2793	361	970	39	12.00	21	\N	0.10	1.20	11
6210	3759	2854	307	960	40	19.00	15	\N	0.00	0.00	19
6211	3746	2705	310	1001	45	13.00	35	\N	0.25	3.25	10
6212	3746	2818	334	963	40	18.00	20	\N	0.00	0.00	18
6213	3621	2915	340	948	41	4.50	15	\N	0.15	0.68	4
6214	3621	2750	323	996	41	34.80	15	\N	0.00	0.00	35
6215	3740	3073	294	978	41	7.45	30	\N	0.00	0.00	7
6216	3678	2769	359	946	44	21.00	20	\N	0.25	5.25	16
6217	3678	2769	304	966	40	14.00	20	\N	0.00	0.00	14
6218	4010	2811	298	984	37	34.00	20	\N	0.10	3.40	31
6219	4010	2811	278	978	41	7.45	6	\N	0.10	0.75	7
6220	3779	3099	339	965	37	9.65	20	\N	0.20	1.93	8
6221	3790	3053	305	971	42	9.50	6	\N	0.10	0.95	9
6222	3790	3053	344	984	38	34.00	30	\N	0.00	0.00	34
6223	3873	2886	336	1001	40	13.00	70	\N	0.05	0.65	12
6224	3873	2886	335	994	43	15.00	40	\N	0.05	0.75	14
6225	3696	3048	309	944	44	81.00	21	\N	0.00	0.00	81
6226	3983	2913	310	982	43	13.25	49	\N	0.20	2.65	11
6227	3983	2913	321	992	39	12.50	10	\N	0.00	0.00	13
6228	3860	2798	282	975	38	53.00	20	\N	0.05	2.65	50
6229	3860	2705	301	989	37	21.05	15	\N	0.15	3.16	18
6230	3825	2915	321	1000	37	18.00	10	\N	0.00	0.00	18
6231	3913	2782	341	989	43	21.05	10	\N	0.00	0.00	21
6232	3913	2782	293	995	43	21.50	14	\N	0.20	4.30	17
6233	4011	2962	359	988	37	33.25	15	\N	0.10	3.33	30
6234	4011	2962	346	960	38	19.00	30	\N	0.00	0.00	19
6235	3594	2732	298	999	40	7.75	25	\N	0.00	0.00	8
6236	4026	3013	299	989	37	21.05	12	\N	0.00	0.00	21
6237	4026	3099	278	995	45	21.50	12	\N	0.00	0.00	22
6238	3675	2922	363	967	38	46.00	6	\N	0.00	0.00	46
6239	3675	2922	311	947	40	9.00	5	\N	0.00	0.00	9
6240	3948	2760	298	982	41	13.25	30	\N	0.00	0.00	13
6241	3620	2708	353	968	38	19.45	21	\N	0.00	0.00	19
6242	3620	2708	276	957	37	2.50	8	\N	0.15	0.38	2
6243	3884	2878	344	978	43	7.45	30	\N	0.00	0.00	7
6244	3884	2878	280	983	44	55.00	12	\N	0.00	0.00	55
6245	3623	2670	328	966	37	14.00	40	\N	0.25	3.50	11
6246	3597	3000	305	988	39	33.25	28	\N	0.00	0.00	33
6247	3597	3000	276	999	39	7.75	30	\N	0.00	0.00	8
6248	3756	3030	291	976	38	7.00	9	\N	0.00	0.00	7
6249	3756	3030	329	984	39	34.00	30	\N	0.00	0.00	34
6250	3610	2962	304	971	40	9.50	15	\N	0.00	0.00	10
6251	3631	2848	332	950	38	31.23	15	\N	0.00	0.00	31
6252	3631	2848	310	960	45	19.00	6	\N	0.25	4.75	14
6253	4021	3013	356	958	40	14.00	14	\N	0.00	0.00	14
6254	4021	2880	278	925	37	18.00	35	\N	0.25	4.50	14
6255	3994	2907	307	1001	37	13.00	30	\N	0.25	3.25	10
6256	3664	2649	336	986	38	49.30	48	\N	0.00	0.00	49
6257	3664	2749	274	987	40	43.90	20	\N	0.00	0.00	44
6258	3745	2708	362	997	39	15.00	15	\N	0.15	2.25	13
6259	3745	2876	336	994	44	15.00	50	\N	0.00	0.00	15
6260	3918	2739	363	948	43	4.50	20	\N	0.00	0.00	5
6261	3801	2840	362	970	44	12.00	18	\N	0.00	0.00	12
6262	3801	3060	320	994	39	15.00	30	\N	0.25	3.75	11
6263	3920	2899	293	994	40	15.00	8	\N	0.05	0.75	14
6264	3920	2899	325	971	39	9.50	12	\N	0.00	0.00	10
6265	3841	3056	344	995	39	21.50	60	\N	0.20	4.30	17
6266	3702	2711	310	1000	42	18.00	35	\N	0.15	2.70	15
6267	3702	2847	274	1000	40	18.00	15	\N	0.00	0.00	18
6268	3588	3070	297	997	42	15.00	35	\N	0.00	0.00	15
6269	3588	3070	335	972	42	12.75	24	\N	0.00	0.00	13
6270	3972	2929	308	956	45	32.00	4	\N	0.00	0.00	32
6271	3925	2825	328	983	44	55.00	8	\N	0.00	0.00	55
6272	3925	2907	277	994	40	15.00	28	\N	0.15	2.25	13
6273	3603	2800	350	960	42	19.00	5	\N	0.00	0.00	19
6274	3603	2649	304	984	37	34.00	10	\N	0.00	0.00	34
6275	3803	3093	300	971	37	9.50	5	\N	0.00	0.00	10
6276	3900	2833	344	977	41	32.80	120	\N	0.00	0.00	33
6277	3900	2739	310	980	39	38.00	30	\N	0.00	0.00	38
6278	3784	2840	344	970	37	12.00	24	\N	0.00	0.00	12
6279	3784	2840	344	982	41	13.25	12	\N	0.25	3.31	10
6280	3724	2899	282	995	39	21.50	30	\N	0.00	0.00	22
6281	3736	2724	337	985	40	28.50	10	\N	0.00	0.00	29
6282	3736	2724	298	993	37	36.00	25	\N	0.05	1.80	34
6283	3805	2847	312	986	37	49.30	20	\N	0.00	0.00	49
6284	3805	2847	318	978	44	7.45	40	\N	0.25	1.86	6
6285	3648	3070	334	995	44	21.50	8	\N	0.00	0.00	22
6286	3659	2979	336	968	41	19.45	50	\N	0.05	0.97	18
6287	3659	2806	344	999	44	7.75	42	\N	0.00	0.00	8
6288	3915	2824	362	950	39	31.23	15	\N	0.00	0.00	31
6289	3915	2901	324	985	44	28.50	5	\N	0.00	0.00	29
6290	3596	2800	294	979	40	24.00	6	\N	0.00	0.00	24
6291	3898	3093	292	935	40	21.00	5	\N	0.00	0.00	21
6292	3898	3080	339	983	38	55.00	10	\N	0.05	2.75	52
6293	4017	2833	335	984	40	34.00	15	\N	0.00	0.00	34
6294	4017	2972	320	974	44	16.25	40	\N	0.00	0.00	16
6295	3998	2753	282	989	41	21.05	10	\N	0.05	1.05	20
6296	3668	2766	287	975	43	53.00	30	\N	0.05	2.65	50
6297	3668	2766	282	1000	39	18.00	20	\N	0.00	0.00	18
6298	3995	2710	278	976	37	7.00	25	\N	0.00	0.00	7
6299	3995	2710	304	1000	38	18.00	20	\N	0.00	0.00	18
6300	3607	3017	318	1001	42	13.00	2	\N	0.10	1.30	12
6301	4006	2748	310	999	45	7.75	20	\N	0.00	0.00	8
6302	4006	2748	358	965	38	9.65	12	\N	0.00	0.00	10
6303	3682	2806	347	940	38	17.45	3	\N	0.00	0.00	17
6304	3682	2806	358	976	39	7.00	18	\N	0.00	0.00	7
6305	3638	2901	362	980	40	38.00	14	\N	0.20	7.60	30
6306	3773	2800	277	926	40	19.00	15	\N	0.20	3.80	15
6307	3773	2755	283	996	39	34.80	35	\N	0.00	0.00	35
6308	3769	2888	277	970	37	12.00	28	\N	0.05	0.60	11
6309	3769	2888	356	964	42	18.40	50	\N	0.20	3.68	15
6310	3819	2972	336	996	45	34.80	7	\N	0.00	0.00	35
6311	3644	2738	287	993	37	36.00	40	\N	0.00	0.00	36
6312	3644	2738	332	993	42	36.00	30	\N	0.00	0.00	36
6313	4051	2845	344	980	39	38.00	28	\N	0.00	0.00	38
6314	4051	2845	311	1000	40	18.00	10	\N	0.00	0.00	18
6315	3583	3061	360	983	45	55.00	25	\N	0.15	8.25	47
6316	3880	2687	341	997	39	15.00	15	\N	0.00	0.00	15
6317	3880	2687	326	993	38	36.00	3	\N	0.00	0.00	36
6318	3737	2691	300	998	39	10.00	5	\N	0.00	0.00	10
6319	3737	2691	322	964	42	18.40	3	\N	0.00	0.00	18
6320	3711	2918	282	993	40	36.00	25	\N	0.25	9.00	27
6321	3637	2852	348	993	44	36.00	20	\N	0.20	7.20	29
6322	3637	2852	344	988	42	33.25	24	\N	0.00	0.00	33
6323	3851	3091	341	994	39	15.00	40	\N	0.00	0.00	15
6324	3851	3091	275	956	39	32.00	10	\N	0.00	0.00	32
6325	4013	2932	323	967	40	46.00	30	\N	0.25	11.50	35
6326	3641	3008	338	999	41	7.75	18	\N	0.00	0.00	8
6327	3641	3008	297	980	39	38.00	60	\N	0.00	0.00	38
6328	3826	2714	296	948	39	4.50	20	\N	0.00	0.00	5
6329	3826	2714	293	963	42	18.00	130	\N	0.10	1.80	16
6330	3606	2842	336	989	39	21.05	80	\N	0.10	2.11	19
6331	4058	3001	329	992	40	12.50	40	\N	0.00	0.00	13
6332	4058	3001	349	966	40	14.00	2	\N	0.00	0.00	14
6333	3975	3012	277	995	39	21.50	12	\N	0.00	0.00	22
6334	3975	3012	356	986	39	49.30	15	\N	0.00	0.00	49
6335	3824	2837	307	935	44	21.00	15	\N	0.25	5.25	16
6336	3855	2961	293	995	45	21.50	16	\N	0.00	0.00	22
6337	3855	2961	317	983	39	55.00	25	\N	0.00	0.00	55
6338	3911	2792	293	999	37	7.75	7	\N	0.20	1.55	6
6339	3911	2852	297	990	40	17.00	50	\N	0.00	0.00	17
6340	3600	3052	351	991	43	14.00	3	\N	0.00	0.00	14
6341	3904	2968	293	975	37	53.00	120	\N	0.05	2.65	50
6342	3904	2932	304	966	43	14.00	20	\N	0.20	2.80	11
6343	3951	2654	278	965	39	9.65	10	\N	0.00	0.00	10
6344	3951	2654	325	986	39	49.30	20	\N	0.00	0.00	49
6345	3951	3008	319	1001	38	13.00	15	\N	0.00	0.00	13
6346	3818	2960	360	998	38	10.00	35	\N	0.00	0.00	10
6347	3818	2960	285	955	45	12.50	1	\N	0.00	0.00	13
6348	3647	2993	307	962	40	263.50	5	\N	0.00	0.00	264
6349	3647	2993	322	996	40	34.80	30	\N	0.15	5.22	30
6350	3647	2993	306	999	37	7.75	10	\N	0.00	0.00	8
6351	3999	2731	335	999	44	7.75	42	\N	0.20	1.55	6
6352	3999	3001	314	953	38	123.79	20	\N	0.05	6.19	118
6353	3794	2663	336	999	37	7.75	40	\N	0.05	0.39	7
6354	3794	2663	296	992	37	12.50	18	\N	0.00	0.00	13
6355	3794	2663	304	980	42	38.00	20	\N	0.15	5.70	32
6356	4046	2965	298	965	42	9.65	20	\N	0.05	0.48	9
6357	4046	2965	364	992	37	12.50	15	\N	0.00	0.00	13
6358	3676	2977	277	976	39	7.00	8	\N	0.00	0.00	7
6359	3676	2977	334	978	42	7.45	6	\N	0.20	1.49	6
6360	3676	2977	293	941	44	39.00	35	\N	0.25	9.75	29
6361	3882	3097	308	993	39	36.00	24	\N	0.20	7.20	29
6362	3882	3097	290	935	43	21.00	20	\N	0.00	0.00	21
6363	4018	2657	311	996	38	34.80	10	\N	0.00	0.00	35
6364	4018	2844	312	983	45	55.00	25	\N	0.00	0.00	55
6365	4018	2657	345	978	37	7.45	7	\N	0.10	0.75	7
6366	3931	2716	281	953	40	123.79	20	\N	0.25	30.95	93
6367	3931	2716	346	986	40	49.30	5	\N	0.25	12.33	37
6368	3729	2768	361	983	40	55.00	15	\N	0.05	2.75	52
6369	3729	2768	345	973	42	20.00	4	\N	0.15	3.00	17
6370	3729	2768	350	962	38	263.50	10	\N	0.00	0.00	264
6371	4059	2707	357	998	39	10.00	15	\N	0.25	2.50	8
6372	4059	2707	300	964	40	18.40	1	\N	0.00	0.00	18
6373	3645	3003	333	1000	38	18.00	50	\N	0.15	2.70	15
6374	3645	3003	361	976	43	7.00	20	\N	0.00	0.00	7
6375	3645	3003	315	994	38	15.00	5	\N	0.00	0.00	15
6376	3965	2666	320	964	44	18.40	30	\N	0.00	0.00	18
6377	3965	2666	339	1001	41	13.00	20	\N	0.00	0.00	13
6378	3971	2999	340	970	37	12.00	35	\N	0.00	0.00	12
6379	3971	2999	357	985	39	28.50	30	\N	0.15	4.28	24
6380	3971	2999	344	957	38	2.50	16	\N	0.00	0.00	3
6381	3874	2948	305	986	40	49.30	20	\N	0.05	2.47	47
6382	3874	2965	312	986	39	49.30	25	\N	0.15	7.40	42
6383	3946	2713	322	965	43	9.65	20	\N	0.00	0.00	10
6384	3946	2713	285	999	38	7.75	20	\N	0.00	0.00	8
6385	3946	2713	338	980	39	38.00	30	\N	0.00	0.00	38
6386	3619	2718	348	975	37	53.00	6	\N	0.00	0.00	53
6387	3619	2718	355	994	42	15.00	6	\N	0.00	0.00	15
6388	3993	2844	319	1001	41	13.00	15	\N	0.10	1.30	12
6389	3993	2844	297	994	44	15.00	9	\N	0.00	0.00	15
6390	3993	2844	290	977	37	32.80	20	\N	0.00	0.00	33
6391	3704	2933	280	981	42	19.50	15	\N	0.00	0.00	20
6392	3704	2716	282	963	37	18.00	21	\N	0.00	0.00	18
6393	4028	2768	337	962	45	263.50	2	\N	0.00	0.00	264
6394	4028	2851	311	984	45	34.00	21	\N	0.00	0.00	34
6395	4028	2995	354	992	40	12.50	24	\N	0.00	0.00	13
6396	3945	3085	343	967	39	46.00	9	\N	0.00	0.00	46
6397	3945	3085	314	988	38	33.25	3	\N	0.00	0.00	33
6398	3963	2681	329	977	42	32.80	9	\N	0.10	3.28	30
6399	3963	2681	354	954	37	25.89	20	\N	0.05	1.29	25
6400	3963	2681	274	1001	37	13.00	2	\N	0.20	2.60	10
6401	3587	2764	293	988	43	33.25	30	\N	0.00	0.00	33
6402	3587	2764	278	1000	45	18.00	21	\N	0.25	4.50	14
6403	3780	2831	320	960	39	19.00	50	\N	0.25	4.75	14
6404	3780	2831	354	996	39	34.80	15	\N	0.10	3.48	31
6405	3780	2944	320	963	40	18.00	10	\N	0.20	3.60	14
6406	3949	2790	349	1001	41	13.00	15	\N	0.00	0.00	13
6407	3949	2790	353	994	37	15.00	12	\N	0.00	0.00	15
6408	3710	2895	357	975	40	53.00	4	\N	0.25	13.25	40
6409	3710	2895	332	946	44	21.00	35	\N	0.00	0.00	21
6410	3710	2713	336	988	44	33.25	48	\N	0.00	0.00	33
6411	3772	2789	349	998	38	10.00	20	\N	0.00	0.00	10
6412	3772	2718	344	995	40	21.50	55	\N	0.20	4.30	17
6413	3776	3055	289	933	43	97.00	3	\N	0.00	0.00	97
6414	3776	3055	312	950	45	31.23	18	\N	0.15	4.68	27
6415	3776	2951	357	994	37	15.00	30	\N	0.15	2.25	13
6416	3809	2839	340	983	41	55.00	42	\N	0.05	2.75	52
6417	3809	2933	338	986	44	49.30	50	\N	0.00	0.00	49
6418	3609	2995	279	942	45	62.50	10	\N	0.00	0.00	63
6419	3609	2995	293	937	39	6.00	65	\N	0.15	0.90	5
6420	3609	2995	328	989	39	21.05	15	\N	0.15	3.16	18
6421	4055	2784	276	966	39	14.00	20	\N	0.00	0.00	14
6422	4055	2784	278	953	44	123.79	10	\N	0.25	30.95	93
6423	3725	2869	313	994	38	15.00	4	\N	0.00	0.00	15
6424	3725	2869	298	988	37	33.25	30	\N	0.25	8.31	25
6425	3725	2869	299	1000	39	18.00	20	\N	0.00	0.00	18
6426	3782	3011	362	986	40	49.30	3	\N	0.00	0.00	49
6427	3782	2938	317	976	44	7.00	8	\N	0.00	0.00	7
6428	3964	2944	308	982	40	13.25	12	\N	0.15	1.99	11
6429	3964	2944	277	991	40	14.00	15	\N	0.00	0.00	14
6430	3964	2831	336	963	38	18.00	80	\N	0.05	0.90	17
6431	3697	3083	278	954	41	25.89	40	\N	0.25	6.47	19
6432	3697	2879	321	977	42	32.80	3	\N	0.00	0.00	33
6433	3807	2728	335	973	43	20.00	42	\N	0.10	2.00	18
6434	3807	2728	345	992	41	12.50	20	\N	0.00	0.00	13
6435	3807	2728	364	975	41	53.00	2	\N	0.00	0.00	53
6436	4060	2789	282	941	45	39.00	16	\N	0.05	1.95	37
6437	4060	2789	303	989	41	21.05	21	\N	0.05	1.05	20
6438	3878	2951	363	952	40	45.60	3	\N	0.00	0.00	46
6439	3878	2951	303	934	41	31.00	10	\N	0.00	0.00	31
6440	3878	2951	278	973	40	20.00	15	\N	0.00	0.00	20
6441	3684	2910	282	988	43	33.25	20	\N	0.00	0.00	33
6442	3684	2910	340	942	37	62.50	25	\N	0.00	0.00	63
6443	3650	2898	336	944	40	81.00	20	\N	0.05	4.05	77
6444	3650	2898	363	1000	39	18.00	10	\N	0.00	0.00	18
6445	3650	2856	297	994	43	15.00	50	\N	0.20	3.00	12
6446	3761	3028	285	997	40	15.00	10	\N	0.00	0.00	15
6447	3761	3028	344	978	40	7.45	32	\N	0.15	1.12	6
6448	4042	2872	321	948	44	4.50	8	\N	0.00	0.00	5
6449	4042	2872	318	989	40	21.05	12	\N	0.05	1.05	20
6450	4042	2872	349	1001	42	13.00	25	\N	0.00	0.00	13
6451	3831	2938	307	1001	37	13.00	40	\N	0.00	0.00	13
6452	3831	2938	302	949	44	14.00	5	\N	0.00	0.00	14
6453	3870	2693	303	992	37	12.50	18	\N	0.00	0.00	13
6454	3870	2693	338	962	45	263.50	40	\N	0.00	0.00	264
6455	3870	2693	291	965	43	9.65	14	\N	0.00	0.00	10
6456	3845	2879	317	954	43	25.89	15	\N	0.05	1.29	25
6457	3845	2879	323	983	40	55.00	40	\N	0.05	2.75	52
6458	3908	2893	312	960	45	19.00	20	\N	0.00	0.00	19
6459	3908	2893	344	999	37	7.75	120	\N	0.05	0.39	7
6460	3908	2893	293	984	39	34.00	100	\N	0.00	0.00	34
6461	3767	3043	323	980	43	38.00	16	\N	0.00	0.00	38
6462	3767	3043	310	954	39	25.89	36	\N	0.00	0.00	26
6463	3750	2684	327	937	40	6.00	5	\N	0.00	0.00	6
6464	3750	2684	319	963	41	18.00	8	\N	0.15	2.70	15
6465	3750	2684	361	994	37	15.00	3	\N	0.25	3.75	11
6466	3643	2823	308	995	40	21.50	30	\N	0.00	0.00	22
6467	3643	2823	297	986	37	49.30	6	\N	0.15	7.40	42
6468	3719	2856	307	992	39	12.50	20	\N	0.00	0.00	13
6469	3719	2856	362	986	39	49.30	35	\N	0.00	0.00	49
6470	3719	2856	361	925	45	18.00	20	\N	0.05	0.90	17
6471	3616	3028	364	985	40	28.50	15	\N	0.00	0.00	29
6472	3616	3006	347	999	42	7.75	14	\N	0.00	0.00	8
6473	3654	3066	339	976	40	7.00	14	\N	0.05	0.35	7
6474	3654	3066	343	965	37	9.65	5	\N	0.00	0.00	10
6475	3654	3066	363	985	37	28.50	5	\N	0.00	0.00	29
6476	3654	3066	303	991	39	14.00	15	\N	0.00	0.00	14
6477	3654	3066	310	953	38	123.79	60	\N	0.25	30.95	93
6478	3654	3066	335	982	40	13.25	15	\N	0.00	0.00	13
6479	3970	2781	335	995	42	21.50	25	\N	0.00	0.00	22
6480	3970	2781	353	978	38	7.45	10	\N	0.00	0.00	7
6481	3970	2781	337	981	37	19.50	20	\N	0.00	0.00	20
6482	3938	2736	342	984	40	34.00	10	\N	0.00	0.00	34
6483	3938	2736	283	984	39	34.00	25	\N	0.25	8.50	26
6484	3938	2736	320	964	38	18.40	20	\N	0.00	0.00	18
6485	4015	2906	277	974	40	16.25	24	\N	0.00	0.00	16
6486	4015	3063	356	987	37	43.90	40	\N	0.00	0.00	44
6487	4015	2906	307	948	41	4.50	35	\N	0.00	0.00	5
6488	4015	3063	314	991	43	14.00	24	\N	0.20	2.80	11
6489	3814	2874	278	999	39	7.75	6	\N	0.00	0.00	8
6490	3814	2874	307	976	39	7.00	12	\N	0.15	1.05	6
6491	3814	2874	275	996	40	34.80	10	\N	0.00	0.00	35
6492	3844	2783	313	1000	40	18.00	20	\N	0.00	0.00	18
6493	3844	2783	302	1000	37	18.00	5	\N	0.00	0.00	18
6494	3844	2783	298	1001	42	13.00	15	\N	0.00	0.00	13
6495	3940	2992	349	982	40	13.25	30	\N	0.20	2.65	11
6496	3940	2684	341	981	40	19.50	30	\N	0.00	0.00	20
6497	3940	2686	282	999	44	7.75	20	\N	0.10	0.78	7
6498	3940	2686	311	985	42	28.50	30	\N	0.00	0.00	29
6499	3674	3024	317	930	39	25.00	20	\N	0.00	0.00	25
6500	3674	3024	361	947	40	9.00	8	\N	0.25	2.25	7
6501	3674	3024	305	960	39	19.00	30	\N	0.20	3.80	15
6502	3667	2856	285	958	43	14.00	20	\N	0.00	0.00	14
6503	3667	2745	336	995	39	21.50	35	\N	0.25	5.38	16
6504	3667	2745	322	991	38	14.00	40	\N	0.15	2.10	12
6505	3734	2742	282	937	44	6.00	20	\N	0.00	0.00	6
6506	3734	2742	344	996	43	34.80	50	\N	0.00	0.00	35
6507	3734	2742	339	973	45	20.00	28	\N	0.00	0.00	20
6508	3734	2742	284	970	40	12.00	15	\N	0.00	0.00	12
6509	3655	3066	283	980	42	38.00	18	\N	0.00	0.00	38
6510	3655	2787	325	955	40	12.50	10	\N	0.00	0.00	13
6511	3655	2787	356	1001	37	13.00	40	\N	0.00	0.00	13
6512	3690	2939	284	983	39	55.00	4	\N	0.00	0.00	55
6513	3690	2939	303	979	39	24.00	4	\N	0.00	0.00	24
6514	3690	2939	283	986	38	49.30	60	\N	0.00	0.00	49
6515	3797	2702	322	928	37	22.00	5	\N	0.00	0.00	22
6516	3797	2648	341	999	45	7.75	50	\N	0.05	0.39	7
6517	3797	2648	274	952	37	45.60	2	\N	0.00	0.00	46
6518	3797	2736	277	955	45	12.50	50	\N	0.05	0.63	12
6519	3658	2822	320	984	41	34.00	24	\N	0.15	5.10	29
6520	3658	3063	297	999	44	7.75	12	\N	0.20	1.55	6
6521	3658	2822	279	975	42	53.00	8	\N	0.00	0.00	53
6522	3907	3019	308	988	44	33.25	8	\N	0.00	0.00	33
6523	3907	3019	327	996	43	34.80	5	\N	0.00	0.00	35
6524	3907	3047	304	999	42	7.75	20	\N	0.15	1.16	7
6525	3742	2783	308	965	39	9.65	24	\N	0.00	0.00	10
6526	3742	2717	335	1000	44	18.00	60	\N	0.00	0.00	18
6527	3742	2717	336	1000	44	18.00	44	\N	0.00	0.00	18
6528	3742	2717	301	984	45	34.00	2	\N	0.15	5.10	29
6529	3792	2992	347	993	39	36.00	10	\N	0.00	0.00	36
6530	3792	2992	328	975	42	53.00	16	\N	0.00	0.00	53
6531	3792	2992	287	986	40	49.30	12	\N	0.15	7.40	42
6532	3864	2935	352	973	38	20.00	40	\N	0.00	0.00	20
6533	3864	2935	293	988	37	33.25	4	\N	0.00	0.00	33
6534	3864	2935	288	970	37	12.00	9	\N	0.00	0.00	12
6535	3651	2745	281	976	45	7.00	40	\N	0.20	1.40	6
6536	3651	2843	299	953	38	123.79	14	\N	0.00	0.00	124
6537	3651	2843	313	957	40	2.50	7	\N	0.00	0.00	3
6538	3651	2843	313	999	42	7.75	10	\N	0.00	0.00	8
6539	3727	2742	348	987	39	43.90	10	\N	0.00	0.00	44
6540	3727	3094	283	999	37	7.75	10	\N	0.00	0.00	8
6541	3727	3064	308	952	37	45.60	20	\N	0.00	0.00	46
6542	4003	3016	297	987	44	43.90	20	\N	0.00	0.00	44
6543	4003	3016	323	968	45	19.45	6	\N	0.15	2.92	17
6544	4003	3016	293	987	44	43.90	35	\N	0.00	0.00	44
6545	3615	2830	297	999	40	7.75	40	\N	0.20	1.55	6
6546	3615	2940	307	962	37	263.50	60	\N	0.00	0.00	264
6547	3615	2940	283	967	38	46.00	9	\N	0.00	0.00	46
6548	3615	2940	344	981	38	19.50	15	\N	0.00	0.00	20
6549	3817	2648	344	960	37	19.00	40	\N	0.00	0.00	19
6550	3817	2648	310	956	38	32.00	35	\N	0.10	3.20	29
6551	3817	2648	327	1001	44	13.00	15	\N	0.00	0.00	13
6552	3671	2822	292	996	44	34.80	20	\N	0.00	0.00	35
6553	3671	2822	338	986	39	49.30	40	\N	0.10	4.93	44
6554	3671	2822	334	965	38	9.65	4	\N	0.00	0.00	10
6555	3656	2862	293	985	38	28.50	66	\N	0.15	4.28	24
6556	3656	3047	336	1000	37	18.00	90	\N	0.20	3.60	14
6557	3656	3047	350	996	37	34.80	2	\N	0.00	0.00	35
6558	3656	3047	297	965	43	9.65	35	\N	0.25	2.41	7
6559	3842	2717	356	983	38	55.00	18	\N	0.05	2.75	52
6560	3842	2653	331	984	37	34.00	4	\N	0.00	0.00	34
6561	3842	2653	336	966	40	14.00	40	\N	0.00	0.00	14
6562	3793	2821	319	976	44	7.00	20	\N	0.25	1.75	5
6563	3793	2992	364	999	44	7.75	30	\N	0.00	0.00	8
6564	3793	2698	329	1001	42	13.00	21	\N	0.05	0.65	12
6565	3979	2877	338	1001	38	13.00	30	\N	0.00	0.00	13
6566	3979	2877	297	979	38	24.00	6	\N	0.00	0.00	24
6567	3979	2877	344	979	40	24.00	40	\N	0.00	0.00	24
6568	3979	2877	351	976	39	7.00	10	\N	0.00	0.00	7
6569	3823	2897	323	1000	39	18.00	6	\N	0.00	0.00	18
6570	3823	2897	363	983	38	55.00	10	\N	0.00	0.00	55
6571	3823	2897	305	953	39	123.79	2	\N	0.25	30.95	93
6572	3592	3064	333	966	44	14.00	14	\N	0.00	0.00	14
6573	3592	3064	293	995	43	21.50	21	\N	0.00	0.00	22
6574	3592	3064	303	984	38	34.00	9	\N	0.00	0.00	34
6575	3843	2902	339	948	38	4.50	10	\N	0.00	0.00	5
6576	3843	2902	274	995	39	21.50	20	\N	0.00	0.00	22
6577	3843	3016	298	995	37	21.50	60	\N	0.05	1.08	20
6578	3843	2902	342	992	38	12.50	2	\N	0.00	0.00	13
6579	3935	2830	320	965	38	9.65	28	\N	0.10	0.97	9
6580	3935	2940	343	1001	38	13.00	18	\N	0.00	0.00	13
6581	3935	2830	277	960	45	19.00	16	\N	0.00	0.00	19
6582	4053	2786	293	994	45	15.00	30	\N	0.00	0.00	15
6583	4053	2786	321	980	40	38.00	5	\N	0.00	0.00	38
6584	4053	2786	337	973	42	20.00	2	\N	0.00	0.00	20
6585	4002	2891	329	934	38	31.00	24	\N	0.15	4.65	26
6586	4002	2891	336	996	39	34.80	35	\N	0.00	0.00	35
6587	4002	2891	307	993	45	36.00	30	\N	0.00	0.00	36
6588	4002	2822	284	967	37	46.00	30	\N	0.00	0.00	46
6589	3730	2862	292	995	40	21.50	50	\N	0.00	0.00	22
6590	3730	2862	360	937	42	6.00	20	\N	0.10	0.60	5
6591	3730	2862	300	975	40	53.00	10	\N	0.00	0.00	53
6592	3810	2814	283	986	37	49.30	21	\N	0.25	12.33	37
6593	3810	2814	312	983	38	55.00	24	\N	0.00	0.00	55
6594	3810	2814	287	987	40	43.90	12	\N	0.00	0.00	44
6595	3717	2821	344	983	43	55.00	100	\N	0.25	13.75	41
6596	3717	2821	344	995	42	21.50	16	\N	0.00	0.00	22
6597	3717	2821	362	983	38	55.00	30	\N	0.00	0.00	55
6598	3717	2821	341	993	43	36.00	36	\N	0.10	3.60	32
6599	3933	2704	328	985	44	28.50	6	\N	0.00	0.00	29
6600	3933	2694	349	978	38	7.45	10	\N	0.00	0.00	7
6601	3933	2694	290	983	44	55.00	30	\N	0.00	0.00	55
6602	3646	2730	303	994	43	15.00	4	\N	0.00	0.00	15
6603	3646	2730	349	995	37	21.50	30	\N	0.00	0.00	22
6604	3646	2730	320	981	37	19.50	28	\N	0.00	0.00	20
6605	4019	2946	305	945	40	10.00	20	\N	0.00	0.00	10
6606	4019	2946	287	987	39	43.90	30	\N	0.00	0.00	44
6607	4019	3064	288	985	38	28.50	4	\N	0.00	0.00	29
6608	4019	2946	347	935	41	21.00	10	\N	0.00	0.00	21
6609	3974	3098	364	986	40	49.30	12	\N	0.00	0.00	49
6610	3974	3098	283	975	42	53.00	24	\N	0.00	0.00	53
6611	3974	3098	359	959	44	18.00	18	\N	0.05	0.90	17
6612	3936	3015	292	929	43	21.35	30	\N	0.25	5.34	16
6613	3936	3015	283	992	43	12.50	42	\N	0.00	0.00	13
6614	3936	3015	304	936	39	38.00	4	\N	0.20	7.60	30
6615	4024	2776	297	1000	44	18.00	50	\N	0.10	1.80	16
6616	4024	2776	314	948	43	4.50	10	\N	0.20	0.90	4
6617	4024	2776	307	985	39	28.50	10	\N	0.20	5.70	23
6618	4024	2776	332	988	38	33.25	25	\N	0.20	6.65	27
6619	3863	2865	285	991	44	14.00	20	\N	0.00	0.00	14
6620	3863	2865	308	981	43	19.50	20	\N	0.00	0.00	20
6621	3863	2891	292	984	44	34.00	50	\N	0.00	0.00	34
6622	3991	2957	326	994	39	15.00	3	\N	0.00	0.00	15
6623	3991	2957	279	985	45	28.50	4	\N	0.00	0.00	29
6624	3991	3051	340	984	38	34.00	35	\N	0.00	0.00	34
6625	3774	3042	300	1001	38	13.00	10	\N	0.00	0.00	13
6626	3774	2866	305	984	40	34.00	15	\N	0.00	0.00	34
6627	3774	3042	339	994	40	15.00	12	\N	0.20	3.00	12
6628	3774	3042	310	965	39	9.65	30	\N	0.10	0.97	9
6629	4049	2981	344	992	37	12.50	55	\N	0.00	0.00	13
6630	4049	2981	319	978	44	7.45	20	\N	0.25	1.86	6
6631	4049	2981	362	958	43	14.00	35	\N	0.00	0.00	14
6632	3899	2694	290	965	37	9.65	9	\N	0.00	0.00	10
6633	3899	2704	335	1001	44	13.00	28	\N	0.15	1.95	11
6634	3899	2704	353	963	37	18.00	20	\N	0.00	0.00	18
6635	3910	2720	317	955	38	12.50	20	\N	0.00	0.00	13
6636	3910	2720	319	937	37	6.00	10	\N	0.05	0.30	6
6637	3910	2720	293	988	40	33.25	130	\N	0.00	0.00	33
6638	3910	2720	331	948	38	4.50	20	\N	0.00	0.00	5
6639	3752	3023	346	940	43	17.45	14	\N	0.05	0.87	17
6640	3752	3023	341	1000	44	18.00	2	\N	0.15	2.70	15
6641	3752	3023	282	943	40	9.20	10	\N	0.25	2.30	7
6642	3752	3023	338	1001	37	13.00	2	\N	0.00	0.00	13
\.


--
-- Data for Name: f_order_transaction; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.f_order_transaction (order_transaction_key, order_date_key, required_date_key, customer_key, employee_key, order_nbr_of_products, order_extended_amt, order_discount_amt, order_total_amt, audit_key) FROM stdin;
6470	3719	2856	361	45	0	\N	0.05	17.10	\N
6254	4021	2880	278	37	0	\N	0.25	13.50	\N
6193	3627	3032	280	40	0	\N	0.00	18.00	\N
5882	4039	2997	321	42	0	\N	0.00	18.00	\N
6306	3773	2800	277	40	40	\N	0.20	15.20	\N
5970	3932	3025	320	37	70	\N	0.00	10.00	\N
6515	3797	2702	322	37	0	\N	0.00	22.00	\N
5901	4032	2956	333	43	0	\N	0.10	19.80	\N
6612	3936	3015	292	43	0	\N	0.25	16.01	\N
6499	3674	3024	317	39	0	\N	0.00	25.00	\N
6076	4036	2802	282	40	0	\N	0.15	34.00	\N
5909	3967	2966	362	40	0	\N	0.25	30.00	\N
6413	3776	3055	289	43	0	\N	0.00	97.00	\N
6585	4002	2891	329	38	0	\N	0.15	26.35	\N
6439	3878	2951	303	41	0	\N	0.00	31.00	\N
6133	3673	3084	302	39	0	\N	0.00	31.00	\N
6043	3677	2805	357	38	0	\N	0.05	29.45	\N
6608	4019	2946	347	41	30	\N	0.00	21.00	\N
6362	3882	3097	290	43	30	\N	0.00	21.00	\N
6335	3824	2837	307	44	30	\N	0.25	15.75	\N
6291	3898	3093	292	40	30	\N	0.00	21.00	\N
6110	3731	2655	316	44	30	\N	0.00	21.00	\N
5930	4008	3002	276	39	30	\N	0.00	21.00	\N
6614	3936	3015	304	39	0	\N	0.20	30.40	\N
5831	3686	2797	360	39	0	\N	0.05	36.10	\N
6636	3910	2720	319	37	0	\N	0.05	5.70	\N
6590	3730	2862	360	42	0	\N	0.10	5.40	\N
6505	3734	2742	282	44	0	\N	0.00	6.00	\N
6463	3750	2684	327	40	0	\N	0.00	6.00	\N
6419	3609	2995	293	39	0	\N	0.15	5.10	\N
5956	3751	2767	290	39	0	\N	0.00	6.00	\N
5841	3957	2976	353	44	0	\N	0.00	6.00	\N
5992	3669	2820	332	40	0	\N	0.00	23.25	\N
5977	3778	2998	360	44	0	\N	0.10	20.93	\N
6639	3752	3023	346	43	0	\N	0.05	16.58	\N
6303	3682	2806	347	38	0	\N	0.00	17.45	\N
6054	3977	2778	332	42	0	\N	0.00	17.45	\N
6436	4060	2789	282	45	0	\N	0.05	37.05	\N
6360	3676	2977	293	44	0	\N	0.25	29.25	\N
5844	3716	2884	317	44	0	\N	0.25	29.25	\N
6442	3684	2910	340	37	0	\N	0.00	62.50	\N
6418	3609	2995	279	45	0	\N	0.00	62.50	\N
6204	4043	2651	343	43	0	\N	0.00	62.50	\N
6641	3752	3023	282	40	0	\N	0.25	6.90	\N
6443	3650	2898	336	40	0	\N	0.05	76.95	\N
6225	3696	3048	309	44	0	\N	0.00	81.00	\N
5857	3758	3046	354	37	0	\N	0.00	81.00	\N
6605	4019	2946	305	40	40	\N	0.00	10.00	\N
6174	3926	2855	291	43	40	\N	0.00	10.00	\N
5974	3624	3086	327	39	40	\N	0.00	10.00	\N
6409	3710	2895	332	44	0	\N	0.00	21.00	\N
6216	3678	2769	359	44	0	\N	0.25	15.75	\N
6200	4007	3077	322	44	0	\N	0.00	21.00	\N
6184	3766	2923	324	39	0	\N	0.00	21.00	\N
6500	3674	3024	361	40	0	\N	0.25	6.75	\N
6239	3675	2922	311	40	0	\N	0.00	9.00	\N
6108	3662	3069	319	44	0	\N	0.15	7.65	\N
6027	3738	2725	289	38	0	\N	0.00	9.00	\N
5913	3743	2697	359	40	0	\N	0.00	9.00	\N
6638	3910	2720	331	38	0	\N	0.00	4.50	\N
6616	4024	2776	314	43	0	\N	0.20	3.60	\N
6575	3843	2902	339	38	0	\N	0.00	4.50	\N
6487	4015	2906	307	41	0	\N	0.00	4.50	\N
6448	4042	2872	321	44	0	\N	0.00	4.50	\N
6328	3826	2714	296	39	0	\N	0.00	4.50	\N
6260	3918	2739	363	43	0	\N	0.00	4.50	\N
6213	3621	2915	340	41	0	\N	0.15	3.83	\N
6452	3831	2938	302	44	0	\N	0.00	14.00	\N
6032	3581	3065	322	44	0	\N	0.00	14.00	\N
6414	3776	3055	312	45	0	\N	0.15	26.55	\N
6288	3915	2824	362	39	0	\N	0.00	31.23	\N
6251	3631	2848	332	38	0	\N	0.00	31.23	\N
6157	3847	2729	317	39	0	\N	0.05	29.67	\N
5987	3984	3034	300	38	0	\N	0.00	31.23	\N
6006	3723	2815	328	39	0	\N	0.00	43.90	\N
6541	3727	3064	308	37	0	\N	0.00	45.60	\N
6517	3797	2648	274	37	0	\N	0.00	45.60	\N
6438	3878	2951	363	40	0	\N	0.00	45.60	\N
6074	3887	2696	279	40	0	\N	0.00	45.60	\N
6065	3997	2690	314	42	0	\N	0.05	43.32	\N
6008	3589	2680	339	44	0	\N	0.00	45.60	\N
6571	3823	2897	305	39	0	\N	0.25	92.84	\N
6536	3651	2843	299	38	0	\N	0.00	123.79	\N
6477	3654	3066	310	38	0	\N	0.25	92.84	\N
6422	4055	2784	278	44	0	\N	0.25	92.84	\N
6366	3931	2716	281	40	0	\N	0.25	92.84	\N
6352	3999	3001	314	38	0	\N	0.05	117.60	\N
5919	3856	2894	331	44	0	\N	0.00	123.79	\N
6462	3767	3043	310	39	0	\N	0.00	25.89	\N
6456	3845	2879	317	43	0	\N	0.05	24.60	\N
6431	3697	3083	278	41	0	\N	0.25	19.42	\N
6399	3963	2681	354	37	0	\N	0.05	24.60	\N
5903	3735	3014	328	40	0	\N	0.00	25.89	\N
6635	3910	2720	317	38	70	\N	0.00	12.50	\N
6518	3797	2736	277	45	70	\N	0.05	11.88	\N
6510	3655	2787	325	40	70	\N	0.00	12.50	\N
6347	3818	2960	285	45	70	\N	0.00	12.50	\N
6149	4033	3026	280	40	70	\N	0.05	11.88	\N
6061	3992	2816	354	43	70	\N	0.05	11.88	\N
5958	3966	3033	344	37	70	\N	0.00	12.50	\N
5941	3944	2934	324	37	70	\N	0.05	11.88	\N
6550	3817	2648	310	38	40	\N	0.10	28.80	\N
6324	3851	3091	275	39	40	\N	0.00	32.00	\N
6270	3972	2929	308	45	40	\N	0.00	32.00	\N
5823	3685	2726	293	37	40	\N	0.20	25.60	\N
6537	3651	2843	313	40	0	\N	0.00	2.50	\N
6380	3971	2999	344	38	0	\N	0.00	2.50	\N
6242	3620	2708	276	37	0	\N	0.15	2.13	\N
6197	3958	2751	359	44	0	\N	0.05	2.38	\N
5980	3850	2734	309	39	0	\N	0.00	2.50	\N
5979	3815	2925	294	38	0	\N	0.00	2.50	\N
5836	3969	2809	348	42	0	\N	0.00	2.50	\N
6631	4049	2981	362	43	0	\N	0.00	14.00	\N
6502	3667	2856	285	43	0	\N	0.00	14.00	\N
6253	4021	3013	356	40	0	\N	0.00	14.00	\N
5951	3680	2994	294	45	0	\N	0.00	14.00	\N
6611	3974	3098	359	44	0	\N	0.05	17.10	\N
6188	3626	2682	298	44	0	\N	0.10	16.20	\N
6154	3988	3062	305	44	0	\N	0.00	18.00	\N
6118	3691	3074	360	38	0	\N	0.00	18.00	\N
5886	4044	2722	311	39	0	\N	0.00	18.00	\N
5846	3584	3078	342	40	0	\N	0.00	18.00	\N
5826	3934	2675	334	40	0	\N	0.00	18.00	\N
6581	3935	2830	277	45	0	\N	0.00	19.00	\N
6549	3817	2648	344	37	0	\N	0.00	19.00	\N
6501	3674	3024	305	39	0	\N	0.20	15.20	\N
6458	3908	2893	312	45	0	\N	0.00	19.00	\N
6403	3780	2831	320	39	0	\N	0.25	14.25	\N
6273	3603	2800	350	42	0	\N	0.00	19.00	\N
6252	3631	2848	310	45	0	\N	0.25	14.25	\N
6234	4011	2962	346	38	0	\N	0.00	19.00	\N
6210	3759	2854	307	40	0	\N	0.00	19.00	\N
6175	3820	2772	314	44	0	\N	0.25	14.25	\N
6112	4035	2912	345	39	0	\N	0.00	19.00	\N
6092	3848	2756	336	43	0	\N	0.10	17.10	\N
5936	3852	2828	314	37	0	\N	0.20	15.20	\N
5923	3943	2926	314	41	0	\N	0.05	18.05	\N
5878	3604	2954	336	38	0	\N	0.00	19.00	\N
5824	3833	2674	286	40	50	\N	0.00	26.00	\N
6546	3615	2940	307	37	0	\N	0.00	263.50	\N
6454	3870	2693	338	45	0	\N	0.00	263.50	\N
6393	4028	2768	337	45	0	\N	0.00	263.50	\N
6370	3729	2768	350	38	0	\N	0.00	263.50	\N
6348	3647	2993	307	40	0	\N	0.00	263.50	\N
5918	3598	3075	332	43	0	\N	0.20	210.80	\N
6634	3899	2704	353	37	0	\N	0.00	18.00	\N
6464	3750	2684	319	41	0	\N	0.15	15.30	\N
6430	3964	2831	336	38	0	\N	0.05	17.10	\N
6405	3780	2944	320	40	0	\N	0.20	14.40	\N
6392	3704	2716	282	37	0	\N	0.00	18.00	\N
6329	3826	2714	293	42	0	\N	0.10	16.20	\N
6212	3746	2818	334	40	0	\N	0.00	18.00	\N
6179	3827	3082	279	44	0	\N	0.00	18.00	\N
6073	3905	2672	329	37	0	\N	0.00	18.00	\N
6042	3765	3095	333	41	0	\N	0.25	13.50	\N
5942	3944	2934	345	37	0	\N	0.15	15.30	\N
5888	4014	3068	312	40	0	\N	0.00	18.00	\N
5870	4012	2970	328	44	0	\N	0.10	16.20	\N
6484	3938	2736	320	38	0	\N	0.00	18.40	\N
6376	3965	2666	320	44	0	\N	0.00	18.40	\N
6372	4059	2707	300	40	0	\N	0.00	18.40	\N
6319	3737	2691	322	42	0	\N	0.00	18.40	\N
6309	3769	2888	356	42	0	\N	0.20	14.72	\N
6206	3835	2964	308	40	0	\N	0.00	18.40	\N
6090	3859	2941	282	37	0	\N	0.10	16.56	\N
6087	3888	2974	317	40	0	\N	0.20	14.72	\N
6047	3630	2673	316	37	0	\N	0.00	18.40	\N
6013	3663	3089	337	40	0	\N	0.00	18.40	\N
5971	3978	2807	335	43	0	\N	0.10	16.56	\N
6632	3899	2694	290	37	0	\N	0.00	9.65	\N
6628	3774	3042	310	39	0	\N	0.10	8.69	\N
6579	3935	2830	320	38	0	\N	0.10	8.69	\N
6558	3656	3047	297	43	0	\N	0.25	7.24	\N
6554	3671	2822	334	38	0	\N	0.00	9.65	\N
6525	3742	2783	308	39	0	\N	0.00	9.65	\N
6474	3654	3066	343	37	0	\N	0.00	9.65	\N
6455	3870	2693	291	43	0	\N	0.00	9.65	\N
6383	3946	2713	322	43	0	\N	0.00	9.65	\N
6356	4046	2965	298	42	0	\N	0.05	9.17	\N
6343	3951	2654	278	39	0	\N	0.00	9.65	\N
6302	4006	2748	358	38	0	\N	0.00	9.65	\N
6220	3779	3099	339	37	0	\N	0.20	7.72	\N
6113	3715	2950	352	39	0	\N	0.00	9.65	\N
6088	3617	2927	345	43	0	\N	0.10	8.69	\N
6009	3589	2680	278	39	0	\N	0.00	9.65	\N
5829	3961	2812	297	42	0	\N	0.15	8.20	\N
6572	3592	3064	333	44	0	\N	0.00	14.00	\N
6561	3842	2653	336	40	0	\N	0.00	14.00	\N
6421	4055	2784	276	39	0	\N	0.00	14.00	\N
6342	3904	2932	304	43	0	\N	0.20	11.20	\N
6332	4058	3001	349	40	0	\N	0.00	14.00	\N
6245	3623	2670	328	37	0	\N	0.25	10.50	\N
6217	3678	2769	304	40	0	\N	0.00	14.00	\N
6153	4040	2973	336	38	0	\N	0.20	11.20	\N
6136	3947	2754	293	44	0	\N	0.15	11.90	\N
6081	3713	3020	310	38	0	\N	0.00	14.00	\N
6063	3989	2796	308	44	0	\N	0.00	14.00	\N
6057	3628	3027	283	39	0	\N	0.05	13.30	\N
6028	3930	2805	349	41	0	\N	0.00	14.00	\N
5910	3755	2873	336	38	0	\N	0.00	14.00	\N
6588	4002	2822	284	37	10	\N	0.00	46.00	\N
6547	3615	2940	283	38	10	\N	0.00	46.00	\N
6396	3945	3085	343	39	10	\N	0.00	46.00	\N
6325	4013	2932	323	40	10	\N	0.25	34.50	\N
6238	3675	2922	363	38	10	\N	0.00	46.00	\N
6033	3796	3049	312	39	10	\N	0.00	46.00	\N
5905	3923	2758	282	37	10	\N	0.05	43.70	\N
5867	3661	2832	349	40	10	\N	0.00	46.00	\N
5835	3969	2809	360	37	10	\N	0.00	46.00	\N
6543	4003	3016	323	45	0	\N	0.15	16.53	\N
6286	3659	2979	336	41	0	\N	0.05	18.48	\N
6241	3620	2708	353	38	0	\N	0.00	19.45	\N
6189	3626	2682	351	40	0	\N	0.00	19.45	\N
6141	3698	2676	353	39	0	\N	0.00	19.45	\N
6116	3708	2849	301	40	0	\N	0.00	19.45	\N
6083	3838	2727	353	40	0	\N	0.00	19.45	\N
6034	3777	3040	362	37	0	\N	0.15	16.53	\N
6017	4038	2882	344	44	0	\N	0.05	18.48	\N
6534	3864	2935	288	37	0	\N	0.00	12.00	\N
6508	3734	2742	284	40	0	\N	0.00	12.00	\N
6378	3971	2999	340	37	0	\N	0.00	12.00	\N
6308	3769	2888	277	37	0	\N	0.05	11.40	\N
6278	3784	2840	344	37	0	\N	0.00	12.00	\N
6261	3801	2840	362	44	0	\N	0.00	12.00	\N
6209	3990	2793	361	39	0	\N	0.10	10.80	\N
6208	3990	2793	274	42	0	\N	0.25	9.00	\N
6031	3581	3065	288	40	0	\N	0.00	12.00	\N
6019	4025	2808	314	40	0	\N	0.20	9.60	\N
5993	3877	2860	339	43	0	\N	0.00	12.00	\N
5852	3953	2969	340	44	0	\N	0.15	10.20	\N
6275	3803	3093	300	37	0	\N	0.00	9.50	\N
6264	3920	2899	325	39	0	\N	0.00	9.50	\N
6250	3610	2962	304	40	0	\N	0.00	9.50	\N
6221	3790	3053	305	42	0	\N	0.10	8.55	\N
6150	4000	2754	361	43	0	\N	0.00	9.50	\N
5996	3962	2656	283	40	0	\N	0.25	7.13	\N
5897	3639	2669	324	39	0	\N	0.20	7.60	\N
6269	3588	3070	335	42	70	\N	0.00	12.75	\N
6072	4056	2945	276	43	70	\N	0.15	10.84	\N
5968	3749	3004	293	40	70	\N	0.15	10.84	\N
6584	4053	2786	337	42	60	\N	0.00	20.00	\N
6532	3864	2935	352	38	60	\N	0.00	20.00	\N
6507	3734	2742	339	45	60	\N	0.00	20.00	\N
6440	3878	2951	278	40	60	\N	0.00	20.00	\N
6433	3807	2728	335	43	60	\N	0.10	18.00	\N
6369	3729	2768	345	42	60	\N	0.15	17.00	\N
6168	4004	2701	344	44	60	\N	0.05	19.00	\N
6104	3853	2909	284	42	60	\N	0.00	20.00	\N
6064	4031	2881	319	40	60	\N	0.00	20.00	\N
6021	3889	2659	312	44	60	\N	0.15	17.00	\N
5969	3749	3004	322	38	60	\N	0.05	19.00	\N
5965	3718	2813	292	37	60	\N	0.00	20.00	\N
5818	3917	2991	307	39	60	\N	0.00	20.00	\N
6485	4015	2906	277	40	0	\N	0.00	16.25	\N
6294	4017	2972	320	44	0	\N	0.00	16.25	\N
6030	3653	3018	356	37	0	\N	0.00	16.25	\N
6610	3974	3098	283	42	0	\N	0.00	53.00	\N
6591	3730	2862	300	40	0	\N	0.00	53.00	\N
6530	3792	2992	328	42	0	\N	0.00	53.00	\N
6521	3658	2822	279	42	0	\N	0.00	53.00	\N
6435	3807	2728	364	41	0	\N	0.00	53.00	\N
6408	3710	2895	357	40	0	\N	0.25	39.75	\N
6386	3619	2718	348	37	0	\N	0.00	53.00	\N
6341	3904	2968	293	37	0	\N	0.05	50.35	\N
6296	3668	2766	287	43	0	\N	0.05	50.35	\N
6228	3860	2798	282	38	0	\N	0.05	50.35	\N
6126	3768	3039	297	38	0	\N	0.00	53.00	\N
6114	3821	2744	336	41	0	\N	0.15	45.05	\N
6049	3634	2853	284	39	0	\N	0.00	53.00	\N
6037	4041	3058	345	44	0	\N	0.00	53.00	\N
5962	3739	2762	333	41	0	\N	0.15	45.05	\N
5900	3942	2709	310	43	0	\N	0.20	42.40	\N
5856	3846	2761	334	42	0	\N	0.10	47.70	\N
5814	3770	2675	354	42	0	\N	0.00	53.00	\N
6568	3979	2877	351	39	0	\N	0.00	7.00	\N
6562	3793	2821	319	44	0	\N	0.25	5.25	\N
6535	3651	2745	281	45	0	\N	0.20	5.60	\N
6490	3814	2874	307	39	0	\N	0.15	5.95	\N
6473	3654	3066	339	40	0	\N	0.05	6.65	\N
6427	3782	2938	317	44	0	\N	0.00	7.00	\N
6374	3645	3003	361	43	0	\N	0.00	7.00	\N
6358	3676	2977	277	39	0	\N	0.00	7.00	\N
6304	3682	2806	358	39	0	\N	0.00	7.00	\N
6298	3995	2710	278	37	0	\N	0.00	7.00	\N
6248	3756	3030	291	38	0	\N	0.00	7.00	\N
6185	3586	3076	315	38	0	\N	0.00	7.00	\N
6151	3858	2668	339	45	0	\N	0.15	5.95	\N
6128	3822	2914	340	38	0	\N	0.00	7.00	\N
6011	3893	2982	352	42	0	\N	0.10	6.30	\N
5887	3922	3005	331	43	0	\N	0.00	7.00	\N
6432	3697	2879	321	42	0	\N	0.00	32.80	\N
6398	3963	2681	329	42	0	\N	0.10	29.52	\N
6390	3993	2844	290	37	0	\N	0.00	32.80	\N
6276	3900	2833	344	41	0	\N	0.00	32.80	\N
6138	3775	2747	276	43	0	\N	0.00	32.80	\N
6085	3733	2741	343	43	0	\N	0.00	32.80	\N
6002	3865	2868	360	44	0	\N	0.00	32.80	\N
5953	3795	2763	345	38	0	\N	0.00	32.80	\N
5850	3802	2665	336	37	0	\N	0.20	26.24	\N
6630	4049	2981	319	44	0	\N	0.25	5.59	\N
6600	3933	2694	349	38	0	\N	0.00	7.45	\N
6480	3970	2781	353	38	0	\N	0.00	7.45	\N
6447	3761	3028	344	40	0	\N	0.15	6.33	\N
6365	4018	2657	345	37	0	\N	0.10	6.71	\N
6359	3676	2977	334	42	0	\N	0.20	5.96	\N
6284	3805	2847	318	44	0	\N	0.25	5.59	\N
6243	3884	2878	344	43	0	\N	0.00	7.45	\N
6219	4010	2811	278	41	0	\N	0.10	6.71	\N
6215	3740	3073	294	41	0	\N	0.00	7.45	\N
6156	3789	2949	356	37	0	\N	0.00	7.45	\N
6107	3720	2963	312	37	0	\N	0.05	7.08	\N
6099	3665	2850	317	44	0	\N	0.20	5.96	\N
6089	3617	2927	278	37	0	\N	0.00	7.45	\N
6066	3997	2690	279	45	0	\N	0.00	7.45	\N
6052	3892	3100	335	38	0	\N	0.25	5.59	\N
6015	3985	2788	357	44	0	\N	0.20	5.96	\N
6010	4029	2699	278	39	0	\N	0.00	7.45	\N
5997	3726	2864	348	39	0	\N	0.00	7.45	\N
5940	3652	3054	309	39	0	\N	0.00	7.45	\N
5927	3836	3059	282	39	0	\N	0.00	7.45	\N
5925	3973	3096	280	40	0	\N	0.00	7.45	\N
5917	3732	2758	301	39	0	\N	0.15	6.33	\N
5914	3895	2857	348	43	0	\N	0.00	7.45	\N
5896	3692	3072	282	45	0	\N	0.00	7.45	\N
5871	3950	3087	342	37	0	\N	0.00	7.45	\N
6567	3979	2877	344	40	0	\N	0.00	24.00	\N
6566	3979	2877	297	38	0	\N	0.00	24.00	\N
6513	3690	2939	303	39	0	\N	0.00	24.00	\N
6290	3596	2800	294	40	0	\N	0.00	24.00	\N
6180	3783	3050	363	38	0	\N	0.00	24.00	\N
6129	3822	2794	338	40	0	\N	0.05	22.80	\N
6124	3872	2679	280	42	0	\N	0.05	22.80	\N
6026	3816	3021	319	37	0	\N	0.25	18.00	\N
5963	3879	2900	344	38	0	\N	0.10	21.60	\N
5907	3636	2752	298	40	0	\N	0.20	19.20	\N
6583	4053	2786	321	40	10	\N	0.00	38.00	\N
6509	3655	3066	283	42	10	\N	0.00	38.00	\N
6461	3767	3043	323	43	10	\N	0.00	38.00	\N
6385	3946	2713	338	39	10	\N	0.00	38.00	\N
6355	3794	2663	304	42	10	\N	0.15	32.30	\N
6327	3641	3008	297	39	10	\N	0.00	38.00	\N
6313	4051	2845	344	39	10	\N	0.00	38.00	\N
6305	3638	2901	362	40	10	\N	0.20	30.40	\N
6277	3900	2739	310	39	10	\N	0.00	38.00	\N
6202	3839	2746	335	42	10	\N	0.05	36.10	\N
6173	3861	2685	352	40	10	\N	0.00	38.00	\N
6135	3947	2757	324	39	10	\N	0.05	36.10	\N
6120	3601	2667	344	42	10	\N	0.20	30.40	\N
6091	3848	2756	360	40	10	\N	0.15	32.30	\N
6059	3976	2859	288	40	10	\N	0.00	38.00	\N
6036	3590	2659	284	38	10	\N	0.00	38.00	\N
5998	3980	3037	333	39	10	\N	0.00	38.00	\N
5948	3712	2661	277	44	10	\N	0.00	38.00	\N
5934	3897	2846	348	44	10	\N	0.25	28.50	\N
5911	3955	2928	338	39	10	\N	0.00	38.00	\N
5894	3896	2758	348	40	10	\N	0.05	36.10	\N
5866	3806	3081	359	44	10	\N	0.00	38.00	\N
5860	4027	2678	358	38	10	\N	0.00	38.00	\N
5827	3866	3088	338	44	10	\N	0.00	38.00	\N
6620	3863	2865	308	43	0	\N	0.00	19.50	\N
6604	3646	2730	320	37	0	\N	0.00	19.50	\N
6548	3615	2940	344	38	0	\N	0.00	19.50	\N
6496	3940	2684	341	40	0	\N	0.00	19.50	\N
6481	3970	2781	337	37	0	\N	0.00	19.50	\N
6391	3704	2933	280	42	0	\N	0.00	19.50	\N
6143	3722	2914	284	40	0	\N	0.00	19.50	\N
6003	3762	2721	352	39	0	\N	0.20	15.60	\N
5981	3605	2656	360	44	0	\N	0.00	19.50	\N
5920	3688	2905	277	42	0	\N	0.00	19.50	\N
5847	3788	3007	342	40	0	\N	0.00	19.50	\N
6495	3940	2992	349	40	0	\N	0.20	10.60	\N
6478	3654	3066	335	40	0	\N	0.00	13.25	\N
6428	3964	2944	308	40	0	\N	0.15	11.26	\N
6279	3784	2840	344	41	0	\N	0.25	9.94	\N
6240	3948	2760	298	41	0	\N	0.00	13.25	\N
6226	3983	2913	310	43	0	\N	0.20	10.60	\N
6201	3839	2746	360	40	0	\N	0.00	13.25	\N
6159	3649	2695	328	39	0	\N	0.00	13.25	\N
5939	4048	2943	364	37	0	\N	0.00	13.25	\N
5892	3706	2870	297	38	0	\N	0.20	10.60	\N
6601	3933	2694	290	44	0	\N	0.00	55.00	\N
6597	3717	2821	362	38	0	\N	0.00	55.00	\N
6595	3717	2821	344	43	0	\N	0.25	41.25	\N
6593	3810	2814	312	38	0	\N	0.00	55.00	\N
6570	3823	2897	363	38	0	\N	0.00	55.00	\N
6559	3842	2717	356	38	0	\N	0.05	52.25	\N
6512	3690	2939	284	39	0	\N	0.00	55.00	\N
6457	3845	2879	323	40	0	\N	0.05	52.25	\N
6416	3809	2839	340	41	0	\N	0.05	52.25	\N
6368	3729	2768	361	40	0	\N	0.05	52.25	\N
6364	4018	2844	312	45	0	\N	0.00	55.00	\N
6337	3855	2961	317	39	0	\N	0.00	55.00	\N
6315	3583	3061	360	45	0	\N	0.15	46.75	\N
6292	3898	3080	339	38	0	\N	0.05	52.25	\N
6271	3925	2825	328	44	0	\N	0.00	55.00	\N
6244	3884	2878	280	44	0	\N	0.00	55.00	\N
6182	3952	2795	305	40	0	\N	0.15	46.75	\N
6166	4054	3050	308	43	0	\N	0.00	55.00	\N
6132	3582	2867	310	37	0	\N	0.20	44.00	\N
6100	3695	2744	276	40	0	\N	0.10	49.50	\N
6096	3591	2834	327	43	0	\N	0.00	55.00	\N
6045	3660	2858	296	42	0	\N	0.00	55.00	\N
6022	3889	2892	312	38	0	\N	0.00	55.00	\N
5995	3962	2656	293	40	0	\N	0.20	44.00	\N
5988	3672	3086	304	42	0	\N	0.00	55.00	\N
5976	3987	3035	283	45	0	\N	0.20	44.00	\N
5975	3987	3035	283	39	0	\N	0.00	55.00	\N
5906	3923	2758	346	43	0	\N	0.15	46.75	\N
5840	3804	2743	322	37	0	\N	0.05	52.25	\N
5820	3868	2955	341	45	0	\N	0.00	55.00	\N
6626	3774	2866	305	40	0	\N	0.00	34.00	\N
6624	3991	3051	340	38	0	\N	0.00	34.00	\N
6621	3863	2891	292	44	0	\N	0.00	34.00	\N
6574	3592	3064	303	38	0	\N	0.00	34.00	\N
6560	3842	2653	331	37	0	\N	0.00	34.00	\N
6528	3742	2717	301	45	0	\N	0.15	28.90	\N
6519	3658	2822	320	41	0	\N	0.15	28.90	\N
6483	3938	2736	283	39	0	\N	0.25	25.50	\N
6482	3938	2736	342	40	0	\N	0.00	34.00	\N
6460	3908	2893	293	39	0	\N	0.00	34.00	\N
6394	4028	2851	311	45	0	\N	0.00	34.00	\N
6293	4017	2833	335	40	0	\N	0.00	34.00	\N
6274	3603	2649	304	37	0	\N	0.00	34.00	\N
6249	3756	3030	329	39	0	\N	0.00	34.00	\N
6222	3790	3053	344	38	0	\N	0.00	34.00	\N
6218	4010	2811	298	37	0	\N	0.10	30.60	\N
6190	3954	2836	275	39	0	\N	0.00	34.00	\N
6176	3820	2772	364	42	0	\N	0.00	34.00	\N
6101	3786	2719	317	39	0	\N	0.25	25.50	\N
6084	3968	2988	287	42	0	\N	0.05	32.30	\N
6080	4022	2690	336	38	0	\N	0.15	28.90	\N
6077	3883	2896	294	43	0	\N	0.15	28.90	\N
6046	3660	2858	340	44	0	\N	0.00	34.00	\N
6029	3930	2805	301	40	0	\N	0.00	34.00	\N
5949	3712	2661	278	39	0	\N	0.00	34.00	\N
5926	3973	3096	336	37	0	\N	0.10	30.60	\N
5924	3701	2689	345	41	0	\N	0.05	32.30	\N
5922	4057	2928	319	37	0	\N	0.20	27.20	\N
5817	3929	3057	349	40	0	\N	0.00	34.00	\N
6623	3991	2957	279	45	0	\N	0.00	28.50	\N
6617	4024	2776	307	39	0	\N	0.20	22.80	\N
6607	4019	3064	288	38	0	\N	0.00	28.50	\N
6599	3933	2704	328	44	0	\N	0.00	28.50	\N
6555	3656	2862	293	38	0	\N	0.15	24.23	\N
6498	3940	2686	311	42	0	\N	0.00	28.50	\N
6475	3654	3066	363	37	0	\N	0.00	28.50	\N
6471	3616	3028	364	40	0	\N	0.00	28.50	\N
6379	3971	2999	357	39	0	\N	0.15	24.23	\N
6289	3915	2901	324	44	0	\N	0.00	28.50	\N
6281	3736	2724	337	40	0	\N	0.00	28.50	\N
6207	3835	2964	346	43	0	\N	0.20	22.80	\N
6115	3708	2683	303	43	0	\N	0.10	25.65	\N
6078	3689	2658	359	43	0	\N	0.20	22.80	\N
6069	3657	2727	362	40	0	\N	0.00	28.50	\N
6005	3723	2660	344	40	0	\N	0.15	24.23	\N
6609	3974	3098	364	40	0	\N	0.00	49.30	\N
6592	3810	2814	283	37	0	\N	0.25	36.98	\N
6553	3671	2822	338	39	0	\N	0.10	44.37	\N
6531	3792	2992	287	40	0	\N	0.15	41.91	\N
6514	3690	2939	283	38	0	\N	0.00	49.30	\N
6469	3719	2856	362	39	0	\N	0.00	49.30	\N
6467	3643	2823	297	37	0	\N	0.15	41.91	\N
6426	3782	3011	362	40	0	\N	0.00	49.30	\N
6417	3809	2933	338	44	0	\N	0.00	49.30	\N
6382	3874	2965	312	39	0	\N	0.15	41.91	\N
6381	3874	2948	305	40	0	\N	0.05	46.84	\N
6367	3931	2716	346	40	0	\N	0.25	36.98	\N
6344	3951	2654	325	39	0	\N	0.00	49.30	\N
6334	3975	3012	356	39	0	\N	0.00	49.30	\N
6283	3805	2847	312	37	0	\N	0.00	49.30	\N
6256	3664	2649	336	38	0	\N	0.00	49.30	\N
6198	3800	2904	293	43	0	\N	0.15	41.91	\N
6171	3919	2779	354	40	0	\N	0.20	39.44	\N
6164	3602	3082	284	42	0	\N	0.00	49.30	\N
6127	3881	2676	339	37	0	\N	0.10	44.37	\N
6125	3768	3039	298	44	0	\N	0.25	36.98	\N
6111	4035	2912	357	37	0	\N	0.00	49.30	\N
6070	3771	2889	324	39	0	\N	0.00	49.30	\N
6014	3629	2919	280	39	0	\N	0.00	49.30	\N
5973	3902	2826	296	44	0	\N	0.00	49.30	\N
5959	3966	3033	309	37	0	\N	0.00	49.30	\N
5904	3721	2774	324	38	0	\N	0.00	49.30	\N
5881	4023	3029	338	37	0	\N	0.00	49.30	\N
5879	3744	2664	338	37	0	\N	0.10	44.37	\N
5875	3785	2931	350	44	0	\N	0.00	49.30	\N
5863	3599	2688	310	42	0	\N	0.00	49.30	\N
5851	3632	2775	336	44	0	\N	0.00	49.30	\N
5842	3700	2797	325	38	0	\N	0.00	49.30	\N
6606	4019	2946	287	39	0	\N	0.00	43.90	\N
6594	3810	2814	287	40	0	\N	0.00	43.90	\N
6544	4003	3016	293	44	0	\N	0.00	43.90	\N
6542	4003	3016	297	44	0	\N	0.00	43.90	\N
6539	3727	2742	348	39	0	\N	0.00	43.90	\N
6486	4015	3063	356	37	0	\N	0.00	43.90	\N
6257	3664	2749	274	40	0	\N	0.00	43.90	\N
5994	3633	2680	310	39	0	\N	0.25	32.93	\N
5967	3937	2656	293	44	0	\N	0.00	43.90	\N
5889	3871	2980	344	45	0	\N	0.15	37.32	\N
6637	3910	2720	293	40	80	\N	0.00	33.25	\N
6618	4024	2776	332	38	80	\N	0.20	26.60	\N
6533	3864	2935	293	37	80	\N	0.00	33.25	\N
6522	3907	3019	308	44	80	\N	0.00	33.25	\N
6441	3684	2910	282	43	80	\N	0.00	33.25	\N
6424	3725	2869	298	37	80	\N	0.25	24.94	\N
6410	3710	2713	336	44	80	\N	0.00	33.25	\N
6401	3587	2764	293	43	80	\N	0.00	33.25	\N
6397	3945	3085	314	38	80	\N	0.00	33.25	\N
6322	3637	2852	344	42	80	\N	0.00	33.25	\N
6246	3597	3000	305	39	80	\N	0.00	33.25	\N
6233	4011	2962	359	37	80	\N	0.10	29.93	\N
6194	3627	3032	303	40	80	\N	0.00	33.25	\N
6139	3775	2747	355	40	80	\N	0.00	33.25	\N
6130	3811	2849	324	44	80	\N	0.10	29.93	\N
6044	3618	2890	338	39	80	\N	0.00	33.25	\N
6035	3590	2659	282	40	80	\N	0.00	33.25	\N
5991	3669	2820	302	40	80	\N	0.00	33.25	\N
5933	3703	3038	293	38	80	\N	0.10	29.93	\N
5854	3699	2835	284	43	80	\N	0.00	33.25	\N
6449	4042	2872	318	40	0	\N	0.05	20.00	\N
6437	4060	2789	303	41	0	\N	0.05	20.00	\N
6420	3609	2995	328	39	0	\N	0.15	17.89	\N
6330	3606	2842	336	39	0	\N	0.10	18.95	\N
6295	3998	2753	282	41	0	\N	0.05	20.00	\N
6236	4026	3013	299	37	0	\N	0.00	21.05	\N
6231	3913	2782	341	43	0	\N	0.00	21.05	\N
6229	3860	2705	301	37	0	\N	0.15	17.89	\N
6162	3760	2978	332	43	0	\N	0.20	16.84	\N
6145	3707	2924	329	40	0	\N	0.05	20.00	\N
6068	3657	2727	310	42	0	\N	0.00	21.05	\N
5944	4009	2883	334	38	0	\N	0.10	18.95	\N
5916	3840	2958	293	37	0	\N	0.05	20.00	\N
5816	3625	3031	357	39	0	\N	0.00	21.05	\N
5815	3625	3031	307	40	0	\N	0.15	17.89	\N
6339	3911	2852	297	40	100	\N	0.00	17.00	\N
6097	3585	3092	292	43	100	\N	0.00	17.00	\N
6007	3679	2735	293	39	100	\N	0.00	17.00	\N
6619	3863	2865	285	44	0	\N	0.00	14.00	\N
6504	3667	2745	322	38	0	\N	0.15	11.90	\N
6488	4015	3063	314	43	0	\N	0.20	11.20	\N
6476	3654	3066	303	39	0	\N	0.00	14.00	\N
6429	3964	2944	277	40	0	\N	0.00	14.00	\N
6340	3600	3052	351	43	0	\N	0.00	14.00	\N
6109	3662	3069	321	40	0	\N	0.00	14.00	\N
6067	3924	2737	331	38	0	\N	0.00	14.00	\N
5849	3764	2967	317	40	0	\N	0.25	10.50	\N
6629	4049	2981	344	37	10	\N	0.00	12.50	\N
6613	3936	3015	283	43	10	\N	0.00	12.50	\N
6578	3843	2902	342	38	10	\N	0.00	12.50	\N
6468	3719	2856	307	39	10	\N	0.00	12.50	\N
6453	3870	2693	303	37	10	\N	0.00	12.50	\N
6434	3807	2728	345	41	10	\N	0.00	12.50	\N
6395	4028	2995	354	40	10	\N	0.00	12.50	\N
6357	4046	2965	364	37	10	\N	0.00	12.50	\N
6354	3794	2663	296	37	10	\N	0.00	12.50	\N
6331	4058	3001	329	40	10	\N	0.00	12.50	\N
6227	3983	2913	321	39	10	\N	0.00	12.50	\N
6187	3798	2662	340	40	10	\N	0.20	10.00	\N
6183	4001	2904	324	37	10	\N	0.00	12.50	\N
6105	3909	2952	336	39	10	\N	0.00	12.50	\N
6086	3733	2741	285	44	10	\N	0.00	12.50	\N
5989	3672	2677	324	43	10	\N	0.20	10.00	\N
5950	3681	2733	348	37	10	\N	0.20	10.00	\N
5899	3875	2715	357	44	10	\N	0.00	12.50	\N
5893	3763	2780	301	40	10	\N	0.00	12.50	\N
5872	3781	2871	321	38	10	\N	0.00	12.50	\N
5868	3741	2771	303	43	10	\N	0.10	11.25	\N
5865	3806	3081	322	38	10	\N	0.00	12.50	\N
5853	4052	2671	339	40	10	\N	0.10	11.25	\N
6598	3717	2821	341	43	0	\N	0.10	32.40	\N
6587	4002	2891	307	45	0	\N	0.00	36.00	\N
6529	3792	2992	347	39	0	\N	0.00	36.00	\N
6361	3882	3097	308	39	0	\N	0.20	28.80	\N
6321	3637	2852	348	44	0	\N	0.20	28.80	\N
6320	3711	2918	282	40	0	\N	0.25	27.00	\N
6317	3880	2687	326	38	0	\N	0.00	36.00	\N
6312	3644	2738	332	42	0	\N	0.00	36.00	\N
6311	3644	2738	287	37	0	\N	0.00	36.00	\N
6282	3736	2724	298	37	0	\N	0.05	34.20	\N
6160	3894	2803	293	38	0	\N	0.25	27.00	\N
6148	4033	3026	360	38	0	\N	0.15	30.60	\N
6094	4045	2777	323	41	0	\N	0.00	36.00	\N
6058	3976	2859	314	40	0	\N	0.10	32.40	\N
5984	3608	2885	341	40	0	\N	0.05	34.20	\N
5960	3890	2908	308	42	0	\N	0.00	36.00	\N
5957	3714	2759	332	38	0	\N	0.00	36.00	\N
5921	3808	2983	359	42	0	\N	0.00	36.00	\N
5915	3840	2958	314	42	0	\N	0.10	32.40	\N
5876	3785	2917	291	37	0	\N	0.00	36.00	\N
5861	3876	2804	319	42	0	\N	0.00	36.00	\N
6627	3774	3042	339	40	10	\N	0.20	12.00	\N
6622	3991	2957	326	39	10	\N	0.00	15.00	\N
6602	3646	2730	303	43	10	\N	0.00	15.00	\N
6582	4053	2786	293	45	10	\N	0.00	15.00	\N
6465	3750	2684	361	37	10	\N	0.25	11.25	\N
6445	3650	2856	297	43	10	\N	0.20	12.00	\N
6423	3725	2869	313	38	10	\N	0.00	15.00	\N
6415	3776	2951	357	37	10	\N	0.15	12.75	\N
6407	3949	2790	353	37	10	\N	0.00	15.00	\N
6389	3993	2844	297	44	10	\N	0.00	15.00	\N
6387	3619	2718	355	42	10	\N	0.00	15.00	\N
6375	3645	3003	315	38	10	\N	0.00	15.00	\N
6323	3851	3091	341	39	10	\N	0.00	15.00	\N
6272	3925	2907	277	40	10	\N	0.15	12.75	\N
6263	3920	2899	293	40	10	\N	0.05	14.25	\N
6262	3801	3060	320	39	10	\N	0.25	11.25	\N
6259	3745	2876	336	44	10	\N	0.00	15.00	\N
6224	3873	2886	335	43	10	\N	0.05	14.25	\N
6205	3885	2911	359	40	10	\N	0.25	11.25	\N
6082	3713	3020	326	39	10	\N	0.00	15.00	\N
6071	4056	2945	312	45	10	\N	0.10	13.50	\N
6050	3906	2659	320	40	10	\N	0.10	13.50	\N
6041	3765	3095	308	44	10	\N	0.00	15.00	\N
6018	4025	2808	277	37	10	\N	0.10	13.50	\N
5954	4020	2765	283	40	10	\N	0.00	15.00	\N
5945	3666	2801	310	44	10	\N	0.00	15.00	\N
5880	3611	2887	311	40	10	\N	0.00	15.00	\N
5873	3837	2942	275	43	10	\N	0.00	15.00	\N
5864	3849	2917	340	40	10	\N	0.00	15.00	\N
5830	3670	2976	280	38	10	\N	0.00	15.00	\N
5825	3934	2675	328	40	10	\N	0.25	11.25	\N
6603	3646	2730	349	37	0	\N	0.00	21.50	\N
6596	3717	2821	344	42	0	\N	0.00	21.50	\N
6589	3730	2862	292	40	0	\N	0.00	21.50	\N
6577	3843	3016	298	37	0	\N	0.05	20.43	\N
6576	3843	2902	274	39	0	\N	0.00	21.50	\N
6573	3592	3064	293	43	0	\N	0.00	21.50	\N
6503	3667	2745	336	39	0	\N	0.25	16.13	\N
6479	3970	2781	335	42	0	\N	0.00	21.50	\N
6466	3643	2823	308	40	0	\N	0.00	21.50	\N
6412	3772	2718	344	40	0	\N	0.20	17.20	\N
6336	3855	2961	293	45	0	\N	0.00	21.50	\N
6333	3975	3012	277	39	0	\N	0.00	21.50	\N
6285	3648	3070	334	44	0	\N	0.00	21.50	\N
6280	3724	2899	282	39	0	\N	0.00	21.50	\N
6265	3841	3056	344	39	0	\N	0.20	17.20	\N
6237	4026	3099	278	45	0	\N	0.00	21.50	\N
6232	3913	2782	293	43	0	\N	0.20	17.20	\N
6191	3613	2947	278	37	0	\N	0.00	21.50	\N
6186	3586	3076	311	40	0	\N	0.00	21.50	\N
6181	3952	2795	305	37	0	\N	0.05	20.43	\N
6170	3939	3071	324	37	0	\N	0.05	20.43	\N
6163	3687	2773	338	37	0	\N	0.00	21.50	\N
6106	3909	2952	307	38	0	\N	0.10	19.35	\N
6038	4050	2975	311	37	0	\N	0.00	21.50	\N
6023	3748	2863	349	43	0	\N	0.00	21.50	\N
6020	3862	3040	360	44	0	\N	0.00	21.50	\N
6012	3893	2982	340	40	0	\N	0.00	21.50	\N
5972	3978	3067	329	38	0	\N	0.00	21.50	\N
5966	3718	2813	338	37	0	\N	0.00	21.50	\N
5952	3680	2799	343	37	0	\N	0.00	21.50	\N
5943	3867	3022	297	41	0	\N	0.00	21.50	\N
5938	4048	2943	310	40	0	\N	0.20	17.20	\N
5929	3828	3022	292	37	0	\N	0.00	21.50	\N
5898	3683	2937	360	41	0	\N	0.10	19.35	\N
5885	4044	2827	360	41	0	\N	0.00	21.50	\N
5874	3705	2827	310	39	0	\N	0.00	21.50	\N
5869	3728	2903	353	37	0	\N	0.00	21.50	\N
6586	4002	2891	336	39	0	\N	0.00	34.80	\N
6557	3656	3047	350	37	0	\N	0.00	34.80	\N
6552	3671	2822	292	44	0	\N	0.00	34.80	\N
6523	3907	3019	327	43	0	\N	0.00	34.80	\N
6506	3734	2742	344	43	0	\N	0.00	34.80	\N
6491	3814	2874	275	40	0	\N	0.00	34.80	\N
6404	3780	2831	354	39	0	\N	0.10	31.32	\N
6363	4018	2657	311	38	0	\N	0.00	34.80	\N
6349	3647	2993	322	40	0	\N	0.15	29.58	\N
6310	3819	2972	336	45	0	\N	0.00	34.80	\N
6307	3773	2755	283	39	0	\N	0.00	34.80	\N
6214	3621	2750	323	41	0	\N	0.00	34.80	\N
6203	4043	2651	320	39	0	\N	0.00	34.80	\N
6172	3919	2779	344	41	0	\N	0.00	34.80	\N
6121	4030	2757	346	38	0	\N	0.00	34.80	\N
6103	4047	2723	284	45	0	\N	0.00	34.80	\N
6093	3901	2996	305	42	0	\N	0.00	34.80	\N
6024	3693	2975	357	40	0	\N	0.00	34.80	\N
6000	3754	2919	289	44	0	\N	0.00	34.80	\N
5961	3739	3036	298	37	0	\N	0.00	34.80	\N
5955	3751	2767	293	42	0	\N	0.10	31.32	\N
5937	3921	2994	335	41	0	\N	0.25	26.10	\N
5902	3903	2959	298	40	0	\N	0.00	34.80	\N
5895	3692	2752	319	39	0	\N	0.15	29.58	\N
5890	3834	2664	312	37	0	\N	0.00	34.80	\N
5862	3593	2942	280	41	0	\N	0.00	34.80	\N
5848	4037	3079	319	39	0	\N	0.00	34.80	\N
5839	3996	2841	358	42	0	\N	0.00	34.80	\N
5837	3787	2989	338	42	0	\N	0.00	34.80	\N
5834	3622	2726	362	41	0	\N	0.05	33.06	\N
5833	3960	2791	306	44	0	\N	0.00	34.80	\N
5813	3635	3010	363	41	0	\N	0.00	34.80	\N
6446	3761	3028	285	40	0	\N	0.00	15.00	\N
6316	3880	2687	341	39	0	\N	0.00	15.00	\N
6268	3588	3070	297	42	0	\N	0.00	15.00	\N
6258	3745	2708	362	39	0	\N	0.15	12.75	\N
6192	3613	2793	344	44	0	\N	0.15	12.75	\N
6165	4054	2829	309	40	0	\N	0.00	15.00	\N
6123	3614	2810	277	37	0	\N	0.00	15.00	\N
6102	3786	2974	341	37	0	\N	0.00	15.00	\N
6098	3665	2850	297	44	0	\N	0.05	14.25	\N
6053	3709	2985	298	44	0	\N	0.20	12.00	\N
5985	3829	2807	361	39	0	\N	0.10	13.50	\N
5843	3799	2712	278	44	0	\N	0.00	15.00	\N
6411	3772	2789	349	38	20	\N	0.00	10.00	\N
6371	4059	2707	357	39	20	\N	0.25	7.50	\N
6346	3818	2960	360	38	20	\N	0.00	10.00	\N
6318	3737	2691	300	39	20	\N	0.00	10.00	\N
6051	3892	3100	308	37	20	\N	0.00	10.00	\N
6004	3986	2692	324	42	20	\N	0.00	10.00	\N
5983	3832	2864	336	40	20	\N	0.00	10.00	\N
5947	3914	3036	293	40	20	\N	0.00	10.00	\N
5946	3666	2801	319	39	20	\N	0.00	10.00	\N
5935	3852	2828	287	42	20	\N	0.15	8.50	\N
5828	3612	2706	293	45	20	\N	0.25	7.50	\N
5819	3886	2984	287	41	20	\N	0.00	10.00	\N
6563	3793	2992	364	44	0	\N	0.00	7.75	\N
6545	3615	2830	297	40	0	\N	0.20	6.20	\N
6540	3727	3094	283	37	0	\N	0.00	7.75	\N
6538	3651	2843	313	42	0	\N	0.00	7.75	\N
6524	3907	3047	304	42	0	\N	0.15	6.59	\N
6520	3658	3063	297	44	0	\N	0.20	6.20	\N
6516	3797	2648	341	45	0	\N	0.05	7.36	\N
6497	3940	2686	282	44	0	\N	0.10	6.98	\N
6489	3814	2874	278	39	0	\N	0.00	7.75	\N
6472	3616	3006	347	42	0	\N	0.00	7.75	\N
6459	3908	2893	344	37	0	\N	0.05	7.36	\N
6384	3946	2713	285	38	0	\N	0.00	7.75	\N
6353	3794	2663	336	37	0	\N	0.05	7.36	\N
6351	3999	2731	335	44	0	\N	0.20	6.20	\N
6350	3647	2993	306	37	0	\N	0.00	7.75	\N
6338	3911	2792	293	37	0	\N	0.20	6.20	\N
6326	3641	3008	338	41	0	\N	0.00	7.75	\N
6301	4006	2748	310	45	0	\N	0.00	7.75	\N
6287	3659	2806	344	44	0	\N	0.00	7.75	\N
6247	3597	3000	276	39	0	\N	0.00	7.75	\N
6235	3594	2732	298	40	0	\N	0.00	7.75	\N
6199	3800	2904	296	40	0	\N	0.00	7.75	\N
6196	3958	2751	314	44	0	\N	0.10	6.98	\N
6178	3827	3082	308	40	0	\N	0.00	7.75	\N
6161	3760	2978	362	44	0	\N	0.20	6.20	\N
6146	3707	2924	294	39	0	\N	0.20	6.20	\N
6144	3791	2819	318	37	0	\N	0.00	7.75	\N
6137	4005	2703	278	39	0	\N	0.10	6.98	\N
6122	4030	2963	317	45	0	\N	0.00	7.75	\N
6117	3927	2971	308	38	0	\N	0.00	7.75	\N
6079	3689	2996	293	39	0	\N	0.00	7.75	\N
6075	4036	2802	344	42	0	\N	0.10	6.98	\N
6055	3813	3009	308	43	0	\N	0.00	7.75	\N
6039	4050	2921	331	41	0	\N	0.00	7.75	\N
6025	3816	3021	297	44	0	\N	0.25	5.81	\N
6001	3865	2868	280	39	0	\N	0.10	6.98	\N
5912	3891	2916	294	40	0	\N	0.15	6.59	\N
5891	3912	3045	281	40	0	\N	0.00	7.75	\N
5877	3642	2838	359	38	0	\N	0.00	7.75	\N
5859	3959	2647	338	40	0	\N	0.00	7.75	\N
5858	3857	2770	353	37	0	\N	0.00	7.75	\N
5845	3584	3041	278	38	0	\N	0.00	7.75	\N
6640	3752	3023	341	44	0	\N	0.15	15.30	\N
6615	4024	2776	297	44	0	\N	0.10	16.20	\N
6569	3823	2897	323	39	0	\N	0.00	18.00	\N
6556	3656	3047	336	37	0	\N	0.20	14.40	\N
6527	3742	2717	336	44	0	\N	0.00	18.00	\N
6526	3742	2717	335	44	0	\N	0.00	18.00	\N
6493	3844	2783	302	37	0	\N	0.00	18.00	\N
6492	3844	2783	313	40	0	\N	0.00	18.00	\N
6444	3650	2898	363	39	0	\N	0.00	18.00	\N
6425	3725	2869	299	39	0	\N	0.00	18.00	\N
6402	3587	2764	278	45	0	\N	0.25	13.50	\N
6373	3645	3003	333	38	0	\N	0.15	15.30	\N
6314	4051	2845	311	40	0	\N	0.00	18.00	\N
6299	3995	2710	304	38	0	\N	0.00	18.00	\N
6297	3668	2766	282	39	0	\N	0.00	18.00	\N
6267	3702	2847	274	40	0	\N	0.00	18.00	\N
6266	3702	2711	310	42	0	\N	0.15	15.30	\N
6230	3825	2915	321	37	0	\N	0.00	18.00	\N
6195	3869	2818	312	37	0	\N	0.00	18.00	\N
6177	3981	3044	344	37	0	\N	0.00	18.00	\N
6169	4004	2701	301	37	0	\N	0.10	16.20	\N
6158	3649	2695	317	43	0	\N	0.20	14.40	\N
6147	3757	2740	279	39	0	\N	0.00	18.00	\N
6140	3916	3039	325	41	0	\N	0.00	18.00	\N
6134	3941	2936	338	41	0	\N	0.00	18.00	\N
6131	3582	2867	280	45	0	\N	0.00	18.00	\N
6095	3591	2834	332	39	0	\N	0.00	18.00	\N
6040	3956	2875	349	45	0	\N	0.15	15.30	\N
5999	3980	3037	297	39	0	\N	0.15	15.30	\N
5990	3830	2650	314	42	0	\N	0.25	13.50	\N
5978	3815	2925	314	39	0	\N	0.00	18.00	\N
5928	3828	2986	290	40	0	\N	0.00	18.00	\N
5908	3812	2669	317	40	0	\N	0.00	18.00	\N
5884	3747	2817	353	43	0	\N	0.00	18.00	\N
5883	3854	2920	311	44	0	\N	0.00	18.00	\N
5838	3982	2652	336	39	0	\N	0.05	17.10	\N
5832	4016	2953	298	40	0	\N	0.15	15.30	\N
6642	3752	3023	338	37	0	\N	0.00	13.00	\N
6633	3899	2704	335	44	0	\N	0.15	11.05	\N
6625	3774	3042	300	38	0	\N	0.00	13.00	\N
6580	3935	2940	343	38	0	\N	0.00	13.00	\N
6565	3979	2877	338	38	0	\N	0.00	13.00	\N
6564	3793	2698	329	42	0	\N	0.05	12.35	\N
6551	3817	2648	327	44	0	\N	0.00	13.00	\N
6511	3655	2787	356	37	0	\N	0.00	13.00	\N
6494	3844	2783	298	42	0	\N	0.00	13.00	\N
6451	3831	2938	307	37	0	\N	0.00	13.00	\N
6450	4042	2872	349	42	0	\N	0.00	13.00	\N
6406	3949	2790	349	41	0	\N	0.00	13.00	\N
6400	3963	2681	274	37	0	\N	0.20	10.40	\N
6388	3993	2844	319	41	0	\N	0.10	11.70	\N
6377	3965	2666	339	41	0	\N	0.00	13.00	\N
6345	3951	3008	319	38	0	\N	0.00	13.00	\N
6300	3607	3017	318	42	0	\N	0.10	11.70	\N
6255	3994	2907	307	37	0	\N	0.25	9.75	\N
6223	3873	2886	336	40	0	\N	0.05	12.35	\N
6211	3746	2705	310	45	0	\N	0.25	9.75	\N
6167	3595	2987	356	44	0	\N	0.25	9.75	\N
6155	3789	3090	324	40	0	\N	0.05	12.35	\N
6152	3858	2668	334	37	0	\N	0.00	13.00	\N
6142	3698	3090	355	45	0	\N	0.00	13.00	\N
6119	3691	3074	329	40	0	\N	0.05	12.35	\N
6062	3992	2816	317	43	0	\N	0.00	13.00	\N
6060	3753	2785	315	39	0	\N	0.00	13.00	\N
6056	3813	3009	301	44	0	\N	0.15	11.05	\N
6048	3634	2853	362	43	0	\N	0.05	12.35	\N
6016	3985	2868	336	40	0	\N	0.10	11.70	\N
5986	3829	2930	334	44	0	\N	0.15	11.05	\N
5982	3605	2656	346	40	0	\N	0.00	13.00	\N
5964	3640	2733	356	44	0	\N	0.00	13.00	\N
5932	4034	2861	356	43	0	\N	0.00	13.00	\N
5931	4034	2801	302	44	0	\N	0.00	13.00	\N
5855	3846	2761	288	44	0	\N	0.00	13.00	\N
5822	3694	2990	308	40	0	\N	0.00	13.00	\N
5821	3928	2700	361	39	0	\N	0.00	13.00	\N
\.


--
-- Data for Name: f_shipment_transaction; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.f_shipment_transaction (order_transaction_key, order_date_key, required_date_key, shipped_date_key, customer_key, employee_key, shipper_key, shipper_nbr_of_products, shipment_extended_amt, shipment_discount_amt, shipment_freight_amt, shipment_total_amt, audit_key) FROM stdin;
5855	3846	2761	4871	379	44	19	0	\N	0.00	79.70	13.00	\N
6152	3858	2668	4879	425	37	19	0	\N	0.00	62.52	13.00	\N
6345	3951	3008	4882	410	38	19	0	\N	0.00	42.13	13.00	\N
6494	3844	2783	4898	389	42	19	0	\N	0.00	33.93	13.00	\N
6255	3994	2907	4910	398	37	19	0	\N	0.25	15.80	9.75	\N
5986	3829	2930	4921	425	44	19	0	\N	0.15	99.23	11.05	\N
6451	3831	2938	4964	398	37	19	0	\N	0.00	4.99	13.00	\N
6223	3873	2886	4966	427	40	19	0	\N	0.05	364.15	12.35	\N
6377	3965	2666	4981	430	41	19	0	\N	0.00	59.78	13.00	\N
6062	3992	2816	5040	408	43	19	0	\N	0.00	36.21	13.00	\N
6140	3916	3039	5112	416	41	19	0	\N	0.00	127.34	18.00	\N
6526	3742	2717	4782	426	44	19	0	\N	0.00	104.47	18.00	\N
6492	3844	2783	4787	404	40	19	0	\N	0.00	19.79	18.00	\N
6556	3656	3047	4790	427	37	19	0	\N	0.20	38.51	14.40	\N
6169	4004	2701	4796	392	37	19	0	\N	0.10	7.46	16.20	\N
5908	3812	2669	4891	408	40	19	0	\N	0.00	110.37	18.00	\N
6267	3702	2847	4903	365	40	19	0	\N	0.00	23.94	18.00	\N
6493	3844	2783	4916	393	37	19	0	\N	0.00	1.36	18.00	\N
6569	3823	2897	4953	414	39	19	0	\N	0.00	44.84	18.00	\N
6040	3956	2875	5045	440	45	19	0	\N	0.15	68.52	15.30	\N
6297	3668	2766	5051	373	39	19	0	\N	0.00	16.97	18.00	\N
5832	4016	2953	5052	389	40	19	0	\N	0.15	208.58	15.30	\N
6131	3582	2867	5066	371	45	19	0	\N	0.00	88.40	18.00	\N
6134	3941	2936	5078	429	41	19	0	\N	0.00	58.98	18.00	\N
6351	3999	2731	5132	426	44	19	0	\N	0.20	110.87	6.20	\N
6196	3958	2751	5140	405	44	19	0	\N	0.10	0.87	6.98	\N
6161	3760	2978	4842	453	44	19	0	\N	0.20	16.34	6.20	\N
6146	3707	2924	4847	385	39	19	0	\N	0.20	3.01	6.20	\N
6117	3927	2971	4865	399	38	19	0	\N	0.00	83.22	7.75	\N
6497	3940	2686	4887	373	44	19	0	\N	0.10	134.64	6.98	\N
6235	3594	2732	4929	389	40	19	0	\N	0.00	203.48	7.75	\N
6545	3615	2830	5000	388	40	19	0	\N	0.20	1.26	6.20	\N
5845	3584	3041	5013	369	38	19	0	\N	0.00	8.98	7.75	\N
6025	3816	3021	5036	388	44	19	0	\N	0.25	16.27	5.81	\N
6459	3908	2893	5050	435	37	19	0	\N	0.05	116.13	7.36	\N
6287	3659	2806	5082	435	44	19	0	\N	0.00	74.58	7.75	\N
5983	3832	2864	4803	427	40	19	20	\N	0.00	17.55	10.00	\N
6318	3737	2691	4856	391	39	19	20	\N	0.00	7.70	10.00	\N
5947	3914	3036	4991	384	40	19	20	\N	0.00	94.77	10.00	\N
6446	3761	3028	4917	376	40	19	0	\N	0.00	2.84	15.00	\N
6102	3786	2974	4918	432	37	19	0	\N	0.00	78.85	15.00	\N
5985	3829	2807	4921	452	39	19	0	\N	0.10	44.12	13.50	\N
6165	4054	2829	4968	400	40	19	0	\N	0.00	45.13	15.00	\N
6098	3665	2850	5022	388	44	19	0	\N	0.05	188.04	14.25	\N
5839	3996	2841	5120	449	42	19	0	\N	0.00	6.01	34.80	\N
6172	3919	2779	4785	435	41	19	0	\N	0.00	200.24	34.80	\N
6310	3819	2972	4856	427	45	19	0	\N	0.00	3.52	34.80	\N
6586	4002	2891	4873	427	39	19	0	\N	0.00	297.18	34.80	\N
5955	3751	2767	4890	384	42	19	0	\N	0.10	126.38	31.32	\N
6121	4030	2757	4927	437	38	19	0	\N	0.00	9.80	34.80	\N
6203	4043	2651	4942	411	39	19	0	\N	0.00	158.44	34.80	\N
5834	3622	2726	4972	453	41	19	0	\N	0.05	4.56	33.06	\N
5895	3692	2752	5003	410	39	19	0	\N	0.15	12.75	29.58	\N
6363	4018	2657	5016	402	38	19	0	\N	0.00	2.33	34.80	\N
6552	3671	2822	5024	383	44	19	0	\N	0.00	185.48	34.80	\N
6576	3843	2902	5143	365	39	19	0	\N	0.00	1.21	21.50	\N
6466	3643	2823	4896	399	40	19	0	\N	0.00	62.09	21.50	\N
6280	3724	2899	4905	373	39	19	0	\N	0.00	63.20	21.50	\N
5929	3828	3022	4935	383	37	19	0	\N	0.00	71.97	21.50	\N
6265	3841	3056	4941	435	39	19	0	\N	0.20	65.10	17.20	\N
6106	3909	2952	4945	398	38	19	0	\N	0.10	68.65	19.35	\N
6589	3730	2862	4953	383	40	19	0	\N	0.00	74.36	21.50	\N
6479	3970	2781	4964	426	42	19	0	\N	0.00	21.19	21.50	\N
6232	3913	2782	4979	384	43	19	0	\N	0.20	78.09	17.20	\N
5966	3718	2813	5035	429	37	19	0	\N	0.00	12.51	21.50	\N
5874	3705	2827	5109	401	39	19	0	\N	0.00	47.30	21.50	\N
6389	3993	2844	5111	388	44	19	10	\N	0.00	1.23	15.00	\N
6415	3776	2951	5111	448	37	19	10	\N	0.15	49.19	12.75	\N
5830	3670	2976	5139	371	38	19	10	\N	0.00	55.28	15.00	\N
6262	3801	3060	4777	411	39	19	10	\N	0.25	45.52	11.25	\N
6263	3920	2899	4801	384	40	19	10	\N	0.05	272.47	14.25	\N
6205	3885	2911	4832	450	40	19	10	\N	0.25	23.55	11.25	\N
6445	3650	2856	4917	388	43	19	10	\N	0.20	88.01	12.00	\N
6423	3725	2869	4967	404	38	19	10	\N	0.00	52.51	15.00	\N
5825	3934	2675	5079	419	40	19	10	\N	0.25	55.09	11.25	\N
6602	3646	2730	5094	394	43	19	10	\N	0.00	3.20	15.00	\N
6160	3894	2803	4954	384	38	19	0	\N	0.25	96.78	27.00	\N
6361	3882	3097	4976	399	39	19	0	\N	0.20	26.52	28.80	\N
6312	3644	2738	4988	423	42	19	0	\N	0.00	117.33	36.00	\N
5861	3876	2804	5041	410	42	19	0	\N	0.00	0.12	36.00	\N
5960	3890	2908	5049	399	42	19	0	\N	0.00	184.41	36.00	\N
6331	4058	3001	4791	420	40	19	10	\N	0.00	157.55	12.50	\N
5853	4052	2671	4871	430	40	19	10	\N	0.10	7.45	11.25	\N
6629	4049	2981	4902	435	37	19	10	\N	0.00	30.09	12.50	\N
6434	3807	2728	4950	436	41	19	10	\N	0.00	143.28	12.50	\N
6578	3843	2902	4958	433	38	19	10	\N	0.00	32.99	12.50	\N
6183	4001	2904	4987	415	37	19	10	\N	0.00	154.68	12.50	\N
6476	3654	3066	4827	394	39	19	0	\N	0.00	38.19	14.00	\N
5849	3764	2967	4829	408	40	19	0	\N	0.25	76.56	10.50	\N
6109	3662	3069	4960	412	40	19	0	\N	0.00	24.91	14.00	\N
6067	3924	2737	5026	422	38	19	0	\N	0.00	69.32	14.00	\N
6340	3600	3052	5085	442	43	19	0	\N	0.00	20.25	14.00	\N
6339	3911	2852	4797	388	40	19	100	\N	0.00	48.20	17.00	\N
6236	4026	3013	4835	390	37	19	0	\N	0.00	30.34	21.05	\N
5944	4009	2883	4924	425	38	19	0	\N	0.10	45.03	18.95	\N
5816	3625	3031	4946	448	39	19	0	\N	0.00	41.34	21.05	\N
5916	3840	2958	4973	384	37	19	0	\N	0.05	162.33	20.00	\N
6295	3998	2753	5095	373	41	19	0	\N	0.05	20.12	20.00	\N
6420	3609	2995	5149	419	39	19	0	\N	0.15	170.97	17.89	\N
6410	3710	2713	5111	427	44	19	80	\N	0.00	212.98	33.25	\N
6322	3637	2852	4912	435	42	19	80	\N	0.00	8.19	33.25	\N
6401	3587	2764	4986	384	43	19	80	\N	0.00	411.88	33.25	\N
5991	3669	2820	5081	393	40	19	80	\N	0.00	18.69	33.25	\N
5889	3871	2980	4922	435	45	19	0	\N	0.15	214.27	37.32	\N
6486	4015	3063	4928	447	37	19	0	\N	0.00	176.48	43.90	\N
6594	3810	2814	5094	378	40	19	0	\N	0.00	47.84	43.90	\N
6417	3809	2933	5111	429	44	19	0	\N	0.00	174.05	49.30	\N
6609	3974	3098	5137	455	40	19	0	\N	0.00	8.72	49.30	\N
6127	3881	2676	4769	430	37	19	0	\N	0.10	22.95	44.37	\N
5973	3902	2826	4778	387	44	19	0	\N	0.00	11.26	49.30	\N
6531	3792	2992	4787	378	40	19	0	\N	0.15	27.19	41.91	\N
6334	3975	3012	4797	447	39	19	0	\N	0.00	65.06	49.30	\N
6125	3768	3039	4880	389	44	19	0	\N	0.25	36.65	36.98	\N
6592	3810	2814	4953	374	37	19	0	\N	0.25	52.52	36.98	\N
6467	3643	2823	5077	388	37	19	0	\N	0.15	44.15	41.91	\N
6607	4019	3064	5137	379	38	19	0	\N	0.00	29.99	28.50	\N
6617	4024	2776	5137	398	39	19	0	\N	0.20	67.26	22.80	\N
6078	3689	2658	5014	450	43	19	0	\N	0.20	105.65	22.80	\N
6599	3933	2704	5094	419	44	19	0	\N	0.00	40.32	28.50	\N
6080	4022	2690	5117	427	38	19	0	\N	0.15	204.47	28.90	\N
6519	3658	2822	5125	411	41	19	0	\N	0.15	27.91	28.90	\N
6249	3756	3030	4848	420	39	19	0	\N	0.00	145.63	34.00	\N
6460	3908	2893	4870	384	39	19	0	\N	0.00	162.75	34.00	\N
6574	3592	3064	4958	394	38	19	0	\N	0.00	59.11	34.00	\N
6218	4010	2811	4979	389	37	19	0	\N	0.10	93.25	30.60	\N
6190	3954	2836	4994	366	39	19	0	\N	0.00	43.90	34.00	\N
6394	4028	2851	5103	402	45	19	0	\N	0.00	154.72	34.00	\N
6292	3898	3080	5147	430	38	19	0	\N	0.05	89.90	52.25	\N
6368	3729	2768	4868	452	40	19	0	\N	0.05	55.23	52.25	\N
6315	3583	3061	4878	451	45	19	0	\N	0.15	79.30	46.75	\N
6100	3695	2744	4909	367	40	19	0	\N	0.10	15.64	49.50	\N
6096	3591	2834	4918	418	43	19	0	\N	0.00	8.12	55.00	\N
6166	4054	3050	4933	399	43	19	0	\N	0.00	58.30	55.00	\N
6570	3823	2897	4958	454	38	19	0	\N	0.00	0.75	55.00	\N
5840	3804	2743	4972	413	37	19	0	\N	0.05	26.93	52.25	\N
6132	3582	2867	4978	401	37	19	0	\N	0.20	33.97	44.00	\N
6416	3809	2839	5006	431	41	19	0	\N	0.05	160.55	52.25	\N
5995	3962	2656	5025	384	40	19	0	\N	0.20	458.78	44.00	\N
6022	3889	2892	5036	403	38	19	0	\N	0.00	11.57	55.00	\N
6593	3810	2814	5089	403	38	19	0	\N	0.00	29.59	55.00	\N
6478	3654	3066	4774	426	40	19	0	\N	0.00	33.05	13.25	\N
6201	3839	2746	5076	451	40	19	0	\N	0.00	1.15	13.25	\N
5892	3706	2870	5083	388	38	19	0	\N	0.20	63.36	10.60	\N
5847	3788	3007	4849	433	40	19	0	\N	0.00	12.69	19.50	\N
6391	3704	2933	4875	371	42	19	0	\N	0.00	7.09	19.50	\N
5920	3688	2905	4973	368	42	19	0	\N	0.00	41.95	19.50	\N
6327	3641	3008	4791	388	39	19	10	\N	0.00	328.74	38.00	\N
6277	3900	2739	4792	401	39	19	10	\N	0.00	89.93	38.00	\N
6313	4051	2845	4837	435	39	19	10	\N	0.00	232.55	38.00	\N
6202	3839	2746	5076	426	42	19	10	\N	0.05	201.29	36.10	\N
6355	3794	2663	5085	395	42	19	10	\N	0.15	28.23	32.30	\N
6124	3872	2679	4927	371	42	19	0	\N	0.05	8.05	22.80	\N
6567	3979	2877	5153	435	40	19	0	\N	0.00	141.16	24.00	\N
5927	3836	3059	5114	373	39	19	0	\N	0.00	96.04	7.45	\N
6010	4029	2699	5121	369	39	19	0	\N	0.00	9.30	7.45	\N
6156	3789	2949	5144	447	37	19	0	\N	0.00	55.92	7.45	\N
6219	4010	2811	4793	369	41	19	0	\N	0.10	55.26	6.71	\N
5896	3692	3072	4869	373	45	19	0	\N	0.00	10.19	7.45	\N
5914	3895	2857	4955	439	43	19	0	\N	0.00	8.63	7.45	\N
6359	3676	2977	5008	425	42	19	0	\N	0.20	21.49	5.96	\N
6002	3865	2868	4780	451	44	19	0	\N	0.00	19.97	32.80	\N
6085	3733	2741	4824	434	43	19	0	\N	0.00	13.37	32.80	\N
6390	3993	2844	4976	381	37	19	0	\N	0.00	79.25	32.80	\N
6432	3697	2879	5080	412	42	19	0	\N	0.00	1.93	32.80	\N
5953	3795	2763	5090	436	38	19	0	\N	0.00	34.86	32.80	\N
6151	3858	2668	4879	430	45	19	0	\N	0.15	0.48	5.95	\N
6490	3814	2874	4957	398	39	19	0	\N	0.15	2.27	5.95	\N
6535	3651	2745	4959	372	45	19	0	\N	0.20	16.16	5.60	\N
6374	3645	3003	4975	452	43	19	0	\N	0.00	4.87	7.00	\N
6011	3893	2982	5032	443	42	19	0	\N	0.10	14.68	6.30	\N
6248	3756	3030	5092	382	38	19	0	\N	0.00	4.40	7.00	\N
6037	4041	3058	4826	436	44	19	0	\N	0.00	4.20	53.00	\N
6591	3730	2862	4854	391	40	19	0	\N	0.00	47.09	53.00	\N
5814	3770	2675	4931	445	42	19	0	\N	0.00	11.61	53.00	\N
6114	3821	2744	4960	427	41	19	0	\N	0.15	171.24	45.05	\N
5962	3739	2762	4962	424	41	19	0	\N	0.15	60.26	45.05	\N
6386	3619	2718	5001	439	37	19	0	\N	0.00	36.68	53.00	\N
6296	3668	2766	5095	378	43	19	0	\N	0.05	96.65	50.35	\N
5969	3749	3004	4907	413	38	19	60	\N	0.05	155.97	19.00	\N
6072	4056	2945	4956	367	43	19	70	\N	0.15	47.45	10.84	\N
6269	3588	3070	5051	426	42	19	70	\N	0.00	4.78	12.75	\N
6221	3790	3053	4852	396	42	19	0	\N	0.10	57.15	8.55	\N
6150	4000	2754	4894	452	43	19	0	\N	0.00	13.41	9.50	\N
6275	3803	3093	5007	391	37	19	0	\N	0.00	4.98	9.50	\N
6031	3581	3065	5138	379	40	19	0	\N	0.00	11.93	12.00	\N
6278	3784	2840	5029	435	37	19	0	\N	0.00	167.05	12.00	\N
6378	3971	2999	5047	431	37	19	0	\N	0.00	47.38	12.00	\N
5993	3877	2860	5064	430	43	19	0	\N	0.00	11.09	12.00	\N
6208	3990	2793	5072	365	42	19	0	\N	0.25	29.46	9.00	\N
6034	3777	3040	4990	453	37	19	0	\N	0.15	60.18	16.53	\N
6017	4038	2882	5048	435	44	19	0	\N	0.05	140.26	18.48	\N
5835	3969	2809	5119	451	37	19	10	\N	0.00	136.54	46.00	\N
6547	3615	2940	4787	374	38	19	10	\N	0.00	14.01	46.00	\N
6325	4013	2932	4844	414	40	19	10	\N	0.25	155.64	34.50	\N
6238	3675	2922	4979	454	38	19	10	\N	0.00	22.76	46.00	\N
6057	3628	3027	4901	374	39	19	0	\N	0.05	62.89	13.30	\N
6245	3623	2670	5070	419	37	19	0	\N	0.25	26.61	10.50	\N
6525	3742	2783	4787	399	39	19	0	\N	0.00	2.08	9.65	\N
6554	3671	2822	4822	425	38	19	0	\N	0.00	34.76	9.65	\N
6455	3870	2693	4917	382	43	19	0	\N	0.00	32.76	9.65	\N
6343	3951	2654	4937	369	39	19	0	\N	0.00	6.79	9.65	\N
6372	4059	2707	5111	391	40	19	0	\N	0.00	1.36	18.40	\N
6376	3965	2666	4830	411	44	19	0	\N	0.00	31.22	18.40	\N
6309	3769	2888	4878	447	42	19	0	\N	0.20	69.19	14.72	\N
6087	3888	2974	4906	408	40	19	0	\N	0.20	45.33	14.72	\N
5971	3978	2807	5106	426	43	19	0	\N	0.10	108.04	16.56	\N
6430	3964	2831	5123	427	38	19	0	\N	0.05	348.14	17.10	\N
5888	4014	3068	5083	403	40	19	0	\N	0.00	4.88	18.00	\N
6393	4028	2768	5149	428	45	19	0	\N	0.00	90.85	263.50	\N
6092	3848	2756	5131	427	43	19	0	\N	0.10	41.90	17.10	\N
6175	3820	2772	5135	405	44	19	0	\N	0.25	26.78	14.25	\N
6234	4011	2962	4831	437	38	19	0	\N	0.00	24.39	19.00	\N
5936	3852	2828	4993	405	37	19	0	\N	0.20	0.45	15.20	\N
5923	3943	2926	5069	405	41	19	0	\N	0.05	19.64	18.05	\N
6210	3759	2854	5072	398	40	19	0	\N	0.00	12.41	19.00	\N
5846	3584	3078	4849	433	40	19	0	\N	0.00	2.94	18.00	\N
5980	3850	2734	4803	400	39	19	0	\N	0.00	0.20	2.50	\N
6197	3958	2751	4818	450	44	19	0	\N	0.05	41.38	2.38	\N
5823	3685	2726	4811	384	37	19	40	\N	0.20	140.51	25.60	\N
6550	3817	2648	4822	401	38	19	40	\N	0.10	91.51	28.80	\N
6510	3655	2787	4916	416	40	19	70	\N	0.00	10.22	12.50	\N
6149	4033	3026	5039	371	40	19	70	\N	0.05	59.14	11.88	\N
6431	3697	3083	5123	369	41	19	0	\N	0.25	109.11	19.42	\N
6352	3999	3001	5085	405	38	19	0	\N	0.05	249.93	117.60	\N
6541	3727	3064	5127	399	37	19	0	\N	0.00	37.97	45.60	\N
6517	3797	2648	4887	365	37	19	0	\N	0.00	40.42	45.60	\N
6065	3997	2690	4888	405	42	19	0	\N	0.05	42.68	43.32	\N
6008	3589	2680	4893	430	44	19	0	\N	0.00	13.95	45.60	\N
6438	3878	2951	4950	454	40	19	0	\N	0.00	0.82	45.60	\N
6074	3887	2696	5026	370	40	19	0	\N	0.00	0.15	45.60	\N
6157	3847	2729	5144	408	39	19	0	\N	0.05	32.10	29.67	\N
6251	3631	2848	5145	423	38	19	0	\N	0.00	96.50	31.23	\N
5987	3984	3034	5011	391	38	19	0	\N	0.00	3.02	31.23	\N
6288	3915	2824	5033	453	39	19	0	\N	0.00	21.72	31.23	\N
6260	3918	2739	4777	454	43	19	0	\N	0.00	16.72	4.50	\N
6027	3738	2725	4938	380	38	19	0	\N	0.00	6.17	9.00	\N
5974	3624	3086	4778	418	39	19	40	\N	0.00	29.83	10.00	\N
6443	3650	2898	5123	427	40	19	0	\N	0.05	46.69	76.95	\N
6225	3696	3048	5017	400	44	19	0	\N	0.00	111.29	81.00	\N
6442	3684	2910	5086	431	37	19	0	\N	0.00	38.06	62.50	\N
6303	3682	2806	4784	438	38	19	0	\N	0.00	2.91	17.45	\N
6614	3936	3015	4902	395	39	19	0	\N	0.20	8.34	30.40	\N
6291	3898	3093	5147	383	40	19	30	\N	0.00	16.56	21.00	\N
5970	3932	3025	5031	411	37	19	70	\N	0.00	34.82	10.00	\N
5882	4039	2997	4922	412	42	19	0	\N	0.00	12.69	18.00	\N
6142	3698	3090	5112	446	45	20	0	\N	0.00	25.41	13.00	\N
6300	3607	3017	4770	409	42	20	0	\N	0.10	45.97	11.70	\N
6048	3634	2853	4817	453	43	20	0	\N	0.05	15.28	12.35	\N
6551	3817	2648	4873	418	44	20	0	\N	0.00	217.86	13.00	\N
5931	4034	2801	4877	393	44	20	0	\N	0.00	10.14	13.00	\N
6625	3774	3042	4902	391	38	20	0	\N	0.00	10.98	13.00	\N
6167	3595	2987	4933	447	44	20	0	\N	0.25	2.92	9.75	\N
5821	3928	2700	4952	452	39	20	0	\N	0.00	13.97	13.00	\N
6580	3935	2940	4953	434	38	20	0	\N	0.00	4.62	13.00	\N
6564	3793	2698	4958	420	42	20	0	\N	0.05	96.35	12.35	\N
6406	3949	2790	5010	440	41	20	0	\N	0.00	424.30	13.00	\N
6511	3655	2787	5042	447	37	20	0	\N	0.00	27.20	13.00	\N
6388	3993	2844	5100	410	41	20	0	\N	0.10	163.97	11.70	\N
5978	3815	2925	4804	405	39	20	0	\N	0.00	95.66	18.00	\N
6195	3869	2818	4818	403	37	20	0	\N	0.00	32.35	18.00	\N
6527	3742	2717	4855	427	44	20	0	\N	0.00	275.79	18.00	\N
6158	3649	2695	4861	408	43	20	0	\N	0.20	174.20	14.40	\N
5990	3830	2650	4893	405	42	20	0	\N	0.25	7.93	13.50	\N
6095	3591	2834	4913	423	39	20	0	\N	0.00	339.22	18.00	\N
6615	4024	2776	4919	388	44	20	0	\N	0.10	59.41	16.20	\N
5883	3854	2920	4936	402	44	20	0	\N	0.00	4.73	18.00	\N
6147	3757	2740	4954	370	39	20	0	\N	0.00	27.71	18.00	\N
6314	4051	2845	5056	402	40	20	0	\N	0.00	61.53	18.00	\N
6177	3981	3044	5058	435	37	20	0	\N	0.00	544.08	18.00	\N
6230	3825	2915	5097	412	37	20	0	\N	0.00	26.31	18.00	\N
5999	3980	3037	5102	388	39	20	0	\N	0.15	17.92	15.30	\N
6137	4005	2703	5124	369	39	20	0	\N	0.10	116.43	6.98	\N
6520	3658	3063	5125	388	44	20	0	\N	0.20	3.26	6.20	\N
5877	3642	2838	5129	450	38	20	0	\N	0.00	40.26	7.75	\N
6301	4006	2748	4770	401	45	20	0	\N	0.00	44.10	7.75	\N
6489	3814	2874	4787	369	39	20	0	\N	0.00	151.52	7.75	\N
6516	3797	2648	4790	432	45	20	0	\N	0.05	30.85	7.36	\N
5859	3959	2647	4794	429	40	20	0	\N	0.00	147.26	7.75	\N
6538	3651	2843	4820	404	42	20	0	\N	0.00	15.17	7.75	\N
6079	3689	2996	4840	384	39	20	0	\N	0.00	789.95	7.75	\N
6039	4050	2921	4846	422	41	20	0	\N	0.00	83.49	7.75	\N
6524	3907	3047	4855	395	42	20	0	\N	0.15	4.98	6.59	\N
6001	3865	2868	4943	371	39	20	0	\N	0.10	156.66	6.98	\N
6563	3793	2992	5000	455	44	20	0	\N	0.00	20.31	7.75	\N
6326	3641	3008	5019	429	41	20	0	\N	0.00	18.66	7.75	\N
6144	3791	2819	5039	409	37	20	0	\N	0.00	13.73	7.75	\N
6178	3827	3082	5058	399	40	20	0	\N	0.00	8.11	7.75	\N
6055	3813	3009	5061	399	43	20	0	\N	0.00	210.19	7.75	\N
5891	3912	3045	5083	372	40	20	0	\N	0.00	77.92	7.75	\N
6122	4030	2963	5087	408	45	20	0	\N	0.00	96.72	7.75	\N
6247	3597	3000	5092	367	39	20	0	\N	0.00	36.13	7.75	\N
6353	3794	2663	5108	427	37	20	0	\N	0.05	42.70	7.36	\N
5819	3886	2984	4811	378	41	20	20	\N	0.00	22.98	10.00	\N
6371	4059	2707	5016	448	39	20	20	\N	0.25	22.11	7.50	\N
6051	3892	3100	5020	399	37	20	20	\N	0.00	30.53	10.00	\N
5935	3852	2828	5037	378	42	20	20	\N	0.15	1.17	8.50	\N
5843	3799	2712	5120	369	44	20	0	\N	0.00	92.69	15.00	\N
6268	3588	3070	5146	388	42	20	0	\N	0.00	152.30	15.00	\N
6123	3614	2810	4789	368	37	20	0	\N	0.00	72.97	15.00	\N
6053	3709	2985	5020	389	44	20	0	\N	0.20	4.93	12.00	\N
6506	3734	2742	5125	435	43	20	0	\N	0.00	400.81	34.80	\N
6093	3901	2996	4773	396	42	20	0	\N	0.00	3.35	34.80	\N
6024	3693	2975	4779	448	40	20	0	\N	0.00	25.09	34.80	\N
6523	3907	3019	4820	418	43	20	0	\N	0.00	49.56	34.80	\N
5862	3593	2942	4908	371	41	20	0	\N	0.00	5.74	34.80	\N
5937	3921	2994	4974	426	41	20	0	\N	0.25	890.78	26.10	\N
6000	3754	2919	5034	380	44	20	0	\N	0.00	9.21	34.80	\N
5837	3787	2989	5052	429	42	20	0	\N	0.00	98.03	34.80	\N
5869	3728	2903	5110	444	37	20	0	\N	0.00	63.79	21.50	\N
6191	3613	2947	5126	369	37	20	0	\N	0.00	138.69	21.50	\N
6012	3893	2982	5133	431	40	20	0	\N	0.00	68.66	21.50	\N
6503	3667	2745	5134	427	39	20	0	\N	0.25	31.89	16.13	\N
6181	3952	2795	4775	396	37	20	0	\N	0.05	116.53	20.43	\N
6170	3939	3071	4796	415	37	20	0	\N	0.05	379.13	20.43	\N
6186	3586	3076	4813	402	40	20	0	\N	0.00	23.73	21.50	\N
6333	3975	3012	4912	368	39	20	0	\N	0.00	146.32	21.50	\N
6596	3717	2821	4959	435	42	20	0	\N	0.00	227.22	21.50	\N
5972	3978	3067	4999	420	38	20	0	\N	0.00	91.48	21.50	\N
6336	3855	2961	5008	384	45	20	0	\N	0.00	11.19	21.50	\N
6020	3862	3040	5036	451	44	20	0	\N	0.00	180.45	21.50	\N
6285	3648	3070	5055	425	44	20	0	\N	0.00	9.53	21.50	\N
6237	4026	3099	5070	369	45	20	0	\N	0.00	95.75	21.50	\N
5952	3680	2799	5090	434	37	20	0	\N	0.00	93.63	21.50	\N
6603	3646	2730	5099	440	37	20	0	\N	0.00	29.59	21.50	\N
5880	3611	2887	5129	402	40	20	10	\N	0.00	41.76	15.00	\N
6050	3906	2659	4771	411	40	20	10	\N	0.10	64.45	13.50	\N
6465	3750	2684	4774	452	37	20	10	\N	0.25	1.66	11.25	\N
6071	4056	2945	4851	403	45	20	10	\N	0.10	21.19	13.50	\N
6224	3873	2886	4852	426	43	20	10	\N	0.05	105.81	14.25	\N
6582	4053	2786	4953	384	45	20	10	\N	0.00	754.26	15.00	\N
5954	4020	2765	4993	374	40	20	10	\N	0.00	47.42	15.00	\N
6018	4025	2808	5048	368	37	20	10	\N	0.10	25.36	13.50	\N
5864	3849	2917	5075	431	40	20	10	\N	0.00	29.76	15.00	\N
6094	4045	2777	4773	414	41	20	0	\N	0.00	66.69	36.00	\N
6320	3711	2918	4837	373	40	20	0	\N	0.25	16.71	27.00	\N
5915	3840	2958	4839	405	42	20	0	\N	0.10	64.19	32.40	\N
6321	3637	2852	4864	439	44	20	0	\N	0.20	73.21	28.80	\N
6529	3792	2992	4887	438	39	20	0	\N	0.00	87.38	36.00	\N
6587	4002	2891	4902	398	45	20	0	\N	0.00	6.27	36.00	\N
6282	3736	2724	4905	389	37	20	0	\N	0.05	59.25	34.20	\N
5984	3608	2885	4999	432	40	20	0	\N	0.05	137.35	34.20	\N
6148	4033	3026	5039	451	38	20	0	\N	0.15	7.28	30.60	\N
5921	3808	2983	5069	450	42	20	0	\N	0.00	36.71	36.00	\N
5865	3806	3081	4815	413	38	20	10	\N	0.00	17.68	12.50	\N
5868	3741	2771	4815	394	43	20	10	\N	0.10	107.83	11.25	\N
6086	3733	2741	4851	376	44	20	10	\N	0.00	17.22	12.50	\N
6453	3870	2693	4870	394	37	20	10	\N	0.00	51.87	12.50	\N
5989	3672	2677	4921	415	43	20	10	\N	0.20	370.61	10.00	\N
6227	3983	2913	4929	412	39	20	10	\N	0.00	1.28	12.50	\N
6395	4028	2995	4986	445	40	20	10	\N	0.00	81.83	12.50	\N
5899	3875	2715	5003	448	44	20	10	\N	0.00	8.56	12.50	\N
5872	3781	2871	5043	412	38	20	10	\N	0.00	0.56	12.50	\N
5950	3681	2733	5054	439	37	20	10	\N	0.20	30.96	10.00	\N
6354	3794	2663	5150	387	37	20	10	\N	0.00	100.60	12.50	\N
6429	3964	2944	4950	368	40	20	0	\N	0.00	3.04	14.00	\N
6504	3667	2745	4957	413	38	20	0	\N	0.15	76.33	11.90	\N
6007	3679	2735	4845	384	39	20	100	\N	0.00	47.94	17.00	\N
6449	4042	2872	4805	409	40	20	0	\N	0.05	90.97	20.00	\N
6231	3913	2782	4831	432	43	20	0	\N	0.00	232.42	21.05	\N
5815	3625	3031	4860	398	40	20	0	\N	0.15	65.83	17.89	\N
6068	3657	2727	4914	401	42	20	0	\N	0.00	16.74	21.05	\N
6437	4060	2789	4950	394	41	20	0	\N	0.05	175.32	20.00	\N
6139	3775	2747	5112	446	40	20	80	\N	0.00	37.60	33.25	\N
6233	4011	2962	4884	450	37	20	80	\N	0.10	47.22	29.93	\N
5933	3703	3038	4899	384	38	20	80	\N	0.10	101.95	29.93	\N
6035	3590	2659	4990	373	40	20	80	\N	0.00	64.56	33.25	\N
6424	3725	2869	5006	389	37	20	80	\N	0.25	76.10	24.94	\N
6618	4024	2776	5044	423	38	20	80	\N	0.20	53.05	26.60	\N
6130	3811	2849	5066	415	44	20	80	\N	0.10	7.15	29.93	\N
6397	3945	3085	5108	405	38	20	80	\N	0.00	43.26	33.25	\N
6606	4019	2946	4854	378	39	20	0	\N	0.00	48.22	43.90	\N
6544	4003	3016	4925	384	44	20	0	\N	0.00	353.07	43.90	\N
6257	3664	2749	4977	365	40	20	0	\N	0.00	61.02	43.90	\N
5994	3633	2680	5034	401	39	20	0	\N	0.25	56.63	32.93	\N
5967	3937	2656	5035	384	44	20	0	\N	0.00	67.88	43.90	\N
6256	3664	2649	5115	427	38	20	0	\N	0.00	810.05	49.30	\N
6426	3782	3011	4828	453	40	20	0	\N	0.00	14.93	49.30	\N
5879	3744	2664	4936	429	37	20	0	\N	0.10	74.16	44.37	\N
6553	3671	2822	4958	429	39	20	0	\N	0.10	61.14	44.37	\N
6014	3629	2919	4961	371	39	20	0	\N	0.00	53.30	49.30	\N
6344	3951	2654	4976	416	39	20	0	\N	0.00	58.13	49.30	\N
6367	3931	2716	5008	437	40	20	0	\N	0.25	257.26	36.98	\N
5863	3599	2688	5041	401	42	20	0	\N	0.00	168.22	49.30	\N
5904	3721	2774	5065	415	38	20	0	\N	0.00	15.66	49.30	\N
5875	3785	2931	5093	441	44	20	0	\N	0.00	17.52	49.30	\N
6382	3874	2965	5100	403	39	20	0	\N	0.15	306.07	41.91	\N
6381	3874	2948	5149	396	40	20	0	\N	0.05	719.78	46.84	\N
6281	3736	2724	5148	428	40	20	0	\N	0.00	22.57	28.50	\N
6005	3723	2660	4779	435	40	20	0	\N	0.15	86.53	24.23	\N
6289	3915	2901	5055	415	44	20	0	\N	0.00	57.75	28.50	\N
6621	3863	2891	5137	383	44	20	0	\N	0.00	278.96	34.00	\N
5926	3973	3096	4839	427	37	20	0	\N	0.10	183.17	30.60	\N
5817	3929	3057	4886	440	40	20	0	\N	0.00	51.30	34.00	\N
6046	3660	2858	4915	431	44	20	0	\N	0.00	64.33	34.00	\N
6029	3930	2805	4990	392	40	20	0	\N	0.00	89.00	34.00	\N
6077	3883	2896	4992	385	43	20	0	\N	0.15	3.53	28.90	\N
6482	3938	2736	5027	433	40	20	0	\N	0.00	8.29	34.00	\N
6176	3820	2772	5058	455	42	20	0	\N	0.00	80.65	34.00	\N
6293	4017	2833	5067	426	40	20	0	\N	0.00	58.33	34.00	\N
6222	3790	3053	5073	435	38	20	0	\N	0.00	352.69	34.00	\N
6101	3786	2719	5087	408	39	20	0	\N	0.25	58.88	25.50	\N
6512	3690	2939	5134	375	39	20	0	\N	0.00	3.26	55.00	\N
6045	3660	2858	4850	387	42	20	0	\N	0.00	1.35	55.00	\N
6337	3855	2961	5056	408	39	20	0	\N	0.00	91.28	55.00	\N
6182	3952	2795	5084	396	40	20	0	\N	0.15	18.53	46.75	\N
6457	3845	2879	5086	414	40	20	0	\N	0.05	120.27	52.25	\N
6595	3717	2821	5094	435	43	20	0	\N	0.25	830.75	41.25	\N
6159	3649	2695	5144	419	39	20	0	\N	0.00	5.24	13.25	\N
6428	3964	2944	4828	399	40	20	0	\N	0.15	30.26	11.26	\N
6240	3948	2760	4884	389	41	20	0	\N	0.00	31.85	13.25	\N
6003	3762	2721	4893	443	39	20	0	\N	0.20	8.24	15.60	\N
6620	3863	2865	4919	399	43	20	0	\N	0.00	120.92	19.50	\N
6481	3970	2781	4928	428	37	20	0	\N	0.00	63.77	19.50	\N
6548	3615	2940	5024	435	38	20	0	\N	0.00	657.54	19.50	\N
6496	3940	2684	5042	432	40	20	0	\N	0.00	13.60	19.50	\N
5866	3806	3081	5110	450	44	20	10	\N	0.00	45.08	38.00	\N
6091	3848	2756	5136	451	40	20	10	\N	0.15	58.59	32.30	\N
5860	4027	2678	4908	449	38	20	10	\N	0.00	1.15	38.00	\N
5934	3897	2846	4974	439	44	20	10	\N	0.25	195.68	28.50	\N
6305	3638	2901	5033	453	40	20	10	\N	0.20	81.88	30.40	\N
6059	3976	2859	5053	379	40	20	10	\N	0.00	65.99	38.00	\N
6173	3861	2685	5058	443	40	20	10	\N	0.00	27.79	38.00	\N
6385	3946	2713	5100	429	39	20	10	\N	0.00	37.52	38.00	\N
5894	3896	2758	5109	439	40	20	10	\N	0.05	191.67	36.10	\N
6583	4053	2786	5153	412	40	20	10	\N	0.00	11.65	38.00	\N
6566	3979	2877	5059	388	38	20	0	\N	0.00	197.30	24.00	\N
5907	3636	2752	5065	389	40	20	0	\N	0.20	54.83	19.20	\N
6099	3665	2850	5116	408	44	20	0	\N	0.20	27.94	5.96	\N
6089	3617	2927	5131	369	37	20	0	\N	0.00	244.79	7.45	\N
6015	3985	2788	4781	448	44	20	0	\N	0.20	7.23	5.96	\N
6600	3933	2694	4959	440	38	20	0	\N	0.00	0.17	7.45	\N
6480	3970	2781	4964	444	38	20	0	\N	0.00	3.51	7.45	\N
5940	3652	3054	4974	400	39	20	0	\N	0.00	20.12	7.45	\N
6052	3892	3100	5023	426	38	20	0	\N	0.25	71.07	5.59	\N
5997	3726	2864	5034	439	39	20	0	\N	0.00	4.34	7.45	\N
6284	3805	2847	5055	409	44	20	0	\N	0.25	51.44	5.59	\N
6276	3900	2833	4905	435	41	20	0	\N	0.00	52.41	32.80	\N
5850	3802	2665	4971	427	37	20	0	\N	0.20	76.83	26.24	\N
6398	3963	2681	5103	420	42	20	0	\N	0.10	71.49	29.52	\N
6562	3793	2821	5143	410	44	20	0	\N	0.25	73.91	5.25	\N
6473	3654	3066	4969	430	40	20	0	\N	0.05	32.96	6.65	\N
6128	3822	2914	5004	431	38	20	0	\N	0.00	60.43	7.00	\N
6427	3782	2938	5006	408	44	20	0	\N	0.00	53.23	7.00	\N
6521	3658	2822	5125	370	42	20	0	\N	0.00	44.65	53.00	\N
5856	3846	2761	4788	425	42	20	0	\N	0.10	6.40	47.70	\N
6126	3768	3039	4880	388	38	20	0	\N	0.00	242.21	53.00	\N
6228	3860	2798	4910	373	38	20	0	\N	0.05	113.15	50.35	\N
5900	3942	2709	4911	401	43	20	0	\N	0.20	42.11	42.40	\N
6408	3710	2895	5152	448	40	20	0	\N	0.25	9.26	39.75	\N
6485	4015	2906	4928	368	40	20	0	\N	0.00	29.61	16.25	\N
6021	3889	2659	4779	403	44	20	60	\N	0.15	8.12	17.00	\N
6532	3864	2935	4822	443	38	20	60	\N	0.00	62.22	20.00	\N
6433	3807	2728	4870	426	43	20	60	\N	0.10	191.27	18.00	\N
6064	4031	2881	4914	410	40	20	60	\N	0.00	102.02	20.00	\N
5818	3917	2991	4963	398	39	20	60	\N	0.00	58.17	20.00	\N
6369	3729	2768	4975	436	42	20	60	\N	0.15	27.33	17.00	\N
6168	4004	2701	4987	435	44	20	60	\N	0.05	48.77	19.00	\N
6440	3878	2951	5077	369	40	20	60	\N	0.00	32.37	20.00	\N
5897	3639	2669	4869	415	39	20	0	\N	0.20	52.84	7.60	\N
6250	3610	2962	4910	395	40	20	0	\N	0.00	33.75	9.50	\N
5996	3962	2656	5034	374	40	20	0	\N	0.25	44.17	7.13	\N
6308	3769	2888	4770	368	37	20	0	\N	0.05	23.72	11.40	\N
6534	3864	2935	4782	379	37	20	0	\N	0.00	0.21	12.00	\N
6209	3990	2793	4942	452	39	20	0	\N	0.10	0.14	10.80	\N
6508	3734	2742	5042	375	40	20	0	\N	0.00	2.17	12.00	\N
6241	3620	2708	4809	444	38	20	0	\N	0.00	2.01	19.45	\N
6189	3626	2682	4818	442	40	20	0	\N	0.00	94.80	19.45	\N
6083	3838	2727	4833	444	40	20	0	\N	0.00	218.15	19.45	\N
6543	4003	3016	5018	414	45	20	0	\N	0.15	32.82	16.53	\N
5867	3661	2832	4812	440	40	20	10	\N	0.00	6.27	46.00	\N
6588	4002	2822	4959	375	37	20	10	\N	0.00	123.83	46.00	\N
6396	3945	3085	5103	434	39	20	10	\N	0.00	72.19	46.00	\N
6572	3592	3064	5143	424	44	20	0	\N	0.00	202.24	14.00	\N
5910	3755	2873	4819	427	38	20	0	\N	0.00	249.06	14.00	\N
6421	4055	2784	4881	367	39	20	0	\N	0.00	58.43	14.00	\N
6063	3989	2796	4901	399	44	20	0	\N	0.00	29.75	14.00	\N
6561	3842	2653	4958	427	40	20	0	\N	0.00	1.12	14.00	\N
6217	3678	2769	4966	395	40	20	0	\N	0.00	7.14	14.00	\N
6342	3904	2932	4986	395	43	20	0	\N	0.20	3.01	11.20	\N
6302	4006	2748	4784	449	38	20	0	\N	0.00	7.79	9.65	\N
6220	3779	3099	4793	430	37	20	0	\N	0.20	4.41	7.72	\N
6113	3715	2950	4798	443	39	20	0	\N	0.00	1.43	9.65	\N
6356	4046	2965	4872	389	42	20	0	\N	0.05	16.85	9.17	\N
6474	3654	3066	4920	434	37	20	0	\N	0.00	53.05	9.65	\N
6088	3617	2927	4960	436	43	20	0	\N	0.10	77.63	8.69	\N
6628	3774	3042	5060	401	39	20	0	\N	0.10	81.73	8.69	\N
6632	3899	2694	5060	381	37	20	0	\N	0.00	7.98	9.65	\N
6090	3859	2941	5117	373	37	20	0	\N	0.10	11.06	16.56	\N
6484	3938	2736	4774	411	38	20	0	\N	0.00	19.80	18.40	\N
6013	3663	3089	4853	428	40	20	0	\N	0.00	38.82	18.40	\N
6206	3835	2964	5076	399	40	20	0	\N	0.00	179.61	18.40	\N
6405	3780	2944	4862	411	40	20	0	\N	0.20	2.71	14.40	\N
6392	3704	2716	4875	373	37	20	0	\N	0.00	63.54	18.00	\N
6042	3765	3095	4915	424	41	20	0	\N	0.25	13.02	13.50	\N
6073	3905	2672	4965	420	37	20	0	\N	0.00	4.99	18.00	\N
6634	3899	2704	5060	444	37	20	0	\N	0.00	15.67	18.00	\N
6212	3746	2818	5107	425	40	20	0	\N	0.00	45.54	18.00	\N
6546	3615	2940	4822	398	37	20	0	\N	0.00	193.37	263.50	\N
6348	3647	2993	5056	398	40	20	0	\N	0.00	124.98	263.50	\N
6581	3935	2830	5143	368	45	20	0	\N	0.00	33.80	19.00	\N
6112	4035	2912	4798	436	39	20	0	\N	0.00	178.43	19.00	\N
6501	3674	3024	4916	396	39	20	0	\N	0.20	33.68	15.20	\N
6252	3631	2848	4926	401	45	20	0	\N	0.25	296.43	14.25	\N
5878	3604	2954	4936	427	38	20	0	\N	0.00	1.96	19.00	\N
6458	3908	2893	5050	403	45	20	0	\N	0.00	77.78	19.00	\N
6273	3603	2800	5055	441	42	20	0	\N	0.00	2.96	19.00	\N
6188	3626	2682	4842	389	44	20	0	\N	0.10	97.18	16.20	\N
6154	3988	3062	4954	396	44	20	0	\N	0.00	4.42	18.00	\N
6611	3974	3098	4959	450	44	20	0	\N	0.05	71.64	17.10	\N
5886	4044	2722	4982	402	39	20	0	\N	0.00	3.43	18.00	\N
5826	3934	2675	4998	425	40	20	0	\N	0.00	3.05	18.00	\N
6118	3691	3074	5104	451	38	20	0	\N	0.00	149.49	18.00	\N
6631	4049	2981	4902	453	43	20	0	\N	0.00	44.72	14.00	\N
6253	4021	3013	4932	447	40	20	0	\N	0.00	299.09	14.00	\N
5836	3969	2809	4997	439	42	20	0	\N	0.00	4.54	2.50	\N
6537	3651	2843	5062	404	40	20	0	\N	0.00	0.02	2.50	\N
6270	3972	2929	4784	399	45	20	40	\N	0.00	3.52	32.00	\N
5941	3944	2934	4924	415	37	20	70	\N	0.05	20.39	11.88	\N
6518	3797	2736	5012	368	45	20	70	\N	0.05	23.72	11.88	\N
6061	3992	2816	5040	445	43	20	70	\N	0.05	46.77	11.88	\N
6462	3767	3043	4823	401	39	20	0	\N	0.00	603.54	25.89	\N
6456	3845	2879	5086	408	43	20	0	\N	0.05	20.37	24.60	\N
6536	3651	2843	4822	390	38	20	0	\N	0.00	121.82	123.79	\N
6422	4055	2784	4875	369	44	20	0	\N	0.25	188.85	92.84	\N
6477	3654	3066	4916	401	38	20	0	\N	0.25	580.91	92.84	\N
6571	3823	2897	5021	396	39	20	0	\N	0.25	25.19	92.84	\N
6366	3931	2716	5150	372	40	20	0	\N	0.25	97.09	92.84	\N
6006	3723	2815	4990	419	39	20	0	\N	0.00	73.02	43.90	\N
6414	3776	3055	5111	403	45	20	0	\N	0.15	0.56	26.55	\N
6032	3581	3065	4781	413	44	20	0	\N	0.00	4.93	14.00	\N
6575	3843	2902	4873	430	38	20	0	\N	0.00	28.71	4.50	\N
6213	3621	2915	5030	431	41	20	0	\N	0.15	14.25	3.83	\N
6108	3662	3069	5117	410	44	20	0	\N	0.15	48.17	7.65	\N
6239	3675	2922	4848	402	40	20	0	\N	0.00	0.90	9.00	\N
5913	3743	2697	4955	450	40	20	0	\N	0.00	0.78	9.00	\N
6216	3678	2769	4793	450	44	20	0	\N	0.25	20.60	15.75	\N
6409	3710	2895	5152	423	44	20	0	\N	0.00	25.22	21.00	\N
6174	3926	2855	5113	382	43	20	40	\N	0.00	1.85	10.00	\N
5857	3758	3046	5098	445	37	20	0	\N	0.00	1.35	81.00	\N
6418	3609	2995	4967	370	45	20	0	\N	0.00	53.83	62.50	\N
5844	3716	2884	5120	408	44	20	0	\N	0.25	25.83	29.25	\N
6436	4060	2789	4881	373	45	20	0	\N	0.05	112.27	37.05	\N
6360	3676	2977	4980	384	44	20	0	\N	0.25	126.66	29.25	\N
6054	3977	2778	5053	423	42	20	0	\N	0.00	5.29	17.45	\N
5977	3778	2998	4836	451	44	20	0	\N	0.10	3.77	20.93	\N
5992	3669	2820	5036	423	40	20	0	\N	0.00	31.29	23.25	\N
6463	3750	2684	4969	418	40	20	0	\N	0.00	1.27	6.00	\N
6419	3609	2995	5154	384	39	20	0	\N	0.15	100.22	5.10	\N
5930	4008	3002	4899	367	39	20	30	\N	0.00	22.00	21.00	\N
6110	3731	2655	4983	407	44	20	30	\N	0.00	11.92	21.00	\N
6362	3882	3097	5016	381	43	20	30	\N	0.00	33.35	21.00	\N
6608	4019	2946	5044	438	41	20	30	\N	0.00	8.80	21.00	\N
6439	3878	2951	5080	394	41	20	0	\N	0.00	19.58	31.00	\N
6585	4002	2891	5153	420	38	20	0	\N	0.15	43.30	26.35	\N
6413	3776	3055	5010	380	43	20	0	\N	0.00	38.24	97.00	\N
5909	3967	2966	4989	453	40	20	0	\N	0.25	23.29	30.00	\N
6515	3797	2702	4855	413	37	20	0	\N	0.00	2.50	22.00	\N
5901	4032	2956	5028	424	43	20	0	\N	0.10	15.51	19.80	\N
6254	4021	2880	4932	369	37	20	0	\N	0.25	13.42	13.50	\N
6470	3719	2856	4969	452	45	20	0	\N	0.05	13.72	17.10	\N
6016	3985	2868	5122	427	40	21	0	\N	0.10	189.09	11.70	\N
5822	3694	2990	4858	399	40	21	0	\N	0.00	81.91	13.00	\N
6119	3691	3074	4865	420	40	21	0	\N	0.05	120.97	12.35	\N
5982	3605	2656	4874	437	40	21	0	\N	0.00	70.29	13.00	\N
5932	4034	2861	4899	447	43	21	0	\N	0.00	13.55	13.00	\N
6060	3753	2785	4901	406	39	21	0	\N	0.00	4.65	13.00	\N
5964	3640	2733	4907	447	44	21	0	\N	0.00	27.36	13.00	\N
6450	4042	2872	4917	440	42	21	0	\N	0.00	5.64	13.00	\N
6155	3789	3090	4954	415	40	21	0	\N	0.05	44.77	12.35	\N
6400	3963	2681	4986	365	37	21	0	\N	0.20	69.53	10.40	\N
6565	3979	2877	5059	429	38	21	0	\N	0.00	55.12	13.00	\N
6056	3813	3009	5105	392	44	21	0	\N	0.15	16.96	11.05	\N
6211	3746	2705	5107	401	45	21	0	\N	0.25	142.33	9.75	\N
6444	3650	2898	5123	454	39	21	0	\N	0.00	8.50	18.00	\N
5838	3982	2652	5139	427	39	21	0	\N	0.05	76.07	17.10	\N
5928	3828	2986	4935	381	40	21	0	\N	0.00	30.54	18.00	\N
6299	3995	2710	4947	395	38	21	0	\N	0.00	1.63	18.00	\N
5884	3747	2817	4982	444	43	21	0	\N	0.00	64.50	18.00	\N
6266	3702	2711	5017	401	42	21	0	\N	0.15	220.31	15.30	\N
6373	3645	3003	5047	424	38	21	0	\N	0.15	45.53	15.30	\N
6402	3587	2764	5103	369	45	21	0	\N	0.25	13.32	13.50	\N
6425	3725	2869	5149	390	39	21	0	\N	0.00	19.26	18.00	\N
6384	3946	2713	4776	376	38	21	0	\N	0.00	19.76	7.75	\N
6540	3727	3094	4820	374	37	21	0	\N	0.00	32.27	7.75	\N
6199	3800	2904	4885	387	40	21	0	\N	0.00	487.38	7.75	\N
5912	3891	2916	4897	385	40	21	0	\N	0.15	3.10	6.59	\N
6350	3647	2993	4937	397	37	21	0	\N	0.00	1.51	7.75	\N
6338	3911	2792	4984	384	37	21	0	\N	0.20	96.43	6.20	\N
6075	4036	2802	5014	435	42	21	0	\N	0.10	367.63	6.98	\N
6472	3616	3006	5015	438	42	21	0	\N	0.00	9.19	7.75	\N
5858	3857	2770	5041	444	37	21	0	\N	0.00	21.18	7.75	\N
5828	3612	2706	4834	384	45	21	20	\N	0.25	146.06	7.50	\N
5946	3666	2801	4924	410	39	21	20	\N	0.00	7.99	10.00	\N
6004	3986	2692	5005	415	42	21	20	\N	0.00	4.07	10.00	\N
6346	3818	2960	5056	451	38	21	20	\N	0.00	73.16	10.00	\N
6411	3772	2789	5103	440	38	21	20	\N	0.00	56.46	10.00	\N
6316	3880	2687	4795	432	39	21	0	\N	0.00	130.79	15.00	\N
6258	3745	2708	4821	453	39	21	0	\N	0.15	139.34	12.75	\N
6192	3613	2793	4885	435	44	21	0	\N	0.15	107.46	12.75	\N
5833	3960	2791	5119	397	44	21	0	\N	0.00	66.29	34.80	\N
6557	3656	3047	5127	441	37	21	0	\N	0.00	4.27	34.80	\N
6349	3647	2993	5128	413	40	21	0	\N	0.15	70.09	29.58	\N
5848	4037	3079	5142	410	39	21	0	\N	0.00	84.81	34.80	\N
6307	3773	2755	4784	374	39	21	0	\N	0.00	243.73	34.80	\N
6103	4047	2723	4840	375	45	21	0	\N	0.00	4.87	34.80	\N
5902	3903	2959	4939	389	40	21	0	\N	0.00	108.26	34.80	\N
5961	3739	3036	4951	389	37	21	0	\N	0.00	135.35	34.80	\N
5813	3635	3010	4963	454	41	21	0	\N	0.00	32.38	34.80	\N
6491	3814	2874	5027	366	40	21	0	\N	0.00	39.92	34.80	\N
6214	3621	2750	5038	414	41	21	0	\N	0.00	6.20	34.80	\N
5890	3834	2664	5083	403	37	21	0	\N	0.00	64.86	34.80	\N
6404	3780	2831	5091	445	39	21	0	\N	0.10	35.43	31.32	\N
5943	3867	3022	5130	388	41	21	0	\N	0.00	5.44	21.50	\N
6038	4050	2975	4846	402	37	21	0	\N	0.00	16.37	21.50	\N
6412	3772	2718	4881	435	40	21	0	\N	0.20	487.57	17.20	\N
6023	3748	2863	4948	440	43	21	0	\N	0.00	147.06	21.50	\N
5885	4044	2827	4949	451	41	21	0	\N	0.00	34.57	21.50	\N
6577	3843	3016	5000	389	37	21	0	\N	0.05	242.95	20.43	\N
5898	3683	2937	5028	451	41	21	0	\N	0.10	0.59	19.35	\N
6163	3687	2773	5063	429	37	21	0	\N	0.00	44.42	21.50	\N
5938	4048	2943	5071	401	40	21	0	\N	0.20	124.12	17.20	\N
6622	3991	2957	5137	417	39	21	10	\N	0.00	4.13	15.00	\N
6387	3619	2718	4776	446	42	21	10	\N	0.00	7.00	15.00	\N
5873	3837	2942	4802	366	43	21	10	\N	0.00	1.61	15.00	\N
5945	3666	2801	4804	401	44	21	10	\N	0.00	35.03	15.00	\N
6323	3851	3091	4807	432	39	21	10	\N	0.00	138.17	15.00	\N
6041	3765	3095	4850	399	44	21	10	\N	0.00	4.41	15.00	\N
6259	3745	2876	4934	427	44	21	10	\N	0.00	398.36	15.00	\N
6375	3645	3003	4975	406	38	21	10	\N	0.00	4.33	15.00	\N
6272	3925	2907	5007	368	40	21	10	\N	0.15	21.74	12.75	\N
6407	3949	2790	5010	444	37	21	10	\N	0.00	54.42	15.00	\N
6082	3713	3020	5026	417	39	21	10	\N	0.00	32.07	15.00	\N
6311	3644	2738	4770	378	37	21	0	\N	0.00	31.43	36.00	\N
5876	3785	2917	4810	382	37	21	0	\N	0.00	24.69	36.00	\N
6317	3880	2687	4837	417	38	21	0	\N	0.00	1.39	36.00	\N
6058	3976	2859	4940	405	40	21	0	\N	0.10	10.64	32.40	\N
5957	3714	2759	5002	423	38	21	0	\N	0.00	122.46	36.00	\N
6598	3717	2821	5018	432	43	21	0	\N	0.10	84.74	32.40	\N
6468	3719	2856	4774	398	39	21	10	\N	0.00	36.71	12.50	\N
6187	3798	2662	4813	431	40	21	10	\N	0.20	50.97	10.00	\N
5893	3763	2780	4857	392	40	21	10	\N	0.00	87.03	12.50	\N
6105	3909	2952	4927	427	39	21	10	\N	0.00	1007.64	12.50	\N
6613	3936	3015	5099	374	43	21	10	\N	0.00	24.12	12.50	\N
6357	4046	2965	5150	455	37	21	10	\N	0.00	23.79	12.50	\N
6488	4015	3063	4957	405	43	21	0	\N	0.20	68.26	11.20	\N
6097	3585	3092	4913	383	43	21	100	\N	0.00	74.46	17.00	\N
6330	3606	2842	4791	427	39	21	0	\N	0.10	42.74	18.95	\N
6229	3860	2705	4979	392	37	21	0	\N	0.15	1.27	17.89	\N
6145	3707	2924	5057	420	40	21	0	\N	0.05	75.89	20.00	\N
6162	3760	2978	5063	423	43	21	0	\N	0.20	35.12	16.84	\N
6441	3684	2910	5123	373	43	21	80	\N	0.00	60.42	33.25	\N
6194	3627	3032	5126	394	40	21	80	\N	0.00	85.46	33.25	\N
6522	3907	3019	4820	399	44	21	80	\N	0.00	105.36	33.25	\N
6044	3618	2890	4846	429	39	21	80	\N	0.00	708.95	33.25	\N
6246	3597	3000	4848	396	39	21	80	\N	0.00	76.13	33.25	\N
6533	3864	2935	4900	384	37	21	80	\N	0.00	74.60	33.25	\N
5854	3699	2835	5074	375	43	21	80	\N	0.00	22.77	33.25	\N
6539	3727	2742	5127	439	39	21	0	\N	0.00	12.96	43.90	\N
6542	4003	3016	4958	388	44	21	0	\N	0.00	208.50	43.90	\N
6111	4035	2912	4786	448	37	21	0	\N	0.00	194.72	49.30	\N
5881	4023	3029	4863	429	37	21	0	\N	0.00	150.15	49.30	\N
6070	3771	2889	4883	415	39	21	0	\N	0.00	7.13	49.30	\N
6514	3690	2939	4904	374	38	21	0	\N	0.00	74.44	49.30	\N
6283	3805	2847	4905	403	37	21	0	\N	0.00	170.88	49.30	\N
6164	3602	3082	4968	375	42	21	0	\N	0.00	29.98	49.30	\N
6171	3919	2779	4985	445	40	21	0	\N	0.20	79.40	39.44	\N
5842	3700	2797	4996	416	38	21	0	\N	0.00	125.77	49.30	\N
5851	3632	2775	4997	427	44	21	0	\N	0.00	229.24	49.30	\N
6469	3719	2856	5015	453	39	21	0	\N	0.00	162.95	49.30	\N
5959	3966	3033	5049	400	37	21	0	\N	0.00	30.34	49.30	\N
6198	3800	2904	5068	384	43	21	0	\N	0.15	477.90	41.91	\N
6069	3657	2727	5118	453	40	21	0	\N	0.00	59.13	28.50	\N
6498	3940	2686	5134	402	42	21	0	\N	0.00	54.15	28.50	\N
6475	3654	3066	4774	454	37	21	0	\N	0.00	38.11	28.50	\N
6555	3656	2862	4790	384	38	21	0	\N	0.15	117.61	24.23	\N
6207	3835	2964	4867	437	43	21	0	\N	0.20	41.89	22.80	\N
6379	3971	2999	4976	448	39	21	0	\N	0.15	130.94	24.23	\N
6471	3616	3028	5077	455	40	21	0	\N	0.00	26.29	28.50	\N
6115	3708	2683	5087	394	43	21	0	\N	0.10	4.32	25.65	\N
6274	3603	2649	5141	395	37	21	0	\N	0.00	210.80	34.00	\N
5924	3701	2689	4799	436	41	21	0	\N	0.05	288.43	32.30	\N
6084	3968	2988	4824	378	42	21	0	\N	0.05	91.76	32.30	\N
5922	4057	2928	4899	410	37	21	0	\N	0.20	34.88	27.20	\N
6560	3842	2653	5024	422	37	21	0	\N	0.00	46.00	34.00	\N
6483	3938	2736	5027	374	39	21	0	\N	0.25	48.83	25.50	\N
6528	3742	2717	5062	392	45	21	0	\N	0.15	2.70	28.90	\N
5949	3712	2661	5090	369	39	21	0	\N	0.00	168.64	34.00	\N
5976	3987	3035	4800	374	45	21	0	\N	0.20	23.65	44.00	\N
6559	3842	2717	4825	447	38	21	0	\N	0.05	65.53	52.25	\N
5975	3987	3035	4836	374	39	21	0	\N	0.00	2.40	55.00	\N
6244	3884	2878	4848	371	44	21	0	\N	0.00	27.94	55.00	\N
5988	3672	3086	4853	395	42	21	0	\N	0.00	24.50	55.00	\N
6271	3925	2825	4903	419	44	21	0	\N	0.00	135.63	55.00	\N
5820	3868	2955	4946	432	45	21	0	\N	0.00	148.33	55.00	\N
5906	3923	2758	4989	437	43	21	0	\N	0.15	26.78	46.75	\N
6364	4018	2844	5016	403	45	21	0	\N	0.00	30.76	55.00	\N
6597	3717	2821	5018	453	38	21	0	\N	0.00	606.19	55.00	\N
6601	3933	2694	5089	381	44	21	0	\N	0.00	149.47	55.00	\N
6279	3784	2840	5148	435	41	21	0	\N	0.25	24.49	9.94	\N
6495	3940	2992	4916	440	40	21	0	\N	0.20	15.55	10.60	\N
5939	4048	2943	4974	455	37	21	0	\N	0.00	3.94	13.25	\N
6226	3983	2913	5073	401	43	21	0	\N	0.20	17.55	10.60	\N
6143	3722	2914	4785	375	40	21	0	\N	0.00	29.60	19.50	\N
5981	3605	2656	4921	451	44	21	0	\N	0.00	22.72	19.50	\N
6120	3601	2667	4816	435	42	21	10	\N	0.20	252.49	30.40	\N
5911	3955	2928	4897	429	39	21	10	\N	0.00	142.08	38.00	\N
5827	3866	3088	4923	429	44	21	10	\N	0.00	48.29	38.00	\N
6036	3590	2659	4938	375	38	21	10	\N	0.00	45.59	38.00	\N
5948	3712	2661	4944	368	44	21	10	\N	0.00	34.24	38.00	\N
5998	3980	3037	4948	424	39	21	10	\N	0.00	73.83	38.00	\N
6509	3655	3066	4957	374	42	21	10	\N	0.00	52.92	38.00	\N
6461	3767	3043	5015	414	43	21	10	\N	0.00	32.45	38.00	\N
6135	3947	2757	5101	415	39	21	10	\N	0.05	188.99	36.10	\N
6180	3783	3050	5135	454	38	21	0	\N	0.00	0.75	24.00	\N
5963	3879	2900	4838	435	38	21	0	\N	0.10	89.16	21.60	\N
6026	3816	3021	4895	410	37	21	0	\N	0.25	148.61	18.00	\N
6129	3822	2794	4970	429	40	21	0	\N	0.05	13.75	22.80	\N
6513	3690	2939	5042	394	39	21	0	\N	0.00	23.39	24.00	\N
6290	3596	2800	5055	385	40	21	0	\N	0.00	10.83	24.00	\N
5871	3950	3087	4866	433	37	21	0	\N	0.00	7.56	7.45	\N
5925	3973	3096	4899	371	40	21	0	\N	0.00	131.70	7.45	\N
6066	3997	2690	4914	370	45	21	0	\N	0.00	8.85	7.45	\N
6243	3884	2878	4941	435	43	21	0	\N	0.00	388.98	7.45	\N
6107	3720	2963	4995	403	37	21	0	\N	0.05	10.95	7.08	\N
6365	4018	2657	5016	436	37	21	0	\N	0.10	137.44	6.71	\N
6447	3761	3028	5050	435	40	21	0	\N	0.15	23.10	6.33	\N
6215	3740	3073	5107	385	41	21	0	\N	0.00	176.81	7.45	\N
5917	3732	2758	5151	392	39	21	0	\N	0.15	1.30	6.33	\N
6138	3775	2747	4930	367	43	21	0	\N	0.00	84.84	32.80	\N
6568	3979	2877	4787	442	39	21	0	\N	0.00	14.91	7.00	\N
6298	3995	2710	4808	369	37	21	0	\N	0.00	110.11	7.00	\N
6358	3676	2977	4830	368	39	21	0	\N	0.00	4.52	7.00	\N
6185	3586	3076	4994	406	38	21	0	\N	0.00	0.94	7.00	\N
6304	3682	2806	5088	449	39	21	0	\N	0.00	11.08	7.00	\N
5887	3922	3005	5109	422	43	21	0	\N	0.00	0.40	7.00	\N
6530	3792	2992	4782	419	42	21	0	\N	0.00	144.38	53.00	\N
6435	3807	2728	4805	455	41	21	0	\N	0.00	12.04	53.00	\N
6049	3634	2853	4843	375	39	21	0	\N	0.00	6.88	53.00	\N
6341	3904	2968	4876	384	37	21	0	\N	0.05	351.53	50.35	\N
6030	3653	3018	4990	447	37	21	0	\N	0.00	145.04	16.25	\N
6294	4017	2972	5095	411	44	21	0	\N	0.00	141.06	16.25	\N
6104	3853	2909	5117	375	42	21	60	\N	0.00	12.36	20.00	\N
5965	3718	2813	4804	383	37	21	60	\N	0.00	83.93	20.00	\N
6507	3734	2742	4916	430	45	21	60	\N	0.00	17.95	20.00	\N
5968	3749	3004	4838	384	40	21	70	\N	0.15	73.79	10.84	\N
6264	3920	2899	4977	416	39	21	0	\N	0.00	0.58	9.50	\N
6261	3801	2840	4777	453	44	21	0	\N	0.00	102.55	12.00	\N
6019	4025	2808	4889	405	40	21	0	\N	0.20	2.74	9.60	\N
5852	3953	2969	5074	431	44	21	0	\N	0.15	12.76	10.20	\N
6141	3698	2676	5112	444	39	21	0	\N	0.00	18.56	19.45	\N
6286	3659	2979	4792	427	41	21	0	\N	0.05	48.92	18.48	\N
6116	3708	2849	5087	392	40	21	0	\N	0.00	72.95	19.45	\N
6033	3796	3049	5122	403	39	21	10	\N	0.00	44.12	46.00	\N
5905	3923	2758	4897	373	37	21	10	\N	0.05	166.31	43.70	\N
6028	3930	2805	4768	440	41	21	0	\N	0.00	14.78	14.00	\N
6081	3713	3020	4824	401	38	21	0	\N	0.00	62.78	14.00	\N
6153	4040	2973	4894	427	38	21	0	\N	0.20	194.67	11.20	\N
6332	4058	3001	4912	440	40	21	0	\N	0.00	1.59	14.00	\N
6136	3947	2754	5039	384	44	21	0	\N	0.15	26.06	11.90	\N
5829	3961	2812	5142	388	42	21	0	\N	0.15	3.67	8.20	\N
6558	3656	3047	4958	388	43	21	0	\N	0.25	8.81	7.24	\N
6383	3946	2713	4981	413	43	21	0	\N	0.00	65.48	9.65	\N
6579	3935	2830	5021	411	38	21	0	\N	0.10	23.60	8.69	\N
6009	3589	2680	5096	369	39	21	0	\N	0.00	3.50	9.65	\N
6319	3737	2691	4856	413	42	21	0	\N	0.00	2.38	18.40	\N
6047	3630	2673	4940	407	37	21	0	\N	0.00	7.48	18.40	\N
5870	4012	2970	4812	419	44	21	0	\N	0.10	257.62	16.20	\N
6464	3750	2684	4896	410	41	21	0	\N	0.15	1.21	15.30	\N
5942	3944	2934	4924	436	37	21	0	\N	0.15	22.21	15.30	\N
6329	3826	2714	5019	384	42	21	0	\N	0.10	145.45	16.20	\N
6179	3827	3082	5058	370	44	21	0	\N	0.00	1.93	18.00	\N
5918	3598	3075	4814	423	43	21	0	\N	0.20	360.63	210.80	\N
6454	3870	2693	4870	429	45	21	0	\N	0.00	280.61	263.50	\N
6370	3729	2768	5047	441	38	21	0	\N	0.00	237.34	263.50	\N
5824	3833	2674	4923	377	40	21	50	\N	0.00	3.25	26.00	\N
6549	3817	2648	5127	435	37	21	0	\N	0.00	211.22	19.00	\N
6403	3780	2831	5103	411	39	21	0	\N	0.25	59.28	14.25	\N
6502	3667	2856	4957	376	43	21	0	\N	0.00	31.51	14.00	\N
5951	3680	2994	5009	385	45	21	0	\N	0.00	13.99	14.00	\N
5979	3815	2925	4806	385	38	21	0	\N	0.00	21.48	2.50	\N
6380	3971	2999	4976	435	38	21	0	\N	0.00	14.62	2.50	\N
6242	3620	2708	5070	367	37	21	0	\N	0.15	4.03	2.13	\N
6324	3851	3091	4797	366	39	21	40	\N	0.00	11.99	32.00	\N
6347	3818	2960	5128	376	45	21	70	\N	0.00	1.10	12.50	\N
5958	3966	3033	5049	435	37	21	70	\N	0.00	126.56	12.50	\N
5903	3735	3014	4939	419	40	21	0	\N	0.00	84.21	25.89	\N
6399	3963	2681	5108	445	37	21	0	\N	0.05	29.78	24.60	\N
5919	3856	2894	4973	422	44	21	0	\N	0.00	53.80	123.79	\N
6452	3831	2938	4862	393	44	21	0	\N	0.00	1.25	14.00	\N
6487	4015	2906	4827	398	41	21	0	\N	0.00	62.74	4.50	\N
6328	3826	2714	5019	387	39	21	0	\N	0.00	37.35	4.50	\N
6448	4042	2872	5050	412	44	21	0	\N	0.00	0.53	4.50	\N
6500	3674	3024	4916	452	40	21	0	\N	0.25	47.59	6.75	\N
6184	3766	2923	4841	415	39	21	0	\N	0.00	91.05	21.00	\N
6200	4007	3077	4885	413	44	21	0	\N	0.00	47.46	21.00	\N
6204	4043	2651	5046	434	43	21	0	\N	0.00	38.64	62.50	\N
5841	3957	2976	4772	444	44	21	0	\N	0.00	13.84	6.00	\N
6505	3734	2742	4855	373	44	21	0	\N	0.00	19.77	6.00	\N
5956	3751	2767	4859	381	39	21	0	\N	0.00	5.45	6.00	\N
6590	3730	2862	4959	451	42	21	0	\N	0.10	29.17	5.40	\N
5831	3686	2797	4834	451	39	21	0	\N	0.05	25.73	36.10	\N
6335	3824	2837	4783	398	44	21	30	\N	0.25	5.32	15.75	\N
6133	3673	3084	4879	393	39	21	0	\N	0.00	6.54	31.00	\N
6043	3677	2805	4892	448	38	21	0	\N	0.05	4.81	29.45	\N
6076	4036	2802	4883	373	40	21	0	\N	0.15	350.64	34.00	\N
6499	3674	3024	4898	408	39	21	0	\N	0.00	32.01	25.00	\N
6612	3936	3015	5137	383	43	21	0	\N	0.25	46.62	16.01	\N
6306	3773	2800	4784	368	40	21	40	\N	0.20	10.96	15.20	\N
6193	3627	3032	5126	371	40	21	0	\N	0.00	30.36	18.00	\N
\.


--
-- Data for Name: r_categories; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.r_categories (categoryid, categoryname, "Description", picture) FROM stdin;
1	Beverages	Soft drinks, coffees, teas, beers, and ales	\N
2	Condiments	Sweet and savory sauces, relishes, spreads, and seasonings	\N
3	Confections	Desserts, candies, and sweet breads	\N
4	Dairy Products	Cheeses	\N
5	Grains/Cereals	Breads, crackers, pasta, and cereal	\N
6	Meat/Poultry	Prepared meats	\N
7	Produce	Dried fruit and bean curd	\N
8	Seafood	Seaweed and fish	\N
\.


--
-- Data for Name: r_suppliers; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.r_suppliers (supplierid, companyname, contactname, contacttitle, "Address", city, region, postalcode, country, phone, fax, homepage) FROM stdin;
1	Exotic Liquids	Charlotte Cooper	Purchasing Manager	49 Gilbert St.	London	\N	EC1 4SD	UK	(171) 555-2222	\N	\N
2	New Orleans Cajun Delights	Shelley Burke	Order Administrator	P.O. Box 78934	New Orleans	LA	70117	USA	(100) 555-4822	\N	\N
3	Grandma Kelly's Homestead	Regina Murphy	Sales Representative	707 Oxford Rd.	Ann Arbor	MI	48104	USA	(313) 555-5735	(313) 555-3349	\N
4	Tokyo Traders	Yoshi Nagase	Marketing Manager	9-8 Sekimai\nMusashino-shi	Tokyo	\N	100	Japan	(03) 3555-5011	\N	\N
5	Cooperativa de Quesos 'Las Cabras'	Antonio del Valle Saavedra 	Export Administrator	Calle del Rosal 4	Oviedo	Asturias	33007	Spain	(98) 598 76 54	\N	\N
6	Mayumi's	Mayumi Ohno	Marketing Representative	92 Setsuko\nChuo-ku	Osaka	\N	545	Japan	(06) 431-7877	\N	Mayumi's (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/mayumi.htm#
7	Pavlova, Ltd.	Ian Devling	Marketing Manager	74 Rose St.\nMoonie Ponds	Melbourne	Victoria	3058	Australia	(03) 444-2343	(03) 444-6588	\N
8	Specialty Biscuits, Ltd.	Peter Wilson	Sales Representative	29 King's Way	Manchester	\N	M14 GSD	UK	(161) 555-4448	\N	\N
9	PB Knäckebröd AB	Lars Peterson	Sales Agent	Kaloadagatan 13	Göteborg	\N	S-345 67	Sweden 	031-987 65 43	031-987 65 91	\N
10	Refrescos Americanas LTDA	Carlos Diaz	Marketing Manager	Av. das Americanas 12.890	São Paulo	\N	5442	Brazil	(11) 555 4640	\N	\N
11	Heli Süßwaren GmbH & Co. KG	Petra Winkler	Sales Manager	Tiergartenstraße 5	Berlin	\N	10785	Germany	(010) 9984510	\N	\N
12	Plutzer Lebensmittelgroßmärkte AG	Martin Bein	International Marketing Mgr.	Bogenallee 51	Frankfurt	\N	60439	Germany	(069) 992755	\N	Plutzer (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/plutzer.htm#
13	Nord-Ost-Fisch Handelsgesellschaft mbH	Sven Petersen	Coordinator Foreign Markets	Frahmredder 112a	Cuxhaven	\N	27478	Germany	(04721) 8713	(04721) 8714	\N
14	Formaggi Fortini s.r.l.	Elio Rossi	Sales Representative	Viale Dante, 75	Ravenna	\N	48100	Italy	(0544) 60323	(0544) 60603	\N
15	Norske Meierier	Beate Vileid	Marketing Manager	Hatlevegen 5	Sandvika	\N	1320	Norway	(0)2-953010	\N	\N
16	Bigfoot Breweries	Cheryl Saylor	Regional Account Rep.	3400 - 8th Avenue\nSuite 210	Bend	OR	97101	USA	(503) 555-9931	\N	\N
17	Svensk Sjöföda AB	Michael Björn	Sales Representative	Brovallavägen 231	Stockholm	\N	S-123 45	Sweden	08-123 45 67	\N	\N
18	Aux joyeux ecclésiastiques	Guylène Nodier	Sales Manager	203, Rue des Francs-Bourgeois	Paris	\N	75004	France	(1) 03.83.00.68	(1) 03.83.00.62	\N
19	New England Seafood Cannery	Robb Merchant	Wholesale Account Agent	Order Processing Dept.\n2100 Paul Revere Blvd.	Boston	MA	02134	USA	(617) 555-3267	(617) 555-3389	\N
20	Leka Trading	Chandra Leka	Owner	471 Serangoon Loop, Suite #402	Singapore	\N	0512	Singapore	555-8787	\N	\N
21	Lyngbysild	Niels Petersen	Sales Manager	Lyngbysild\nFiskebakken 10	Lyngby	\N	2800	Denmark	43844108	43844115	\N
22	Zaanse Snoepfabriek	Dirk Luchte	Accounting Manager	Verkoop\nRijnweg 22	Zaandam	\N	9999 ZZ	Netherlands	(12345) 1212	(12345) 1210	\N
23	Karkki Oy	Anne Heikkonen	Product Manager	Valtakatu 12	Lappeenranta	\N	53120	Finland	(953) 10956	\N	\N
24	G'day, Mate	Wendy Mackenzie	Sales Representative	170 Prince Edward Parade\nHunter's Hill	Sydney	NSW	2042	Australia	(02) 555-5914	(02) 555-4873	G'day Mate (on the World Wide Web)#http://www.microsoft.com/accessdev/sampleapps/gdaymate.htm#
25	Ma Maison	Jean-Guy Lauzon	Marketing Manager	2960 Rue St. Laurent	Montréal	Québec	H1J 1C3	Canada	(514) 555-9022	\N	\N
26	Pasta Buttini s.r.l.	Giovanni Giudici	Order Administrator	Via dei Gelsomini, 153	Salerno	\N	84100	Italy	(089) 6547665	(089) 6547667	\N
27	Escargots Nouveaux	Marie Delamare	Sales Manager	22, rue H. Voiron	Montceau	\N	71300	France	85.57.00.07	\N	\N
28	Gai pâturage	Eliane Noz	Sales Representative	Bat. B\n3, rue des Alpes	Annecy	\N	74000	France	38.76.98.06	38.76.98.58	\N
29	Forêts d'érables	Chantal Goulet	Accounting Manager	148 rue Chasseur	Ste-Hyacinthe	Québec	J2S 7S8	Canada	(514) 555-2955	(514) 555-2921	\N
\.


--
-- Name: d_customer_customer_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_customer_customer_key_seq', 455, true);


--
-- Name: d_employee_employee_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_employee_employee_key_seq', 45, true);


--
-- Name: d_order_date_dim_order_date_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_order_date_dim_order_date_key_seq', 4060, true);


--
-- Name: d_order_transaction_order_transaction_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_order_transaction_order_transaction_key_seq', 6642, true);


--
-- Name: d_product_product_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_product_product_key_seq', 1078, true);


--
-- Name: d_required_date_required_date_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_required_date_required_date_key_seq', 3100, true);


--
-- Name: d_shipped_date_shipped_date_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_shipped_date_shipped_date_key_seq', 5154, true);


--
-- Name: d_shipper_shipper_key_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.d_shipper_shipper_key_seq', 21, true);


--
-- Name: d_customer d_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_customer
    ADD CONSTRAINT d_customer_pkey PRIMARY KEY (customer_key);


--
-- Name: d_employee d_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_employee
    ADD CONSTRAINT d_employee_pkey PRIMARY KEY (employee_key);


--
-- Name: d_order_date_dim d_order_date_dim_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_order_date_dim
    ADD CONSTRAINT d_order_date_dim_pkey PRIMARY KEY (order_date_key);


--
-- Name: d_order_transaction d_order_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_order_transaction
    ADD CONSTRAINT d_order_transaction_pkey PRIMARY KEY (order_transaction_key);


--
-- Name: d_product d_product_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_product
    ADD CONSTRAINT d_product_pkey PRIMARY KEY (product_key);


--
-- Name: d_required_date d_required_date_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_required_date
    ADD CONSTRAINT d_required_date_pkey PRIMARY KEY (required_date_key);


--
-- Name: d_shipped_date d_shipped_date_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_shipped_date
    ADD CONSTRAINT d_shipped_date_pkey PRIMARY KEY (shipped_date_key);


--
-- Name: d_shipper d_shipper_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_shipper
    ADD CONSTRAINT d_shipper_pkey PRIMARY KEY (shipper_key);


--
-- Name: r_categories r_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.r_categories
    ADD CONSTRAINT r_categories_pkey PRIMARY KEY (categoryid);


--
-- Name: r_suppliers r_suppliers_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.r_suppliers
    ADD CONSTRAINT r_suppliers_pkey PRIMARY KEY (supplierid);


--
-- Name: d_product_product_id_idx; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX d_product_product_id_idx ON public.d_product USING btree (product_id);


--
-- Name: d_product d_product_product_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_product
    ADD CONSTRAINT d_product_product_category_id_fkey FOREIGN KEY (product_category_id) REFERENCES public.r_categories(categoryid) ON DELETE CASCADE;


--
-- Name: d_product d_product_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.d_product
    ADD CONSTRAINT d_product_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES public.r_suppliers(supplierid) ON DELETE CASCADE;


--
-- Name: f_order_line_item f_order_line_item_employee_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_line_item
    ADD CONSTRAINT f_order_line_item_employee_key_fkey FOREIGN KEY (employee_key) REFERENCES public.d_employee(employee_key) ON DELETE CASCADE;


--
-- Name: f_order_line_item f_order_line_item_order_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_line_item
    ADD CONSTRAINT f_order_line_item_order_date_key_fkey FOREIGN KEY (order_date_key) REFERENCES public.d_order_date_dim(order_date_key) ON DELETE CASCADE;


--
-- Name: f_order_line_item f_order_line_item_order_transaction_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_line_item
    ADD CONSTRAINT f_order_line_item_order_transaction_key_fkey FOREIGN KEY (order_transaction_key) REFERENCES public.d_order_transaction(order_transaction_key) ON DELETE CASCADE;


--
-- Name: f_order_line_item f_order_line_item_required_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_line_item
    ADD CONSTRAINT f_order_line_item_required_date_key_fkey FOREIGN KEY (required_date_key) REFERENCES public.d_required_date(required_date_key) ON DELETE CASCADE;


--
-- Name: f_order_transaction f_order_transaction_employee_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_transaction
    ADD CONSTRAINT f_order_transaction_employee_key_fkey FOREIGN KEY (employee_key) REFERENCES public.d_employee(employee_key) ON DELETE CASCADE;


--
-- Name: f_order_transaction f_order_transaction_order_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_transaction
    ADD CONSTRAINT f_order_transaction_order_date_key_fkey FOREIGN KEY (order_date_key) REFERENCES public.d_order_date_dim(order_date_key) ON DELETE CASCADE;


--
-- Name: f_order_transaction f_order_transaction_order_transaction_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_transaction
    ADD CONSTRAINT f_order_transaction_order_transaction_key_fkey FOREIGN KEY (order_transaction_key) REFERENCES public.d_order_transaction(order_transaction_key) ON DELETE CASCADE;


--
-- Name: f_order_transaction f_order_transaction_required_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_order_transaction
    ADD CONSTRAINT f_order_transaction_required_date_key_fkey FOREIGN KEY (required_date_key) REFERENCES public.d_required_date(required_date_key) ON DELETE CASCADE;


--
-- Name: f_shipment_transaction f_shipment_transaction_employee_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_shipment_transaction
    ADD CONSTRAINT f_shipment_transaction_employee_key_fkey FOREIGN KEY (employee_key) REFERENCES public.d_employee(employee_key) ON DELETE CASCADE;


--
-- Name: f_shipment_transaction f_shipment_transaction_order_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_shipment_transaction
    ADD CONSTRAINT f_shipment_transaction_order_date_key_fkey FOREIGN KEY (order_date_key) REFERENCES public.d_order_date_dim(order_date_key) ON DELETE CASCADE;


--
-- Name: f_shipment_transaction f_shipment_transaction_order_transaction_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_shipment_transaction
    ADD CONSTRAINT f_shipment_transaction_order_transaction_key_fkey FOREIGN KEY (order_transaction_key) REFERENCES public.d_order_transaction(order_transaction_key) ON DELETE CASCADE;


--
-- Name: f_shipment_transaction f_shipment_transaction_required_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_shipment_transaction
    ADD CONSTRAINT f_shipment_transaction_required_date_key_fkey FOREIGN KEY (required_date_key) REFERENCES public.d_required_date(required_date_key) ON DELETE CASCADE;


--
-- Name: f_shipment_transaction f_shipment_transaction_shipped_date_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_shipment_transaction
    ADD CONSTRAINT f_shipment_transaction_shipped_date_key_fkey FOREIGN KEY (shipped_date_key) REFERENCES public.d_shipped_date(shipped_date_key) ON DELETE CASCADE;


--
-- Name: f_shipment_transaction f_shipment_transaction_shipper_key_fkey; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.f_shipment_transaction
    ADD CONSTRAINT f_shipment_transaction_shipper_key_fkey FOREIGN KEY (shipper_key) REFERENCES public.d_shipper(shipper_key) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

