create table if not exists
    D_Shipped_Date (
        Shipped_Date_Key serial primary key,
        Shipped_Date_ID_yyyymmdd varchar(8),
        "Shipped_Day_Date_yyyy-mm-dd" varchar(10),
        Shipped_Full_Date timestamp,
        Shipped_Day_Of_Week_Name varchar(10),
        Shipped_Day_Of_Week_Abbrev varchar(3)
    )
;
