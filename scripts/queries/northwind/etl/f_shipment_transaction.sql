drop table if exists
	tmp_orders
;
create temp table
	tmp_orders
as (
	select
		*
	from
		foreign_staging.orders
)
;

drop table if exists
	tmp_orderdetails
;
create temp table
	tmp_orderdetails
as (
	select
		*
	from
		foreign_staging.orderdetails
)
;

drop table if exists
	tmp_products
;
create temp table
	tmp_products
as (
	select
		*
	from
		foreign_staging.products
)
;

drop table if exists
	tmp_shippers
;
create temp table
	tmp_shippers
as (
	select
		*
	from
		foreign_staging.shippers
)
;

truncate table
	f_shipment_transaction
;

insert into
	f_shipment_transaction
select
	dot.order_transaction_key,
	ddd.order_date_key,
	drd.required_date_key,
	dsd.shipped_date_key,
	dc.customer_key,
	de.employee_key,
	dsh.shipper_key,
	pr.unitsonorder,
	null,
	odd.discount,
	ord.freight,
	dp.product_standard_unit_price - (odd.discount * dp.product_standard_unit_price)
from
	d_order_transaction dot
join
	tmp_orders ord
on
	dot.order_id = ord.orderid
join
	d_order_date_dim ddd
on
	ord.orderdate = ddd.order_full_date
join
	d_required_date drd
on
	ord.requireddate = drd.required_full_date
join
	d_customer dc
on
	ord.customerid = dc.customer_id
join
	tmp_orderdetails odd
on
	ord.orderid = odd.orderid
join
	d_product dp
on
	odd.productid = dp.product_id
join
	d_employee de
on
	ord.employeeid = de.employee_id
join
	tmp_products pr
on
	odd.productid = pr.productid
join
	tmp_shippers sh
on
	ord.shipvia = sh.shipperid
join
	d_shipper dsh
on
	dsh.shipper_id = sh.shipperid
join
	d_shipped_date dsd
on
	ord.shippeddate = dsd.shipped_full_date
;