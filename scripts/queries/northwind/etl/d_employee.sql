drop table if exists
    tmp_employees
;
create temp table
    tmp_employees
as (
    select
        *
    from
        foreign_staging.employees p
)
;
create index on tmp_employees(employeeid)
;

delete from
    d_employee
where
    employee_id
in (
    select
        employeeid
    from
        tmp_employees
)
;

insert into
    d_employee
(
	employee_id,
	employee_last_name,
	employee_first_name,
	employee_title,
	employee_title_of_courtesy,
	employee_birth_date,
	employee_hire_date,
	employee_home_phone,
	employee_extension,
	employee_street_address,
	employee_postal_code_id,
	employee_city_name,
	employee_region_name,
	employee_country_name
)
select
	employeeid,
	lastname,
	firstname,
	title,
	titleofcourtesy,
	birthdate,
	hiredate,
	homephone,
	extension,
	"Address",
	postalcode,
	city,
	region,
	country
from
	foreign_staging.employees
;
