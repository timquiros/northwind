insert into
    r_categories
select
    *
from
    foreign_staging.categories
where
    categoryid not in (
        select
            categoryid
        from
            r_categories
    )
;