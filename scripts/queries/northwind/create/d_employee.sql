create table if not exists
    D_Employee (
        Employee_Key serial primary key,
        Employee_ID int4,
        Employee_Last_Name varchar(100),
        Employee_First_Name varchar(100),
        Employee_Title varchar(100),
        Employee_Title_Of_Courtesy varchar(5),
        Employee_Birth_Date timestamp,
        Employee_Hire_Date timestamp,
        Employee_Home_Phone varchar(50),
        Employee_Extension int4,
        Employee_Street_Address text,
        Employee_Postal_Code_ID varchar(50),
        Employee_City_Name varchar(50),
        Employee_Region_Name varchar(50),
        Employee_Country_Name varchar(50)
    )
;