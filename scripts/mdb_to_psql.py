import psycopg2
import os
import csv
from constants import CURRENT_DIR

def main():
    try:
        #query to list all tables from public schema
        tbl_list_qry = """
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema='public';
        """

        #establish db connection
        conn = psycopg2.connect(dbname='northwind_source', user='admin', password='admin', host='localhost', port='5432')
        cur = conn.cursor()

        #execute query
        cur.execute(tbl_list_qry)

        #create list of all tables and csv
        list_tbl = [tbl[0] for tbl in cur.fetchall()]
        print(list_tbl)
        query_dir = os.path.join(CURRENT_DIR, '../db/exports/')

        #for each csv file for a table, remove the header
        for tbl in list_tbl:
            with open(query_dir + tbl + '.csv', 'r') as f:
                with open(query_dir + tbl + '_out.csv', 'w') as fr:
                    reader = csv.reader(f)
                    next(reader)
                    for line in f:
                        fr.write(line)
            #copy out.csv to respective database table
            insert_query = """
                    COPY public.{} from stdin with csv delimiter ','
                    null as '' QUOTE AS '\"' ESCAPE AS '\"'
                """.format(tbl)
            with open(query_dir + tbl + '_out.csv', 'r') as f:
                cur.copy_expert(insert_query, f)

        #commit and close connection
        conn.commit()
        cur.close()
        conn.close()
    
    except Exception as e:
        raise e

if __name__ == '__main__':
    main()