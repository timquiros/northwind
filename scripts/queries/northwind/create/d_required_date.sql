create table if not exists
    D_Required_Date (
        Required_Date_Key serial primary key,
        Required_Date_ID_yyyymmdd varchar(8),
        "Required_Day_Date_yyyy-mm-dd" varchar(10),
        Required_Full_Date timestamp,
        Required_Day_Of_Week_Name varchar(10),
        Requried_Day_Of_Week_Abbrev varchar(3)
    )
;
