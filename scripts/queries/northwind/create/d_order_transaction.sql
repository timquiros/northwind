create table if not exists
    D_Order_Transaction (
        Order_Transaction_Key serial primary key,
        Order_ID int4,
        Audit_Key int4
    )
;