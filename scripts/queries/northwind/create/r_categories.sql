create table if not exists
    R_Categories (
        CategoryID int4 primary key,
        CategoryName varchar(50),
        "Description" text,
        Picture bytea
    )
;
