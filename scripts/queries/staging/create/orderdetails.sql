create table if not exists
    OrderDetails (
        OrderID int4,
        ProductID int4,
        UnitPrice numeric(10,2),
        Quantity int4,
        Discount numeric(3,2),
        primary key (OrderID, ProductID)
    )
;