import os

CURRENT_DIR = os.getcwd()

pkeys = {
    'categories': 'CategoryID',
    'customers': 'CustomerID',
    'employees': 'EmployeeID',
    'orderdetails': 'OrderID',
    'orders': 'OrderID',
    'products': 'ProductID',
    'shippers': 'ShipperID',
    'suppliers': 'SupplierID'
}