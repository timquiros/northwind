create table if not exists
    Shippers (
        ShipperID int4 primary key,
        CompanyName text,
        Phone varchar(50)
    )
;