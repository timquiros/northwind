create table if not exists
    Employees (
        EmployeeID int4 primary key,
        LastName varchar(100),
        FirstName varchar(100),
        Title varchar(100),
        TitleOfCourtesy varchar(5),
        BirthDate timestamp,
        HireDate timestamp,
        "Address" text,
        City varchar(50),
        Region varchar(50),
        PostalCode varchar(50),
        Country varchar(50),
        HomePhone varchar(50),
        Extension int4,
        Photo bytea,
        Notes text,
        ReportsTo int4
    )
;