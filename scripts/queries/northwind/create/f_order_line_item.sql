create table if not exists
    F_Order_Line_Item (
        Order_Transaction_Key int4 references D_Order_Transaction(Order_Transaction_Key) on delete cascade,
        Order_Date_Key int4 references D_Order_Date_Dim(Order_Date_Key) on delete cascade,
        Required_Date_Key int4 references D_Required_Date(Required_Date_Key) on delete cascade,
        Customer_Key int4 references D_Customer(Customer_Key) on delete cascade,
        Product_Key int4 references D_Product(Product_Key) on delete cascade,
        Employee_Key int4 references D_Employee(Employee_Key) on delete cascade,
        Item_Unit_Price numeric(10,2),
        Item_Quantity int4,
        Item_Extended_Amt int4,
        Item_Discount_Amt numeric(3,2),
        Item_Total_Amt numeric(10,2),
        Audit_Key int4
    )
;