create table if not exists
    D_Order_Date_Dim (
        Order_Date_Key serial primary key,
        Order_Date_ID_yyyymmdd varchar(8),
        "Order_Day_Date_yyyy-mm-dd" varchar(10),
        Order_Full_Date timestamp,
        Order_Day_Of_Week_Name varchar(10),
        Order_Day_Of_Week_Abbrev varchar(3)
    )
;