create table if not exists
    R_Suppliers (
        SupplierID int4 primary key,
        CompanyName text,
        ContactName text,
        ContactTitle text,
        "Address" text,
        City varchar(50),
        Region varchar(50),
        PostalCode varchar(50),
        Country varchar(50),
        Phone varchar(25),
        Fax varchar(25),
        HomePage text
    )
;