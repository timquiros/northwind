drop table if exists
    tmp_order_transaction
;
create temp table
    tmp_order_transaction
as (
    select
        *
    from
        foreign_staging.orders p
)
;

delete from
    d_order_transaction
where
    order_id
in (
    select
		orderid
    from
        tmp_order_transaction
)
;

insert into
    d_order_transaction
(
	order_id,
	audit_key
)
select
	orderid,
	null
from
	foreign_staging.orders
;


