import psycopg2
import os
from constants import CURRENT_DIR

def create_foreign_connection():
    query = """
        create extension if not exists postgres_fdw;
        drop server if exists nwind_staging cascade;
        create server nwind_staging foreign data wrapper postgres_fdw options (host 'localhost', dbname 'northwind_stag');
        create user mapping for admin server nwind_staging options (user 'admin', password 'admin'); 
        drop schema if exists foreign_staging;
        create schema foreign_staging;
        import foreign schema public from server nwind_staging into foreign_staging;
    """

    conn = psycopg2.connect(
        dbname='northwind_prod',
        user='admin',
        password='admin',
        host='localhost',
        port='5432'
        )
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()
    conn.close()

def process_data():
    conn = psycopg2.connect(
        dbname='northwind_prod',
        user='admin',
        password='admin',
        host='localhost',
        port='5432'
        )
    cur = conn.cursor()

    #list all tables from the source
    #loop from these tables and insert to each
    tbl_list_qry = """
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema='public';
        """
    cur.execute(tbl_list_qry)
    tbl_src = [tbl[0] for tbl in cur.fetchall()]

    create_dir = os.path.join(CURRENT_DIR, 'queries/northwind/create/')
    etl_dir = os.path.join(CURRENT_DIR, 'queries/northwind/etl/')

    #Loop through each tables
    for tbl in tbl_src:
        print('-------------------------------------')
        print('Processing table {}'.format(tbl))

        for qdir in [create_dir, etl_dir]:
            #read query
            query_file = open(qdir + tbl + '.sql', 'r')
            file_create = query_file.read()
            #create the table if not already there
            cur.execute(file_create)
            if qdir == etl_dir:
                print("Number of inserted rows: {}".format(cur.rowcount))
            conn.commit()
    
    cur.close()
    conn.close()
    print('SUCCESSFUL!')

if __name__ == '__main__':
    create_foreign_connection()
    process_data()