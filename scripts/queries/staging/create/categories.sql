create table if not exists
    Categories (
        CategoryID int4 primary key,
        CategoryName varchar(50),
        "Description" text,
        Picture bytea
    )
;
