insert into
    r_suppliers
select
    *
from
    foreign_staging.suppliers
where
    supplierid not in (
        select
            supplierid
        from
            r_suppliers
    )
;