import sqlite3
import psycopg2
import os
import sys
from constants import CURRENT_DIR, pkeys

#establish db_connections from source and staging
conn_s = psycopg2.connect(
    dbname='northwind_source',
    user='admin',
    password='admin',
    host='localhost',
    port='5432'
    )
conn_d = psycopg2.connect(
    dbname='northwind_stag',
    user='admin',
    password='admin',
    host='localhost',
    port='5432'
    )
cur_s = conn_s.cursor()
cur_d = conn_d.cursor()

#list all tables from the source
#loop from these tables and insert to each
tbl_list_qry = """
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema='public';
    """
cur_s.execute(tbl_list_qry)
tbl_src = [tbl[0] for tbl in cur_s.fetchall()]

qry_dir = os.path.join(CURRENT_DIR, 'queries/staging/create/')

#Loop through each tables
for tbl in tbl_src:
    print('-------------------------------------')
    print('Processing table {}'.format(tbl))
    #open the creation query of that table
    query_file = open(qry_dir + tbl + '.sql', 'r')
    file_create = query_file.read()
    #create the table if not already there
    cur_d.execute(file_create)
    conn_d.commit()

    #fetch latest contents of the table
    cur_s.execute("select * from {}".format(tbl))
    results_s = cur_s.fetchall()
    
    #delete from the primary key so you can take advantage of the index
    #filtering from the index/pkey tends to be much faster
    #delete instead of updating the table based from a key
    print('Inserting fresh data into the table...')
    for row in results_s:
        #pkey of customers table is a string so a '' is needed
        key = row[0] if tbl != 'customers' else "'" + str(row[0]) + "'"
        #delete rows where that pkey is found
        cur_d.execute("""
            delete from
                {0}
            where
                {1} = {2}
        """.format(
            tbl,
            pkeys[tbl],
            key
        ))
        #insert new rows in the table
        cur_d.execute("insert into {} values %s".format(tbl), (row,))
    print('-------------------------------------')

conn_d.commit()
cur_s.close()
cur_d.close()
conn_s.close()
conn_d.close()
print('SUCCESSFUL!')