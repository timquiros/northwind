drop table if exists
    tmp_orders
;
create temp table
    tmp_orders
as (
    select
        *
    from
        foreign_staging.orders p
)
;

delete from
    d_required_date
where
    required_date_id_yyyymmdd
in (
    select
        to_char(requireddate, 'yyyymmdd')
    from
        tmp_orders
)
;

insert into
    d_required_date
(
	required_date_id_yyyymmdd,
	"Required_Day_Date_yyyy-mm-dd",
	required_full_date,
	required_day_of_week_name,
	requried_day_of_week_abbrev
)
select
	to_char(requireddate, 'yyyymmdd'),
	to_char(requireddate, 'yyyy-mm-dd'),
	requireddate,
	to_char(requireddate, 'Day'),
	to_char(requireddate, 'Dy')
from
	foreign_staging.orders
group by
    1,2,3,4,5
;

