drop table if exists
    tmp_shipped_date
;
create temp table
    tmp_shipped_date
as (
    select
        *
    from
        foreign_staging.orders p
)
;

delete from
    d_shipped_date
where
    Shipped_Date_ID_yyyymmdd
in (
    select
		to_char(shippeddate, 'yyyymmdd')
    from
        tmp_shipped_date
)
;

insert into
    d_shipped_date
(
	shipped_date_id_yyyymmdd,
	"Shipped_Day_Date_yyyy-mm-dd",
	shipped_full_date,
	shipped_day_of_week_name,
	shipped_day_of_week_abbrev
)
select
	to_char(shippeddate, 'yyyymmdd'),
	to_char(shippeddate, 'yyyy-mm-dd'),
	shippeddate,
	to_char(shippeddate, 'Day'),
	to_char(shippeddate, 'Dy')
from
	foreign_staging.orders
group by
    1,2,3,4,5
;


