drop table if exists
	tmp_orders
;
create temp table
	tmp_orders
as (
	select
		*
	from
		foreign_staging.orders
)
;

drop table if exists
	tmp_orderdetails
;
create temp table
	tmp_orderdetails
as (
	select
		*
	from
		foreign_staging.orderdetails
)
;

truncate table
	f_order_line_item
;

insert into
	f_order_line_item
select
	dot.order_transaction_key,
	ddd.order_date_key,
	drd.required_date_key,
	dc.customer_key,
	dp.product_key,
	de.employee_key,
	dp.product_standard_unit_price,
	odd.quantity,
	null,
	odd.discount,
	odd.discount * dp.product_standard_unit_price,
	dp.product_standard_unit_price - (odd.discount * dp.product_standard_unit_price)
from
	d_order_transaction dot
join
	tmp_orders ord
on
	dot.order_id = ord.orderid
join
	d_order_date_dim ddd
on
	ord.orderdate = ddd.order_full_date
join
	d_required_date drd
on
	ord.requireddate = drd.required_full_date
join
	d_customer dc
on
	ord.customerid = dc.customer_id
join
	tmp_orderdetails odd
on
	ord.orderid = odd.orderid
join
	d_product dp
on
	odd.productid = dp.product_id
join
	d_employee de
on
	ord.employeeid = de.employee_id
;