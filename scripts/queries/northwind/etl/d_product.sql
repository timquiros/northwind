drop table if exists
    tmp_products
;
create temp table
    tmp_products
as (
    select
        *
    from
        foreign_staging.products p
)
;
create index on tmp_products(productid)
;

delete from
    d_product
where
    product_id
in (
    select
        productid
    from
        tmp_products
)
;

insert into
    d_product 
(
    product_id,
    product_name,
    product_category_id,
    product_category_name,
    product_quantity_per_unit,
    product_standard_unit_price,
    product_reorder_level,
    product_discontinued_ind,
    supplier_id,
    supplier_company_name,
    supplier_contact_title,
    supplier_street_address,
    supplier_phone,
    supplier_fax,
    supplier_home_page,
    supplier_postal_code_id,
    supplier_city_name,
    supplier_region_name,
    supplier_country_code,
    supplier_country_name,
    current_row_ind,
    effective_date,
    audit_key
)
select
	p.productid,
	p.productname,
	c.categoryid,
	c.categoryname,
	p.quantityperunit,
	p.unitprice,
	p.reorderlevel,
	p.discontinued,
	s.supplierid,
	s.companyname,
	s.contacttitle,
	s."Address",
	s.phone,
	s.fax,
	s.homepage,
	s.postalcode,
	s.city,
	s.region,
	null, --country_code not provided
	s.country,
	null,
	null,
	null
from
	foreign_staging.products p
join
	foreign_staging.suppliers s
on
	p.supplierid = s.supplierid
join
	foreign_staging.categories c
on
	p.categoryid = c.categoryid