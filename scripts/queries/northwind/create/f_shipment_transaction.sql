create table if not exists
    F_Shipment_Transaction (
        Order_Transaction_Key int4 references D_Order_Transaction(Order_Transaction_Key) on delete cascade,
        Order_Date_Key int4 references D_Order_Date_Dim(Order_Date_Key) on delete cascade,
        Required_Date_Key int4 references D_Required_Date(Required_Date_Key) on delete cascade,
        Shipped_Date_Key int4 references D_Shipped_Date(Shipped_Date_Key) on delete cascade,
        Customer_Key int4 references D_Customer(Customer_Key) on delete cascade,
        Employee_Key int4 references D_Employee(Employee_Key) on delete cascade,
        Shipper_Key int4 references D_Shipper(Shipper_Key) on delete cascade,
        Shipper_Nbr_Of_Products int4,
        Shipment_Extended_Amt numeric(10,2),
        Shipment_Discount_Amt numeric(3,2),
        Shipment_Freight_Amt numeric(10,2),
        Shipment_Total_Amt numeric(10,2),
        Audit_Key int4
    )
;