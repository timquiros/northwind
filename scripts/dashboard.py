import psycopg2
import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html

def db_connect():
    conn = psycopg2.connect(
            dbname='northwind_prod',
            user='admin',
            password='admin',
            host='localhost',
            port='5432'
            )
    cur = conn.cursor()

    return conn, cur

query1 = """
    select
        pr.product_name,
        pr.product_standard_unit_price as price,
        sum(foli.item_quantity) as quantity
    from
        d_product pr
    join
        f_order_line_item foli
    on
        pr.product_key = foli.product_key
    group by
        1,2
    order by
        3 desc
    limit 5
"""
conn, cur = db_connect()
df1 = pd.read_sql(query1, conn)
cur.close()
conn.close()

query2 = """
    select
        dsd.shipped_full_date as ship_date,
        sum(fst.shipper_nbr_of_products) as num_shipped_products
    from
        d_shipped_date dsd
    join
        f_shipment_transaction fst
    on
        dsd.shipped_date_key = fst.shipped_date_key
    group by
        1
    order by
        1
"""
conn, cur = db_connect()
df2 = pd.read_sql(query2, conn)
cur.close()
conn.close()

query3 = """
    select
        dc.customer_country_name as country,
        sum(fot.order_total_amt) as order_amt,
        sum(foli.item_quantity) as order_qty
    from
        d_customer dc
    join
        f_order_transaction fot
    on
        dc.customer_key = fot.customer_key
    join
        f_order_line_item foli
    on
        fot.order_date_key = foli.order_date_key
    group by
        1
"""
conn, cur = db_connect()
df3 = pd.read_sql(query3, conn)
cur.close()
conn.close()


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1(children='Northwind Analytics Dashboard'),

    html.Div(children='''
        An overview of the Northwind Supply Chain Performance
    '''),

    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': df1['product_name'], 'y': df1['quantity'], 'type': 'bar', 'name': 'Order Quantity'},
                {'x': df1['product_name'], 'y': df1['price'], 'type': 'line', 'name': 'Product Price'},
            ],
            'layout': {
                'title': 'Top 5 Products per Order Qty with Prices'
            }
        }
    ),

    dcc.Graph(
        id='example-graph2',
        figure={
            'data': [
                {'x': df2['ship_date'], 'y': df2['num_shipped_products'], 'type': 'line', 'name': 'Shipments'},
                # {'x': ['a', 'b', 'c'], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                # {'x': ['a', 'b', 'c'], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
            ],
            'layout': {
                'title': 'No. of Shipped Products Over Time'
            }
        }
    ),

    dcc.Graph(
        id='correlation order',
        figure={
            'data': [
                dict(
                    x=df3[df3['country'] == i]['order_amt'],
                    y=df3[df3['country'] == i]['order_qty'],
                    text=df3[df3['country'] == i]['country'],
                    mode='markers',
                    opacity=0.7,
                    marker={
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name=i
                ) for i in df3['country'].unique()
            ],
            'layout': dict(
                xaxis={'type': 'log', 'title': 'Total Amount'},
                yaxis={'title': 'Quantity'},
                # margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
                legend={'x': 0, 'y': 1},
                hovermode='closest',
                title='Correlation of Order Amount and Quantity'

            )
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)