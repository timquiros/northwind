drop table if exists
    tmp_customers
;
create temp table
    tmp_customers
as (
    select
        *
    from
        foreign_staging.customers p
)
;
create index on tmp_customers(customerid)
;

delete from
    d_customer
where
    customer_id
in (
    select
        customerid
    from
        tmp_customers
)
;

insert into
    d_customer
(
	customer_id,
	customer_name,
	customer_contact_name,
	customer_contact_title,
	customer_phone,
	customer_fax,
	customer_street_address,
	customer_postal_code_id,
	customer_city_name,
	customer_region_name,
	customer_country_code,
	customer_country_name,
	audit_key
)
select
	customerid,
	companyname,
	contactname,
	contacttitle,
	phone,
	fax,
	"Address",
	postalcode,
	city,
	region,
	null,
	country,
	null
from
	foreign_staging.customers
;
