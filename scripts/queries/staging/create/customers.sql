create table if not exists
    Customers (
        CustomerID varchar(5) primary key,
        CompanyName varchar(100),
        ContactName varchar(100),
        ContactTitle varchar(100),
        "Address" text,
        City varchar(50),
        Region varchar(50),
        PostalCode varchar(50),
        Country varchar(50),
        Phone varchar(50),
        Fax varchar(50)
    )
;
