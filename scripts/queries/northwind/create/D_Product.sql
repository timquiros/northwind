create table if not exists
    D_Product (
        Product_Key serial primary key,
        Product_ID int4,
        Product_Name text,
        Product_Category_ID int4 references R_Categories(CategoryID) on delete cascade,
        Product_Category_Name text,
        Product_Quantity_Per_Unit text,
        Product_Standard_Unit_Price numeric(10,2),
        Product_Reorder_Level int4,
        Product_Discontinued_Ind boolean,
        Supplier_ID int4 references R_Suppliers(SupplierID) on delete cascade,
        Supplier_Company_Name text,
        Supplier_Contact_Title text,
        Supplier_Street_Address text,
        Supplier_Phone varchar(25),
        Supplier_Fax varchar(25),
        Supplier_Home_Page text,
        Supplier_Postal_Code_ID varchar(50),
        Supplier_Region_Name varchar(50),
        Supplier_City_Name varchar(50),
        Supplier_Country_Code varchar(50),
        Supplier_Country_Name text,
        Current_Row_Ind int4,
        Effective_Date timestamp,
        Audit_Key int4
    )
;