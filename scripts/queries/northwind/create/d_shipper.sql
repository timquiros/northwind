create table if not exists
    D_Shipper (
        Shipper_Key serial primary key,
        Shipper_ID int4,
        Shipper_Company_Name text,
        Current_Shipper_Phone varchar(50),
        Previous_Shipper_Phone varchar(50),
        Effective_Date timestamp,
        Current_Row_Ind int4,
        Audit_Key int4
    )
;