drop table if exists
    tmp_shipper
;
create temp table
    tmp_shipper
as (
    select
        *
    from
        foreign_staging.shippers p
)
;

delete from
    d_shipper
where
    shipper_id
in (
    select
		shipperid
    from
        tmp_shipper
)
;

insert into
    d_shipper
(
	shipper_id,
	shipper_company_name,
	current_shipper_phone,
	previous_shipper_phone,
	effective_date,
	current_row_ind,
	audit_key
)
select
	shipperid,
	companyname,
	phone,
	null,
	null,
	null,
	null
from
	foreign_staging.shippers
;


