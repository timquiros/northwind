create table if not exists
    D_Customer (
        Customer_Key serial primary key,
        Customer_ID varchar(5),
        Customer_Name varchar(100),
        Customer_Contact_Name varchar(100),
        Customer_Contact_Title varchar(100),
        Customer_Phone varchar(50),
        Customer_Fax varchar(50),
        Customer_Street_Address text,
        Customer_Postal_Code_ID varchar(50),
        Customer_City_Name varchar(50),
        Customer_Region_Name varchar(50),
        Customer_Country_Code varchar(5),
        Customer_Country_Name varchar(50),
        Audit_Key int4
    )
;