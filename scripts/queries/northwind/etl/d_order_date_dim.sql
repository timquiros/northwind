drop table if exists
    tmp_orders
;
create temp table
    tmp_orders
as (
    select
        *
    from
        foreign_staging.orders p
)
;

delete from
    d_order_date_dim
where
    order_date_id_yyyymmdd
in (
    select
        to_char(orderdate, 'yyyymmdd')
    from
        tmp_orders
)
;

insert into
    d_order_date_dim
(
	order_date_id_yyyymmdd,
	"Order_Day_Date_yyyy-mm-dd",
	order_full_date,
	order_day_of_week_name,
	order_day_of_week_abbrev
)
select
	to_char(orderdate, 'yyyymmdd'),
	to_char(orderdate, 'yyyy-mm-dd'),
	orderdate,
	to_char(orderdate, 'Day'),
	to_char(orderdate, 'Dy')
from
	foreign_staging.orders
group by
    1,2,3,4,5
;

